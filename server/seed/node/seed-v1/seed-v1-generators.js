const MongoClient = require('mongodb').MongoClient;
const ObjectID = require('mongodb').ObjectID;
const cleanCollection = require('../lib/clean-db').cleanCollection;

var lineReader = require('readline').createInterface({
  input: require('fs').createReadStream('./json/generators.json')
});

var lines = []

lineReader.on('line', function (line) {
  let toConvert = JSON.parse(line)

  

  console.log(toConvert);
  // lines.push({
  //   _id: ObjectID(toConvert._id['$oid']),
  //   firstName: toConvert.firstName,
  //   lastName: toConvert.lastName,
  //   displayName: toConvert.displayName,
  //   email: toConvert.email,
  //   phone: toConvert.phone,
  //   password: toConvert.password,
  //   salt: toConvert.salt,
  //   roles: toConvert.roles,
  //   updated: Date.now(),
  // })
});

lineReader.on('close', function (line) {
  // connectAndSeed()
});

function seedTestUsers(db) {
  const collection = db.collection('users');
  return cleanCollection(db, 'users')
    .then(() => collection.insertMany(lines))
    .then(() => { console.log('Seeded Users'); })
}

function connectAndSeed() {
  return new Promise(resolve => {
    MongoClient.connect(`mongodb://localhost:27017`, function(err, client) {
      if (err) { throw err; }

      console.log("Connected successfully to server");

      const db = client.db('f2s');
      client = client;

      seedTestUsers(db)
        .then(() => resolve())
    });
  });
}