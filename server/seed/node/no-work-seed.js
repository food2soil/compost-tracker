const MongoClient = require('mongodb').MongoClient;
const ObjectID = require('mongodb').ObjectID;
const NodeGeocoder = require('node-geocoder');
const cleanDb = require('./lib/clean-db').cleanDb;

let geocoder = NodeGeocoder({
  provider: 'google',
  httpAdapter: 'https',
  apiKey: 'AIzaSyDZ3qachnbE0TYgkqQnYd1Vyx_GB7VEBDY',
  formatter: null
})

connectAndSeed()
  .then(() => console.log('finished seed'))
  .catch((err) => {
    console.log('Error when seeding database');
    console.error(err);
  });

function seedWork(db) {
  const collection = db.collection('works');
  return collection.insertMany([{
    "_id" : ObjectID("5b1828616ce5f8077675ef2c"),
    "hub" : ObjectID("5b0f2c729eae1a03d20cba61"),
    "bayEvents" : [{
      "bay" : ObjectID("5b01b091a3f65e97ae98b87c"),
      "type" : "fill",
      "bucketsHarvested" : null,
      "isFullHarvest" : null,
      "date" : new Date()
    }, {
      "bay" : ObjectID("5b01b091a3f65e97ae98b87e"),
      "type" : "fill",
      "bucketsHarvested" : null,
      "isFullHarvest" : null,
      "date" : new Date(),
    }],
    "bayMeasurements" : [ ],
    "timeSpent" : 12,
    "technician" : ObjectID("5b32cd9c406b3e5966bfdabd"),
    "notes" : "Stuff happened man...",
    "date" : new Date(),
    "isApproved" : false,
    "undoBays" : [{
      "destinations" : [ ], "measurements" : [ ], "activeBatches" : [ ], "_id" : ObjectID("5b01b091a3f65e97ae98b877"), "name" : "H1","isStart" : false, "isHolding" : true, "displayOrder" : 0
    }, {
      "destinations" : [ ], "measurements" : [ ], "activeBatches" : [ ], "_id" : ObjectID("5b01b091a3f65e97ae98b878"), "name" : "H2", "isStart" : false, "isHolding" : true, "displayOrder" : 0
    }, {
      "destinations" : [ ], "measurements" : [ ], "activeBatches" : [ ], "_id" : ObjectID("5b01b091a3f65e97ae98b879"), "name" : "H3", "isStart" : false, "isHolding" : true, "displayOrder" : 0
    }, {
      "destinations" : [ ], "measurements" : [ ], "activeBatches" : [ ], "_id" : ObjectID("5b01b091a3f65e97ae98b87a"), "name" : "H4", "isStart" : false, "isHolding" : true, "displayOrder" : 0
    }, {
      "destinations" : [ "5b01b091a3f65e97ae98b877", "5b01b091a3f65e97ae98b878" ], "measurements" : [ ], "activeBatches" : [ ], "_id" : ObjectID("5b01b091a3f65e97ae98b87b"), "name" : "1", "isStart" : false, "displayOrder" : 1
    }, {
      "destinations" : [ "5b01b091a3f65e97ae98b87b" ], "measurements" : [ ], "activeBatches" : [ ], "_id" : ObjectID("5b01b091a3f65e97ae98b87c"), "name" : "2", "isStart" : true, "displayOrder" : 2
    }, {
      "destinations" : [ "5b01b091a3f65e97ae98b87d" ], "measurements" : [ ], "activeBatches" : [ ], "_id" : ObjectID("5b01b091a3f65e97ae98b87e"), "name" : "3", "isStart" : true, "displayOrder" : 3
    }, {
      "destinations" : [ "5b01b091a3f65e97ae98b879", "5b01b091a3f65e97ae98b87a" ], "measurements" : [ ], "activeBatches" : [ ], "_id" : ObjectID("5b01b091a3f65e97ae98b87d"), "name" : "4", "displayOrder" : 4
    }],
    "undoFinishedBatches" : [ ],
    "isCarbonNeeded" : true
  }])
  .then(() => { console.log('Seeded Work'); });
}

function seedGenerators(db) {
  // Get the documents collection
  let generators = [{
    name: 'Priya Indian Cuisine',
    created: new Date(),
    isActive: true,
    generatorType: 'Restaurant',
    collectionType: 'Pickup',
    containerHistory: [{
      id: ObjectID(),
      volumePerContainer: 32,
      containersPerWeek: 4,
      dateStartedUsing: new Date()
    }],
    address: {
      primaryLine: '2574 Esplanade',
      city: 'Chico',
      state: 'CA',
      zip: '95973'
    },
    contacts: [{
      _id: ObjectID(),
      name: 'Resigned Lady',
      title: 'Owner',
      phone: '1234568909',
      isPrimary: true
    }, {
      _id: ObjectID(),
      name: 'Little Kid',
      phone: '1234568909',
      email: 'kid@priya.com'
    }, {
      _id: ObjectID(),
      name: 'Serious Guy',
      email: 'guy@priya.com'
    }]
  }, {
    name: 'Sierra Nevada Brewing Company',
    created: new Date(),
    isActive: true,
    generatorType: 'Restaurant',
    containerHistory: [{
      id: ObjectID(),
      volumePerContainer: 32,
      containersPerWeek: 8,
      dateStartedUsing: new Date()
    }],
    address: {
      primaryLine: '1075 E 20th St',
      city: 'Chico',
      state: 'CA',
      zip: '95928'
    }
  }, {
    name: 'China House',
    created: new Date(),
    isActive: true,
    generatorType: 'Restaurant',
    containerHistory: [{
      id: ObjectID(),
      volumePerContainer: 5,
      containersPerWeek: 4,
      dateStartedUsing: new Date()
    }],
    address: {
      primaryLine: '951 Nord Ave',
      city: 'Chico',
      state: 'CA',
      zip: '95926'
    }
  }, {
    name: 'Rick Delhommer',
    created: new Date(),
    isActive: true,
    generatorType: 'Residential',
    containerHistory: [{
      id: ObjectID(),
      volumePerContainer: 32,
      containersPerWeek: 4,
      dateStartedUsing: new Date()
    }],
    address: {
      primaryLine: '881 Mangrove Ave',
      city: 'Chico',
      state: 'CA',
      zip: '95926'
    }
  }, {
    name: 'Amanda Li',
    created: new Date(),
    isActive: true,
    generatorType: 'Residential',
    containerHistory: [{
      id: ObjectID(),
      volumePerContainer: 32,
      containersPerWeek: 4,
      dateStartedUsing: new Date()
    }],
    address: {
      primaryLine: '1531 Esplanade',
      city: 'Chico',
      state: 'CA',
      zip: '95926'
    }
  }, {
    name: 'Anthony Reihm',
    created: new Date(),
    isActive: true,
    generatorType: 'Residential',
    containerHistory: [{
      id: ObjectID(),
      volumePerContainer: 32,
      containersPerWeek: 4,
      dateStartedUsing: new Date()
    }],
    address: {
      primaryLine: '100 Wildwood Ave',
      city: 'Chico',
      state: 'CA',
      zip: '95973'
    }
  }, {
    name: 'Jarita Ta',
    created: new Date(),
    isActive: true,
    generatorType: 'Residential',
    containerHistory: [{
      id: ObjectID(),
      volumePerContainer: 32,
      containersPerWeek: 4,
      dateStartedUsing: new Date()
    }],
    address: {
      primaryLine: '1950 E 20th St',
      city: 'Chico',
      state: 'CA',
      zip: '95928'
    }
  }, {
    name: 'Brian Lewis',
    created: new Date(),
    isActive: true,
    generatorType: 'Residential',
    containerHistory: [{
      id: ObjectID(),
      volumePerContainer: 32,
      containersPerWeek: 4,
      dateStartedUsing: new Date()
    }],
    address: {
      primaryLine: '801 East Ave',
      secondaryLine: 'Ste 110',
      city: 'Chico',
      state: 'CA',
      zip: '95926'
    }
  }];

  const collection = db.collection('generators');

  return Promise.all(generators.map(x => {
    let stringifiedAddress =
      x.address.primaryLine + ', ' +
      x.address.city + ', ' +
      x.address.state + ', ' +
      x.address.zip;

    return geocoder.geocode(stringifiedAddress).then(res => {
      if (res) {
        x.address.loc = [res[0].longitude, res[0].latitude];
      }
    });
  }))
  .then(() => collection.insertMany(generators))
  .then(() => { console.log('Seeded Generators'); })
}

function seedBays(db) {
  // Get the documents collection
  const collection = db.collection('bays');
  return collection.insertMany([{
    "name" : "H1",
    "isStart" : false,
    "destinations" : [ ],
    "isHolding" : true,
    "displayOrder" : 0,
    activeBatches: [],
    "_id" : ObjectID("5b01b091a3f65e97ae98b877")
  }, {
    "name" : "H2",
    "isStart" : false,
    "destinations" : [ ],
    "isHolding" : true,
    "displayOrder" : 0,
    activeBatches: [],
    "_id" : ObjectID("5b01b091a3f65e97ae98b878")
  }, {
    "name" : "H3",
    "isStart" : false,
    "destinations" : [ ],
    "isHolding" : true,
    "displayOrder" : 0,
    activeBatches: [],
    "_id" : ObjectID("5b01b091a3f65e97ae98b879")
  }, {
    "name" : "H4",
    "isStart" : false,
    "destinations" : [ ],
    "isHolding" : true,
    "displayOrder" : 0,
    activeBatches: [],
    "_id" : ObjectID("5b01b091a3f65e97ae98b87a")
  }, {
    "name" : "1",
    "destinations" : [ "5b01b091a3f65e97ae98b877", "5b01b091a3f65e97ae98b878" ],
    "isStart" : false,
    "displayOrder" : 1,
    activeBatches: [],
    "_id" : ObjectID("5b01b091a3f65e97ae98b87b")
  }, {
    "name" : "2",
    "destinations" : [ "5b01b091a3f65e97ae98b87b" ],
    "isStart" : true,
    "displayOrder" : 2,
    activeBatches: [],
    "_id" : ObjectID("5b01b091a3f65e97ae98b87c")
  }, {
    "name" : "3",
    "destinations" : [ "5b01b091a3f65e97ae98b87d" ],
    "isStart" : true,
    "displayOrder" : 3,
    activeBatches: [],
    "_id" : ObjectID("5b01b091a3f65e97ae98b87e")
  }, {
    "name" : "4",
    "destinations" : [ "5b01b091a3f65e97ae98b879", "5b01b091a3f65e97ae98b87a" ],
    "displayOrder" : 4,
    activeBatches: [],
    "_id" : ObjectID("5b01b091a3f65e97ae98b87d")
  }, {
    "name" : "H1",
    "isStart" : false,
    "destinations" : [ ],
    "isHolding" : true,
    "displayOrder" : 0,
    activeBatches: [],
    "_id" : ObjectID("5b01b091a3f65e97ae98b887")
  }, {
    "name" : "H2",
    "isStart" : false,
    "destinations" : [ ],
    "isHolding" : true,
    "displayOrder" : 0,
    activeBatches: [],
    "_id" : ObjectID("5b01b091a3f65e97ae98b888")
  }, {
    "name" : "H3",
    "isStart" : false,
    "destinations" : [ ],
    "isHolding" : true,
    "displayOrder" : 0,
    activeBatches: [],
    "_id" : ObjectID("5b01b091a3f65e97ae98b889")
  }, {
    "name" : "H4",
    "isStart" : false,
    "destinations" : [ ],
    "isHolding" : true,
    "displayOrder" : 0,
    activeBatches: [],
    "_id" : ObjectID("5b01b091a3f65e97ae98b88a")
  }, {
    "name" : "1",
    "destinations" : [ "5b01b091a3f65e97ae98b887", "5b01b091a3f65e97ae98b888" ],
    "isStart" : false,
    "displayOrder" : 1,
    measurements: [],
    activeBatches: [],
    "_id" : ObjectID("5b01b091a3f65e97ae98b88b")
  }, {
    "name" : "2",
    "destinations" : [ "5b01b091a3f65e97ae98b88b" ],
    "isStart" : true,
    "displayOrder" : 2,
    measurements: [],
    activeBatches: [],
    "_id" : ObjectID("5b01b091a3f65e97ae98b88c")
  }, {
    "name" : "3",
    "destinations" : [ "5b01b091a3f65e97ae98b88d" ],
    "isStart" : true,
    "displayOrder" : 3,
    measurements: [],
    activeBatches: [],
    "_id" : ObjectID("5b01b091a3f65e97ae98b88e")
  }, {
    "name" : "4",
    "destinations" : [ "5b01b091a3f65e97ae98b889", "5b01b091a3f65e97ae98b88a" ],
    "displayOrder" : 4,
    measurements: [],
    activeBatches: [],
    "_id" : ObjectID("5b01b091a3f65e97ae98b88d")
  }])
  .then(() => { console.log('Seeded Bays'); })
}

function seedHubs(db) {
  // Get the documents collection
  const collection = db.collection('hubs');
  return collection.insertMany([{
    _id: ObjectID("5b0f2c729eae1a03d20cba61"),
    created: new Date(),
    isActive: true,
    bays: [
      ObjectID("5b01b091a3f65e97ae98b877"),
      ObjectID("5b01b091a3f65e97ae98b878"),
      ObjectID("5b01b091a3f65e97ae98b879"),
      ObjectID("5b01b091a3f65e97ae98b87a"),
      ObjectID("5b01b091a3f65e97ae98b87b"),
      ObjectID("5b01b091a3f65e97ae98b87c"),
      ObjectID("5b01b091a3f65e97ae98b87e"),
      ObjectID("5b01b091a3f65e97ae98b87d")
    ],
    "batches" : [ ],
    "name" : "Test Hub",
    "address" : {
      "primaryLine" : "76 Saint Francis Dr",
      "city" : "Chico",
      "state" : "CA",
      "zip" : "95926",
      "loc" : [ -121.8134129, 39.75017649999999 ]
    },
    "contacts" : [{
      "_id": ObjectID(),
      "name" : "Rick Delhommer",
      "title" : "Hub Dude",
      "phone" : "1231512323",
      "isPrimary" : true,
    }],
    "workRecords" : [ ],
    "generators" : [ ],
    "technicians" : [ ],
    "notes" : "Do the thing and access the stuff!",
    capacityGallonsPerWeek: 500
  },{
    created: new Date(),
    isActive: true,
    bays: [
      ObjectID("5b01b091a3f65e97ae98b887"),
      ObjectID("5b01b091a3f65e97ae98b888"),
      ObjectID("5b01b091a3f65e97ae98b889"),
      ObjectID("5b01b091a3f65e97ae98b88a"),
      ObjectID("5b01b091a3f65e97ae98b88b"),
      ObjectID("5b01b091a3f65e97ae98b88c"),
      ObjectID("5b01b091a3f65e97ae98b88e"),
      ObjectID("5b01b091a3f65e97ae98b88d")
    ],
    "batches" : [ ],
    "name" : "Test Hub 2",
    "address" : {
      "primaryLine" : "664 E 1st Ave",
      "city" : "Chico",
      "state" : "CA",
      "zip" : "95926",
      "loc" : [-121.8348027, 39.7424802]
    },
    "contacts" : [{
      "name" : "Rick Delhommer",
      "title" : "Hub Dude",
      "phone" : "1231512323",
      "isPrimary" : true,
    }],
    "workRecords" : [ ],
    "generators" : [ ],
    "technicians" : [ ],
    "notes" : "Do the thing and access the stuff!",
    capacityGallonsPerWeek: 500
  }])
  .then(() => { console.log('Seeded Hubs'); })
}

function seedTestUsers(db) {
  const collection = db.collection('users');
  return collection.insertMany([
    {
      "_id" : ObjectID("5b32cd9c406b3e5966bfdabd"),
      "firstName" : "Test",
      "lastName" : "All",
      "email" : "rdelhommer+ta@gmail.com",
      "password" : "dkT/cmisG/WqlseMwPhcx8vd00Lirbpr8+lNtru6guhbwWWh0KRsJLhfPkvG4EsycnRtRg1LLhhG5hL182pfLQ==",
      "phone" : "2222222222",
      "roles" : [ 1, 2 ],
      "created" : new Date(),
      "displayName" : "Test All",
      "salt" : "zdX1pYg2Nb7+cVpqj9vXPg=="
    },
    {
      "_id" : ObjectID("5b32cd9c406b3e5966bfdabe"),
      "firstName" : "Test",
      "lastName" : "Admin",
      "email" : "rdelhommer+a@gmail.com",
      "password" : "dkT/cmisG/WqlseMwPhcx8vd00Lirbpr8+lNtru6guhbwWWh0KRsJLhfPkvG4EsycnRtRg1LLhhG5hL182pfLQ==",
      "phone" : "2222222222",
      "roles" : [ 2 ],
      "created" : new Date(),
      "displayName" : "Test Admin",
      "salt" : "zdX1pYg2Nb7+cVpqj9vXPg=="
    }, {
      "_id": ObjectID("5b0f2c729eae1a03d20cba5e"),
      "firstName" : "Test",
      "lastName" : "Technician",
      "email" : "rdelhommer+t@gmail.com",
      "password" : "dkT/cmisG/WqlseMwPhcx8vd00Lirbpr8+lNtru6guhbwWWh0KRsJLhfPkvG4EsycnRtRg1LLhhG5hL182pfLQ==",
      "phone" : "1111111111",
      "roles" : [ 1 ],
      "created" : new Date(),
      "displayName" : "Test Technician",
      "salt" : "zdX1pYg2Nb7+cVpqj9vXPg=="
    }
  ])
  .then(() => { console.log('Seeded Users'); })
}

function connectAndSeed() {
  return new Promise(resolve => {
    MongoClient.connect(`mongodb://localhost:27017`, function(err, client) {
      if (err) { throw err; }

      console.log("Connected successfully to server");

      const db = client.db('f2s');
      client = client;

      cleanDb(db)
        .then(() => seedTestUsers(db))
        .then(() => seedBays(db))
        .then(() => seedHubs(db))
        .then(() => seedWork(db))
        .then(() => seedGenerators(db))
        .then(() => client.close())
        .then(() => resolve());
    });
  });
}

function newGuid() {
  var d = new Date().getTime();
  if (typeof performance !== 'undefined' && typeof performance.now === 'function'){
      d += performance.now(); //use high-precision timer if available
  }

  return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
    var r = (d + Math.random() * 16) % 16 | 0;
    d = Math.floor(d / 16);
    return (c === 'x' ? r : (r & 0x3 | 0x8)).toString(16);
  });
}
