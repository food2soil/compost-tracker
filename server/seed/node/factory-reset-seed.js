const MongoClient = require('mongodb').MongoClient;
const crypto = require('crypto');
const cleanCollection = require('./lib/clean-db').cleanCollection;

if (process.argv.indexOf('--pw') === -1) {
  throw new Error('Please provide a password for the superuser');
}

if (process.argv.indexOf('--uri') === -1) {
  console.warn('No MongoDB URI Provided')
}

if (process.argv.indexOf('--db') === -1) {
  console.warn('No MongoDB DB Provided')
}

let superUserPassword = process.argv[process.argv.indexOf('--pw') + 1];
let salt = crypto.randomBytes(16).toString('base64');
let hashedPassword = crypto.pbkdf2Sync(superUserPassword, new Buffer(salt, 'base64'), 10000, 64, 'SHA1')
  .toString('base64');

let mongoDbUri = process.argv[process.argv.indexOf('--uri') + 1]
let mongoDbDb = process.argv[process.argv.indexOf('--db') + 1]

connectAndSeed()
  .then(() => console.log('finished seed'))
  .catch((err) => {
    console.log('Error when seeding database');
    console.error(err);
  });

function seedSuperUser(db) {
  const collection = db.collection('users');
  return collection.remove({})
    .then(() => collection.insertMany([
      {
        firstName : 'Super',
        lastName : 'User',
        email : 'rdelhommer@gmail.com',
        password : hashedPassword,
        phone : '8325242888',
        roles : [ 3 ],
        created : new Date(),
        displayName : 'Super User',
        salt : salt
      }
    ]))
    .then(() => { console.log('Seeded Users'); })
}

function connectAndSeed() {
  return new Promise(resolve => {
    MongoClient.connect(mongoDbUri, function(err, client) {
      if (err) { throw err; }

      console.log('Connected successfully to server');

      const db = client.db(mongoDbDb);
      client = client;

      cleanCollection(db, 'users')
        .then(() => seedSuperUser(db))
        .then(() => client.close())
        .then(() => resolve());
    });
  });
}
