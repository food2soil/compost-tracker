function cleanDb(db) {
  return db.listCollections({}, {nameOnly: true}).toArray()
  .then(collections =>{
    return Promise.all(collections.map(c => db.dropCollection(c.name)))
  })
  .then(() => {
    console.log('Cleaned DB');
  })
}

function cleanCollection(db, collectionName) {
  return db.listCollections({}, {nameOnly: true}).toArray()
  .then(collections =>{
    return Promise.all(collections
      .filter(x => !collectionName || x.name === collectionName)
      .map(c => db.dropCollection(c.name)))
  })
  .then(() => {
    console.log('Cleaned ' + collectionName);
  })
}
module.exports = { cleanDb, cleanCollection };
