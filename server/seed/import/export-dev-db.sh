#!/bin/bash

BASEDIR=$(dirname "$0")

mongoexport --db f2s -c bays -o $BASEDIR/json/bays.json
mongoexport --db f2s -c generators -o $BASEDIR/json/generators.json
mongoexport --db f2s -c hubs -o $BASEDIR/json/hubs.json
mongoexport --db f2s -c invitations -o $BASEDIR/json/invitations.json
mongoexport --db f2s -c users -o $BASEDIR/json/users.json
mongoexport --db f2s -c works -o $BASEDIR/json/works.json
