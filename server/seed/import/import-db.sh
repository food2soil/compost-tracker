#!/bin/bash

BASEDIR=$(dirname "$0")

echo "==============="
echo "= IMPORT BAYS ="
echo "==============="
mongo f2s --eval "db.bays.remove({})"
mongoimport  --db f2s -c bays $BASEDIR/json/bays.json

echo "====================="
echo "= IMPORT GENERATORS ="
echo "====================="
mongo f2s --eval "db.generators.remove({})"
mongoimport  --db f2s -c generators $BASEDIR/json/generators.json

echo "==============="
echo "= IMPORT HUBS ="
echo "==============="
mongo f2s --eval "db.hubs.remove({})"
mongoimport  --db f2s -c hubs $BASEDIR/json/hubs.json

echo "======================"
echo "= IMPORT INVITATIONS ="
echo "======================"
mongo f2s --eval "db.invitations.remove({})"
mongoimport  --db f2s -c invitations $BASEDIR/json/invitations.json

echo "================"
echo "= IMPORT USERS ="
echo "================"
mongo f2s --eval "db.users.remove({})"
mongoimport  --db f2s -c users $BASEDIR/json/users.json

echo "================"
echo "= IMPORT WORKS ="
echo "================"
mongo f2s --eval "db.works.remove({})"
mongoimport  --db f2s -c works $BASEDIR/json/works.json

echo "==========================="
echo "= IMPORT TECHNICIAN STATS ="
echo "==========================="
mongo f2s --eval "db.technicianstats.remove({})"
# mongoimport  --db f2s -c works $BASEDIR/json/works.json
