import { database } from "../../../data-model/mongoose.config";
import { Types, Aggregate } from "mongoose";
import { IGeneratorStats, IGenerator } from "../../../data-model/generator/generator.model";
import { GeneratorShared } from "mean-au-ts-shared";

function _listGeneratorStats(query: any = {}): Promise<IGeneratorStats[]> {
  // console.log(query);
  return database.Generator.find(query)
    .then(generators => {
      return generators.map(x => <IGeneratorStats>{
        id: x.id,
        name: x.name,
        weeklyGallonsComposted: GeneratorShared.getWeeklyCollection(x),
        weeklyEmissionsReduced: GeneratorShared.getWeeklyMileageEmissionsReduced(x)
      })
    })
}

export function listGeneratorStats(query: any = {}): Promise<IGeneratorStats[]> {
  return _listGeneratorStats(query)
    // .then(x => {console.log(x); return x;})
    .then(x => x)
}

export function readGeneratorStats(id: Types.ObjectId): Promise<IGeneratorStats> {
  return _listGeneratorStats({ _id: id })
    .then(x => x[0])
}
