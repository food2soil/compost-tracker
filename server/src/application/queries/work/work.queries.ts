import { Types, DocumentQuery } from "mongoose";
import { IWorkModel } from "../../../data-model/work/work.model";
import { database } from "../../../data-model/mongoose.config";

export function getHubMostRecentWork(hubId: Types.ObjectId): DocumentQuery<IWorkModel, IWorkModel> {
  return database.Work.findOne({ hub: hubId })
    .sort({ date: 'desc'})
}
