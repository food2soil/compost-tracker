import { Types } from "mongoose";
import { IBatchModel } from "../../../data-model/batch/batch.model";
import { database } from "../../../data-model/mongoose.config";
import { BatchRequests } from "mean-au-ts-shared";

export function findBatches(query: BatchRequests.List): Promise<IBatchModel[]> {
  let activeQuery = {}
  let finishedQuery = {}

  if (query.hub) {
    activeQuery['activeBatches.stats.hub'] = query.hub
    finishedQuery['finishedBatches.stats.hub'] = query.hub
  }

  if (query.isActive === 'true') {
    activeQuery['activeBatches.stats.isActive'] = true
    finishedQuery = null
  } else if (query.isActive === 'false') {
    activeQuery = null
    finishedQuery['finishedBatches.stats.isActive'] = false
  }

  let checkActive = activeQuery 
    ? database.Bay.find(activeQuery) 
    : Promise.resolve([{ activeBatches: []}])
  let checkFinished = finishedQuery 
    ? database.Hub.find(finishedQuery) 
    : Promise.resolve([{ finishedBatches: []}])

  return Promise.all([checkActive, checkFinished])
    .then(results => {
      let activeBatches = results[0]
        .map(x => x.activeBatches)
        .reduce((a,b) => a.concat(b),[])
      let finishedBatches = results[1]
        .map(x => x.finishedBatches)
        .reduce((a,b) => a.concat(b),[])
      return activeBatches.concat(finishedBatches)
    });
}

export function findBatchById(id: Types.ObjectId): Promise<IBatchModel> {
  let activeQuery = { "activeBatches._id": id }
  let finishedQuery = { "finishedBatches._id": id }

  let checkActive = database.Bay.findOne(activeQuery)
  let checkFinished = database.Hub.findOne(finishedQuery)

  return Promise.all([checkActive, checkFinished])
    .then(results => {
      if (results[0]) {
        return results[0].activeBatches.find(x => x.id.toString() === id.toString())
      } else if (results[1]) {
        return results[1].finishedBatches.find(x => x.id.toString() === id.toString())
      }

      return undefined
    });
}
