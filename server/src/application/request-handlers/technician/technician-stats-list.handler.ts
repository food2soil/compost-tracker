import { IRequestHandler, IAuthenticatedRequest } from "../request-handler";
import { ITechnicianStats } from "../../../data-model/stats/technician/technician-stats.model";
import { database } from "../../../data-model/mongoose.config";

class ListHandler implements IRequestHandler<void, ITechnicianStats[]> {
  populate(req: IAuthenticatedRequest<void>): Promise<IAuthenticatedRequest<void>> {
    return Promise.resolve(req);
  }

  validate(req: IAuthenticatedRequest<void>): Promise<IAuthenticatedRequest<void>> {
    return Promise.resolve(req);
  }

  execute(req: IAuthenticatedRequest<void>): Promise<ITechnicianStats[]> {
    return database.TechnicianStats.find()
      .then(x => x)
  }
}

export const listTechnicianStatsHandler = new ListHandler();
