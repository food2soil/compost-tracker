import { database } from "../../../data-model/mongoose.config";
import { HandlerError } from "../../../api/handler.error";
import { IRequestHandler, ISingleRecordRequest } from "../request-handler";
import { IUserModel } from "../../../data-model/user/user.model";
import { Enums } from "mean-au-ts-shared";

declare type ValidateExt = (req: ISingleRecordRequest<IUserModel>) => void

class ReadHandler implements IRequestHandler<void, IUserModel> {
  constructor(
    private validateExt: ValidateExt = () => undefined
  ) { }

  populate(req: ISingleRecordRequest<IUserModel>): Promise<ISingleRecordRequest<IUserModel>> {
    if (!req.params.id) return Promise.reject(new HandlerError(400, 'No ID was provided'));

    let query = {
      _id: req.params.id,
      roles: { $in: [Enums.UserRoles.Technician, Enums.UserRoles.Admin] }
    }

    return database.User.findOne(query)
      .then(x => {
        req.data = x;
        return req;
      });
  }

  validate(req: ISingleRecordRequest<IUserModel>): Promise<ISingleRecordRequest<IUserModel>> {
    if (!req.data) return Promise.reject(new HandlerError(404, 'This technician does not exist'));

    this.validateExt(req)

    return Promise.resolve(req);
  }

  execute(req: ISingleRecordRequest<IUserModel>): Promise<IUserModel> {
    return Promise.resolve(req.data);
  }
}

export const readTechnicianHandler = new ReadHandler();


let validateExt: ValidateExt = (req) => {
  if (req.user.id.toString() !== req.data.id.toString())
    throw new HandlerError(403, 'Forbidden')
}

export const readTechnicianTechnicianRoleHandler = new ReadHandler(validateExt)