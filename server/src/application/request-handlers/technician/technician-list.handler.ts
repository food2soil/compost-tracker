import { database } from "../../../data-model/mongoose.config";
import { IRequestHandler, IAuthenticatedRequest } from "../request-handler";
import { Enums } from "mean-au-ts-shared";
import { IUserModel } from "../../../data-model/user/user.model";

export class ListHandler implements IRequestHandler<void, IUserModel[]> {
  populate(req: IAuthenticatedRequest<void>): Promise<IAuthenticatedRequest<void>> {
    return Promise.resolve(req);
  }

  validate(req: IAuthenticatedRequest<void>): Promise<IAuthenticatedRequest<void>> {
    return Promise.resolve(req);
  }

  execute(req: IAuthenticatedRequest<void>): Promise<IUserModel[]> {
    return database.User.find({ roles: Enums.UserRoles.Technician })
      .then(technicians => technicians)
  }
}

export const listHandler = new ListHandler();
