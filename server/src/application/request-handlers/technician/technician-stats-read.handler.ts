import { database } from "../../../data-model/mongoose.config";
import { HandlerError } from "../../../api/handler.error";
import { IRequestHandler, ISingleRecordRequest } from "../request-handler";
import { ITechnicianStats } from "../../../data-model/stats/technician/technician-stats.model";

class ReadHandler implements IRequestHandler<void, ITechnicianStats> {
  populate(req: ISingleRecordRequest<ITechnicianStats>): Promise<ISingleRecordRequest<ITechnicianStats>> {
    if (!req.params.id) return Promise.reject(new HandlerError(400, 'No ID was provided'));

    return database.TechnicianStats.findOne({ technician: req.params.id })
      .then(x => {
        req.data = x;
        return req;
      })
  }

  validate(req: ISingleRecordRequest<ITechnicianStats>): Promise<ISingleRecordRequest<ITechnicianStats>> {
    if (!req.data) return Promise.reject(new HandlerError(404, 'This technician does not exist'));

    return Promise.resolve(req);
  }

  execute(req: ISingleRecordRequest<ITechnicianStats>): Promise<ITechnicianStats> {
    return Promise.resolve(req.data);
  }
}

export const readTechnicianStatsHandler = new ReadHandler();
