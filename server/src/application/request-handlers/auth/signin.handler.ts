import { UserRequests, Validation, Utilities } from "mean-au-ts-shared";
import { HandlerError } from "../../../api/handler.error";
import { database } from "../../../data-model/mongoose.config";
import { logger } from "../../../lib/winston.lib";
import { aureliaValidator } from "../../../lib/validator.lib";
import { ValidationRules, ValidationController } from "aurelia-validation";
import { IRequestHandler, IAuthenticatedRequest } from "../request-handler";
import { IUserModel } from "../../../data-model/user/user.model";

class SignInHandler implements IRequestHandler<UserRequests.SignIn, IUserModel> {
  populate(req: IAuthenticatedRequest<UserRequests.SignIn>): Promise<IAuthenticatedRequest<UserRequests.SignIn>> {
    return Promise.resolve(req);
  }

  validate(req: IAuthenticatedRequest<UserRequests.SignIn>): Promise<IAuthenticatedRequest<UserRequests.SignIn>> {
    Validation.ensureDecoratorsOn(UserRequests.SignIn, ValidationRules)
    return aureliaValidator.validateObject(req.body).then(result => req);
  }

  execute(req: IAuthenticatedRequest<UserRequests.SignIn>): Promise<IUserModel> {
    return database.User.findOne({
      email: req.body.email.toLowerCase()
    }).then(user => {
      if (!user || !user.isAuthenticated(req.body.password))
        throw new HandlerError(401, 'Invalid username or password');

      logger.info(`Successful signin: ${user.email}`)

      /**
       * NOTE:
       * The user should be attached to the request so that
       * a token can be generated from its id in the token response builder step
       */
      req.user = user;

      return user
    });
  }
}

export const signInHandler = new SignInHandler();
