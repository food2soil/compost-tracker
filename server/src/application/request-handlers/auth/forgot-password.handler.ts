import { UserRequests, Validation } from "mean-au-ts-shared";
import { ValidationRules } from "aurelia-validation";
import { aureliaValidator } from "../../../lib/validator.lib";
import { HandlerError } from "../../../api/handler.error";
import { database } from "../../../data-model/mongoose.config";
import { IRequestHandler, IAuthenticatedRequest } from "../request-handler";

class ForgotPasswordHandler implements IRequestHandler<UserRequests.ForgotPassword, void> {
  populate(req: IAuthenticatedRequest<UserRequests.ForgotPassword>): Promise<IAuthenticatedRequest<UserRequests.ForgotPassword>> {
    return database.User.findOne({ 
      resetPasswordToken: req.body.forgotPasswordToken,
      resetPasswordExpires: { $gte: Date.now()}
    }).then(user => {
      req.user = user;
      return req;
    });
  }

  validate(req: IAuthenticatedRequest<UserRequests.ForgotPassword>): Promise<IAuthenticatedRequest<UserRequests.ForgotPassword>> {
    if (!req.user) throw new HandlerError(422, 'Invalid reset password token');

    Validation.ensureDecoratorsOn(UserRequests.ForgotPassword, ValidationRules)
    return aureliaValidator.validateObject(req.body).then(result => req);
  }

  execute(req: IAuthenticatedRequest<UserRequests.ForgotPassword>): Promise<void> {
    req.user.password = req.body.newPassword;
    req.user.resetPasswordToken = null;
    req.user.resetPasswordExpires = null;

    // Save and remove the user from the request since it's technically an anonymous request
    return req.user.save()
      .then(user => { delete req.user; })
      .catch(err => {
        throw HandlerError.fromError(422, err);
      })
  }
}

export const forgotPasswordHandler = new ForgotPasswordHandler();
