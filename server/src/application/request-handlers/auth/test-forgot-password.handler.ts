import { UserRequests, Validation } from "mean-au-ts-shared";
import { ValidationRules } from "aurelia-validation";
import { aureliaValidator } from "../../../lib/validator.lib";
import { HandlerError } from "../../../api/handler.error";
import { database } from "../../../data-model/mongoose.config";
import { IRequestHandler, IAuthenticatedRequest } from "../request-handler";

class TestForgotPasswordHandler implements IRequestHandler<UserRequests.TestForgotPassword, void> {
  populate(req: IAuthenticatedRequest<UserRequests.TestForgotPassword>): Promise<IAuthenticatedRequest<UserRequests.TestForgotPassword>> {
    return Promise.resolve(req);
  }

  validate(req: IAuthenticatedRequest<UserRequests.TestForgotPassword>): Promise<IAuthenticatedRequest<UserRequests.TestForgotPassword>> {
    Validation.ensureDecoratorsOn(UserRequests.TestForgotPassword, ValidationRules)
    return aureliaValidator.validateObject(req.query).then(result => req);
  }

  execute(req: IAuthenticatedRequest<UserRequests.TestForgotPassword>): Promise<void> {
    // ensure a user exists with that token
    return database.User.findOne({ 
      resetPasswordToken: req.query.forgotPasswordToken,
      resetPasswordExpires: { $gte: Date.now()}
    }).then(user => {
      if (!user) throw new HandlerError(422, 'Invalid reset password token')
    });
  }
}

export const testForgotPasswordHandler = new TestForgotPasswordHandler();
