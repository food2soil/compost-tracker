import { IRequestHandler, IAuthenticatedRequest } from "../request-handler";
import { HandlerError } from "../../../api/handler.error";

class TestErrorHandler implements IRequestHandler<void, void> {
  constructor(private code: number) { }

  populate(req: IAuthenticatedRequest<void>): Promise<IAuthenticatedRequest<void>> {
    return Promise.resolve(req)
  }

  validate(req: IAuthenticatedRequest<void>): Promise<IAuthenticatedRequest<void>> {
    return Promise.resolve(req)
  }

  execute(req: IAuthenticatedRequest<void>): Promise<void> {
    throw new HandlerError(this.code, `Test ${this.code} error`)
    return Promise.resolve()
  }
}

export const test401Handler = new TestErrorHandler(401);
export const test422Handler = new TestErrorHandler(422);
