import { jwtBlacklist } from "../../../lib/jwt-blacklist.lib";
import { IRequestHandler, IAuthenticatedRequest } from "../request-handler";
import { getTokenFromRequest } from "../../strategies/jwt.strategy";

class SignOutHandler implements IRequestHandler<void, void> {
  populate(req: IAuthenticatedRequest<void>): Promise<IAuthenticatedRequest<void>> {
    return Promise.resolve(req);
  }

  validate(req: IAuthenticatedRequest<void>): Promise<IAuthenticatedRequest<void>> {
      return Promise.resolve(req);
  }

  execute(req: IAuthenticatedRequest<void>): Promise<void> {
      // Blacklist the token from this request
      jwtBlacklist.addToBlacklist(getTokenFromRequest(req));
      delete req.user;

      return Promise.resolve();
  }
}

export const signOutHandler = new SignOutHandler();
