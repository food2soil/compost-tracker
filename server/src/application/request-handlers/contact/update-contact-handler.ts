import { Model } from 'mongoose';
import { database } from "../../../data-model/mongoose.config";
import { HandlerError } from "../../../api/handler.error";
import { IRequestHandler, ISingleRecordRequest } from "../request-handler";
import { Validation, ContactRequests } from "mean-au-ts-shared";
import { ValidationRules } from "aurelia-validation";
import { aureliaValidator } from "../../../lib/validator.lib";
import { IContact, IContactsModel } from "../../../data-model/shared/contact/contact.model";

class UpdateHandler<TModel extends IContactsModel> implements IRequestHandler<ContactRequests.Update, IContact> {
  constructor(private model: Model<TModel>) { }

  populate(req: ISingleRecordRequest<TModel, ContactRequests.Update>): Promise<ISingleRecordRequest<TModel, ContactRequests.Update>> {
    return this.model.findOne({'contacts._id': req.params.id}).then(x => {
      req.data = x;
      return req;
    });
  }

  validate(req: ISingleRecordRequest<TModel, ContactRequests.Update>): Promise<ISingleRecordRequest<TModel, ContactRequests.Update>> {
    if (!req.data) throw new HandlerError(404, 'This record does not exist');

    Validation.ensureDecoratorsOn(ContactRequests.Update, ValidationRules)
    return aureliaValidator.validateObject(req.body).then(result => req);
  }

  execute(req: ISingleRecordRequest<TModel, ContactRequests.Update>): Promise<IContact> {
    let toUpdate = req.data.contacts.find(c => c.id.toString() === req.params.id);

    toUpdate.name = req.body.name;
    toUpdate.title = req.body.title;
    toUpdate.phone = req.body.phone;
    toUpdate.email = req.body.email;
    toUpdate.isPrimary = req.body.isPrimary

    return req.data.save().then(x => toUpdate);
  }
}

export const updateGeneratorContactHandler = new UpdateHandler(database.Generator);
export const updateHubContactHandler = new UpdateHandler(database.Hub);
