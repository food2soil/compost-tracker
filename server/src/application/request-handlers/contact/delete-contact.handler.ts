import { Model } from "mongoose";
import { database } from "../../../data-model/mongoose.config";
import { HandlerError } from "../../../api/handler.error";
import { IRequestHandler, ISingleRecordRequest } from "../request-handler";
import { IContactsModel } from "../../../data-model/shared/contact/contact.model";

class DeleteHandler<TModel extends IContactsModel> implements IRequestHandler<void, void> {
  constructor(private model: Model<TModel>) { }

  populate(req: ISingleRecordRequest<TModel>): Promise<ISingleRecordRequest<TModel>> {
    return this.model.findOne({'contacts._id': req.params.id}).then(x => {
      req.data = x;
      return req;
    });
  }

  validate(req: ISingleRecordRequest<TModel>): Promise<ISingleRecordRequest<TModel>> {
    if (!req.data) throw new HandlerError(404, 'This record does not exist');
    if (req.data.contacts.length === 1) throw new HandlerError(422, 'Can not delete the last contact');

    return Promise.resolve(req);
  }

  execute(req: ISingleRecordRequest<TModel>): Promise<void> {
    return req.data.update({$pull: {contacts: {_id: req.params.id}}})
      .then(res => undefined)
  }
}

export const deleteGeneratorContactHandler = new DeleteHandler(database.Generator);
export const deleteHubContactHandler = new DeleteHandler(database.Hub);
