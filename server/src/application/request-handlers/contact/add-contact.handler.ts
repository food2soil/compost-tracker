import { HandlerError } from "../../../api/handler.error";
import { Validation, ContactRequests, ContactRead } from "mean-au-ts-shared";
import { ValidationRules } from "aurelia-validation";
import { aureliaValidator } from "../../../lib/validator.lib";
import { IRequestHandler, ISingleRecordRequest } from "../request-handler";
import { ContactModel } from "../../../data-model/shared/contact/contact.schema";
import { Model } from "mongoose";
import { IContactsModel } from "../../../data-model/shared/contact/contact.model";

export class AddContactHandler<TModel extends IContactsModel> implements IRequestHandler<ContactRequests.Create, ContactRead.ContactViewModel> {
  constructor(private model: Model<TModel>) { }

  populate(req: ISingleRecordRequest<TModel, ContactRequests.Create>): Promise<ISingleRecordRequest<TModel, ContactRequests.Create>> {
    return this.model.findById(req.params.id).then(x => {
      req.data = x;
      return req;
    })
  }

  validate(req: ISingleRecordRequest<TModel, ContactRequests.Create>): Promise<ISingleRecordRequest<TModel, ContactRequests.Create>> {
    if (!req.data) throw new HandlerError(404, 'This record does not exist');

    Validation.ensureDecoratorsOn(ContactRequests.Create, ValidationRules)
    return aureliaValidator.validateObject(req.body).then(result => req);
  }

  execute(req: ISingleRecordRequest<TModel, ContactRequests.Create>): Promise<ContactRead.ContactViewModel> {
    let newContact = new ContactModel({
      name: req.body.name,
      title: req.body.title,
      phone: req.body.phone,
      email: req.body.email,
      isPrimary: req.body.isPrimary
    })

    req.data.contacts.push(newContact)

    let ret: ContactRead.ContactViewModel = {
      id: newContact.id.toString(),
      name: newContact.name,
      title: newContact.title,
      phone: newContact.phone,
      email: newContact.email,
      isPrimary: newContact.isPrimary
    }

    return req.data.save().then(x => ret);
  }
}
