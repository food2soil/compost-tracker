import { HandlerError } from "../../../api/handler.error";
import { database } from "../../../data-model/mongoose.config";
import { IRequestHandler, ISingleRecordRequest } from "../request-handler";
import { IInvitationModel, IInvitation } from "../../../data-model/invitations/invitation.model";

class ReadHandler implements IRequestHandler<void, IInvitationModel> {
  constructor(
    private populatePaths: (keyof IInvitation)[] = []
  ) { }

  populate(req: ISingleRecordRequest<IInvitationModel>): Promise<ISingleRecordRequest<IInvitationModel>> {
    // ensure an invitation exists with that token
    return database.Invitation.findById(req.params.id)
      .populate(this.populatePaths.length === 0 ? '' : this.populatePaths.join(' '))
      .then(invitation => {
        req.data = invitation;
        return req;
      });
  }

  validate(req: ISingleRecordRequest<IInvitationModel>): Promise<ISingleRecordRequest<IInvitationModel>> {
    if (!req.data) throw new HandlerError(404, 'The invitation does not exist')

    return Promise.resolve(req)
  }

  execute(req: ISingleRecordRequest<IInvitationModel>): Promise<IInvitationModel> {
    return Promise.resolve(req.data)
  }
}

export const readInvitationHandler = new ReadHandler();
export const readInvitationWithInvitedByHandler = new ReadHandler(['invitedBy']);
export const readInvitationWithInvitedByAndHubHandler = new ReadHandler(['invitedBy', 'hubs'])
