import { database } from "../../../data-model/mongoose.config";
import { IRequestHandler, ISingleRecordRequest } from "../request-handler";
import { IInvitationModel } from "../../../data-model/invitations/invitation.model";
import { mailer } from "../../../lib/mailer.lib";
import { config } from "../../../config/config";
import { HandlerError } from "../../../api/handler.error";

export class ResendInviteHandler implements IRequestHandler<void, void> {
  populate(req: ISingleRecordRequest<IInvitationModel>): Promise<ISingleRecordRequest<IInvitationModel>> {
    return database.Invitation.findById(req.params.id).then(invite => {
      req.data = invite;
      return req;
    })
  }

  validate(req: ISingleRecordRequest<IInvitationModel>): Promise<ISingleRecordRequest<IInvitationModel>> {
    if (!req.data) return Promise.reject(new HandlerError(422, 'This invitation has already been claimed'));
    return Promise.resolve(req);
  }

  execute(req: ISingleRecordRequest<IInvitationModel>): Promise<void> {
    return new Promise(resolve => {
      var message =
        `Hello ${req.data.firstName} ${req.data.lastName}!<br/>` +
        '<br/>' +
        `This is an invitation from ${req.user.firstName} ${req.user.lastName} to create an account with Food2Soil.` +
        'To create your account, please click the link below.<br/>' +
        `<a href="${config.auth.domain}/claim-invite/${req.data.id}">Claim your Account!</a>` +
        `<hr>` +
        'This is an automated message.  Please do not reply to this email.<br/>' +
        `If you need to contact ${req.user.firstName} ${req.user.lastName}, you may email them at <a href="mailto:${req.user.email}">${req.user.email}</a>`

      mailer.send(req.data.email, 'Food2Soil Account Invitation', message)
      return resolve();
    });
  }
}

export const resendInviteHandler = new ResendInviteHandler();
