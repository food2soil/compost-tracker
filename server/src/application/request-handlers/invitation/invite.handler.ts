import { database } from "../../../data-model/mongoose.config";
import { Validation, InvitationRequests, Enums } from "mean-au-ts-shared";
import { IUserModel } from "../../../data-model/user/user.model";
import { IRequestHandler, ISingleRecordRequest } from "../request-handler";
import { ValidationRules } from "aurelia-validation";
import { aureliaValidator } from "../../../lib/validator.lib";
import { IInvitation } from "../../../data-model/invitations/invitation.model";
import { mailer } from "../../../lib/mailer.lib";
import { config } from "../../../config/config";
import { HandlerError } from "../../../api/handler.error";
import { Types } from "mongoose";

export class InviteHandler implements IRequestHandler<InvitationRequests.InviteTechnician, void> {
  populate(req: ISingleRecordRequest<IUserModel, InvitationRequests.InviteTechnician>): Promise<ISingleRecordRequest<IUserModel, InvitationRequests.InviteTechnician>> {
    return database.User.findOne({ email: req.body.email }).then(user => {
      req.data = user;
      return req;
    })
  }

  validate(req: ISingleRecordRequest<IUserModel, InvitationRequests.InviteTechnician>): Promise<ISingleRecordRequest<IUserModel, InvitationRequests.InviteTechnician>> {
    if (req.data) return Promise.reject(new HandlerError(422, 'This email is already registered to a user'));

    Validation.ensureDecoratorsOn(InvitationRequests.InviteTechnician, ValidationRules)
    return aureliaValidator.validateObject(req.body).then(result => req);
  }

  execute(req: ISingleRecordRequest<IUserModel, InvitationRequests.InviteTechnician>): Promise<void> {
    let roles = [Enums.UserRoles.Technician]
    if (req.body.isAdmin) {
      roles.push(Enums.UserRoles.Admin)
    }

    var invite: IInvitation = {
      email: req.body.email,
      firstName: req.body.firstName,
      lastName: req.body.lastName,
      roles: roles,
      invitedBy: req.user.id,
      hubs: req.body.hubs.map(x => Types.ObjectId(x))
    }

    return new database.Invitation(invite).save().then(savedInvite => {
      var message =
        `Hello ${req.body.firstName} ${req.body.lastName}!<br/>` +
        '<br/>' +
        `This is an invitation from ${req.user.firstName} ${req.user.lastName} to create an account with Food2Soil.` +
        'To create your account, please click the link below.<br/>' +
        `<a href="${config.auth.domain}/claim-invite/${savedInvite.id}">Claim your Account!</a>` +
        `<hr>` +
        'This is an automated message.  Please do not reply to this email.<br/>' +
        `If you need to contact ${req.user.firstName} ${req.user.lastName}, you may email them at <a href="mailto:${req.user.email}">${req.user.email}</a>`

      mailer.send(savedInvite.email, 'Food2Soil Account Invitation', message)
    });
  }
}

export const inviteHandler = new InviteHandler();
