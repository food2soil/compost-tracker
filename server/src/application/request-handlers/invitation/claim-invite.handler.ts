import { Validation, InvitationRequests } from "mean-au-ts-shared";
import { ValidationRules } from "aurelia-validation";
import { aureliaValidator } from "../../../lib/validator.lib";
import { HandlerError } from "../../../api/handler.error";
import { database } from "../../../data-model/mongoose.config";
import { IRequestHandler, ISingleRecordRequest } from "../request-handler";
import { IInvitationModel } from "../../../data-model/invitations/invitation.model";
import { logger } from "../../../lib/winston.lib";
import { MongoError } from "mongodb";
import { TechnicianStatsModel } from "../../../data-model/stats/technician/technician-stats.schema";

class ClaimInviteHandler implements IRequestHandler<InvitationRequests.ClaimInvite, void> {
  populate(req: ISingleRecordRequest<IInvitationModel, InvitationRequests.ClaimInvite>): Promise<ISingleRecordRequest<IInvitationModel, InvitationRequests.ClaimInvite>> {
    return database.Invitation.findById(req.body.inviteToken)
      .then(invite => {
        req.data = invite;
        return req;
      });
  }

  validate(req: ISingleRecordRequest<IInvitationModel, InvitationRequests.ClaimInvite>): Promise<ISingleRecordRequest<IInvitationModel, InvitationRequests.ClaimInvite>> {
    if (!req.data) return Promise.reject(new HandlerError(422, 'Invalid reset password token'))

    Validation.ensureDecoratorsOn(InvitationRequests.ClaimInvite, ValidationRules)
    return aureliaValidator.validateObject(req.body).then(result => req);
  }

  execute(req: ISingleRecordRequest<IInvitationModel, InvitationRequests.ClaimInvite>): Promise<void> {
    // Init user and add missing fields
    var user = new database.User(req.body);
    user.firstName = req.data.firstName;
    user.lastName = req.data.lastName;
    user.email = req.data.email;
    user.roles = req.data.roles;
    user.displayName = user.firstName + ' ' + user.lastName;
    user.stats = {
      hoursAllTime: 0,
      bucketsHarvestedAllTime: 0,
      monthly: []
    }

    // Then save the user
    return user.save()
      .then(saved => {
        if (req.data.hubs.length === 0) return Promise.resolve(saved)

        // Link the new user to any specified hubs
        return database.Hub.updateMany(
          {_id: { $in: req.data.hubs } },
          { $push: { technicians: saved.id } })
          .then(() => saved)
      })
      .then(() => {
        logger.info(`Successful signup: ${req.data.email}`)
      })
      .then(() => {
        return req.data.remove().then(() => undefined);
      })
      .catch((err: MongoError) => {
        if (err.code === 11000) {
          throw new HandlerError(422, `The provided email is already in use`)
        }

        throw err;
      });
  }
}

export const claimInviteHandler = new ClaimInviteHandler();
