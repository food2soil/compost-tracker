import { database } from "../../../data-model/mongoose.config";
import { IRequestHandler, IAuthenticatedRequest } from "../request-handler";
import { IInvitationModel, IInvitation } from "../../../data-model/invitations/invitation.model";

export class InvitationListHandler implements IRequestHandler<void, IInvitationModel[]> {
  constructor(
    private populatePaths: (keyof IInvitation)[] = []
  ) { }

  populate(req: IAuthenticatedRequest<void>): Promise<IAuthenticatedRequest<void>> {
    return Promise.resolve(req);
  }

  validate(req: IAuthenticatedRequest<void>): Promise<IAuthenticatedRequest<void>> {
    return Promise.resolve(req);
  }

  execute(req: IAuthenticatedRequest<void>): Promise<IInvitationModel[]> {
    return database.Invitation.find()
      .populate(this.populatePaths.length === 0 ? '' : this.populatePaths.join(' '))
      .then(pendingInvites => pendingInvites)
  }
}

export const invitationListHandler = new InvitationListHandler();
export const invitationWithHubsListHandler = new InvitationListHandler(['hubs']);
