import { database } from "../../../data-model/mongoose.config";
import { Validation, InvitationRequests, Enums } from "mean-au-ts-shared";
import { IUserModel } from "../../../data-model/user/user.model";
import { IRequestHandler, ISingleRecordRequest } from "../request-handler";
import { ValidationRules } from "aurelia-validation";
import { aureliaValidator } from "../../../lib/validator.lib";

export class CheckEmailHandler implements IRequestHandler<InvitationRequests.CheckEmail, boolean> {
  populate(req: ISingleRecordRequest<IUserModel, InvitationRequests.CheckEmail>): Promise<ISingleRecordRequest<IUserModel, InvitationRequests.CheckEmail>> {
    return database.User.findOne({ email: req.query.email })
      .then(user => {
        req.data = user;
        return req;
      })
  }

  validate(req: ISingleRecordRequest<IUserModel, InvitationRequests.CheckEmail>): Promise<ISingleRecordRequest<IUserModel, InvitationRequests.CheckEmail>> {
    Validation.ensureDecoratorsOn(InvitationRequests.CheckEmail, ValidationRules)
    return aureliaValidator.validateObject(req.body).then(result => req);
  }

  execute(req: ISingleRecordRequest<IUserModel, InvitationRequests.CheckEmail>): Promise<boolean> {
    return Promise.resolve(!req.data)
  }
}

export const checkEmailHandler = new CheckEmailHandler();
