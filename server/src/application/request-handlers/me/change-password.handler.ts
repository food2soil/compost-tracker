import { HandlerError } from "../../../api/handler.error";
import { IRequestHandler, IAuthenticatedRequest } from "../request-handler";
import { Validation, UserRequests } from "mean-au-ts-shared";
import { ValidationRules } from "aurelia-validation";
import { aureliaValidator } from "../../../lib/validator.lib";

class ChangePasswordHandler implements IRequestHandler<UserRequests.ChangePassword, void> {
  populate(req: IAuthenticatedRequest<UserRequests.ChangePassword>): Promise<IAuthenticatedRequest<UserRequests.ChangePassword>> {
    return Promise.resolve(req);
  }

  validate(req: IAuthenticatedRequest<UserRequests.ChangePassword>): Promise<IAuthenticatedRequest<UserRequests.ChangePassword>> {
    let rules = ValidationRules
      .ensure((x: UserRequests.ChangePassword) => x.currentPassword)
      .satisfies((v: any) => {
        return req.user.isAuthenticated(v);
      })
      .withMessage('Your current password is not correct');

    Validation.ensureDecoratorsOn(UserRequests.ChangePassword, rules)
    return aureliaValidator.validateObject(req.body).then(result => req);
  }

  execute(req: IAuthenticatedRequest<UserRequests.ChangePassword>): Promise<void> {
    req.user.password = req.body.newPassword;

    return req.user.save()
      .then(user => undefined)
      .catch(err => {
        throw HandlerError.fromError(422, err);
      })
  }
}

export const changePasswordHandler = new ChangePasswordHandler();
