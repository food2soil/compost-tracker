import { IUser } from "../../../data-model/user/user.model";
import { HandlerError } from "../../../api/handler.error";
import { IRequestHandler, IAuthenticatedRequest } from "../request-handler";

class ProfileHandler implements IRequestHandler<void, IUser> {
  populate(req: IAuthenticatedRequest<void>): Promise<IAuthenticatedRequest<void>> {
    return Promise.resolve(req);
  }
  validate(req: IAuthenticatedRequest<void>): Promise<IAuthenticatedRequest<void>> {
    if (req.user.isAnonymous())
      return Promise.reject(new HandlerError(401, 'You must be logged in to access this.'));

    return Promise.resolve(req);
  }

  execute(req: IAuthenticatedRequest<void>): Promise<IUser> {
    return Promise.resolve(req.user);
  }
}

export const profileHandler = new ProfileHandler();
