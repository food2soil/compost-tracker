import { IRequestHandler, IAuthenticatedRequest } from "../request-handler";
import { Validation, UserRequests } from "mean-au-ts-shared";
import { ValidationRules } from "aurelia-validation";
import { aureliaValidator } from "../../../lib/validator.lib";

class UpdateProfileHandler implements IRequestHandler<UserRequests.EditProfile, void> {
  populate(req: IAuthenticatedRequest<UserRequests.EditProfile>): Promise<IAuthenticatedRequest<UserRequests.EditProfile>> {
    return Promise.resolve(req);
  }

  validate(req: IAuthenticatedRequest<UserRequests.EditProfile>): Promise<IAuthenticatedRequest<UserRequests.EditProfile>> {
    Validation.ensureDecoratorsOn(UserRequests.EditProfile, ValidationRules)
    return aureliaValidator.validateObject(req.body).then(result => req);
  }

  execute(req: IAuthenticatedRequest<UserRequests.EditProfile>): Promise<void> {
    req.user.firstName = req.body.firstName;
    req.user.lastName = req.body.lastName;
    req.user.email = req.body.email;
    req.user.phone = req.body.phone;

    return req.user.save().then(user => undefined);
  }
}

export const updateProfileHandler = new UpdateProfileHandler();
