import { database } from "../../../data-model/mongoose.config";
import { HandlerError } from "../../../api/handler.error";
import { IRequestHandler, ISingleRecordRequest } from "../request-handler";
import { Validation, GeneratorRequests } from "mean-au-ts-shared";
import { ValidationRules } from "aurelia-validation";
import { aureliaValidator } from "../../../lib/validator.lib";
import { IGeneratorModel } from "../../../data-model/generator/generator.model";
import { ContainerHistoryModel } from "../../../data-model/generator/container-history/container-history.schema";
import { IContainerHistory } from "../../../data-model/generator/container-history/container-history.model";

class UpdateHandler implements IRequestHandler<GeneratorRequests.Update, void> {
  populate(req: ISingleRecordRequest<IGeneratorModel, GeneratorRequests.Update>): Promise<ISingleRecordRequest<IGeneratorModel, GeneratorRequests.Update>> {
    return database.Generator.findById(req.params.id).then(x => {
      req.data = x;
      return req;
    })
  }

  validate(req: ISingleRecordRequest<IGeneratorModel, GeneratorRequests.Update>): Promise<ISingleRecordRequest<IGeneratorModel, GeneratorRequests.Update>> {
    if (!req.data) throw new HandlerError(404, 'This record does not exist');

    Validation.ensureDecoratorsOn(GeneratorRequests.Update, ValidationRules)
    return aureliaValidator.validateObject(req.body).then(result => req);
  }

  execute(req: ISingleRecordRequest<IGeneratorModel, GeneratorRequests.Update>): Promise<void> {
    req.data.name = req.body.name;
    req.data.generatorType = req.body.generatorType;
    req.data.collectionType = req.body.collectionType;

    req.data.address = {
      primaryLine: req.body.addressPrimaryLine,
      secondaryLine: req.body.addressSecondaryLine,
      city: req.body.addressCity,
      state: req.body.addressState,
      zip: req.body.addressZip,
    };

    if (req.body.isNewContainer) {
      let container: IContainerHistory = {
        volumePerContainer: req.body.containerType,
        containersPerWeek: req.body.containersPerWeek,
        dateStartedUsing: new Date()
      }

      req.data.containerHistory.push(new ContainerHistoryModel(container))
    }

    return req.data.save().then(x => undefined);
  }
}

export const updateHandler = new UpdateHandler();
