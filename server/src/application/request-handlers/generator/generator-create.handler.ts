import { database } from "../../../data-model/mongoose.config";
import { HandlerError } from "../../../api/handler.error";
import { IRequestHandler, IAuthenticatedRequest } from "../request-handler";
import { GeneratorRequests, Validation } from "mean-au-ts-shared";
import { ValidationRules } from "aurelia-validation";
import { aureliaValidator } from "../../../lib/validator.lib";
import { IAddress } from "../../../data-model/shared/address/address.model";
import { IContact } from "../../../data-model/shared/contact/contact.model";
import { IGenerator, IGeneratorModel } from "../../../data-model/generator/generator.model";
import { IContainerHistory } from '../../../data-model/generator/container-history/container-history.model';

class CreateHandler implements IRequestHandler<GeneratorRequests.Create, IGeneratorModel> {
  populate(req: IAuthenticatedRequest<GeneratorRequests.Create>): Promise<IAuthenticatedRequest<GeneratorRequests.Create>> {
    return Promise.resolve(req);
  }

  validate(req: IAuthenticatedRequest<GeneratorRequests.Create>): Promise<IAuthenticatedRequest<GeneratorRequests.Create>> {
    Validation.ensureDecoratorsOn(GeneratorRequests.Create, ValidationRules)
    return aureliaValidator.validateObject(req.body).then(result => req);
  }

  execute(req: IAuthenticatedRequest<GeneratorRequests.Create>): Promise<IGeneratorModel> {
    let address: IAddress = {
      primaryLine: req.body.addressPrimaryLine,
      secondaryLine: req.body.addressSecondaryLine,
      city: req.body.addressCity,
      state: req.body.addressState,
      zip: req.body.addressZip,
    };

    let primaryContact: IContact = {
      name: req.body.primaryContactName,
      title: req.body.primaryContactTitle,
      phone: req.body.primaryContactPhone,
      email: req.body.primaryContactEmail,
      isPrimary: true
    }

    let containerHistory: IContainerHistory = {
      volumePerContainer: req.body.containerType,
      containersPerWeek: req.body.containersPerWeek,
      dateStartedUsing: new Date()
    }

    let init: Partial<IGenerator> = {
      created: new Date(),
      isActive: true,
      name: req.body.name,
      address: address,
      contacts: [primaryContact],
      containerHistory: [containerHistory],
      generatorType: req.body.generatorType,
      collectionType: req.body.collectionType
    };

    let generator = new database.Generator(init);

    return generator.save()
      .then(() => generator)
      .catch((err: Error) => {
        throw new HandlerError(422, err.message)
      })
  }
}

export const createHandler = new CreateHandler();
