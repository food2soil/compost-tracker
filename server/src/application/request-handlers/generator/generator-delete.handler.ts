import { database } from "../../../data-model/mongoose.config";
import { DeleteHandler } from "../base/delete.handler";

export const deleteHandler = new DeleteHandler(database.Generator);
