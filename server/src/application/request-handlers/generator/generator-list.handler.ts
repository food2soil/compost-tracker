import { database } from '../../../data-model/mongoose.config';
import { IRequestHandler, IAuthenticatedRequest } from '../request-handler';
import { IGeneratorModel } from '../../../data-model/generator/generator.model';

class ListHandler implements IRequestHandler<void, IGeneratorModel[]> {
  populate(req: IAuthenticatedRequest<void>): Promise<IAuthenticatedRequest<void>> {
    return Promise.resolve(req);
  }

  validate(req: IAuthenticatedRequest<void>): Promise<IAuthenticatedRequest<void>> {
    return Promise.resolve(req);
  }

  execute(req: IAuthenticatedRequest<void>): Promise<IGeneratorModel[]> {
    return database.Generator.find({ isActive: true })
      .then(generators => generators)
  }
}

export const listHandler = new ListHandler();
