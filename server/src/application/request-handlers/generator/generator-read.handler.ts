import { database } from "../../../data-model/mongoose.config";
import { ReadHandler } from "../base/read.handler";

export const readHandler = new ReadHandler(database.Generator);
