import { IUserModel } from "../../data-model/user/user.model";
import { Request } from 'express';

export interface ITypedRequest<TBody> extends Request {
  body: TBody
  query: TBody
}

export interface IAuthenticatedRequest<TBody> extends ITypedRequest<TBody> {
  user: IUserModel
}

export interface ISingleRecordRequest<TData = void, TBody = void> extends IAuthenticatedRequest<TBody> {
  data: TData
  params: { id: string }
}

export interface IEmbeddedRecordRequest<TBody> extends IAuthenticatedRequest<TBody> {
  params: {
    id: string
    embeddedId: string
  }
}

export interface IRequestHandler<TRequestBody, TResponseData> {
  populate(req: IAuthenticatedRequest<TRequestBody>): Promise<IAuthenticatedRequest<TRequestBody>>
  validate(req: IAuthenticatedRequest<TRequestBody>): Promise<IAuthenticatedRequest<TRequestBody>>
  execute(req: IAuthenticatedRequest<TRequestBody>): Promise<TResponseData | TResponseData[]>
}
