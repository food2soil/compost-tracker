import { database } from "../../../data-model/mongoose.config";
import { HubRequests } from "mean-au-ts-shared";
import { IRequestHandler, IAuthenticatedRequest } from "../request-handler";
import { IHubModel } from "../../../data-model/hub/hub.model";

class ListHandler implements IRequestHandler<HubRequests.List, IHubModel[]> {
  populate(req: IAuthenticatedRequest<HubRequests.List>): Promise<IAuthenticatedRequest<HubRequests.List>> {
    return Promise.resolve(req);
  }
  validate(req: IAuthenticatedRequest<HubRequests.List>): Promise<IAuthenticatedRequest<HubRequests.List>> {
    return Promise.resolve(req);
  }
  execute(req: IAuthenticatedRequest<HubRequests.List>): Promise<IHubModel[]> {
    let query: any = { }

    if (req.query.hasActiveBatches === 'true') {
      query['stats.numActiveBatches'] = { $gt: 0 }
    } else if (req.query.hasActiveBatches === 'false') {
      query['stats.numActiveBatches'] = 0
    }

    if (req.query.technician) {
      query.technicians = req.query.technician
    }

    if (req.query.needsCarbon === 'true') {
      query['stats.needsCarbon'] = true
    } else if (req.query.needsCarbon === 'false') {
      query['stats.needsCarbon'] = false
    }

    return database.Hub.find(query)
      .populate('stats.lastWork')
      .then(hubs => hubs)
  }
}

export const listHandler = new ListHandler();
