import { database } from "../../../data-model/mongoose.config";
import { HandlerError } from "../../../api/handler.error";
import { IRequestHandler, IAuthenticatedRequest } from "../request-handler";
import { HubRequests, Validation } from "mean-au-ts-shared";
import { ValidationRules } from "aurelia-validation";
import { aureliaValidator } from "../../../lib/validator.lib";
import { IAddress } from "../../../data-model/shared/address/address.model";
import { IContact } from "../../../data-model/shared/contact/contact.model";
import { IHub } from '../../../data-model/hub/hub.model';
import { BayModel } from '../../../data-model/bay/bay.schema';
import { HubStatsModel } from "../../../data-model/stats/hub/hub-stats.schema";
import { getDefaultBays } from "../../../data-model/hub/ext/default-bays.ext";

class CreateHandler implements IRequestHandler<HubRequests.Create, IHub> {
  populate(req: IAuthenticatedRequest<HubRequests.Create>): Promise<IAuthenticatedRequest<HubRequests.Create>> {
    return Promise.resolve(req);
  }

  validate(req: IAuthenticatedRequest<HubRequests.Create>): Promise<IAuthenticatedRequest<HubRequests.Create>> {
    Validation.ensureDecoratorsOn(HubRequests.Create, ValidationRules)
    return aureliaValidator.validateObject(req.body).then(result => req);
  }

  execute(req: IAuthenticatedRequest<HubRequests.Create>): Promise<IHub> {
    let address: IAddress = {
      primaryLine: req.body.addressPrimaryLine,
      secondaryLine: req.body.addressSecondaryLine,
      city: req.body.addressCity,
      state: req.body.addressState,
      zip: req.body.addressZip,
    };

    let primaryContact: IContact = {
      name: req.body.primaryContactName,
      title: req.body.primaryContactTitle,
      phone: req.body.primaryContactPhone,
      email: req.body.primaryContactEmail,
      isPrimary: true
    }

    let init: Partial<IHub> = {
      created: new Date(),
      isActive: true,
      bays: [],  // Default bays is set a couple of lines down
      name: req.body.name,
      address: address,
      contacts: [primaryContact],
      generators: [],
      technicians: [],
      isCollectionPoint: req.body.isCollectionPoint,
      isComposting: req.body.isComposting,
      notes: req.body.notes,
      capacityGallonsPerWeek: req.body.capacityGallonsPerWeek,
      finishedBatches: [],
      stats: {
        finishedCompostMade: 0,
        lastWork: null,
        needsCarbon: false,
        numActiveBatches: 0,
        numResidential: 0,
        numRestaurants: 0,
        usagePercentage: 0
      }
    };

    let hub = new database.Hub(init);
    hub.bays = getDefaultBays();

    return Promise.all(hub.bays.map(x => x.save()))
      .then(() => hub.save())
      .then(() => hub)
      .catch((err: Error) => {
      throw new HandlerError(422, err.message)
    })
  }
}

export const createHandler = new CreateHandler();
