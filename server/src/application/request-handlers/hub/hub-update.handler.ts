import { database } from "../../../data-model/mongoose.config";
import { HandlerError } from "../../../api/handler.error";
import { IRequestHandler, ISingleRecordRequest } from "../request-handler";
import { HubRequests, Validation } from "mean-au-ts-shared";
import { ValidationRules } from "aurelia-validation";
import { aureliaValidator } from "../../../lib/validator.lib";
import { IHubModel } from "../../../data-model/hub/hub.model";

class UpdateHandler implements IRequestHandler<HubRequests.Update, void> {
  populate(req: ISingleRecordRequest<IHubModel, HubRequests.Update>): Promise<ISingleRecordRequest<IHubModel, HubRequests.Update>> {
    return database.Hub.findById(req.params.id).then(x => {
      req.data = x;
      return req;
    })
  }
  validate(req: ISingleRecordRequest<IHubModel, HubRequests.Update>): Promise<ISingleRecordRequest<IHubModel, HubRequests.Update>> {
    if (!req.data) throw new HandlerError(404, 'This record does not exist');

    Validation.ensureDecoratorsOn(HubRequests.Update, ValidationRules)
    return aureliaValidator.validateObject(req.body).then(result => req);
  }

  execute(req: ISingleRecordRequest<IHubModel, HubRequests.Update>): Promise<void> {
    req.data.name = req.body.name;
    req.data.isActive = req.body.isActive;
    req.data.notes = req.body.notes;
    req.data.address = {
      primaryLine: req.body.addressPrimaryLine,
      secondaryLine: req.body.addressSecondaryLine,
      city: req.body.addressCity,
      state: req.body.addressState,
      zip: req.body.addressZip,
    };

    return req.data.save().then(x => undefined);
  }
}

export const updateHandler = new UpdateHandler();
