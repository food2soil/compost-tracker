import { IRequestHandler, ISingleRecordRequest } from "../../request-handler";
import { database } from "../../../../data-model/mongoose.config";
import { IBayModel } from "../../../../data-model/bay/bay.model";
import { IHubPopulatedBaysModel } from "../../../../data-model/hub/hub.model";
import { HandlerError } from "../../../../api/handler.error";

class HubBaysListHandler implements IRequestHandler<void, IBayModel[]> {
  populate(req: ISingleRecordRequest<IHubPopulatedBaysModel>): Promise<ISingleRecordRequest<IHubPopulatedBaysModel>> {
    return database.Hub.findById(req.params.id)
      .populate('bays')
      .then((hub: IHubPopulatedBaysModel) => {
        req.data = hub
        return req
      })
  }

  validate(req: ISingleRecordRequest<IHubPopulatedBaysModel>): Promise<ISingleRecordRequest<IHubPopulatedBaysModel>> {
    if (!req.data) return Promise.reject(new HandlerError(404, 'This hub does not exist'))

    return Promise.resolve(req);
  }

  execute(req: ISingleRecordRequest<IHubPopulatedBaysModel>): Promise<IBayModel[]> {
    return Promise.resolve(req.data.bays)
  }
}

export const hubBaysListHandler = new HubBaysListHandler();
