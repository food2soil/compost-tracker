import { IRequestHandler, ISingleRecordRequest } from "../../request-handler";
import { database } from "../../../../data-model/mongoose.config";
import { IHubPopulatedBaysModel } from "../../../../data-model/hub/hub.model";
import { HandlerError } from "../../../../api/handler.error";
import { IBatchModel } from "../../../../data-model/batch/batch.model";
import * as _ from 'lodash'

class HubActiveBatchesListHandler implements IRequestHandler<void, IBatchModel[]> {
  populate(req: ISingleRecordRequest<IHubPopulatedBaysModel>): Promise<ISingleRecordRequest<IHubPopulatedBaysModel>> {
    return database.Hub.findById(req.params.id)
      .populate('bays')
      .then((hub: IHubPopulatedBaysModel) => {
        req.data = hub
        return req
      })
  }

  validate(req: ISingleRecordRequest<IHubPopulatedBaysModel>): Promise<ISingleRecordRequest<IHubPopulatedBaysModel>> {
    if (!req.data) return Promise.reject(new HandlerError(404, 'This hub does not exist'))

    return Promise.resolve(req);
  }

  execute(req: ISingleRecordRequest<IHubPopulatedBaysModel>): Promise<IBatchModel[]> {
    let ret: IBatchModel[] = []
    req.data.bays.forEach(x => ret = ret.concat(x.activeBatches))

    return Promise.resolve(_.sortBy(ret, 'name'))
  }
}

export const hubActiveBatchesListHandler = new HubActiveBatchesListHandler();
