import { database } from "../../../data-model/mongoose.config";
import { HandlerError } from "../../../api/handler.error";
import { IRequestHandler, ISingleRecordRequest } from "../request-handler";
import { Validation, HubRequests } from "mean-au-ts-shared";
import { ValidationRules } from "aurelia-validation";
import { aureliaValidator } from "../../../lib/validator.lib";
import { IHubModel } from "../../../data-model/hub/hub.model";
import { Types } from "mongoose";

class UpdateTechniciansHandler implements IRequestHandler<HubRequests.UpdateTechnicians, void> {
  populate(req: ISingleRecordRequest<IHubModel, HubRequests.UpdateTechnicians>): Promise<ISingleRecordRequest<IHubModel, HubRequests.UpdateTechnicians>> {
    return database.Hub.findById(req.params.id).then(x => {
      req.data = x;
      return req;
    })
  }

  validate(req: ISingleRecordRequest<IHubModel, HubRequests.UpdateTechnicians>): Promise<ISingleRecordRequest<IHubModel, HubRequests.UpdateTechnicians>> {
    if (!req.data) throw new HandlerError(404, 'This record does not exist');

    Validation.ensureDecoratorsOn(HubRequests.UpdateTechnicians, ValidationRules)
    return aureliaValidator.validateObject(req.body).then(result => req);
  }

  execute(req: ISingleRecordRequest<IHubModel, HubRequests.UpdateTechnicians>): Promise<void> {
    req.data.technicians = req.body.primaryTechnicianIds.map(x => Types.ObjectId(x));

    return req.data.save().then(x => undefined);
  }
}

export const updateTechniciansHandler = new UpdateTechniciansHandler();
