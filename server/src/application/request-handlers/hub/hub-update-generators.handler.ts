import { database } from "../../../data-model/mongoose.config";
import { HandlerError } from "../../../api/handler.error";
import { IRequestHandler, ISingleRecordRequest } from "../request-handler";
import { Validation, HubRequests, Enums, GeneratorShared } from "mean-au-ts-shared";
import { ValidationRules } from "aurelia-validation";
import { aureliaValidator } from "../../../lib/validator.lib";
import { IHubModel } from "../../../data-model/hub/hub.model";
import { Types } from "mongoose";

class UpdateGeneratorsHandler implements IRequestHandler<HubRequests.UpdateGenerators, void> {
  populate(req: ISingleRecordRequest<IHubModel, HubRequests.UpdateGenerators>): Promise<ISingleRecordRequest<IHubModel, HubRequests.UpdateGenerators>> {
    return database.Hub.findById(req.params.id).then(x => {
      req.data = x;
      return req;
    })
  }

  validate(req: ISingleRecordRequest<IHubModel, HubRequests.UpdateGenerators>): Promise<ISingleRecordRequest<IHubModel, HubRequests.UpdateGenerators>> {
    if (!req.data) throw new HandlerError(404, 'This record does not exist');

    Validation.ensureDecoratorsOn(HubRequests.UpdateGenerators, ValidationRules)
    return aureliaValidator.validateObject(req.body).then(result => req);
  }

  execute(req: ISingleRecordRequest<IHubModel, HubRequests.UpdateGenerators>): Promise<void> {
    req.data.generators = req.body.generatorIds.map(x => Types.ObjectId(x));
    
    return database.Generator.find({ _id: { $in: req.data.generators }})
      .then(x => {
        req.data.stats.numResidential = x
          .filter(y => y.generatorType === Enums.GeneratorType.Residential)
          .length

        req.data.stats.numRestaurants = x
          .filter(y => y.generatorType === Enums.GeneratorType.Restaurant)
          .length

        let weeklyCollection = x
          .map(y => GeneratorShared.getWeeklyCollection(y))
          .reduce((a,b) => a + b, 0)
        
        req.data.stats.usagePercentage = weeklyCollection / req.data.capacityGallonsPerWeek

        return req.data.save()
      })
      .then(() => undefined)
  }
}

export const updateGeneratorsHandler = new UpdateGeneratorsHandler();
