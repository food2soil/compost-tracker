import { database } from "../../../../data-model/mongoose.config";
import { HandlerError } from "../../../../api/handler.error";
import { IHubModel, IHub } from "../../../../data-model/hub/hub.model";
import { IRequestHandler, ISingleRecordRequest } from "../../request-handler";

class ReadHandler implements IRequestHandler<void, IHubModel> {
  constructor(
    private populatePaths: (keyof IHub)[] = []
  ) { }

  populate(req: ISingleRecordRequest<IHubModel>): Promise<ISingleRecordRequest<IHubModel>> {
    if (!req.params.id) return Promise.reject(new HandlerError(400, 'No ID was provided'));

    return database.Hub.findById(req.params.id)
      .populate(this.populatePaths.length === 0 ? '' : this.populatePaths.join(' '))
      .populate({ path: 'stats', populate: { path: 'lastWork' }})
      .then(x => {
        req.data = x;
        return req;
      });
  }

  validate(req: ISingleRecordRequest<IHubModel>): Promise<ISingleRecordRequest<IHubModel>> {
    if (!req.data) return Promise.reject(new HandlerError(404, 'This record does not exist'));

    return Promise.resolve(req);
  }

  execute(req: ISingleRecordRequest<IHubModel>): Promise<IHubModel> {
    return Promise.resolve(req.data);
  }
}

export const readHubHandler = new ReadHandler();
export const readHubWithTechniciansHandler = new ReadHandler(['technicians'])
export const readHubWithBaysHandler = new ReadHandler(['bays'])
export const readHubWithGeneratorsHandler = new ReadHandler(['generators'])
export const readHubWithTechniciansAndBaysHandler = new ReadHandler(['technicians', 'bays'])
export const readHubWithTechniciansAndGeneratorsHandler = new ReadHandler(['technicians', 'generators'])
export const readHubWithGeneratorsAndBaysHandler = new ReadHandler(['generators', 'bays'])
export const readHubWithTechniciansBaysAndGeneratorsHandler = new ReadHandler(['technicians', 'bays', 'generators'])
