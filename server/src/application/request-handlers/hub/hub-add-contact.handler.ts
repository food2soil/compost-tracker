import { AddContactHandler } from "../contact/add-contact.handler";
import { database } from "../../../data-model/mongoose.config";

export const addContactHandler = new AddContactHandler(database.Hub);
