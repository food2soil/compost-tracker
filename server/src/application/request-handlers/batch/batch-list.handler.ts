import { BatchRequests } from "mean-au-ts-shared";
import { IAuthenticatedRequest, IRequestHandler } from "../request-handler";
import { findBatches } from "../../queries/batch/batch.queries";
import { IBatchModel } from "../../../data-model/batch/batch.model";

class ListHandler implements IRequestHandler<BatchRequests.List, IBatchModel[]> {
  populate(req: IAuthenticatedRequest<BatchRequests.List>): Promise<IAuthenticatedRequest<BatchRequests.List>> {
    return Promise.resolve(req);
  }
  validate(req: IAuthenticatedRequest<BatchRequests.List>): Promise<IAuthenticatedRequest<BatchRequests.List>> {
    return Promise.resolve(req);
  }
  execute(req: IAuthenticatedRequest<BatchRequests.List>): Promise<IBatchModel[]> {
    return findBatches(req.query)
  }
}

export const listBatchHandler = new ListHandler();
