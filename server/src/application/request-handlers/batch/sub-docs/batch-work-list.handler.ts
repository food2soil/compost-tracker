import { HandlerError } from "../../../../api/handler.error";
import { IRequestHandler, ISingleRecordRequest } from "../../request-handler";
import { findBatchById } from "../../../queries/batch/batch.queries";
import { Types } from "mongoose";
import { IBatchModel } from "../../../../data-model/batch/batch.model";
import { IWorkModel, IWork } from "../../../../data-model/work/work.model";
import { database } from "../../../../data-model/mongoose.config";

class ReadHandler implements IRequestHandler<void, IWorkModel[]> {
  constructor(
    private populatePaths: (keyof IWork)[] = []
  ) { }

  populate(req: ISingleRecordRequest<IBatchModel>): Promise<ISingleRecordRequest<IBatchModel>> {
    if (!req.params.id) return Promise.reject(new HandlerError(400, 'No ID was provided'));

    return findBatchById(Types.ObjectId(req.params.id))
      .then(batch => {
        req.data = batch
        return req
      })
  }

  validate(req: ISingleRecordRequest<IBatchModel>): Promise<ISingleRecordRequest<IBatchModel>> {
    if (!req.data) return Promise.reject(new HandlerError(404, 'This batch does not exist'));

    return Promise.resolve(req);
  }

  execute(req: ISingleRecordRequest<IBatchModel>): Promise<IWorkModel[]> {
    let workIds = req.data.measurements.map(x => x.createdBy)
      .concat(req.data.events.map(x => x.createdBy))

    return database.Work.find({ _id: { $in: workIds }})
      .populate(this.populatePaths.length === 0 ? '' : this.populatePaths.join(' '))
      .then(work => work)
  }
}

export const batchWorkListHandler = new ReadHandler();
export const batchWorkWithTechListHandler = new ReadHandler(['technician']);
