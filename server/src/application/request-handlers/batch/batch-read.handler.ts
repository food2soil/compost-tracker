import { HandlerError } from "../../../api/handler.error";
import { IRequestHandler, ISingleRecordRequest } from "../request-handler";
import { findBatchById } from "../../queries/batch/batch.queries";
import { Types } from "mongoose";
import { IBatchModel } from "../../../data-model/batch/batch.model";

class ReadHandler implements IRequestHandler<void, IBatchModel> {
  populate(req: ISingleRecordRequest<IBatchModel>): Promise<ISingleRecordRequest<IBatchModel>> {
    if (!req.params.id) return Promise.reject(new HandlerError(400, 'No ID was provided'));

    return findBatchById(Types.ObjectId(req.params.id))
      .then(batch => {
        req.data = batch
        return req
      })
  }

  validate(req: ISingleRecordRequest<IBatchModel>): Promise<ISingleRecordRequest<IBatchModel>> {
    if (!req.data) return Promise.reject(new HandlerError(404, 'This batch does not exist'));

    return Promise.resolve(req);
  }

  execute(req: ISingleRecordRequest<IBatchModel>): Promise<IBatchModel> {
    return Promise.resolve(req.data);
  }
}

export const readBatchHandler = new ReadHandler();
