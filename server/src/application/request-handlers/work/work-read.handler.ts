import { database } from "../../../data-model/mongoose.config";
import { HandlerError } from "../../../api/handler.error";
import { IRequestHandler, ISingleRecordRequest } from "../request-handler";
import { IWorkModel, IWork } from "../../../data-model/work/work.model";

class ReadHandler implements IRequestHandler<void, IWorkModel> {
  constructor(
    private populatePaths: (keyof IWork)[] = []
  ) { }

  populate(req: ISingleRecordRequest<IWorkModel>): Promise<ISingleRecordRequest<IWorkModel>> {
    if (!req.params.id) return Promise.reject(new HandlerError(400, 'No ID was provided'));

    let query = {
      _id: req.params.id
    }

    return database.Work.findOne(query)
      .populate(this.populatePaths.length === 0 ? '' : this.populatePaths.join(' '))
      .then(x => {
        req.data = x;
        return req;
      });
  }

  validate(req: ISingleRecordRequest<IWorkModel>): Promise<ISingleRecordRequest<IWorkModel>> {
    if (!req.data) return Promise.reject(new HandlerError(404, 'This record does not exist'));

    return Promise.resolve(req);
  }

  execute(req: ISingleRecordRequest<IWorkModel>): Promise<IWorkModel> {
    return Promise.resolve(req.data);
  }
}

export const readWorkHandler = new ReadHandler();
export const readWorkWithHubHandler = new ReadHandler(['hub']);
export const readWorkWithTechnicianHandler = new ReadHandler(['technician']);
export const readWorkWithHubAndTechnicianHandler = new ReadHandler(['hub', 'technician']);