import { database } from "../../../data-model/mongoose.config";
import { HandlerError } from "../../../api/handler.error";
import { IRequestHandler, ISingleRecordRequest } from "../request-handler";
import { WorkRequests, Validation } from "mean-au-ts-shared";
import { ValidationRules } from "aurelia-validation";
import { aureliaValidator } from "../../../lib/validator.lib";
import { Types } from 'mongoose';
import { IWorkModel } from '../../../data-model/work/work.model';
import { BayEventModel } from "../../../data-model/work/bay-event/bay-event.schema";
import { IBayEvent } from "../../../data-model/work/bay-event/bay-event.model";
import { BayMeasurementModel } from "../../../data-model/work/bay-measurements/bay-measurement.schema";
import { IBayMeasurement } from "../../../data-model/work/bay-measurements/bay-measurement.model";

declare type ValidateExt = (req: ISingleRecordRequest<IWorkModel, WorkRequests.Edit>) => void

class UpdateHandler implements IRequestHandler<WorkRequests.Edit, void> {
  constructor(
    private validateExt: ValidateExt = () => undefined
  ) { }

  populate(req: ISingleRecordRequest<IWorkModel, WorkRequests.Edit>): Promise<ISingleRecordRequest<IWorkModel, WorkRequests.Edit>> {
    return database.Work.findById(req.params.id)
      .then(work => {
        req.data = work
        return req
      })
  }

  validate(req: ISingleRecordRequest<IWorkModel, WorkRequests.Edit>): Promise<ISingleRecordRequest<IWorkModel, WorkRequests.Edit>> {
    if (!req.data) return Promise.reject(new HandlerError(404, 'The work record does not exist'))
    if (req.data.isApproved) return Promise.reject(new HandlerError(422, 'Only unapproved work records can be editted'))

    this.validateExt(req)
    
    Validation.ensureDecoratorsOn(WorkRequests.Edit, ValidationRules)

    return aureliaValidator.validateObject(req.body)
      .then(() => req)
  }

  execute(req: ISingleRecordRequest<IWorkModel, WorkRequests.Edit>): Promise<void> {
    req.data.date = req.body.date
    req.data.timeSpent = req.body.hoursSpent
    req.data.notes = req.body.notes
    req.data.technician = Types.ObjectId(req.body.technicianId)
    req.data.isCarbonNeeded = req.body.isCarbonNeeded

    req.data.bayEvents = req.body.bayEvents
      .map(x => new BayEventModel(<IBayEvent>{
        bay: Types.ObjectId(x.bay),
        bucketsHarvested: x.bucketsHarvested,
        type: x.type,
        isFullHarvest: x.isFullHarvest,
        destination: Types.ObjectId(x.destination)
      }))

    req.data.bayMeasurements = req.body.bayMeasurements
      .map(x => new BayMeasurementModel(<IBayMeasurement>{
        bay: Types.ObjectId(x.bay),
        temperature: x.temperature
      }))

    return req.data.save().then(() => undefined)
  }
}

export const updateHandler = new UpdateHandler();

let validateExt: ValidateExt = (req) => {
  if (req.user.id.toString() !== req.data.technician.toString())
    throw new HandlerError(403, 'Forbidden')
}

export const updateTechnicianRoleHandler = new UpdateHandler(validateExt)