import { database } from "../../../data-model/mongoose.config";
import { HandlerError } from "../../../api/handler.error";
import { IRequestHandler, IAuthenticatedRequest, ISingleRecordRequest } from "../request-handler";
import { WorkRequests, Validation } from "mean-au-ts-shared";
import { ValidationRules } from "aurelia-validation";
import { aureliaValidator } from "../../../lib/validator.lib";
import { IBayEvent } from '../../../data-model/work/bay-event/bay-event.model';
import { Types } from 'mongoose';
import { IWork } from '../../../data-model/work/work.model';
import { IBayMeasurement } from '../../../data-model/work/bay-measurements/bay-measurement.model';
import { getHubMostRecentWork } from "../../queries/work/work.queries";

declare type ValidateExt = (req: IAuthenticatedRequest<WorkRequests.Create>) => void

class CreateHandler implements IRequestHandler<WorkRequests.Create, void> {
  constructor(
    private validateExt: ValidateExt = () => undefined
  ) { }

  populate(req: IAuthenticatedRequest<WorkRequests.Create>): Promise<IAuthenticatedRequest<WorkRequests.Create>> {
    if (!req.body.date) {
      req.body.date = new Date()
    }

    return Promise.resolve(req)
  }

  validate(req: IAuthenticatedRequest<WorkRequests.Create>): Promise<IAuthenticatedRequest<WorkRequests.Create>> {
    Validation.ensureDecoratorsOn(WorkRequests.Create, ValidationRules)

    this.validateExt(req)

    return aureliaValidator.validateObject(req.body)
      .then(() => {
        let workQuery: Partial<IWork> = {
          hub: Types.ObjectId(req.body.hubId),
          isApproved: false
        };

        return database.Work.findOne(workQuery)
      })
      .then(pendingWork => {
        if (pendingWork)
          throw new HandlerError(422, 'This hub has pending work. An admin must approve or delete any pending work on this hub before new work can be logged');

        // Get the last work on the hub
        return getHubMostRecentWork(Types.ObjectId(req.body.hubId))
      })
      .then(lastWork => {
        if (lastWork && req.body.date && lastWork.date > req.body.date)
          throw new HandlerError(422, 'The date of this work record is invalid.  It must be after the hub\'s current last work record.');

        return req
      })
  }

  execute(req: IAuthenticatedRequest<WorkRequests.Create>): Promise<void> {
    let bayEvents: IBayEvent[] = req.body.bayEvents.map(x => {
      return <IBayEvent>{
        bay: Types.ObjectId(x.bay),
        destination: x.destination ? Types.ObjectId(x.destination) : null,
        type: x.type,
        bucketsHarvested: x.bucketsHarvested,
        isFullHarvest: x.isFullHarvest
      }
    })

    let bayMeasurements: IBayMeasurement[] = req.body.bayMeasurements
      .filter(x => x.temperature)
      .map(x => {
        return <IBayMeasurement>{
          bay: Types.ObjectId(x.bay),
          temperature: x.temperature,
        }
      })

    let init: Partial<IWork> = {
      hub: Types.ObjectId(req.body.hubId),
      bayEvents: bayEvents,
      bayMeasurements: bayMeasurements,
      timeSpent: req.body.hoursSpent,
      technician: Types.ObjectId(req.body.technicianId),
      notes: req.body.notes,
      date: new Date(req.body.date) || new Date(),
      isCarbonNeeded: req.body.isCarbonNeeded
    };

    let work = new database.Work(init);
    return work.save()
      .then(() => database.Hub.findById(Types.ObjectId(req.body.hubId)))
      .then(hub => {
        hub.stats.lastWork = work.id
        hub.stats.needsCarbon = work.isCarbonNeeded
        return hub.save()
      })
      .then(() => undefined)
  }
}

export const createHandler = new CreateHandler();

let validateExt: ValidateExt = (req) => {
  if (req.user.id.toString() !== req.body.technicianId.toString())
    throw new HandlerError(403, 'Forbidden')
}

export const createTechnicianRoleHandler = new CreateHandler(validateExt)
