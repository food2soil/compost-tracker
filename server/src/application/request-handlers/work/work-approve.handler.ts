import { database } from "../../../data-model/mongoose.config";
import { HandlerError } from "../../../api/handler.error";
import { IRequestHandler, ISingleRecordRequest } from "../request-handler";
import { IApproveWorkModel } from "../../../data-model/work/work.model";

class ApproveHandler implements IRequestHandler<void, void> {
  populate(req: ISingleRecordRequest<IApproveWorkModel>): Promise<ISingleRecordRequest<IApproveWorkModel>> {
    return database.Work.findById(req.params.id)
      .populate({
        path: 'hub',
        populate: {
          path: 'bays',
          model: 'Bay'
        }
      })
      .then((x: IApproveWorkModel) => {
        req.data = x
        return req
      })
  }

  validate(req: ISingleRecordRequest<IApproveWorkModel>): Promise<ISingleRecordRequest<IApproveWorkModel>> {
    if (!req.data) return Promise.reject(new HandlerError(404, 'This record does not exist'));

    return Promise.resolve(req)
  }

  execute(req: ISingleRecordRequest<IApproveWorkModel>): Promise<void> {
    return req.data.validateAndApproveWork(req.data)
      .catch((err: Error) => {
        throw new HandlerError(422, err.message)
      })
  }
}

export const approveWorkHandler = new ApproveHandler();
