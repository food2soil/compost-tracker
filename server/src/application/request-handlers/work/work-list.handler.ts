import { database } from "../../../data-model/mongoose.config";
import { WorkRequests } from "mean-au-ts-shared";
import { IRequestHandler, IAuthenticatedRequest } from "../request-handler";
import { IWork, IWorkModel } from "../../../data-model/work/work.model";
import { Types, Query } from "mongoose";
import { config } from "../../../config/config";

class ListWorkHandler implements IRequestHandler<WorkRequests.List, IWorkModel[]> {
  constructor(
    private populatePaths: (keyof IWork)[] = []
  ) { }

  populate(req: IAuthenticatedRequest<WorkRequests.List>): Promise<IAuthenticatedRequest<WorkRequests.List>> {
    return Promise.resolve(req);
  }

  validate(req: IAuthenticatedRequest<WorkRequests.List>): Promise<IAuthenticatedRequest<WorkRequests.List>> {
    return Promise.resolve(req);
  }

  execute(req: IAuthenticatedRequest<WorkRequests.List>): Promise<IWorkModel[]> {
    let query: any = {
      date: {
        $gte: req.query.from ? req.query.from.valueOf() : 0,
        $lte: req.query.to ? req.query.to.valueOf() : new Date(8640000000000000).valueOf()
      }
    }

    if (req.query.hub) {
      query.hub = Types.ObjectId(req.query.hub)
    }

    if (req.query.technician) {
      query.technician = Types.ObjectId(req.query.technician)
    }

    if (req.query.isApproved != null) {
      query.isApproved = req.query.isApproved
    }

    let sort: Partial<IWork> = req.query.sortBy ? { } : null
    if (sort) {
      sort[req.query.sortBy] = req.query.sortDir
    }

    let limit = req.query.limit == null
      ? config.express.maxResponseLength
      : Number(req.query.limit)

    return database.Work.find(query)
      .populate(this.populatePaths.length === 0 ? '' : this.populatePaths.join(' '))
      .sort(sort)
      .limit(limit)
      .then(work => {
        return work
      })
  }
}

export const listWorkHandler = new ListWorkHandler();
export const listWorkWithTechnicianHandler = new ListWorkHandler(['technician']);
export const listWorkWithHubHandler = new ListWorkHandler(['hub']);
export const listWorkWithHubTechnicianHandler = new ListWorkHandler(['hub', 'technician']);
