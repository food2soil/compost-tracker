import { HandlerError } from "../../../api/handler.error";
import { IRequestHandler, ISingleRecordRequest } from "../request-handler";
import { IWorkModel } from "../../../data-model/work/work.model";
import { database } from "../../../data-model/mongoose.config";

declare type ValidateExt = (req: ISingleRecordRequest<IWorkModel>) => void

class DeleteHandler implements IRequestHandler<void, void> {
  constructor(
    private validateExt: ValidateExt = () => undefined
  ) { }

  populate(req: ISingleRecordRequest<IWorkModel>): Promise<ISingleRecordRequest<IWorkModel>> {
    return database.Work.findById(req.params.id).then(x => {
      req.data = x;
      return req;
    });
  }

  validate(req: ISingleRecordRequest<IWorkModel>): Promise<ISingleRecordRequest<IWorkModel>> {
    if (!req.data) throw new HandlerError(404, 'This record does not exist');
    if (req.data.isApproved) throw new HandlerError(422, 'Cannot delete an approved work record.  Undo the approval first')

    this.validateExt(req)

    return Promise.resolve(req);
  }

  execute(req: ISingleRecordRequest<IWorkModel>): Promise<void> {
    return req.data.remove().then(x => undefined);
  }
}

export const deleteHandler = new DeleteHandler()

let validateExt: ValidateExt = (req) => {
  if (req.user.id.toString() !== req.data.technician.toString())
    throw new HandlerError(403, 'Forbidden')
}

export const deleteTechnicianRoleHandler = new DeleteHandler(validateExt)
