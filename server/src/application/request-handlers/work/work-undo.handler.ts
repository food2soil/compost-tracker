import { database } from "../../../data-model/mongoose.config";
import { HandlerError } from "../../../api/handler.error";
import { IRequestHandler, ISingleRecordRequest } from "../request-handler";
import { IUndoWorkModel } from "../../../data-model/work/work.model";
import { validateAndUndoWork } from "../../../data-model/work/ext/undo-work.ext";

class UndoHandler implements IRequestHandler<void, void> {
  populate(req: ISingleRecordRequest<IUndoWorkModel>): Promise<ISingleRecordRequest<IUndoWorkModel>> {
    return database.Work.findById(req.params.id)
      .populate({
        path: 'hub',
        populate: {
          path: 'bays',
          model: 'Bay'
        }
      })
      .then((x: IUndoWorkModel) => {
        req.data = x
        return req
      })
  }

  validate(req: ISingleRecordRequest<IUndoWorkModel>): Promise<ISingleRecordRequest<IUndoWorkModel>> {
    if (!req.data) return Promise.reject(new HandlerError(404, 'This record does not exist'));

    return Promise.resolve(req)
  }

  execute(req: ISingleRecordRequest<IUndoWorkModel>): Promise<void> {

    return validateAndUndoWork(req.data)
      .catch((err: Error) => {
        throw new HandlerError(422, err.message)
      })
  }
}

export const undoWorkHandler = new UndoHandler();
