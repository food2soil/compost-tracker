import { HandlerError } from "../../../api/handler.error";
import { IRequestHandler, ISingleRecordRequest } from "../request-handler";
import { Document, Model } from "mongoose";

export class DeleteHandler<TModel extends Document> implements IRequestHandler<void, void> {
  constructor(private model: Model<TModel>) { }

  populate(req: ISingleRecordRequest<TModel>): Promise<ISingleRecordRequest<TModel>> {
    return this.model.findById(req.params.id).then(x => {
      req.data = x;
      return req;
    });
  }

  validate(req: ISingleRecordRequest<TModel>): Promise<ISingleRecordRequest<TModel>> {
    if (!req.data) throw new HandlerError(404, 'This record does not exist');

    return Promise.resolve(req);
  }

  execute(req: ISingleRecordRequest<TModel>): Promise<void> {
    return req.data.remove().then(x => undefined);
  }
}
