import { IRequestHandler, IAuthenticatedRequest } from "../request-handler";
import { Document, Model } from "mongoose";

export class ListHandler<TRequestQuery, TModel extends Document> implements IRequestHandler<TRequestQuery, TModel[]> {
  constructor(private model: Model<TModel>) { }

  populate(req: IAuthenticatedRequest<TRequestQuery>): Promise<IAuthenticatedRequest<TRequestQuery>> {
    return Promise.resolve(req);
  }

  validate(req: IAuthenticatedRequest<TRequestQuery>): Promise<IAuthenticatedRequest<TRequestQuery>> {
    return Promise.resolve(req);
  }

  execute(req: IAuthenticatedRequest<TRequestQuery>): Promise<TModel[]> {
    return this.model.find(req.query || {}).then(x => x);
  }
}
