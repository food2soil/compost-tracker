import { HandlerError } from "../../../api/handler.error";
import { IRequestHandler, ISingleRecordRequest } from "../request-handler";
import { Document, Model } from "mongoose";

export class ReadHandler<TModel extends Document> implements IRequestHandler<void, TModel> {
  constructor(private model: Model<TModel>) { }

  populate(req: ISingleRecordRequest<TModel>): Promise<ISingleRecordRequest<TModel>> {
    if (!req.params.id) return Promise.reject(new HandlerError(400, 'No ID was provided'));

    return this.model.findById(req.params.id).then(x => {
      req.data = x;
      return req;
    });
  }

  validate(req: ISingleRecordRequest<TModel>): Promise<ISingleRecordRequest<TModel>> {
    if (!req.data) return Promise.reject(new HandlerError(404, 'This record does not exist'));

    return Promise.resolve(req);
  }

  execute(req: ISingleRecordRequest<TModel>): Promise<TModel> {
    return Promise.resolve(req.data);
  }
}
