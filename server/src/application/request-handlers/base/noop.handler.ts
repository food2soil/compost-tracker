import { IRequestHandler, IAuthenticatedRequest } from "../request-handler";

class NoopHandler implements IRequestHandler<any, any> {
  populate(req: IAuthenticatedRequest<any>): Promise<IAuthenticatedRequest<any>> {
    return Promise.resolve(req)
  }

  validate(req: IAuthenticatedRequest<any>): Promise<IAuthenticatedRequest<any>> {
    return Promise.resolve(req)
  }

  execute(req: IAuthenticatedRequest<any>): Promise<any> {
    return Promise.resolve()
  }
}

export const noopHandler = new NoopHandler();
