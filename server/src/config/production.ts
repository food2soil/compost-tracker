import { DefaultConfig } from "../config/default";

export class ProductionConfig extends DefaultConfig {
  constructor() {
    super();

    // PRODUCTION CONFIG OVERRIDES
    this.express.host = process.env.HOST
    this.express.port = Number(process.env.PORT)
    
    this.mongo.db = process.env.SCRAPWORKS_MONGODB_DB
    this.mongo.uri = process.env.MONGODB_URI;
    this.mongo.debug = false;

    this.auth.domain = process.env.FULL_DOMAIN;

    this.logger.level = 'info';
  }
}
