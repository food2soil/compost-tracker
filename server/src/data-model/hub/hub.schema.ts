import { model, Schema } from "mongoose";
import { IHubModel } from "./hub.model";
import { ContactModel } from "../shared/contact/contact.schema";
import { AddressModel } from "../shared/address/address.schema";
import { validateOneBayHook } from "./hooks/validate-one-bay.hook";
import { validateOneContactHook } from "../shared/hooks/validate-one-contact.hook";
import { getDefaultBays } from "./ext/default-bays.ext";
import { BatchModel } from "../batch/batch.schema";
import { HubStatsModel } from "../stats/hub/hub-stats.schema";

let HubSchema = new Schema({
  created: {
    type: Date,
    default: Date.now
  },
  name: {
    type: String,
    trim: true,
    required: 'Name cannot be blank'
  },
  bays: [{
    type: Schema.Types.ObjectId,
    ref: 'Bay'
  }],
  contacts: [ContactModel.schema],
  address: AddressModel.schema,
  technicians: [{
    type: Schema.Types.ObjectId,
    ref: 'User'
  }],
  generators: [{
    type: Schema.Types.ObjectId,
    ref: 'Generator'
  }],
  isActive: {
    type: Boolean,
    default: true
  },
  isCollectionPoint: {
    type: Boolean,
  },
  isComposting: {
    type: Boolean,
  },
  notes: {
    type: String
  },
  capacityGallonsPerWeek: {
    type: Number
  },
  finishedBatches: [BatchModel.schema],
  isPrivate: {
    type: Boolean,
    default: false
  },
  stats: HubStatsModel.schema
});

HubSchema.pre('validate', validateOneContactHook);
HubSchema.pre('validate', validateOneBayHook);

export const HubModel = model<IHubModel>('Hub', HubSchema);
