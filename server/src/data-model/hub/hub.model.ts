import * as mongoose from "mongoose";
import { IContact, IContactsModel, IContactModel } from "../shared/contact/contact.model";
import { IAddress } from "../shared/address/address.model";
import { IGenerator, IGeneratorModel } from "../generator/generator.model";
import { IUser, IUserModel } from "../user/user.model";
import { IBatch, IBatchModel } from "../batch/batch.model";
import { IWorkModel, IWork } from "../work/work.model";
import { IBayModel, IBay } from "../bay/bay.model";
import { IHubStats } from "../stats/hub/hub-stats.model";

export interface IHub {
  id?: mongoose.Types.ObjectId
  created: Date
  name: string
  bays: mongoose.Types.ObjectId[] | IBay[]
  contacts: IContact[]
  address: IAddress
  technicians: mongoose.Types.ObjectId[] | IUser[]
  generators: mongoose.Types.ObjectId[] | IGenerator[]
  isActive: boolean
  isCollectionPoint: boolean
  isComposting: boolean
  notes: string
  capacityGallonsPerWeek: number
  finishedBatches: IBatch[]
  isPrivate: boolean
  stats: IHubStats
}

export interface IHubModel extends IHub, IContactsModel {
  id: mongoose.Types.ObjectId
  bays: mongoose.Types.ObjectId[] | IBayModel[]
  contacts: IContactModel[]
  workRecords: IWorkModel[]
  technicians: mongoose.Types.ObjectId[] | IUserModel[]
  generators: mongoose.Types.ObjectId[] | IGeneratorModel[]
  finishedBatches: IBatchModel[]
}

export interface IHubPopulatedBaysModel extends IHubModel {
  bays: IBayModel[]
}
