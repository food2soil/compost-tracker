import { HookNextFunction } from "mongoose";
import { IHubModel } from "../hub.model";

export function validateOneBayHook(next: HookNextFunction) {
  var self: IHubModel = this;

  if (self.bays.length === 0) {
    return next(new Error('At least one bay is required.'));
  }

  return next();
}
