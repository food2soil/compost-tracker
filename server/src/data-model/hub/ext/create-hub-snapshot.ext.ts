import { IBatchModel } from "../../batch/batch.model";
import { database } from "../../mongoose.config";
import { IHubPopulatedBaysModel } from "../hub.model";
import { IBayModel } from "../../bay/bay.model";

export interface IHubSnapshot {
  bays: IBayModel[]
  finishedBatches: IBatchModel[]
}

export function createHubSnapshot(hubId: string): Promise<IHubSnapshot> {
  return database.Hub.findById(hubId)
    .populate('bays')
    .then(hub => {
      return {
        bays: (<IHubPopulatedBaysModel>hub).bays.slice(),
        finishedBatches: hub.finishedBatches.slice()
      }
    })
}
