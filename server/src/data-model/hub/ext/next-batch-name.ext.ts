import { IHubPopulatedBaysModel } from "../hub.model";
import { getLastBatch } from "./get-last-batch.ext";

export function nextBatchName(hub: IHubPopulatedBaysModel): string {
  let lastBatch = getLastBatch(hub);

  let nextBatchNameAlpha = lastBatch ? String.fromCharCode(lastBatch.name.charCodeAt(0) + 1) : 'A';

  let lastBatchNameYear = lastBatch ? lastBatch.name.substring(1) : null;
  let nextBatchNameYear = new Date().getUTCFullYear().toString().substr(2, 2);

  if (nextBatchNameAlpha === '[' || lastBatchNameYear !== nextBatchNameYear) {
    // Loop back to A after we pass Z or if it's a new year
    nextBatchNameAlpha = 'A';
  }

  return nextBatchNameAlpha + nextBatchNameYear;
}
