import { IHubModel } from "../hub.model";
import { IBayModel } from "../../bay/bay.model";
import { BayModel } from "../../bay/bay.schema";
import { Types } from "mongoose";

export function getDefaultBays(): IBayModel[] {
  // Holding Bays
  let holdingOne: IBayModel = new BayModel({
    _id: new Types.ObjectId(),
    name: 'H1',
    isStart: false,
    destinations: [],
    isHolding: true,
    displayOrder: 0
  })
  let holdingTwo: IBayModel = new BayModel({
    _id: new Types.ObjectId(),
    name: 'H2',
    isStart: false,
    destinations: [],
    isHolding: true,
    displayOrder: 0
  })
  let holdingThree: IBayModel = new BayModel({
    _id: new Types.ObjectId(),
    name: 'H3',
    isStart: false,
    destinations: [],
    isHolding: true,
    displayOrder: 5
  })
  let holdingFour: IBayModel = new BayModel({
    _id: new Types.ObjectId(),
    name: 'H4',
    isStart: false,
    destinations: [],
    isHolding: true,
    displayOrder: 5
  })

  // Active Bays
  let activeOne: IBayModel = new BayModel({
    name: '1',
    destinations: [holdingOne._id, holdingTwo._id],
    isStart: false,
    displayOrder: 1
  })
  let activeTwo: IBayModel = new BayModel({
    name: '2',
    destinations: [activeOne._id],
    isStart: true,
    displayOrder: 2
  })
  let activeFour: IBayModel = new BayModel({
    name: '4',
    destinations: [holdingThree._id, holdingFour._id],
    displayOrder: 4
  })
  let activeThree: IBayModel = new BayModel({
    name: '3',
    destinations: [activeFour._id],
    isStart: true,
    displayOrder: 3
  })

  return [holdingOne, holdingTwo, holdingThree, holdingFour, activeOne, activeTwo, activeThree, activeFour]
};
