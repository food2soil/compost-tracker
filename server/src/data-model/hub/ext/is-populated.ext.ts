import { IHub } from "../hub.model";
import { Types } from "mongoose";

export function isHubBaysPopulated(hub: IHub) {
  return hub.bays.length > 0 && hub.bays[0].constructor !== Types.ObjectId
}
