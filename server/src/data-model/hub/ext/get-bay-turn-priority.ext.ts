import { IHubPopulatedBaysModel } from "../hub.model";
import { Types } from "mongoose";
import { IBayModel } from "../../bay/bay.model";

function longestPathToHarvest(
  hub: IHubPopulatedBaysModel,
  bay: IBayModel,
  numDestinations: number = 0
): number {
  if (!bay.destinations || bay.destinations.length === 0) return numDestinations

  let longestPath = numDestinations
  for (let i = 0; i < bay.destinations.length; i++) {
    let dest = bay.destinations[i];
    let destBay = hub.bays.find(x => x.id.toString() === dest)

    let newPath = longestPathToHarvest(hub, destBay, longestPath + 1)
    longestPath = longestPath < newPath
      ? newPath
      : longestPath
  }

  return longestPath
}

/**
 * Returns a number that indicates the turn priority for a bay;
 * The lower the number, the higher the priority
 * Bays that are close to harvest bays have higher priority
 *
 * ex - You have three bays 1 -> 2 -> 3 -> Harvest
 * For an action that turns all three bays, the turn order would be
 * 3, 2, and finally 1.
 *
 * @param hub The hub containing the bay
 * @param bayId The id of the bay to get turn priority for
 */
export function getBayTurnPriority(hub: IHubPopulatedBaysModel, bayId: Types.ObjectId): number {
  let bayToTurn = hub.bays.find(x => x.id.toString() === bayId.toString())
  if (!bayToTurn) throw new Error('The bay does not exist on the provided hub')

  return longestPathToHarvest(hub, bayToTurn)
}
