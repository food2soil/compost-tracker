import { IHubPopulatedBaysModel } from "../hub.model";
import { IBatch } from "../../batch/batch.model";

export function getLastBatch(hub: IHubPopulatedBaysModel): IBatch {
  let activeBatches = hub.finishedBatches.slice()
  hub.bays.forEach(x => {
    activeBatches = activeBatches.concat(x.activeBatches.slice())
  })

  if (activeBatches.length === 0) return null
  if (activeBatches.length === 1) return activeBatches[0]

  let highestSwappedName = ''
  activeBatches.forEach(x => {
    let swappedName = x.name.substring(1) + x.name.substring(0, 1)
    highestSwappedName = swappedName > highestSwappedName
      ? swappedName
      : highestSwappedName
  })

  return activeBatches.find(x =>
    x.name.substring(1) + x.name.substring(0, 1) === highestSwappedName)
}
