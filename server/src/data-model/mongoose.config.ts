import * as mongoose from 'mongoose'
import { Db } from 'mongodb';
import * as path from 'path'
import * as childProcess from 'child_process'
import * as fs from 'fs'
import { IGeneratorModel } from './generator/generator.model';
import { GeneratorModel } from './generator/generator.schema';
import { HubModel } from './hub/hub.schema';
import { IHubModel } from './hub/hub.model';
import { IInvitationModel } from './invitations/invitation.model';
import { InvitationModel } from './invitations/invitation.schema';
import { IWorkModel } from './work/work.model';
import { WorkModel } from './work/work.schema';
import { IBayModel } from './bay/bay.model';
import { BayModel } from './bay/bay.schema';
import { ITechnicianStatsModel } from './stats/technician/technician-stats.model';
import { TechnicianStatsModel } from './stats/technician/technician-stats.schema';
import { HubStatsModel } from './stats/hub/hub-stats.schema';
import { IHubStatsModel } from './stats/hub/hub-stats.model';
import { BatchStatsModel } from './stats/batch/batch-stats.schema';
import { IBatchStatsModel } from './stats/batch/batch-stats.model';
import { rebuildHubStats } from './stats/hub/ext/rebuild-hub-stats.ext';
import { rebuildBatchStats } from './stats/batch/ext/rebuild-batch-stats.ext';
import { rebuildTechnicianStats } from './stats/technician/ext/rebuild-technician-stats.ext';
import { IUserModel } from './user/user.model';
import { UserModel } from './user/user.schema';
import { config } from '../config/config';
import { logger } from '../lib/winston.lib';

export interface IModelHelper<TInit, TModel extends mongoose.Document> {
  model: mongoose.Model<TModel>
  build: (init: TInit) => TModel
}

class Mongoose {
  db: Db

  get BatchStats(): mongoose.Model<IBatchStatsModel> {
    return BatchStatsModel
  }

  get Bay(): mongoose.Model<IBayModel> {
    return BayModel;
  }

  get Generator(): mongoose.Model<IGeneratorModel> {
    return GeneratorModel;
  }

  get Hub(): mongoose.Model<IHubModel> {
    return HubModel;
  }

  get HubStats(): mongoose.Model<IHubStatsModel> {
    return HubStatsModel
  }

  get Invitation(): mongoose.Model<IInvitationModel> {
    return InvitationModel;
  }

  get TechnicianStats(): mongoose.Model<ITechnicianStatsModel> {
    return TechnicianStatsModel
  }

  get User(): mongoose.Model<IUserModel> {
    return UserModel;
  }

  get Work(): mongoose.Model<IWorkModel> {
    return WorkModel;
  }

  loadModel<TModel extends mongoose.Document>(name: string, Schema: mongoose.Schema) {
    mongoose.model<TModel>(name, Schema);
  };

  connect(): Promise<any> {
    return new Promise(resolve => {
      return mongoose.connect(
        config.mongo.uri,
        config.mongo.options,
        (err) => {
          if (err) {
            logger.error('Could not connect to MongoDB!');
            logger.error(err.message);
            throw err;
          }

          mongoose.set('debug', config.mongo.debug);
          this.db = mongoose.connection.db;

          logger.info(`Connected to MongoDB at: ${config.mongo.uri}`)
          resolve();
        })
    })
  };

  disconnect(): Promise<void> {
    return mongoose.disconnect().then(() => {
      logger.info('Disconnected from MongoDB');
    })
  }

  seed(): Promise<void> {
    return new Promise((resolve) => {
      if (!config.mongo.seed) return resolve();

      let db = config.mongo.db;
  
      // drop the current db
      logger.debug(`MongoDB - Dropping database: ${db}`)
      childProcess.execSync(`mongo ${db} --eval "db.dropDatabase()"`);
  
      // seed it
      fs.readdirSync(path.join(process.cwd(), 'seed', )).forEach((f) => {
        let collection = f.split('.')[0];
        let command = `mongoimport -d ${db} -c ${collection} --file seed/${f}`
        logger.debug(`MongoDB - Seeding: ${db}/${collection}`)
        childProcess.execSync(command);

        return resolve()
      });  
    });
  }

  buildStats(): Promise<void> {
    return rebuildTechnicianStats()
      .then(() => rebuildHubStats())
      .then(() => rebuildBatchStats())
  }
}

export const database = new Mongoose()
