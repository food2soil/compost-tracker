import * as mongoose from "mongoose";
import { Enums } from "mean-au-ts-shared";

export interface IBayEvent {
  id?: mongoose.Types.ObjectId
  type: Enums.BayEvent
  bay: mongoose.Types.ObjectId
  destination: mongoose.Types.ObjectId
  bucketsHarvested: number
  isFullHarvest: boolean
}

export interface IBayEventModel extends IBayEvent, mongoose.Document {
  id: mongoose.Types.ObjectId
}
