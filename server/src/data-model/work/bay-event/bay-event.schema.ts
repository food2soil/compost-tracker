import { model, Schema } from "mongoose";
import { IBayEventModel } from "./bay-event.model";
import { Enums } from "mean-au-ts-shared";

var BayEventSchema = new Schema({
  type: {
    type: String,
    enum: [Enums.BayEvent.Fill, Enums.BayEvent.Turn, Enums.BayEvent.Harvest],
    required: 'Bay event must have a type.'
  },
  bay: {
    type: Schema.Types.ObjectId,
    ref: 'Bay'
  },
  destination: {
    type: Schema.Types.ObjectId,
    ref: 'Bay'
  },
  bucketsHarvested: {
    type: Number
  },
  isFullHarvest: {
    type: Boolean
  }
});

export const BayEventModel = model<IBayEventModel>('BayEvent', BayEventSchema);
