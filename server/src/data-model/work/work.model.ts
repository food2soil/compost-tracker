import * as mongoose from "mongoose";
import { IBayEvent, IBayEventModel } from "./bay-event/bay-event.model";
import { IBatch, IBatchModel } from "../batch/batch.model";
import { IHub, IHubPopulatedBaysModel, IHubModel } from "../hub/hub.model";
import { IBayMeasurementModel, IBayMeasurement } from "./bay-measurements/bay-measurement.model";
import { IBay, IBayModel } from "../bay/bay.model";
import { IUser, IUserModel } from "../user/user.model";

export interface IWorkExt {
}

export interface IWork {
  id?: mongoose.Types.ObjectId
  date: Date
  bayEvents: IBayEvent[]
  bayMeasurements: IBayMeasurement[]
  timeSpent: number
  notes?: string
  technician: mongoose.Types.ObjectId | IUser
  hub: mongoose.Types.ObjectId | IHub
  isApproved: boolean
  undoBays: IBay[]
  undoFinishedBatches: IBatch[]
  isCarbonNeeded: boolean
}

export interface IWorkModel extends IWork, IWorkExt, mongoose.Document {
  id: mongoose.Types.ObjectId
  technician: mongoose.Types.ObjectId | IUserModel
  hub: mongoose.Types.ObjectId | IHubModel
  bayEvents: IBayEventModel[]
  bayMeasurements: IBayMeasurementModel[]
  undoBays: IBayModel[]
  undoFinishedBatches: IBatchModel[]
}

export interface IApproveWorkExt {
  validateAndApproveWork: (work: IApproveWorkModel) => Promise<void>
}

export interface IApproveWorkModel extends IWorkModel, IApproveWorkExt {
  hub: IHubPopulatedBaysModel
}

export interface IUndoWorkModel extends IWorkModel {
  hub: IHubPopulatedBaysModel
}
