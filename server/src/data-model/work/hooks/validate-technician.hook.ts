import { HookNextFunction } from "mongoose";
import { IWorkModel } from "../work.model";
import { database } from "../../mongoose.config";
import { HandlerError } from "../../../api/handler.error";

export function validateTechnicianHook(next: HookNextFunction) {
  var self: IWorkModel = this;

  database.User.findById(self.technician)
    .then(user => {
      var error;

      if (!user) error = new HandlerError(422, 'The connected technician does not exist')
      return next(error);
    })
}
