import { HookNextFunction, Types } from "mongoose";
import { IWorkModel } from "../work.model";
import { database } from "../../mongoose.config";
import { HandlerError } from "../../../api/handler.error";
import { isHubBaysPopulated } from "../../hub/ext/is-populated.ext";
import { Enums } from "mean-au-ts-shared";
import { IBay } from "../../bay/bay.model";

export function validateHubHook(next: HookNextFunction) {
  var self: IWorkModel = this;

  database.Hub.findById(self.hub)
    .then(hub => {
      if (!hub) return next(new HandlerError(422, 'The connected hub does not exist'))

      // Bay Events are invalid if there is some event whos bay is not on the hub
      let bayEventInvalid = self.bayEvents.some(event => {
        if (isHubBaysPopulated(hub)) {
          return !(<IBay[]>hub.bays).some(hubBay => {
            return event.bay.toString() === hubBay.id.toString()
          })
        } else {
          return !(<Types.ObjectId[]>hub.bays).some(hubBay => {
            return event.bay.toString() === hubBay.toString()
          })
        }
      })


      if (bayEventInvalid)
        return next(new HandlerError(422, 'A bay event was specified for a bay that does not exist on the hub'))

      let missingDestination = self.bayEvents
        .filter(x => x.type === Enums.BayEvent.Turn)
        .some(x => !x.destination)
      if (missingDestination)
        return next(new HandlerError(422, 'A turn bay event was created without a destination'))

      // Bay Events are invalid if there is some turn event whos destination is not on the hub
      let bayEventDestinationInvalid = self.bayEvents
        .filter(x => x.type === Enums.BayEvent.Turn)
        .some(event => {
          if (isHubBaysPopulated(hub)) {
            return !(<IBay[]>hub.bays).some(hubBay => {
              return event.destination.toString() === hubBay.id.toString()
            })
          } else {
            return !(<Types.ObjectId[]>hub.bays).some(hubBay => {
              return event.destination.toString() === hubBay.toString()
            })
          }
        })


      if (bayEventDestinationInvalid)
        return next(new HandlerError(422, 'A turn bay event was specified with a destination that does not exist on the hub'))

      // Bay measurements are invalid if there is some measurement whos bay is not on the hub
      let bayMeasurementInvalid = self.bayMeasurements.some(m => {
        if (isHubBaysPopulated(hub)) {
          return !(<IBay[]>hub.bays).some(hubBay => {
            return m.bay.toString() === hubBay.id.toString()
          })
        } else {
          return !(<Types.ObjectId[]>hub.bays).some(hubBay => {
            return m.bay.toString() === hubBay.toString()
          })
        }
      })

      if (bayMeasurementInvalid)
        return next(new HandlerError(422, 'A bay measurement was specified for a bay that does not exist on the hub'))

      return next()
    })
}
