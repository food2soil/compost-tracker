import { database } from "../../../mongoose.config";
import { logger } from "../../../../lib/winston.lib";
import { Enums } from "mean-au-ts-shared";
import { IApproveWorkModel } from "../../work.model";
import { createHubSnapshot } from "../../../hub/ext/create-hub-snapshot.ext";
import { validateAndFillBay } from "./fill-bay.ext";
import { validateAndTurnBay } from "./turn-bay.ext";
import { validateAndProcessMeasurements } from "./process-measurements.ext";
import { getBayTurnPriority } from "../../../hub/ext/get-bay-turn-priority.ext";
import { validateAndHarvestBay } from "./harvest-bay.ext";
import { getMonthlyStatsMonthKey, getMonthlyStatsYearKey } from "../../../stats/ext/get-monthly-stat-keys.ext";

function canApproveWork(work: IApproveWorkModel): Promise<void> {
    if (work.isApproved) {
      return Promise.reject(new Error('This work record is already approved'));
    }

    // Error if another work is pending for this hub with an earlier date
    return database.Work.find({
      hub: work.hub,
      isApproved: false,
      date: { '$lt': work.date }
    }).then(x => {
      if (x.length === 0) return Promise.resolve()

      return Promise.reject(
        new Error('There is another work record that must be approved before this one'))
    })
}

function approveWork(work: IApproveWorkModel): Promise<void> {
  return new Promise((resolve, reject) => {
    logger.info(`Work ${work.id}: Approval started`)

    // create the undo snapshot of all active bays
    return createHubSnapshot(work.hub.id.toString())
      .then(snapshot => {
        // Set the snapshot
        work.undoBays = snapshot.bays
        work.undoFinishedBatches = snapshot.finishedBatches

        // Save the work here so that we have the snapshot to undo in case of error
        return work.save()
      })
      .then(() => {
        // Process Measurements
        return validateAndProcessMeasurements(work)
      })
      .then(() => {
        // Process harvests
        let harvests = work.bayEvents
          .filter(x => x.type === Enums.BayEvent.Harvest)
          .reduce((promiseChain, x) => {
            return promiseChain.then(() => validateAndHarvestBay(work, x))
          }, Promise.resolve())
      })
      .then(() => {
        // Sort bays by turn priority and then process turns sequentially
        return work.bayEvents
          .map(event => {
            let bay = work.hub.bays.find(x => x.id.toString() === event.bay.toString())

            return {
              bayId: event.bay,
              bayName: bay.name,
              turnPriority: getBayTurnPriority(work.hub, event.bay),
              event: event
            }
          })
          // Remove any bays that dont have a turn event
          .filter(x => x.event && x.event.type === Enums.BayEvent.Turn)
          // Sort by turn priority
          .sort((a, b) => a.turnPriority - b.turnPriority)
          // Turn each ordered bay in series
          .reduce((promiseChain, x) => {
            return promiseChain.then(() => validateAndTurnBay(work, x.event))
          }, Promise.resolve())
      })
      .then(() => {
        // Process fills sequentially (order doesnt matter)
        return work.bayEvents
          .filter(x => x.type === Enums.BayEvent.Fill)
          .reduce((promiseChain, x) => {
            return promiseChain.then(() => validateAndFillBay(work, x))
          }, Promise.resolve())
      })
      .then(() => {
        return database.User.findById(work.technician)
      })
      .then(user => {
        work.isApproved = true;

        // Update hub stats
        work.hub.stats.needsCarbon = work.isCarbonNeeded

        // Update technician stats
        user.stats.hoursAllTime += work.timeSpent
        let bucketsHarvested = work.bayEvents
          .map(x => x.bucketsHarvested)
          .reduce((a,b) => a + b, 0)
        user.stats.bucketsHarvestedAllTime += bucketsHarvested

        let month = getMonthlyStatsMonthKey(work.date)
        let year = getMonthlyStatsYearKey(work.date)
        let existingMonthlyStats = user.stats.monthly.find(x => x.month === month && x.year === year)

        if (!existingMonthlyStats) {
          user.stats.monthly.push({
            month: month,
            year: year,
            stats: {
              bucketsHarvested: bucketsHarvested,
              hours: work.timeSpent
            }
          })
        } else {
          existingMonthlyStats.stats.bucketsHarvested += bucketsHarvested
          existingMonthlyStats.stats.hours += work.timeSpent
        }

        // Save the docs
        return work.save()
          .then(() => work.hub.save())
          .then(() => user.save())
      })
      .then(() => {
        logger.info(`Work ${work.id}: Approval finished`)
      })
      .then(() => resolve())
      .catch(err => reject(err))
  });
}

export function validateAndApproveWork(work: IApproveWorkModel): Promise<void> {
  return canApproveWork(work)
    .then(() => approveWork(work));
}
