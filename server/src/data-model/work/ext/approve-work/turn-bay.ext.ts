import { IApproveWorkModel } from "../../work.model";
import { logger } from "../../../../lib/winston.lib";
import { IBayEventModel } from "../../bay-event/bay-event.model";
import { BatchEventModel } from "../../../batch/batch-events/batch-event.schema";
import { IBatchEvent } from "../../../batch/batch-events/batch-event.model";

function canTurnBay(work: IApproveWorkModel, bayEvent: IBayEventModel): Promise<void> {
  return new Promise((resolve, reject) => {
    let bayToTurn = work.hub.bays.find(x => x.id.toString() === bayEvent.bay.toString())

    if (!bayToTurn)
      throw new Error('The requested bay does not belong to the specified host')

    if (!bayToTurn.activeBatches || bayToTurn.activeBatches.length === 0)
      throw new Error('Cannot turn a bay that has no batches');

    return resolve();
  });
}

function turnBay(work: IApproveWorkModel, bayEvent: IBayEventModel): Promise<void> {
  return new Promise((resolve, reject) => {
    let bayToTurn = work.hub.bays.find(x => x.id.toString() === bayEvent.bay.toString())
    let destinationBay = work.hub.bays.find(x => x.id.toString() === bayEvent.destination.toString())
    logger.info(`Bay ${bayToTurn.id}: Turn into ${destinationBay.id} started`)

    // Move the batches to the destination bay
    destinationBay.activeBatches = destinationBay.activeBatches
      .concat(bayToTurn.activeBatches.slice())
    bayToTurn.activeBatches = []

    let tempBatchEvent = <Partial<IBatchEvent>>{
      type: bayEvent.type,
      startBay: bayEvent.bay,
      endBay: bayEvent.destination,
      createdBy: work.id,
      date: work.date
    }

    // Add a new batch event to each of the turned batches
    destinationBay.activeBatches.forEach(x =>
      x.events.push(new BatchEventModel(tempBatchEvent)))

    // Save the altered bays
    return destinationBay.save()
      .then(() => bayToTurn.save())
      .then(() => {
        destinationBay.activeBatches.forEach(x =>{
          logger.info(`Batch ${x.id}: Turned ${x.name} into ${destinationBay.id}`)
        })

        logger.info(`Bay ${bayToTurn.id}: Turn into ${destinationBay.id} finished`)

        // Finish the promise
        resolve()
      })
      .catch(err => {throw new Error(err.message)})
  });
}

export function validateAndTurnBay(work: IApproveWorkModel, bayEvent: IBayEventModel): Promise<void> {
  return canTurnBay(work, bayEvent)
    .then(() => turnBay(work, bayEvent));
}
