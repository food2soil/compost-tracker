import { IApproveWorkModel } from "../../work.model";
import { BatchMeasurementModel } from "../../../batch/batch-measurements/batch-measurement.schema";
import { IBatchMeasurement } from "../../../batch/batch-measurements/batch-measurement.model";
import { logger } from "../../../../lib/winston.lib";

function validateMeasurements(work: IApproveWorkModel): Promise<void> {
  return new Promise((resolve, reject) => {
    // Bay measurements are invalid if there is some measurement whos bay is not on the hub
    let bayMeasurementInvalid = work.bayMeasurements.some(m => {
      return !work.hub.bays
        .some(hubBay => m.bay.toString() === hubBay.id.toString())
    })

    if (bayMeasurementInvalid)
      throw new Error('A bay measurement was provided for a bay that does not exist on the hub')

    return resolve()
  });
}

function processMeasurements(work: IApproveWorkModel): Promise<void> {
  return new Promise((resolve, reject) => {
    logger.info(`Work ${work.id}: Start processing measurements`)

    let baysToSave = []
    work.bayMeasurements.forEach(m => {
      logger.info(`Bay ${m.bay}: Process Measurements`)
      let bayToProcess = work.hub.bays.find(x => x.id.toString() === m.bay.toString())

      let batchMeasurement = new BatchMeasurementModel(<Partial<IBatchMeasurement>>{
        date: work.date,
        temperature: m.temperature,
        createdBy: work.id
      })

      // Add the measurement to each active batch in the bay
      bayToProcess.activeBatches.forEach(x => x.measurements.push(batchMeasurement))
      baysToSave.push(bayToProcess)
    })

    // Save the affected bays
    let bayIdsToSave = work.bayMeasurements.map(x => x.bay.toString());
    return Promise.all(baysToSave.map(x => x.save()))
      .then(() => {
        logger.info(`Work ${work.id}: Finished processing measurements`)

        // Finish the promise
        resolve()
      })
  });
}

export function validateAndProcessMeasurements(work: IApproveWorkModel): Promise<void> {
  return validateMeasurements(work)
    .then(() => processMeasurements(work))
}
