import { logger } from "../../../../lib/winston.lib";
import { IBatchEvent } from "../../../batch/batch-events/batch-event.model";
import { Enums } from "mean-au-ts-shared";
import { BatchEventModel } from "../../../batch/batch-events/batch-event.schema";
import { IApproveWorkModel } from "../../work.model";
import { HandlerError } from "../../../../api/handler.error";
import { IBayEventModel } from "../../bay-event/bay-event.model";

function canHarvestBay(work: IApproveWorkModel, bayEvent: IBayEventModel): Promise<void> {
  return new Promise((resolve, reject) => {
    let bayToHarvest = work.hub.bays.find(x => x.id.toString() === bayEvent.bay.toString())
    if (!bayToHarvest)
      throw new Error('The requested bay does not belong to the specified host')

    if (bayToHarvest.activeBatches.length === 0)
      throw new Error('Cannot harvest a bay that has no batches')

    return resolve()
  });
}

function harvestBay(work: IApproveWorkModel, bayEvent: IBayEventModel): Promise<void> {
  return new Promise((resolve, reject) => {
    let bayToHarvest = work.hub.bays.find(x => x.id.toString() === bayEvent.bay.toString());
    logger.info(`Bay ${bayToHarvest.id}: Harvest started`)

    // Only update stats for a non-full harvest
    if (!bayEvent.isFullHarvest) {
      bayToHarvest.activeBatches.forEach(x => {
        x.stats.finishedCompostMade += bayEvent.bucketsHarvested / bayToHarvest.activeBatches.length
      })

      work.hub.stats.finishedCompostMade += bayEvent.bucketsHarvested

      return bayToHarvest.save()
        .then(() => work.hub.save())
        .then(() => resolve())
    }

    let finishedBatches = bayToHarvest.activeBatches.slice()

    // create a new event
    let tempBatchEvent: Partial<IBatchEvent> = {
      type: Enums.BayEvent.Harvest,
      startBay: bayToHarvest.id,
      createdBy: work.id,
      date: work.date
    }

    // Add a new batch event and update stats for each of the harvested batches
    finishedBatches.forEach(x => {
      x.events.push(new BatchEventModel(tempBatchEvent))

      x.stats.isActive = false
      x.stats.finishedCompostMade += bayEvent.bucketsHarvested / bayToHarvest.activeBatches.length
    })

    // update hub stats
    work.hub.stats.finishedCompostMade += bayEvent.bucketsHarvested

    // Add the newly harvested batches to the hub's finished batches collection
    work.hub.finishedBatches = work.hub.finishedBatches
      .concat(finishedBatches)

    // Clear the harvested bays active batches collection
    bayToHarvest.activeBatches = []

    // save the hub and harvested bay...
    return bayToHarvest.save()
      .then(() => work.hub.save())
      .then(() => {
        logger.info(`Bay ${bayToHarvest.id}: Harvest finished`)

        // Finish the promise
        resolve()
      })
      .catch(err => {throw new HandlerError(422, err.message)})
  });
}

export function validateAndHarvestBay(work: IApproveWorkModel, bayEvent: IBayEventModel): Promise<void> {
  return canHarvestBay(work, bayEvent)
    .then(() => harvestBay(work, bayEvent));
}

