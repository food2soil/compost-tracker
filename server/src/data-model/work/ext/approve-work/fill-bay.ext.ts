import { logger } from "../../../../lib/winston.lib";
import { IBatchEvent } from "../../../batch/batch-events/batch-event.model";
import { Enums } from "mean-au-ts-shared";
import { BatchEventModel } from "../../../batch/batch-events/batch-event.schema";
import { IBatch } from "../../../batch/batch.model";
import { BatchModel } from "../../../batch/batch.schema";
import { IApproveWorkModel } from "../../work.model";
import { HandlerError } from "../../../../api/handler.error";
import { IBayEventModel } from "../../bay-event/bay-event.model";
import { nextBatchName } from "../../../hub/ext/next-batch-name.ext";

function canFillBay(work: IApproveWorkModel, bayEvent: IBayEventModel): Promise<void> {
  return new Promise((resolve, reject) => {
    let bayToFill = work.hub.bays.find(x => x.id.toString() === bayEvent.bay.toString())
    if (!bayToFill)
      throw new Error('The requested bay does not belong to the specified host')

    if (bayToFill.activeBatches.length > 0)
      throw new Error('Cannot fill a bay that has batches')

    return resolve()
  });
}

function fillBay(work: IApproveWorkModel, bayEvent: IBayEventModel): Promise<void> {
  return new Promise((resolve, reject) => {
    let bayToFill = work.hub.bays.find(x => x.id.toString() === bayEvent.bay.toString());

    logger.info(`Bay ${bayToFill.id}: Fill started`)

    // create a new event
    let batchEvent: Partial<IBatchEvent> = {
      type: Enums.BayEvent.Fill,
      endBay: bayToFill.id,
      createdBy: work.id,
      date: work.date
    }

    // create a new batch...
    let batch: Partial<IBatch> = {
      name: nextBatchName(work.hub),
      events: [new BatchEventModel(batchEvent)],
      created: work.date,
      stats: {
        hub: work.hub,
        isActive: true,
        finishedCompostMade: 0
      }
    }
    let newBatch = new BatchModel(batch)

    // add to the batch collection...
    bayToFill.activeBatches.push(newBatch);

    // save the filled bay...
    return bayToFill.save()
      .then(() => {
        // update hub stats
        work.hub.stats.numActiveBatches++
        return work.hub.save()
      })
      .then(() => {
        logger.info(`Bay ${bayToFill.id}: Created new batch ${newBatch.id}`)
        logger.info(`Bay ${bayToFill.id}: Fill finished`)

        // Finish the promise
        resolve()
      })
      .catch(err => {throw new HandlerError(422, err.message)})
  });
}

export function validateAndFillBay(work: IApproveWorkModel, bayEvent: IBayEventModel): Promise<void> {
  return canFillBay(work, bayEvent)
    .then(() => fillBay(work, bayEvent));
}

