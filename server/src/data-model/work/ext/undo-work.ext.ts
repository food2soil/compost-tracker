import { logger } from "../../../lib/winston.lib";
import { IUndoWorkModel } from "../work.model";
import { database } from "../../mongoose.config";

function canUndoWork(work: IUndoWorkModel): Promise<void> {
    if (!work.isApproved) return Promise.reject(new Error('This work record is not approved'));

    // Error if another work for this hub is approved with a later date
    return database.Work.find({
      hub: work.hub,
      isApproved: true,
      date: { '$gt': work.date }
    }).then(x => {
      if (x.length === 0) return Promise.resolve()

      return Promise.reject(
        new Error('There is another work record that must be undone before this one'))
    })
}

function undoWork(work: IUndoWorkModel): Promise<void> {
  return new Promise((resolve, reject) => {
    logger.info(`Work ${work.id}: Undo started`)

    // Reset the hub's finished batches
    work.hub.finishedBatches = work.undoFinishedBatches.slice()

    // Reset each bay's active batches
    work.undoBays.forEach(x => {
      let hubBay = work.hub.bays.find(bay => bay.id.toString() === x.id.toString())
      if (!hubBay) return

      hubBay.activeBatches = x.activeBatches.slice()
    })

    // Reset the work to unapproved state
    work.undoBays = []
    work.undoFinishedBatches = []
    work.isApproved = false

    // Update hub stats
    work.hub.stats.numActiveBatches = 0
    work.hub.bays.forEach(x => {
      work.hub.stats.numActiveBatches += x.activeBatches.length
    })
    
    work.bayEvents.forEach(x => {
      work.hub.stats.finishedCompostMade -= x.bucketsHarvested 
    })

    work.hub.stats.lastWork = work._id
    work.hub.stats.needsCarbon = work.isCarbonNeeded

    // Save the bays, hub, and work
    Promise.all(work.hub.bays.map(x => x.save()))
      .then(() => work.hub.save())
      .then(() => work.save())
      .then(() => logger.info(`Work ${work.id}: Undo finished`))
      .then(() => resolve())
      .catch(err => reject(err))
  });
}

export function validateAndUndoWork(work: IUndoWorkModel): Promise<void> {
  return canUndoWork(work)
    .then(() => undoWork(work));
}
