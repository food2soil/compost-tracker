import { IWorkModel } from "../work.model";

export function getFinishedCompostMade(work: IWorkModel[]) {
    return work
        .map(y => y.bayEvents.filter(z => !!z.bucketsHarvested).map(z => z.bucketsHarvested))
        .reduce((a,b) => a.concat(b), [])
        .reduce((a,b) => a + b, 0)
}