import * as mongoose from "mongoose";

export interface IBayMeasurement {
  id?: mongoose.Types.ObjectId
  temperature: number
  bay: mongoose.Types.ObjectId
}

export interface IBayMeasurementModel extends IBayMeasurement, mongoose.Document {
  id: mongoose.Types.ObjectId
}
