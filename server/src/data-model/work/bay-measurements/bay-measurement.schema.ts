import { model, Schema } from "mongoose";
import { IBayMeasurementModel } from "./bay-measurement.model";

var BayMeasurementSchema = new Schema({
  temperature: {
    type: Number,
    required: true
  },
  bay: {
    type: Schema.Types.ObjectId,
    ref: 'Bay',
    required: true
  }
});

export const BayMeasurementModel = model<IBayMeasurementModel>('BayMeasurement', BayMeasurementSchema);
