import { model, Schema } from "mongoose";
import { IWorkExt, IWorkModel, IApproveWorkExt } from "./work.model";
import { BatchModel } from "../batch/batch.schema";
import { validateTechnicianHook } from "./hooks/validate-technician.hook";
import { validateHubHook } from "./hooks/validate-hub.hook";
import { validateAndApproveWork } from "./ext/approve-work/approve-work.ext";
import { BayModel } from "../bay/bay.schema";
import { BayEventModel } from "./bay-event/bay-event.schema";
import { BayMeasurementModel } from "./bay-measurements/bay-measurement.schema";

let WorkSchema = new Schema({
  date: {
    type: Date,
    default: Date.now
  },
  bayEvents: [BayEventModel.schema],
  bayMeasurements: [BayMeasurementModel.schema],
  timeSpent: {
    type: Number,
    required: 'Time spent cannot be 0'
  },
  notes: {
    type: String,
    default: '',
    trim: true
  },
  technician: {
    type: Schema.Types.ObjectId,
    ref: 'User',
    required: 'Technician can not be empty'
  },
  hub: {
    type: Schema.Types.ObjectId,
    ref: 'Hub',
    required: 'Hub can not be empty'
  },
  isApproved: {
    type: Boolean,
    default: false
  },
  undoBays: [BayModel.schema],
  undoFinishedBatches: [BatchModel.schema],
  isCarbonNeeded: {
    type: Boolean,
    default: false
  }
});

let extensions: IWorkExt & IApproveWorkExt = {
  validateAndApproveWork: validateAndApproveWork
};

Object.assign(WorkSchema.methods, extensions);

WorkSchema.pre('validate', validateTechnicianHook);
WorkSchema.pre('validate', validateHubHook);

export const WorkModel = model<IWorkModel>('Work', WorkSchema);
