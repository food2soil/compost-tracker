import { model, Schema, Document } from "mongoose";
import { IBatchMeasurementModel } from "./batch-measurement.model";

var BatchMeasurementSchema = new Schema({
  date: {
    type: Date,
    default: Date.now,
    required: 'Batch measurement must have a date.'
  },
  temperature: {
    type: Number,
    required: 'Batch temperature is required.'
  },
  createdBy: {
    type: Schema.Types.ObjectId,
    ref: 'Work'
  }
});

export const BatchMeasurementModel = model<IBatchMeasurementModel>('BatchMeasurement', BatchMeasurementSchema);
