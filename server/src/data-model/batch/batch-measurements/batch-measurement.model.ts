import * as mongoose from "mongoose";

export interface IBatchMeasurement {
  id: mongoose.Types.ObjectId
  date: Date
  temperature: number
  createdBy: mongoose.Types.ObjectId
}

export interface IBatchMeasurementModel extends IBatchMeasurement, mongoose.Document {
  id: mongoose.Types.ObjectId
}
