import { model, Schema } from "mongoose";
import { IBatchEventModel } from "./batch-event.model";
import { Enums } from "mean-au-ts-shared";

var BatchEventSchema = new Schema({
  type: {
    type: String,
    enum: [Enums.BayEvent.Fill, Enums.BayEvent.Turn, Enums.BayEvent.Harvest],
    required: 'Batch event must have a type.'
  },
  date: {
    type: Date,
    default: Date.now,
    required: 'Batch event must have a date.'
  },
  startBay: {
    type: Schema.Types.ObjectId,
    ref: 'Bay'
  },
  endBay: {
    type: Schema.Types.ObjectId,
    ref: 'Bay'
  },
  createdBy: {
    type: Schema.Types.ObjectId,
    ref: 'Work',
    required: true
  }
});

export const BatchEventModel = model<IBatchEventModel>('BatchEvent', BatchEventSchema);
