import * as mongoose from "mongoose";
import { Enums } from "mean-au-ts-shared";

export interface IBatchEvent {
  id: mongoose.Types.ObjectId
  type: Enums.BayEvent
  date: Date
  startBay?: mongoose.Types.ObjectId
  endBay?: mongoose.Types.ObjectId
  createdBy: mongoose.Types.ObjectId
}

export interface IBatchEventModel extends IBatchEvent, mongoose.Document {
  id: mongoose.Types.ObjectId
}
