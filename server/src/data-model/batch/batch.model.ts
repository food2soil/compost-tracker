import * as mongoose from "mongoose";
import { IBatchMeasurement } from "./batch-measurements/batch-measurement.model";
import { IBatchEvent } from "./batch-events/batch-event.model";
import { IBatchStats } from "../stats/batch/batch-stats.model";

export interface IBatch {
  id: mongoose.Types.ObjectId
  created: Date
  name: string
  measurements: IBatchMeasurement[]
  events: IBatchEvent[]
  notes: string
  stats: IBatchStats
}

export interface IBatchModel extends IBatch, mongoose.Document {
  id: mongoose.Types.ObjectId
}
