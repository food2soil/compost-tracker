import { model, Schema, Document } from "mongoose";
import { IBatchModel } from "./batch.model";
import { validateOneEventHook } from "./hooks/validate-one-event.hook";
import { BatchMeasurementModel } from "./batch-measurements/batch-measurement.schema";
import { BatchEventModel } from "./batch-events/batch-event.schema";
import { BatchStatsModel } from "../stats/batch/batch-stats.schema";

var BatchSchema = new Schema({
  created: {
    type: Date,
    default: Date.now()
  },
  name: {
    type: String,
    required: 'Batch must have a name.'
  },
  measurements: [BatchMeasurementModel.schema],
  events: [BatchEventModel.schema],
  notes: {
    type: String
  },
  stats: BatchStatsModel.schema
});

BatchSchema.pre('validate', validateOneEventHook);

export const BatchModel = model<IBatchModel>('FinishedBatch', BatchSchema);
