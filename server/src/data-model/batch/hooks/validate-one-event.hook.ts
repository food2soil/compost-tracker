import { HookNextFunction } from "mongoose";
import { IBatchModel } from "../batch.model";

export function validateOneEventHook(next: HookNextFunction) {
  var self: IBatchModel = this;

  if (self.events.length === 0) {
    return next(new Error('At least one batch event is required.'));
  }

  return next();
}
