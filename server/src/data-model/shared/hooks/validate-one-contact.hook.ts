import { HookNextFunction } from "mongoose";
import { IContact, IContactsModel } from "../contact/contact.model";

export function validateOneContactHook(next: HookNextFunction) {
  var self: IContactsModel = this;

  if (self.contacts.length === 0) {
    return next(new Error('At least one contact is required.'));
  }

  return next();
}
