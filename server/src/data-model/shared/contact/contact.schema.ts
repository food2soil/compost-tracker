import { model, Schema, Document, Types } from "mongoose";
import { IContactModel } from "./contact.model";
import { validateMethodHook } from "./hooks/validate-method.hook";
import { Utilities } from "mean-au-ts-shared";

var ContactSchema = new Schema({
  name: {
    type: String,
    trim: true,
    require: 'Contact name must be provided'
  },
  title: {
    type: String,
    trim: true
  },
  phone: {
    type: String,
    trim: true
  },
  email: {
    type: String,
    trim: true
  },
  isPrimary: {
    type: Boolean
  }
});

ContactSchema.pre('validate', validateMethodHook);

export const ContactModel = model<IContactModel>('Contact', ContactSchema);
