import * as mongoose from "mongoose";

export interface IContactExt { }

export interface IContact {
  id?: mongoose.Types.ObjectId
  name: string
  title?: string
  phone?: string
  email?: string
  isPrimary: boolean
}

export interface IContactsModel extends mongoose.Document {
  contacts: IContactModel[]
}

export interface IContactModel extends IContact, IContactExt, mongoose.Document {
  id: mongoose.Types.ObjectId
}
