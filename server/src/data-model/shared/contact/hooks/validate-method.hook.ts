import { HookNextFunction } from "mongoose";
import { IContactModel } from "../contact.model";

export function validateMethodHook(next: HookNextFunction) {
  var self: IContactModel = this;

  if (!self.phone && !self.email) {
    return next(new Error('Contact must provide either a phone number or email address.'));
  }

  return next();
}
