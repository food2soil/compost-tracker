import * as mongoose from "mongoose";

export interface IAddressExt { }

export interface IAddress {
  id?: mongoose.Types.ObjectId
  primaryLine: string,
  secondaryLine?: string,
  city: string
  state: string
  zip: string
  loc?: [number, number]
}

export interface IAddressModel extends IAddress, IAddressExt, mongoose.Document {
  id: mongoose.Types.ObjectId
}
