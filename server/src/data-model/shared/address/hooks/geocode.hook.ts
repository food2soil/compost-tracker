import { HookNextFunction } from "mongoose";
import { geocoder } from "../../../../lib/geocoder.lib";
import { IAddressModel } from "../address.model";
import { logger } from "../../../../lib/winston.lib";

export function geocodeHook(next: HookNextFunction) {
  var self: IAddressModel = this;

  geocoder.tryGeocodeNewAddress(null, self)
    .then((res) => {
      if (res) {
        self.loc = [res[0].longitude, res[0].latitude];
      }

      return next();
    })
    .catch(function (err) {
      logger.warn('WARN: ' + err);
      // We failed to geocode the address.  Save anyways :(
      return next();
    });
}
