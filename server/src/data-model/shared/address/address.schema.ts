import { model, Schema, Document } from "mongoose";
import { IAddressModel } from "./address.model";
import { geocodeHook } from "./hooks/geocode.hook";

var AddressSchema = new Schema({
  primaryLine: {
    type: String,
    required: 'Address is required'
  },
  secondaryLine: {
    type: String
  },
  city: {
    type: String,
    required: 'City is required'
  },
  state: {
    type: String,
    required: 'State is required'
  },
  zip: {
    type: String,
    required: 'Zip code is required'
  },
  loc: {
    type: [Number],
    index: '2dsphere'
  }
});

AddressSchema.pre('validate', geocodeHook);

export const AddressModel = model<IAddressModel>('Address', AddressSchema);
