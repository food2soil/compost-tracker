import { Types } from "mongoose";
import { database } from "../../../mongoose.config";
import { HubStatsModel } from "../hub-stats.schema";
import { logger } from "../../../../lib/winston.lib";
import { IWorkModel } from "../../../work/work.model";
import { getFinishedCompostMade } from "../../../work/ext/finished-compost-made.ext";
import { IHubModel } from "../../../hub/hub.model";
import { IBayModel } from "../../../bay/bay.model";
import { IGeneratorModel } from "../../../generator/generator.model";
import { IHubStatsModel } from "../hub-stats.model";
import { Enums, GeneratorShared } from "mean-au-ts-shared";

interface IJoinedHubModel extends IHubModel {
  bays: IBayModel[]
  work: IWorkModel[]
  generators: IGeneratorModel[]
  stats: IHubStatsModel
}

export function rebuildHubStats(): Promise<void> {
  return database.Hub.aggregate()
    .lookup({
      from: 'bays',
      localField: 'bays',
      foreignField: '_id',
      as: 'bays'
    })
    .lookup({
      from: 'generators',
      localField: 'generators',
      foreignField: '_id',
      as: 'generators'
    })
    .lookup({
      from: 'works',
      localField: '_id',
      foreignField: 'hub',
      as: 'work'
    })
    .lookup({
      from: 'hubstats',
      localField: '_id',
      foreignField: 'hub',
      as: 'stats'
    })
    .project({
      _id: 1,
      created: 1,
      name: 1,
      capacityGallonsPerWeek: 1,
      bays: 1,
      generators: 1,
      work: 1,
      stats: { $arrayElemAt: ['$stats', 0] }
    })
    .then((joinedHubs: IJoinedHubModel[]) => {
      return Promise.all(joinedHubs.map(joined => {
        return database.Hub.findById(Types.ObjectId(joined._id))
          .then(h => {
            if (!h.stats) {
              h.stats = new HubStatsModel()
            }
    
            h.stats.lastWork = joined.work
              .sort((a,b) => b.date.valueOf() - a.date.valueOf())[0]
            
            h.stats.numActiveBatches = joined.bays
              .map(x => x.activeBatches.length)
              .reduce((a, b) => a + b, 0)
    
            h.stats.finishedCompostMade = getFinishedCompostMade(joined.work)
            h.stats.numResidential = joined.generators
              .filter(x => x.generatorType === Enums.GeneratorType.Residential)
              .length
      
            h.stats.numRestaurants = joined.generators
              .filter(x => x.generatorType === Enums.GeneratorType.Restaurant)
              .length
    
            h.stats.needsCarbon = h.stats.lastWork && h.stats.lastWork.isCarbonNeeded
    
            let weeklyCollection = joined.generators
              .map(x => GeneratorShared.getWeeklyCollection(x))
              .reduce((a,b) => a + b, 0)
            
            h.stats.usagePercentage = weeklyCollection / h.capacityGallonsPerWeek

            return h.save()
          })
          .then(() => logger.debug(`Rebuilt Hub Stats for ${joined.name}`))
      }))
      .then(() => undefined)
    })
}
