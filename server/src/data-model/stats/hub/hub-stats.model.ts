import * as mongoose from "mongoose";
import { IWorkModel } from "../../work/work.model";
import { IHubModel } from "../../hub/hub.model";
import { IUserModel } from "../../user/user.model";

export interface IHubStats {
  lastWork: mongoose.Types.ObjectId | IWorkModel
  numActiveBatches: number
  finishedCompostMade: number
  numResidential: number
  numRestaurants: number
  needsCarbon: boolean
  usagePercentage: number
}

export interface IHubStatsModel extends IHubStats, mongoose.Document {
  id: mongoose.Types.ObjectId
}
