import { model, Schema } from "mongoose";
import { IHubStatsModel } from "./hub-stats.model";

let HubStatsSchema = new Schema({
  lastWork: {
    type: Schema.Types.ObjectId,
    ref: 'Work'
  },
  numActiveBatches: {
    type: Number,
    default: 0
  },
  finishedCompostMade: {
    type: Number,
    default: 0
  },
  numResidential: {
    type: Number,
    default: 0
  },
  numRestaurants: {
    type: Number,
    default: 0
  },
  needsCarbon: {
    type: Boolean,
    default: false
  },
  usagePercentage: {
    type: Number,
    default: 0
  }
});

export const HubStatsModel = model<IHubStatsModel>('HubStats', HubStatsSchema);
