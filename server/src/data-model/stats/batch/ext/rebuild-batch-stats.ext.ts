import { database } from "../../../mongoose.config";
import { IBatchModel } from "../../../batch/batch.model";
import { IWorkModel } from "../../../work/work.model";
import { Types } from "mongoose";
import { getFinishedCompostMade } from "../../../work/ext/finished-compost-made.ext";
import { BatchStatsModel } from "../batch-stats.schema";
import { logger } from "../../../../lib/winston.lib";

interface IJoinedBatchModel extends IBatchModel {
  work: IWorkModel[]
  hub: Types.ObjectId
  isActive: boolean
}

function _rebuildActiveBatchStats(): Promise<void> {
  return database.Hub.aggregate()
    .lookup({
      from: 'bays',
      localField: 'bays',
      foreignField: '_id',
      as: 'bays'
    })
    .unwind('$bays')
    .unwind('$bays.activeBatches')
    .lookup({
      from: 'works',
      localField: 'bays.activeBatches.events.createdBy',
      foreignField: '_id',
      as: 'work'
    })
    .project({
      _id: '$bays.activeBatches._id',
      created: '$bays.activeBatches.created',
      name: '$bays.activeBatches.name',
      measurements: '$bays.activeBatches.measurements',
      events: '$bays.activeBatches.events',
      notes: '$bays.activeBatches.notes',
      hub: '$_id',
      work: '$work',
    })
    .then((allJoined: IJoinedBatchModel[]) => {
      return Promise.all(allJoined.map(joined => {
        return database.Bay.findOne({ 'activeBatches._id': joined._id })
          .then(toSave => {
            let toRebuild = toSave.activeBatches.find(x => x._id.toString() === joined._id.toString())
            if (!toRebuild.stats) {
              toRebuild.stats = new BatchStatsModel()
            }

            toRebuild.stats.hub = joined.hub
            toRebuild.stats.finishedCompostMade = getFinishedCompostMade(joined.work)
            toRebuild.stats.isActive = true

            return toSave.save()
          })
          .then(() => logger.debug(`Rebuilt Batch Stats for ${joined.name}`))
      }))
      .then(() => undefined)
    })
}

function _rebuildFinishedBatchStats(): Promise<void> {
  return database.Hub.aggregate()
    .unwind('$finishedBatches')
    .lookup({
      from: 'works',
      localField: 'finishedBatches.events.createdBy',
      foreignField: '_id',
      as: 'work'
    })
    .project({
      _id: '$finishedBatches._id',
      created: '$finishedBatches.created',
      name: '$finishedBatches.name',
      measurements: '$finishedBatches.measurements',
      events: '$finishedBatches.events',
      notes: '$finishedBatches.notes',
      hub: '$_id',
      work: '$work',
    })
    .then((allJoined: IJoinedBatchModel[]) => {
      return Promise.all(allJoined.map(joined => {
        return database.Hub.findOne({ 'finishedBatches._id': joined._id })
          .then(toSave => {
            let toRebuild = toSave.finishedBatches.find(x => x._id.toString() === joined._id.toString())
            if (!toRebuild.stats) {
              toRebuild.stats = new BatchStatsModel()
            }
            
            toRebuild.stats.hub = joined.hub
            toRebuild.stats.finishedCompostMade =  getFinishedCompostMade(joined.work)
            toRebuild.stats.isActive = false

            return toSave.save()
          })
          .then(() => logger.debug(`Rebuilt Batch Stats for ${joined.name}`))
      }))
      .then(() => undefined)
    })
}

export function rebuildBatchStats(): Promise<void> {
  return Promise.all([
    _rebuildActiveBatchStats(),
    _rebuildFinishedBatchStats()
  ])
  .then(() => undefined)
}