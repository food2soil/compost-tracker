import * as mongoose from "mongoose";
import { IHubModel } from "../../hub/hub.model";

export interface IBatchStats {
  hub: mongoose.Types.ObjectId | IHubModel
  finishedCompostMade: number
  isActive: boolean
}

export interface IBatchStatsModel extends IBatchStats, mongoose.Document {
  id: mongoose.Types.ObjectId
}
