import { model, Schema } from "mongoose";
import { IBatchStatsModel } from "./batch-stats.model";

let BatchStatsSchema = new Schema({
  hub: {
    type: Schema.Types.ObjectId,
    ref: 'Hub'
  },
  finishedCompostMade: Number,
  isActive: Boolean
});

export const BatchStatsModel = model<IBatchStatsModel>('BatchStats', BatchStatsSchema);
