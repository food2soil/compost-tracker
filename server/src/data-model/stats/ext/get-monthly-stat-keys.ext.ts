export function getMonthlyStatsMonthKey(date: Date) {
  let split = date.toLocaleDateString().split('-')
  return split[1]
}

export function getMonthlyStatsYearKey(date: Date) {
  let split = date.toLocaleDateString().split('-')
  return split[0]
}