import * as mongoose from "mongoose";

export interface IMonthlyTechnicianStats {
  hours: number
  bucketsHarvested: number
}

export interface ITechnicianStats {
  hoursAllTime: number
  bucketsHarvestedAllTime: number
  monthly: {
    month: string
    year: string
    stats: IMonthlyTechnicianStats
  }[]
}

export interface ITechnicianStatsModel extends ITechnicianStats, mongoose.Document {
  id: mongoose.Types.ObjectId
}
