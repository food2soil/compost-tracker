import { model, Schema } from "mongoose";
import { ITechnicianStatsModel } from "./technician-stats.model";

let TechnicianStatsSchema = new Schema({
  hoursAllTime: {
    type: Number,
    default: 0
  },
  bucketsHarvestedAllTime: {
    type: Number,
    default: 0
  },
  monthly: [{
    month: String,
    year: String,
    stats: {
      hours: {
        type: Number,
        default: 0
      },
      bucketsHarvested: {
        type: Number,
        default: 0
      }
    }
  }]
});

export const TechnicianStatsModel = model<ITechnicianStatsModel>('TechnicianStats', TechnicianStatsSchema);
