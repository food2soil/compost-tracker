import { Types } from "mongoose";
import { database } from "../../../mongoose.config";
import { TechnicianStatsModel } from "../technician-stats.schema";
import { logger } from "../../../../lib/winston.lib";
import { IWorkModel } from "../../../work/work.model";
import { IMonthlyTechnicianStats, ITechnicianStats } from "../technician-stats.model";
import { getFinishedCompostMade } from "../../../work/ext/finished-compost-made.ext";
import { IUserModel } from "../../../user/user.model";

interface IJoinedUserModel extends IUserModel {
  _id: string
  displayName: string
  work: IWorkModel[]
  stats: ITechnicianStats
}

export function rebuildTechnicianStats(): Promise<void> {
  return database.User.aggregate()
    .lookup({
      from: 'works',
      localField: '_id',
      foreignField: 'technician',
      as: 'work'
    })
    .project({
      _id: 1,
      displayName: 1,
      work: 1,
      stats: 1
    })
    .then((joinedUsers: IJoinedUserModel[]) => {
      return Promise.all(joinedUsers.map(joinedUser => {
        return database.User.findById(Types.ObjectId(joinedUser._id))
          .then(user => {
            if (!user.stats) {
              user.stats = new TechnicianStatsModel()
            }
    
            // Calculate global stats
            user.stats.hoursAllTime = joinedUser.work
              .map(x => x.timeSpent)
              .reduce((a, b) => a + b, 0)
      
            user.stats.bucketsHarvestedAllTime = getFinishedCompostMade(joinedUser.work)
      
            // Categorize work by month
            let byMonth: {[key: string]: IWorkModel[]} = {}
            joinedUser.work.forEach(x => {
              let split = x.date.toLocaleDateString().split('-')
              let year = split[0]
              let month = split[1]
      
              let key = `${month}-${year}`
              if (!byMonth[key]) {
                byMonth[key] = [x]
              } else {
                byMonth[key].push(x)
              }
            })
      
            // sum each category and rewrite the monthly stats
            user.stats.monthly = Object.keys(byMonth)
              .map(k => {
                let split = k.split('-')
                let stats: IMonthlyTechnicianStats = {
                  hours: byMonth[k]
                    .map(x => x.timeSpent)
                    .reduce((a, b) => a + b, 0),
                  bucketsHarvested: getFinishedCompostMade(byMonth[k])
                }
      
                return {
                  month: split[0],
                  year: split[1],
                  stats: stats
                }
              })
    
            return user.save()
              .then(() => logger.debug(`Updated Technician Stats for ${user.displayName}`))
          })
      }))
      .then(() => undefined)
    })
}
