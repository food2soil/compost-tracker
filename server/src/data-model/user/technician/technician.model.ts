import { IUser, IUserModel } from "../user.model";

export interface ITechnician extends IUser {
  hoursWorkedAllTime: number
  hoursWorkedMonthly: number[]
}

export interface ITechnicianModel extends ITechnician, IUserModel { }
