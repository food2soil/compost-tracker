import { IUserModel } from "../../../data-model/user/user.model";
import { Enums } from "mean-au-ts-shared";

export function satisfiesRole(role: Enums.UserRoles): boolean {
  var self: IUserModel = this;

  return role <= self.getHighestRole();
};
