import { IUserModel } from "../../../data-model/user/user.model";
import { Enums } from "mean-au-ts-shared";

export function getHighestRole(): Enums.UserRoles {
  var self: IUserModel = this;

  return self.roles.sort()[self.roles.length - 1];
};
