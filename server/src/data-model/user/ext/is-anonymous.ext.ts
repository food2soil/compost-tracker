import { IUserModel } from '../../../data-model/user/user.model';
import { Enums } from 'mean-au-ts-shared';

export function isAnonymous(): boolean {
  var self: IUserModel = this;

  // User is not admin or technician
  return !self.roles || self.roles.length === 0 || self.roles.indexOf(Enums.UserRoles.Anonymous) !== -1;
};
