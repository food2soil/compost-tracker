import * as mongoose from "mongoose";
import { IBatch, IBatchModel } from "../batch/batch.model";

export interface IBay {
  id: mongoose.Types.ObjectId
  name: string
  isStart?: boolean
  destinations: string[]
  isHolding?: boolean
  displayOrder: number
  activeBatches: IBatch[]
}

export interface IBayModel extends IBay, mongoose.Document {
  id: mongoose.Types.ObjectId
  activeBatches: IBatchModel[]
}
