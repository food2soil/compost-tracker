import { model, Schema, Document } from "mongoose";
import { IBayModel } from "./bay.model";
import { BatchModel } from "../batch/batch.schema";

var BaySchema = new Schema({
  name: {
    type: String,
    required: 'Bay must have a name.'
  },
  isStart: {
    type: Boolean
  },
  destinations: [{
    type: String
  }],
  isHolding: {
    type: Boolean
  },
  displayOrder: {
    type: Number
  },
  activeBatches: [BatchModel.schema]
});

export const BayModel = model<IBayModel>('Bay', BaySchema);
