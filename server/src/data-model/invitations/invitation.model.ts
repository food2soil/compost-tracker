import * as mongoose from "mongoose";
import { Enums } from "mean-au-ts-shared";
import { IUser } from "../user/user.model";
import { IHub } from "../hub/hub.model";

export interface IInvitation {
  firstName: string
  lastName: string
  email: string
  roles: Enums.UserRoles[]
  invitedBy: mongoose.Types.ObjectId | IUser
  hubs: mongoose.Types.ObjectId[] | IHub[]
}

export interface IInvitationModel extends IInvitation, mongoose.Document {
  id: mongoose.Types.ObjectId
}
