import { model, Schema, Document } from "mongoose";
import { IInvitationModel } from "./invitation.model";
import { Enums } from "mean-au-ts-shared";

let InvitationSchema = new Schema({
  firstName: {
    type: String,
    required: true,
    trim: true
  },
  lastName: {
    type: String,
    required: true,
    trim: true
  },
  roles: {
    type: [{
      type: Number,
      enum: [
        Enums.UserRoles.Owner,
        Enums.UserRoles.Admin,
        Enums.UserRoles.Technician
      ]
    }],
    default: [Enums.UserRoles.Technician],
    required: 'Please provide at least one role'
  },
  email: {
    type: String,
    index: {
      unique: true,
      sparse: true // For this to work on a previously indexed field, the index must be dropped & the application restarted.
    },
    lowercase: true,
    trim: true,
    required: true
  },
  created: {
    type: Date,
    default: Date.now
  },
  invitedBy: {
    type: Schema.Types.ObjectId,
    ref: 'User'
  },
  hubs: [{
    type: Schema.Types.ObjectId,
    ref: 'Hub'
  }]
});

export const InvitationModel = model<IInvitationModel>('Invitation', InvitationSchema);
