import { model, Schema, Document } from "mongoose";
import { IGeneratorModel } from "./generator.model";
import { AddressModel } from "../shared/address/address.schema";
import { ContactModel } from "../shared/contact/contact.schema";
import { ContainerHistoryModel } from "./container-history/container-history.schema";
import { validateOneContactHook } from "../shared/hooks/validate-one-contact.hook";
import { validateOneContainerHook } from "./hooks/validate-one-container.hook";
import { Enums } from "mean-au-ts-shared";

let GeneratorSchema = new Schema({
  created: {
    type: Date,
    default: Date.now
  },
  name: {
    type: String,
    trim: true,
    required: 'Name cannot be blank'
  },
  containerHistory: [ContainerHistoryModel.schema],
  contacts: [ContactModel.schema],
  address: AddressModel.schema,
  isActive: {
    type: Boolean,
    default: true
  },
  generatorType: {
    type: String,
    enum: [
      Enums.GeneratorType.Residential,
      Enums.GeneratorType.Restaurant
    ],
    required: 'Please provide the generator type'
  },
  collectionType: {
    type: String,
    enum: [
      Enums.CollectionType.Dropoff,
      Enums.CollectionType.Pickup
    ],
    required: 'Please provide the collection type'
  },
  isPrivate: {
    type: Boolean,
    default: false
  }
});

GeneratorSchema.pre('validate', validateOneContactHook);
GeneratorSchema.pre('validate', validateOneContainerHook);

export const GeneratorModel = model<IGeneratorModel>('Generator', GeneratorSchema);
