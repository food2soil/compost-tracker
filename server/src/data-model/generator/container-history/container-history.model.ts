import * as mongoose from "mongoose";

export interface IContainerHistory {
  id?: mongoose.Types.ObjectId
  volumePerContainer: number
  containersPerWeek: number
  dateStartedUsing: Date
}

export interface IContainerHistoryModel extends IContainerHistory, mongoose.Document {
  id: mongoose.Types.ObjectId
}
