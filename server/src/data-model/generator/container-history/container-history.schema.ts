import { model, Schema, Document, Types } from "mongoose";
import { IContainerHistoryModel } from "./container-history.model";

var ContainerHistorySchema = new Schema({
  volumePerContainer: {
    type: Number,
    default: 32,
    required: 'Container history must have a volume'
  },
  containersPerWeek: {
    type: Number,
    required: 'Container history must have containers per week'
  },
  dateStartedUsing: {
    type: Date,
    default: Date.now
  }
});

export const ContainerHistoryModel = model<IContainerHistoryModel>('ContainerHistory', ContainerHistorySchema);
