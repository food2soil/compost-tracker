import { IGeneratorModel } from "../../../data-model/generator/generator.model";
import { HookNextFunction } from "mongoose";

export function validateOneContainerHook(next: HookNextFunction) {
  var self: IGeneratorModel = this;

  if (self.containerHistory.length === 0) {
    return next(new Error('At least one container is required.'));
  }

  return next();
}
