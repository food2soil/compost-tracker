import * as mongoose from "mongoose";
import { IContainerHistory, IContainerHistoryModel } from "./container-history/container-history.model";
import { IContact, IContactModel, IContactsModel } from "../shared/contact/contact.model";
import { IAddress, IAddressModel } from "../shared/address/address.model";
import { Enums } from "mean-au-ts-shared";

export interface IGeneratorStats {
  id?: mongoose.Types.ObjectId
  name: string
  weeklyEmissionsReduced: number
  weeklyGallonsComposted: number
}

export interface IGenerator {
  id?: mongoose.Types.ObjectId
  created: Date
  name: string
  containerHistory: IContainerHistory[]
  contacts: IContact[]
  address: IAddress
  isActive: boolean
  generatorType: Enums.GeneratorType
  collectionType: Enums.CollectionType
  isPrivate: boolean
}

export interface IGeneratorModel extends IGenerator, IContactsModel {
  id: mongoose.Types.ObjectId
  containerHistory: IContainerHistoryModel[]
  contacts: IContactModel[]
}
