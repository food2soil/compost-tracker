import { Response, NextFunction } from "express";
import { Enums } from "mean-au-ts-shared";
import { IAuthenticatedRequest } from "../application/request-handlers/request-handler";

function restrictToRoles(...roles: Enums.UserRoles[]) {
  return (req: IAuthenticatedRequest<void>, res: Response, next: NextFunction) => {
    if (roles.some(r => req.user.satisfiesRole(r))) {
      return next();
    }

    return res.status(403).send({
      message: 'You are not authorized to perform this action'
    });
  }
}

export const acl = {
  restrictToRoles: restrictToRoles
}
