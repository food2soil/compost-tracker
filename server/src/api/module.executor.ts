import { Request, Response, NextFunction } from 'express';
import { IResponseBuilder } from '../api/response-builder/response-builder.lib';
import { HandlerError } from '../api/handler.error';
import { logger } from '../lib/winston.lib';
import { GeneralDto, Utilities, Enums } from 'mean-au-ts-shared';
import { IAuthenticatedRequest, IRequestHandler } from '../application/request-handlers/request-handler';

export interface IHandlerMap<TRequestBody, TResponseData> {
  roles: Enums.UserRoles[],
  handler: IRequestHandler<TRequestBody, TResponseData>
}

export interface IResponseMask {
  roles: Enums.UserRoles[],
  Mask: new () => any
}

export class ModuleExecutor {
  responseBuilder: IResponseBuilder

  private handleError(err: HandlerError): { code: number, message: string } {
    let body: GeneralDto.ErrorResponseBody;
    if (err.constructor === HandlerError) {
      // Special handling for our custom error type
      body = {
        message: err.message
      }

      return {
        code: err.httpCode || 500,
        message: err.message
      }
    }

    // Log and send 500 if the error was unexpected
    logger.error(err.message)
    logger.error(err.stack)

    body = {
      message: 'An error occurred'
    }

    return {
      code: 500,
      message: 'An error occurred'
    }
  }

  execute<TRequestBody, TResponseData>(
    handlerMaps: IHandlerMap<TRequestBody, TResponseData>[],
    ExpectedBody: new () => TRequestBody,
    responseMasks: IResponseMask[]
  ) {
    return (req: IAuthenticatedRequest<any>, res: Response) => {
      try {
        if (ExpectedBody) {
          // transform the request body/query into the expected type.
          // this discards any properties that are not on the expected type.
          req.body = Utilities.castTo<TRequestBody>(req.body, ExpectedBody);
          req.query = Utilities.castTo<TRequestBody>(req.query, ExpectedBody);
        }

        // TODO: Check through each role that the user has
        // validate and handle the request
        let highestRole = req.user ? req.user.getHighestRole() : Enums.UserRoles.Anonymous;
        let handlerMap = handlerMaps.find(x => x.roles.indexOf(highestRole) !== -1);
        if (!handlerMap) {
          // Authenticated as anonymous but no anonymous map exists
          throw new HandlerError(403, 'Forbidden')
        }

        let handler = handlerMap.handler;
        handler.populate(req)
          .then(req => handler.validate(req))
          .then(req => handler.execute(req))
          .then(responseData => {
            if (responseMasks && responseMasks.length > 0) {
              let highestRole = req.user ? req.user.getHighestRole() : Enums.UserRoles.Anonymous;
              let maskToUse = responseMasks.find(x => x.roles.indexOf(highestRole) !== -1)
              if (maskToUse && maskToUse.Mask) {
                if (Array.isArray(responseData)) {
                  responseData = Utilities.castToArray(responseData, maskToUse.Mask);
                } else {
                  responseData = Utilities.castTo(responseData, maskToUse.Mask);
                }
              } else if (maskToUse && maskToUse.Mask === null) {
                responseData = null
              }else {
                throw new HandlerError(500, `Dev Error: No mask defined for role ${Enums.UserRoles[highestRole]} on path ${req.path}`)
              }
            }

            if (!this.responseBuilder) {
              return res.json(responseData);
            }

            // Build the response
            return res.json(this.responseBuilder.buildResponse(req, responseData));
          }).catch((err: HandlerError) => {
            let responseCtx = this.handleError(err);
            return res.status(responseCtx.code).send({ message: responseCtx.message });
          })
      } catch (err) {
        let responseCtx = this.handleError(err);
        return res.status(responseCtx.code).send({ message: responseCtx.message });
      }
    }
  }
}
