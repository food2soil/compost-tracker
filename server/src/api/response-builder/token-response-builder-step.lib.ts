import { IResponseBuilderStep } from "../../api/response-builder/response-builder-step.lib";
import { jsonwebtoken } from "../../lib/jsonwebtoken.lib";
import { GeneralDto } from "mean-au-ts-shared";
import { IAuthenticatedRequest } from "../../application/request-handlers/request-handler";
import { getTokenFromRequest, shouldTokenBeRefreshed } from "../../application/strategies/jwt.strategy";

export class TokenResponseBuilderStep implements IResponseBuilderStep {
  pipe(response: GeneralDto.SuccessResponseBody, req: IAuthenticatedRequest<any>): GeneralDto.SuccessResponseBody {
    if (!req.user || req.user.isAnonymous()) {
      return response;
    }

    if (req.route.path === '/signin' || req.route.path === '/signup') {
      // Create a new token
      response.token = jsonwebtoken.createToken(req.user.id)
    } else {
      // Check to see if the current request's token should be refreshed
      let token = getTokenFromRequest(req)
      response.token = shouldTokenBeRefreshed(token)
        ? jsonwebtoken.refreshToken(token)
        : null
    }

    return response;
  }
}
