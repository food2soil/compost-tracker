import {GeneralDto} from 'mean-au-ts-shared';
import { IAuthenticatedRequest } from '../../application/request-handlers/request-handler';

export interface IResponseBuilderStep {
  pipe(response: GeneralDto.SuccessResponseBody, req: IAuthenticatedRequest<any>): GeneralDto.SuccessResponseBody
}
