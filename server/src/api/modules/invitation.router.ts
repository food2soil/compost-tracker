import { Enums, InvitationRead, InvitationRequests, InvitationWithInvitedByRead, InvitationWithInvitedByAndHubsRead, InvitationWithHubsRead } from "mean-au-ts-shared";
import { DefaultResponseBuilder } from "../response-builder/default-response-builder.lib";
import { ModuleExecutor } from "../module.executor";
import { Router } from "express";
import { inviteHandler } from "../../application/request-handlers/invitation/invite.handler";
import { readInvitationHandler, readInvitationWithInvitedByHandler, readInvitationWithInvitedByAndHubHandler } from "../../application/request-handlers/invitation/invitation-read.handler";
import { claimInviteHandler } from "../../application/request-handlers/invitation/claim-invite.handler";
import { invitationListHandler, invitationWithHubsListHandler } from "../../application/request-handlers/invitation/invitation-list.handler";
import { resendInviteHandler } from "../../application/request-handlers/invitation/resend-invite.handler";
import { deleteHandler } from "../../application/request-handlers/invitation/delete-invite.handler";
import { checkEmailHandler } from "../../application/request-handlers/invitation/invitation-check-email.handler";

const executor = new ModuleExecutor();
const router = Router();

router
  .get('/list', executor.execute([{
    roles: [Enums.UserRoles.Admin, Enums.UserRoles.Owner],
    handler: invitationListHandler
  }], null, [{
    roles: [Enums.UserRoles.Admin, Enums.UserRoles.Owner],
    Mask: InvitationRead.InvitationViewModel
  }]))
  .get('/list/with-hubs', executor.execute([{
    roles: [Enums.UserRoles.Admin, Enums.UserRoles.Owner],
    handler: invitationWithHubsListHandler
  }], null, [{
    roles: [Enums.UserRoles.Admin, Enums.UserRoles.Owner],
    Mask: InvitationWithHubsRead.InvitationViewModel
  }]))
  .get('/read/:id', executor.execute([{
    roles: [Enums.UserRoles.Anonymous],
    handler: readInvitationHandler
  }], null, [{
    roles: [Enums.UserRoles.Anonymous, Enums.UserRoles.Admin, Enums.UserRoles.Owner],
    Mask: InvitationRead.InvitationViewModel
  }]))
  .get('/read/:id/with-invited-by', executor.execute([{
    roles: [Enums.UserRoles.Anonymous],
    handler: readInvitationWithInvitedByHandler
  }], null, [{
    roles: [Enums.UserRoles.Anonymous, Enums.UserRoles.Admin, Enums.UserRoles.Owner],
    Mask: InvitationWithInvitedByRead.InvitationViewModel
  }]))
  .get('/read/:id/with-invited-by-hub', executor.execute([{
    roles: [Enums.UserRoles.Anonymous],
    handler: readInvitationWithInvitedByAndHubHandler
  }], null, [{
    roles: [Enums.UserRoles.Anonymous, Enums.UserRoles.Admin, Enums.UserRoles.Owner],
    Mask: InvitationWithInvitedByAndHubsRead.InvitationViewModel
  }]))
  .get('/check-email', executor.execute([{
    roles: [Enums.UserRoles.Admin, Enums.UserRoles.Owner],
    handler: checkEmailHandler
  }], null, null))
  .post('/send', executor.execute([{
    roles: [Enums.UserRoles.Admin, Enums.UserRoles.Owner],
    handler: inviteHandler
  }], InvitationRequests.InviteTechnician, null))
  .post('/claim', executor.execute([{
    roles: [Enums.UserRoles.Anonymous],
    handler: claimInviteHandler
  }], InvitationRequests.ClaimInvite, null))
  .post('/pending/:id', executor.execute([{
    roles: [Enums.UserRoles.Admin, Enums.UserRoles.Owner],
    handler: resendInviteHandler
  }], null, null))
  .delete('/pending/:id', executor.execute([{
    roles: [Enums.UserRoles.Admin, Enums.UserRoles.Owner],
    handler: deleteHandler
  }], null, null))

executor.responseBuilder = new DefaultResponseBuilder();

export const invitationRouter = router;
