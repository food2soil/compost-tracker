import { DefaultResponseBuilder } from "../../api/response-builder/default-response-builder.lib";
import { Enums, UserRead, UserRequests } from "mean-au-ts-shared";
import { ModuleExecutor } from "../module.executor";
import { Router } from "express";
import { profileHandler } from "../../application/request-handlers/me/profile.handler";
import { updateProfileHandler } from "../../application/request-handlers/me/update-profile.handler";
import { changePasswordHandler } from "../../application/request-handlers/me/change-password.handler";
import { noopHandler } from "../../application/request-handlers/base/noop.handler";

const executor = new ModuleExecutor();
const router = Router();

router
  // Read - Self
  .get('/', executor.execute([{
    roles: [Enums.UserRoles.Anonymous],
    handler: noopHandler
  }, {
    roles: [Enums.UserRoles.Technician, Enums.UserRoles.Admin, Enums.UserRoles.Owner],
    handler: profileHandler
  }], null, [{
    roles: [Enums.UserRoles.Technician, Enums.UserRoles.Admin, Enums.UserRoles.Owner],
    Mask: UserRead.UserViewModel
  }, {
    roles: [Enums.UserRoles.Anonymous],
    Mask: null
  }]))
  // Update - Profile
  .put('/', executor.execute([{
    roles: [Enums.UserRoles.Technician, Enums.UserRoles.Admin, Enums.UserRoles.Owner],
    handler: updateProfileHandler
  }], UserRequests.EditProfile, null))
  // Update - Change Password
  .post('/change-password', executor.execute([{
    roles: [Enums.UserRoles.Technician, Enums.UserRoles.Admin, Enums.UserRoles.Owner],
    handler: changePasswordHandler
  }], UserRequests.ChangePassword, null))

executor.responseBuilder = new DefaultResponseBuilder();

export const meRouter = router;
