import { Enums, UserRead } from "mean-au-ts-shared";
import { DefaultResponseBuilder } from "../response-builder/default-response-builder.lib";
import { ModuleExecutor } from "../module.executor";
import { Router } from "express";
import { listHandler } from "../../application/request-handlers/technician/technician-list.handler";
import { readTechnicianHandler, readTechnicianTechnicianRoleHandler } from "../../application/request-handlers/technician/technician-read.handler";

const executor = new ModuleExecutor();
const router = Router();

router
  .get('/list', executor.execute([{
    roles: [Enums.UserRoles.Technician, Enums.UserRoles.Admin, Enums.UserRoles.Owner],
    handler: listHandler
  }], null, [{
    roles: [Enums.UserRoles.Technician, Enums.UserRoles.Admin, Enums.UserRoles.Owner],
    Mask: UserRead.UserViewModel
  }]))
  .get('/read/:id', executor.execute([{
    roles: [Enums.UserRoles.Admin, Enums.UserRoles.Owner],
    handler: readTechnicianHandler
  }, {
    roles: [Enums.UserRoles.Technician],
    handler: readTechnicianTechnicianRoleHandler
  }], null, [{
    roles: [Enums.UserRoles.Technician, Enums.UserRoles.Admin, Enums.UserRoles.Owner],
    Mask: UserRead.UserViewModel
  }]))

executor.responseBuilder = new DefaultResponseBuilder();

export const technicianRouter = router;
