import { DefaultResponseBuilder } from "../../api/response-builder/default-response-builder.lib";
import { ModuleExecutor } from "../module.executor";
import { Router } from "express";
import { Enums, GeneratorRequests, ContactRequests, GeneratorRead } from "mean-au-ts-shared";
import { listHandler } from "../../application/request-handlers/generator/generator-list.handler";
import { createHandler } from "../../application/request-handlers/generator/generator-create.handler";
import { readHandler } from "../../application/request-handlers/generator/generator-read.handler";
import { addContactHandler } from "../../application/request-handlers/generator/generator-add-contact.handler";
import { deleteGeneratorContactHandler } from "../../application/request-handlers/contact/delete-contact.handler";
import { updateGeneratorContactHandler } from "../../application/request-handlers/contact/update-contact-handler";
import { updateHandler } from "../../application/request-handlers/generator/generator-update.handler";

const executor = new ModuleExecutor();
const router = Router();

router
  // List
  .get('/list', executor.execute([{
    roles: [Enums.UserRoles.Technician, Enums.UserRoles.Admin, Enums.UserRoles.Owner],
    handler: listHandler
  }], null, [{
    roles: [Enums.UserRoles.Technician, Enums.UserRoles.Admin, Enums.UserRoles.Owner],
    Mask: GeneratorRead.GeneratorViewModel
  }]))
  // Create
  .post('/', executor.execute([{
    roles: [Enums.UserRoles.Admin, Enums.UserRoles.Owner],
    handler: createHandler
  }], GeneratorRequests.Create, [{
    roles: [Enums.UserRoles.Admin, Enums.UserRoles.Owner],
    Mask: GeneratorRead.GeneratorViewModel
  }]))
  // READ
  // Read - No populate
  .get('/read/:id', executor.execute([{
    roles: [Enums.UserRoles.Technician, Enums.UserRoles.Admin, Enums.UserRoles.Owner],
    handler: readHandler
  }], null, [{
    roles: [Enums.UserRoles.Technician, Enums.UserRoles.Admin, Enums.UserRoles.Owner],
    Mask: GeneratorRead.GeneratorViewModel
  }]))
  // Update
  .put('/:id', executor.execute([{
    roles: [Enums.UserRoles.Admin, Enums.UserRoles.Owner],
    handler: updateHandler
  }], GeneratorRequests.Update, null))
  // .delete('/:id', [
  //   acl.restrictToRoles(Enums.UserRoles.Admin),
  //   executor.execute<void, void>(deleteHandler, null)
  // ])
  .post('/:id/contacts', executor.execute([{
    roles: [Enums.UserRoles.Admin, Enums.UserRoles.Owner],
    handler: addContactHandler
  }], ContactRequests.Create, null))
  .put('/:generatorId/contacts/:id', executor.execute([{
    roles: [Enums.UserRoles.Admin, Enums.UserRoles.Owner],
    handler: updateGeneratorContactHandler
  }], ContactRequests.Update, null))
  .delete('/:generatorId/contacts/:id', executor.execute([{
    roles: [Enums.UserRoles.Admin, Enums.UserRoles.Owner],
    handler: deleteGeneratorContactHandler
  }], null, null))

executor.responseBuilder = new DefaultResponseBuilder();

export const generatorRouter = router;
