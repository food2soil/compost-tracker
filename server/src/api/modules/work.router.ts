import { DefaultResponseBuilder } from "../../api/response-builder/default-response-builder.lib";
import { ModuleExecutor } from "../module.executor";
import { Router } from "express";
import { Enums, WorkRead, WorkWithTechnicianRead, WorkRequests, WorkWithHubRead, WorkWithHubAndTechnicianRead } from "mean-au-ts-shared";
import { createHandler, createTechnicianRoleHandler } from "../../application/request-handlers/work/work-create.handler";
import { approveWorkHandler } from "../../application/request-handlers/work/work-approve.handler";
import { undoWorkHandler } from "../../application/request-handlers/work/work-undo.handler";
import { deleteHandler, deleteTechnicianRoleHandler } from "../../application/request-handlers/work/work-delete.handler";
import { listWorkHandler, listWorkWithTechnicianHandler, listWorkWithHubHandler, listWorkWithHubTechnicianHandler } from "../../application/request-handlers/work/work-list.handler";
import { readWorkHandler, readWorkWithTechnicianHandler, readWorkWithHubHandler, readWorkWithHubAndTechnicianHandler } from "../../application/request-handlers/work/work-read.handler";
import { updateHandler, updateTechnicianRoleHandler } from "../../application/request-handlers/work/work-update.handler";

const executor = new ModuleExecutor();
const router = Router();

router
  .get('/list', executor.execute([{
    roles: [Enums.UserRoles.Technician, Enums.UserRoles.Admin, Enums.UserRoles.Owner],
    handler: listWorkHandler
  }], WorkRequests.List, [{
    roles: [Enums.UserRoles.Technician, Enums.UserRoles.Admin, Enums.UserRoles.Owner],
    Mask: WorkRead.WorkViewModel
  }]))
  .get('/list/with-tech', executor.execute([{
    roles: [Enums.UserRoles.Technician, Enums.UserRoles.Admin, Enums.UserRoles.Owner],
    handler: listWorkWithTechnicianHandler
  }], WorkRequests.List, [{
    roles: [Enums.UserRoles.Technician, Enums.UserRoles.Admin, Enums.UserRoles.Owner],
    Mask: WorkWithTechnicianRead.WorkViewModel
  }]))
  .get('/list/with-hub', executor.execute([{
    roles: [Enums.UserRoles.Technician, Enums.UserRoles.Admin, Enums.UserRoles.Owner],
    handler: listWorkWithHubHandler
  }], WorkRequests.List, [{
    roles: [Enums.UserRoles.Technician, Enums.UserRoles.Admin, Enums.UserRoles.Owner],
    Mask: WorkWithHubRead.WorkViewModel
  }]))
  .get('/list/with-hub-tech', executor.execute([{
    roles: [Enums.UserRoles.Technician, Enums.UserRoles.Admin, Enums.UserRoles.Owner],
    handler: listWorkWithHubTechnicianHandler
  }], WorkRequests.List, [{
    roles: [Enums.UserRoles.Technician, Enums.UserRoles.Admin, Enums.UserRoles.Owner],
    Mask: WorkWithHubAndTechnicianRead.WorkViewModel
  }]))
  // Create
  .post('/', executor.execute([{
    roles: [Enums.UserRoles.Admin, Enums.UserRoles.Owner],
    handler: createHandler
  }, {
    roles: [Enums.UserRoles.Technician],
    handler: createTechnicianRoleHandler
  }], WorkRequests.Create, null))
  // Read
  .get('/read/:id', executor.execute([{
    roles: [Enums.UserRoles.Technician, Enums.UserRoles.Admin, Enums.UserRoles.Owner],
    handler: readWorkHandler
  }], null, [{
    roles: [Enums.UserRoles.Technician, Enums.UserRoles.Admin, Enums.UserRoles.Owner],
    Mask: WorkRead.WorkViewModel
  }]))
  .get('/read/:id/with-tech', executor.execute([{
    roles: [Enums.UserRoles.Technician, Enums.UserRoles.Admin, Enums.UserRoles.Owner],
    handler: readWorkWithTechnicianHandler
  }], null, [{
    roles: [Enums.UserRoles.Technician, Enums.UserRoles.Admin, Enums.UserRoles.Owner],
    Mask: WorkWithTechnicianRead.WorkViewModel
  }]))
  .get('/read/:id/with-hub', executor.execute([{
    roles: [Enums.UserRoles.Technician, Enums.UserRoles.Admin, Enums.UserRoles.Owner],
    handler: readWorkWithHubHandler
  }], null, [{
    roles: [Enums.UserRoles.Technician, Enums.UserRoles.Admin, Enums.UserRoles.Owner],
    Mask: WorkWithHubRead.WorkViewModel
  }]))
  .get('/read/:id/with-hub-tech', executor.execute([{
    roles: [Enums.UserRoles.Technician, Enums.UserRoles.Admin, Enums.UserRoles.Owner],
    handler: readWorkWithHubAndTechnicianHandler
  }], null, [{
    roles: [Enums.UserRoles.Technician, Enums.UserRoles.Admin, Enums.UserRoles.Owner],
    Mask: WorkWithHubAndTechnicianRead.WorkViewModel
  }]))
  // Approve
  .post('/:id/approve', executor.execute([{
    roles: [Enums.UserRoles.Admin, Enums.UserRoles.Owner],
    handler: approveWorkHandler
  }], null, null))
  // Undo
  .post('/:id/undo', executor.execute([{
    roles: [Enums.UserRoles.Admin, Enums.UserRoles.Owner],
    handler: undoWorkHandler
  }], null, null))
  // Delete
  .delete('/:id', executor.execute([{
    roles: [Enums.UserRoles.Admin, Enums.UserRoles.Owner],
    handler: deleteHandler
  }, {
    roles: [Enums.UserRoles.Technician],
    handler: deleteTechnicianRoleHandler
  }], null, null))
  // Update
  .put('/:id', executor.execute([{
    roles: [Enums.UserRoles.Admin, Enums.UserRoles.Owner],
    handler: updateHandler
  }, {
    roles: [Enums.UserRoles.Technician],
    handler: updateTechnicianRoleHandler
  }], WorkRequests.Edit, null))

executor.responseBuilder = new DefaultResponseBuilder();

export const workRouter = router;
