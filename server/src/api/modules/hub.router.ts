import { DefaultResponseBuilder } from "../../api/response-builder/default-response-builder.lib";
import { ModuleExecutor } from "../module.executor";
import { Router } from "express";
import { Enums, HubRequests, HubRead, BayRead, HubWithTechniciansRead, HubWithBaysRead, HubWithGeneratorsRead, HubWithTechniciansAndBaysRead, HubWithTechniciansAndGeneratorsRead, HubWithBaysAndGeneratorsRead, HubWithTechniciansBaysAndGeneratorsRead, BatchRead, ContactRequests } from "mean-au-ts-shared";
import { listHandler } from "../../application/request-handlers/hub/hub-list.handler";
import { createHandler } from "../../application/request-handlers/hub/hub-create.handler";
import { updateHandler } from "../../application/request-handlers/hub/hub-update.handler";
import { updateGeneratorsHandler } from "../../application/request-handlers/hub/hub-update-generators.handler";
import { readHubWithTechniciansHandler, readHubWithBaysHandler, readHubWithGeneratorsHandler, readHubWithTechniciansAndBaysHandler, readHubWithTechniciansAndGeneratorsHandler, readHubWithGeneratorsAndBaysHandler, readHubWithTechniciansBaysAndGeneratorsHandler, readHubHandler } from "../../application/request-handlers/hub/read/hub-read.handler";
import { updateTechniciansHandler } from "../../application/request-handlers/hub/hub-update-technicians.handler";
import { hubBaysListHandler } from "../../application/request-handlers/hub/sub-docs/hub-bays-list.handler";
import { hubActiveBatchesListHandler } from "../../application/request-handlers/hub/sub-docs/hub-active-batches-list.handler";
import { addContactHandler } from "../../application/request-handlers/hub/hub-add-contact.handler";
import { deleteHubContactHandler } from "../../application/request-handlers/contact/delete-contact.handler";
import { updateHubContactHandler } from "../../application/request-handlers/contact/update-contact-handler";

const executor = new ModuleExecutor();
const router = Router();

router
  .get('/list', executor.execute([{
    roles: [Enums.UserRoles.Technician, Enums.UserRoles.Admin, Enums.UserRoles.Owner],
    handler: listHandler
  }], null, [{
    roles: [Enums.UserRoles.Technician, Enums.UserRoles.Admin, Enums.UserRoles.Owner],
    Mask: HubRead.HubViewModel
  }]))
  .post('/', executor.execute([{
    roles: [Enums.UserRoles.Admin, Enums.UserRoles.Owner],
    handler: createHandler
  }], HubRequests.Create, [{
    roles: [Enums.UserRoles.Admin, Enums.UserRoles.Owner],
    Mask: HubRead.HubViewModel
  }]))
  // READS
  // Read - No populate
  .get('/read/:id', executor.execute([{
    roles: [Enums.UserRoles.Technician, Enums.UserRoles.Admin, Enums.UserRoles.Owner],
    handler: readHubHandler
  }], null, [{
    roles: [Enums.UserRoles.Technician, Enums.UserRoles.Admin, Enums.UserRoles.Owner],
    Mask: HubRead.HubViewModel
  }]))
  // Read - Populate Technicians
  .get('/read/:id/with-tech', executor.execute([{
    roles: [Enums.UserRoles.Technician, Enums.UserRoles.Admin, Enums.UserRoles.Owner],
    handler: readHubWithTechniciansHandler
  }], null, [{
    roles: [Enums.UserRoles.Technician, Enums.UserRoles.Admin, Enums.UserRoles.Owner],
    Mask: HubWithTechniciansRead.HubViewModel
  }]))
  // Read - Populate Bays
  .get('/read/:id/with-bay', executor.execute([{
    roles: [Enums.UserRoles.Technician, Enums.UserRoles.Admin, Enums.UserRoles.Owner],
    handler: readHubWithBaysHandler
  }], null, [{
    roles: [Enums.UserRoles.Technician, Enums.UserRoles.Admin, Enums.UserRoles.Owner],
    Mask: HubWithBaysRead.HubViewModel
  }]))
  // Read - Populate Generators
  .get('/read/:id/with-gen', executor.execute([{
    roles: [Enums.UserRoles.Technician, Enums.UserRoles.Admin, Enums.UserRoles.Owner],
    handler: readHubWithGeneratorsHandler
  }], null, [{
    roles: [Enums.UserRoles.Technician, Enums.UserRoles.Admin, Enums.UserRoles.Owner],
    Mask: HubWithGeneratorsRead.HubViewModel
  }]))
  // Read - Populate Technicians & Bays
  .get('/read/:id/with-tech-bay', executor.execute([{
    roles: [Enums.UserRoles.Technician, Enums.UserRoles.Admin, Enums.UserRoles.Owner],
    handler: readHubWithTechniciansAndBaysHandler
  }], null, [{
    roles: [Enums.UserRoles.Technician, Enums.UserRoles.Admin, Enums.UserRoles.Owner],
    Mask: HubWithTechniciansAndBaysRead.HubViewModel
  }]))
  // Read - Populate Technicians & Generators
  .get('/read/:id/with-tech-gen', executor.execute([{
    roles: [Enums.UserRoles.Technician, Enums.UserRoles.Admin, Enums.UserRoles.Owner],
    handler: readHubWithTechniciansAndGeneratorsHandler
  }], null, [{
    roles: [Enums.UserRoles.Technician, Enums.UserRoles.Admin, Enums.UserRoles.Owner],
    Mask: HubWithTechniciansAndGeneratorsRead.HubViewModel
  }]))
  // Read - Populate Bays & Generators
  .get('/read/:id/with-bay-gen', executor.execute([{
    roles: [Enums.UserRoles.Technician, Enums.UserRoles.Admin, Enums.UserRoles.Owner],
    handler: readHubWithGeneratorsAndBaysHandler
  }], null, [{
    roles: [Enums.UserRoles.Technician, Enums.UserRoles.Admin, Enums.UserRoles.Owner],
    Mask: HubWithBaysAndGeneratorsRead.HubViewModel
  }]))
  // Read - Populate Technicians & Bays & Generators
  .get('/read/:id/with-tech-bay-gen', executor.execute([{
    roles: [Enums.UserRoles.Technician, Enums.UserRoles.Admin, Enums.UserRoles.Owner],
    handler: readHubWithTechniciansBaysAndGeneratorsHandler
  }], null, [{
    roles: [Enums.UserRoles.Technician, Enums.UserRoles.Admin, Enums.UserRoles.Owner],
    Mask: HubWithTechniciansBaysAndGeneratorsRead.HubViewModel
  }]))
  // SUBDOCS
  // Subdoc List - Bays
  .get('/read/:id/bays', executor.execute([{
    roles: [Enums.UserRoles.Technician, Enums.UserRoles.Admin, Enums.UserRoles.Owner],
    handler: hubBaysListHandler
  }], null, [{
    roles: [Enums.UserRoles.Technician, Enums.UserRoles.Admin, Enums.UserRoles.Owner],
    Mask: BayRead.BayViewModel
  }]))
  // Subdoc List - Active Batches
  .get('/read/:id/active-batches', executor.execute([{
    roles: [Enums.UserRoles.Technician, Enums.UserRoles.Admin, Enums.UserRoles.Owner],
    handler: hubActiveBatchesListHandler
  }], null, [{
    roles: [Enums.UserRoles.Technician, Enums.UserRoles.Admin, Enums.UserRoles.Owner],
    Mask: BatchRead.BatchViewModel
  }]))
  .put('/:id', executor.execute([{
    roles: [Enums.UserRoles.Admin, Enums.UserRoles.Owner],
    handler: updateHandler
  }], null, null))
  .put('/:id/technicians', executor.execute([{
    roles: [Enums.UserRoles.Admin, Enums.UserRoles.Owner],
    handler: updateTechniciansHandler
  }], null, null))
  .put('/:id/generators', executor.execute([{
    roles: [Enums.UserRoles.Admin, Enums.UserRoles.Owner],
    handler: updateGeneratorsHandler
  }], null, null))
  // CONTACT MANAGEMENT
  .post('/:id/contacts', executor.execute([{
    roles: [Enums.UserRoles.Admin, Enums.UserRoles.Owner],
    handler: addContactHandler
  }], ContactRequests.Create, null))
  .put('/:hubId/contacts/:id', executor.execute([{
    roles: [Enums.UserRoles.Admin, Enums.UserRoles.Owner],
    handler: updateHubContactHandler
  }], ContactRequests.Update, null))
  .delete('/:hubId/contacts/:id', executor.execute([{
    roles: [Enums.UserRoles.Admin, Enums.UserRoles.Owner],
    handler: deleteHubContactHandler
  }], null, null))

executor.responseBuilder = new DefaultResponseBuilder();

export const hubRouter = router;
