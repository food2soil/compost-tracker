import { Enums, BatchRead, WorkRead, WorkWithTechnicianRead } from "mean-au-ts-shared";
import { DefaultResponseBuilder } from "../response-builder/default-response-builder.lib";
import { ModuleExecutor } from "../module.executor";
import { Router } from "express";
import { readBatchHandler } from "../../application/request-handlers/batch/batch-read.handler";
import { batchWorkWithTechListHandler, batchWorkListHandler } from "../../application/request-handlers/batch/sub-docs/batch-work-list.handler";
import { listBatchHandler } from "../../application/request-handlers/batch/batch-list.handler";

const executor = new ModuleExecutor();
const router = Router();

router
  .get('/list', executor.execute([{
    roles: [Enums.UserRoles.Technician, Enums.UserRoles.Admin, Enums.UserRoles.Owner],
    handler: listBatchHandler
  }], null, [{
    roles: [Enums.UserRoles.Technician, Enums.UserRoles.Admin, Enums.UserRoles.Owner],
    Mask: BatchRead.BatchViewModel
  }]))
  .get('/read/:id', executor.execute([{
    roles: [Enums.UserRoles.Technician, Enums.UserRoles.Admin, Enums.UserRoles.Owner],
    handler: readBatchHandler
  }], null, [{
    roles: [Enums.UserRoles.Technician, Enums.UserRoles.Admin, Enums.UserRoles.Owner],
    Mask: BatchRead.BatchViewModel
  }]))
  .get('/read/:id/work', executor.execute([{
    roles: [Enums.UserRoles.Technician, Enums.UserRoles.Admin, Enums.UserRoles.Owner],
    handler: batchWorkListHandler
  }], null, [{
    roles: [Enums.UserRoles.Technician, Enums.UserRoles.Admin, Enums.UserRoles.Owner],
    Mask: WorkRead.WorkViewModel
  }]))
  .get('/read/:id/work-with-tech', executor.execute([{
    roles: [Enums.UserRoles.Technician, Enums.UserRoles.Admin, Enums.UserRoles.Owner],
    handler: batchWorkWithTechListHandler
  }], null, [{
    roles: [Enums.UserRoles.Technician, Enums.UserRoles.Admin, Enums.UserRoles.Owner],
    Mask: WorkWithTechnicianRead.WorkViewModel
  }]))

executor.responseBuilder = new DefaultResponseBuilder();

export const batchRouter = router;
