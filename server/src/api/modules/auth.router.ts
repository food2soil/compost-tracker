import { DefaultResponseBuilder } from "../../api/response-builder/default-response-builder.lib";
import { Enums, UserRequests, UserRead } from "mean-au-ts-shared";
import { ModuleExecutor } from "../module.executor";
import { Router } from "express";
import { signInHandler } from "../../application/request-handlers/auth/signin.handler";
import { signOutHandler } from "../../application/request-handlers/auth/signout.handler";
import { sendForgotPasswordHandler } from "../../application/request-handlers/auth/send-forgot-password.handler";
import { testForgotPasswordHandler } from "../../application/request-handlers/auth/test-forgot-password.handler";
import { forgotPasswordHandler } from "../../application/request-handlers/auth/forgot-password.handler";
import { test401Handler, test422Handler } from "../../application/request-handlers/auth/test-error.handler";

const executor = new ModuleExecutor();
const router = Router();

router
  .post('/signin', executor.execute([{
    roles: [Enums.UserRoles.Anonymous],
    handler: signInHandler
  }], UserRequests.SignIn, [{
    roles: [Enums.UserRoles.Technician, Enums.UserRoles.Admin, Enums.UserRoles.Owner],
    Mask: UserRead.UserViewModel
  }]))
  .post('/signout', executor.execute([{
    roles: [Enums.UserRoles.Anonymous, Enums.UserRoles.Technician, Enums.UserRoles.Admin, Enums.UserRoles.Owner],
    handler: signOutHandler
  }], null, null))
  .post('/send-forgot-password', executor.execute([{
    roles: [Enums.UserRoles.Anonymous],
    handler: sendForgotPasswordHandler
  }], UserRequests.SendForgotPassword, null))
  .get('/forgot-password', executor.execute([{
    roles: [Enums.UserRoles.Anonymous],
    handler: testForgotPasswordHandler
  }], UserRequests.TestForgotPassword, null))
  .post('/forgot-password', executor.execute([{
    roles: [Enums.UserRoles.Anonymous],
    handler: forgotPasswordHandler
  }], UserRequests.ForgotPassword, null))
  .get('/test-401', executor.execute([{
    roles: [Enums.UserRoles.Anonymous, Enums.UserRoles.Admin],
    handler: test401Handler
  }], null, null))
  .get('/test-422', executor.execute([{
    roles: [Enums.UserRoles.Anonymous, Enums.UserRoles.Admin],
    handler: test422Handler
  }], null, null))

executor.responseBuilder = new DefaultResponseBuilder();

export const authRouter = router;
