import * as path from 'path';
import * as express from 'express';
import * as morgan from 'morgan';
import * as bodyParser from 'body-parser';
import * as http from 'http';
import { generatorRouter } from './modules/generator.router';
import { hubRouter } from './modules/hub.router';
import { technicianRouter } from './modules/technician.router';
import { invitationRouter } from './modules/invitation.router';
import { workRouter } from './modules/work.router';
import { batchRouter } from './modules/batch.router';
import { config } from '../config/config';
import { logger } from '../lib/winston.lib';
import { authWorkflow } from '../application/strategies/jwt.strategy';
import { authRouter } from './modules/auth.router';
import { meRouter } from './modules/me.router';

class App {
  private express: express.Application;

  constructor() {
    this.express = express();
    this.middleware();
    this.routes();
  }

  start() {
    const server = http.createServer(this.express);
    server.on('error', (err: Error) => {
      logger.error('Could not start server to due the following error')
      logger.error(err.message);
    });

    server.on('listening', () => {
      logger.info(`Started server at ${config.express.host}:${config.express.port}`)
    });

    server.listen(config.express.port, config.express.host);
  }

  private middleware(): void {
    // logger
    this.express.use(morgan('dev'));

    // body parser
    this.express.use(bodyParser.json());
    this.express.use(bodyParser.urlencoded({ extended: true }));

    // Authentication
    this.express.use(/\/api\/(?!auth).*/, authWorkflow);
  }

  private routes() {
    // static content first
    this.express.use('/', express.static(path.resolve('./server/dist/public')));

    // api stuff
    this.express.use('/api/auth', authRouter);
    this.express.use('/api/batch', batchRouter);
    this.express.use('/api/generator', generatorRouter);
    this.express.use('/api/hub', hubRouter);
    this.express.use('/api/me', meRouter);
    this.express.use('/api/technician', technicianRouter);
    this.express.use('/api/invite', invitationRouter);
    this.express.use('/api/work', workRouter);

    // setup the fallback route last so we dont overwrite anything
    this.express.use('*', (req, resp) => {
      resp.sendFile(path.resolve('./server/dist/public/index.html'))
    })
  }
}

export const appRouter = new App();
