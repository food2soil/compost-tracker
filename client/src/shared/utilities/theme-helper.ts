export type Entity = 'collection-point' | 'hub' | 'generator' | 'home' | 'technician' | 'work'
export type Contexts = 'danger' | 'success' | 'warning' | 'primary' | 'secondary' | 'info';

export function typeToIcon(type: Entity) {
  switch (type) {
    case 'collection-point':
      return 'fa-shopping-basket';
    case 'hub':
      return 'fa-leaf';
    case 'generator':
      return 'fa-cutlery';
    case 'home':
      return 'fa-home';
    case 'technician':
      return 'fa-users';
    case 'work':
      return 'fa-clock-o';
    default:
      throw new Error(`The provided type (${type}) is not a valid entity`)
  }
}
