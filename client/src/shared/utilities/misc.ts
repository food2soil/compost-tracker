import { ISelectOption } from "resources/elements/form-group/form-group-select/form-group-select";

export function enumToSelectOptions(Enum: any): ISelectOption[] {
  return Object.keys(Enum).map(k => {
    return {
      value: Enum[k],
      name: Enum[Enum[k]]
    }
  })
}
