import { autoinject } from "aurelia-framework";
import { TechnicianApi } from "../../apis/technician.api";
import { IGridRowProvider } from "resources/elements/grid/grid-row-provider/grid-row-provider";
import { GridColumn } from "resources/elements/grid/grid-column/grid-column";
import { LocalGridRowProvider } from "resources/elements/grid/grid-row-provider/local-grid-row-provider";
import { IGridRowClick, IGridRowClickContext } from "resources/elements/grid/grid-row-action/grid-row-click";
import { DialogController } from "aurelia-dialog";
import { UserRead } from "mean-au-ts-shared";
import { ToggleTd } from "../../grid-templates/toggle-td";
import { IGridConfig } from "resources/elements/grid/grid";

export interface ISelectTechnicianModalActivate {
  initialSelections: UserRead.UserViewModel[]
  postSelect: (selectedTechnicians: UserRead.UserViewModel[]) => Promise<void>
}

@autoinject
export class SelectTechnicianModal {

  isBusy: boolean
  postSelect: (selectedGenerators: UserRead.UserViewModel[]) => Promise<void>
  allTechnicians: (UserRead.UserViewModel & IGridRowClickContext)[]
  gridConfig: IGridConfig<UserRead.UserViewModel | IGridRowClickContext>

  constructor(
    private technicianApi: TechnicianApi,
    public dialogController: DialogController
  ) { }

  selectTechnician(row: UserRead.UserViewModel & IGridRowClickContext) {
    row.__gridRow_isActive = !row.__gridRow_isActive;
  }

  ok() {
    this.isBusy = true;
    
    this.postSelect(this.allTechnicians.filter(x => x.__gridRow_isActive))
      .then(() => this.dialogController.ok())
      .catch(() => this.dialogController.cancel())
  }

  activate(params: ISelectTechnicianModalActivate) {
    this.postSelect = params.postSelect || (() => Promise.resolve())

    return this.technicianApi.list().then(techs => {
      this.allTechnicians = <(UserRead.UserViewModel & IGridRowClickContext)[]>techs;

      if (params.initialSelections) {
        this.allTechnicians.forEach(x => {
          if (params.initialSelections.some(y => y.id === x.id)) {
            this.selectTechnician(<UserRead.UserViewModel & IGridRowClickContext>x);
            }
        })
      }

      this.gridConfig = {
        rowProvider: new LocalGridRowProvider<UserRead.UserViewModel & IGridRowClickContext>(this.allTechnicians),
        columns: [
          new GridColumn<IGridRowClickContext>({
            ViewModel: ToggleTd,
            tdClass: 'td-fit-content'
          }),
          new GridColumn<UserRead.UserViewModel>({
            title: 'Name',
            key: 'displayName',
            isSearchable: true
          })
        ],
        rowClick: { action: ToggleTd.toggleRow }
      }
    });
  }
}
