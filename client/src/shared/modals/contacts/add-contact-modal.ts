import { ContactRequests, Validation } from "mean-au-ts-shared";
import { autoinject, ComponentDetached } from "aurelia-framework";
import { DialogController } from 'aurelia-dialog';
import { IValidator } from "../../services/validator/validator.service";
import { FormWrap } from "resources/elements/form-wrap/form-wrap";
import { ValidationRules } from "aurelia-validation";

export interface IAddContactModalModel {
  owningDocId: string
  addContactEndPoint: (owningDocId: string, contact: ContactRequests.Create) => Promise<void>
  onSaved?: () => Promise<void>
}

@autoinject
export class AddContactModal implements ComponentDetached {
  contactBody: ContactRequests.Create = new ContactRequests.Create();
  form: FormWrap

  isAdding: boolean
  owningDocId: string
  addContactEndPoint: (owningDocId: string, contact: ContactRequests.Create) => Promise<void>
  onSaved: () => Promise<void>

  constructor(
    public dialogController: DialogController,
    private validator: IValidator
  ) {
    Validation.ensureDecoratorsOn(ContactRequests.Create, ValidationRules);
  }

  save() {
    this.isAdding = true;

    this.validator.validateObject(this.contactBody, true)
      .then(() => this.addContactEndPoint(this.owningDocId, this.contactBody))
      .then(() => this.onSaved())
      .then(newContact => this.dialogController.ok(newContact))
      .catch(() => this.isAdding = false)
  }

  activate(params: IAddContactModalModel) {
    this.owningDocId = params.owningDocId
    this.addContactEndPoint = params.addContactEndPoint || (() => Promise.resolve())
    this.onSaved = params.onSaved || (() => Promise.resolve())
  }

  detached() {
    this.form.clear();
  }
}
