import { ContactRequests, Validation, ContactRead } from "mean-au-ts-shared";
import { autoinject, ComponentDetached, transient } from "aurelia-framework";
import { DialogController } from 'aurelia-dialog';
import { IValidator } from "../../services/validator/validator.service";
import { FormWrap } from "resources/elements/form-wrap/form-wrap";
import { ValidationRules } from "aurelia-validation";

export interface IEditContactModalModel {
  owningDocId: string
  editContactEndPoint: (owningDocId: string, contact: ContactRequests.Update) => Promise<void>
  onSaved?: () => Promise<void>
  toEdit: ContactRead.ContactViewModel
}

@autoinject
@transient()
export class EditContactModal {
  contactBody: ContactRequests.Update = new ContactRequests.Update();
  form: FormWrap

  isSaving: boolean
  owningDocId: string
  editContactEndPoint: (owningDocId: string, contact: ContactRequests.Update) => Promise<void>
  onSaved: () => Promise<void>

  constructor(
    public dialogController: DialogController,
    private validator: IValidator
  ) {
    Validation.ensureDecoratorsOn(ContactRequests.Update, ValidationRules);
  }

  save() {
    this.isSaving = true;

    this.validator.validateObject(this.contactBody, true)
      .then(() => this.editContactEndPoint(this.owningDocId, this.contactBody))
      .then(() => this.onSaved())
      .then(() => this.dialogController.ok(this.contactBody))
      .catch(() => this.isSaving = false)
  }

  activate(params: IEditContactModalModel) {
    this.owningDocId = params.owningDocId
    this.editContactEndPoint = params.editContactEndPoint || (() => Promise.resolve())
    this.onSaved = params.onSaved || (() => Promise.resolve())

    Object.keys(this.contactBody).forEach(k => this.contactBody[k] = params.toEdit[k]);
  }
}
