import { ContactRequests } from "mean-au-ts-shared";
import { autoinject, ComponentDetached } from "aurelia-framework";
import { DialogController } from 'aurelia-dialog';
import { FormWrap } from "resources/elements/form-wrap/form-wrap";
import { RoutableComponentActivate } from "aurelia-router";

export interface IConfirmModalModel {
  prompt?: string
  title?: string
  okLabel?: string
  okDelegate?: () => Promise<void>
  closeOnError?: boolean
}

@autoinject
export class ConfirmModal implements RoutableComponentActivate {
  prompt: string
  title: string
  okLabel: string
  okDelegate: () => Promise<void>
  isWorking: boolean
  closeOnError: boolean

  constructor(
    public dialogController: DialogController,
  ) {

  }

  ok() {
    this.isWorking = true

    this.okDelegate()
      .then(() => this.dialogController.ok())
      .catch(() => {
        if (this.closeOnError) this.dialogController.cancel()

        this.isWorking = false
      })
  }

  activate(params: any): void {
    this.prompt = params.prompt || 'Are you sure?'
    this.title = params.title || 'Confirm Action'
    this.okLabel = params.okLabel || 'Yes'
    this.okDelegate = params.okDelegate || (() => Promise.resolve())
    this.closeOnError = params.closeOnError
  }
}
