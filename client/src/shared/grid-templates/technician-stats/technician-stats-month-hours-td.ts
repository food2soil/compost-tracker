import { ITdTemplate, ITdModel } from "resources/elements/grid/grid-column/grid-column";
import { getThisMonthsStats, getLastMonthsStats, UserRead } from "mean-au-ts-shared";

export interface IMonthStatsData {
  lastMonth?: boolean
}

export class TechnicianStatsMonthHoursTd implements ITdTemplate<UserRead.UserViewModel, IMonthStatsData> {
  row: UserRead.UserViewModel
  hours: number
  
  activate(params: ITdModel<UserRead.UserViewModel, IMonthStatsData>) {
    this.row = params.row

    let stats = params.extraData.lastMonth
      ? getLastMonthsStats(this.row.stats.monthly)
      : getThisMonthsStats(this.row.stats.monthly)

    this.hours = stats ? stats.stats.hours : 0
  }
}
