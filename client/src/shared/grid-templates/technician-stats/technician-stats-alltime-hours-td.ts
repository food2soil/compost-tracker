import { ITdTemplate, ITdModel } from "resources/elements/grid/grid-column/grid-column";
import { UserRead } from "mean-au-ts-shared";

export class TechnicianStatsAllTimeHoursTd implements ITdTemplate<UserRead.UserViewModel, void> {
  row: UserRead.UserViewModel
  
  activate(params: ITdModel<UserRead.UserViewModel, void>) {
    this.row = params.row
  }
}
