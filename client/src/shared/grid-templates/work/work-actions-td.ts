import { ITdTemplate, ITdModel } from "resources/elements/grid/grid-column/grid-column";
import { Enums, BayRead, WorkRead, WorkReadBase } from "mean-au-ts-shared";
import { transient } from "aurelia-framework";

export interface IWorkActionsData {
  bays: BayRead.BayViewModel[]
}

@transient()
export class WorkActionsTd implements ITdTemplate<WorkReadBase.BaseWorkViewModel, IWorkActionsData> {
  row: WorkReadBase.BaseWorkViewModel
  actions: string[]

  activate(params: ITdModel<WorkReadBase.BaseWorkViewModel, IWorkActionsData>) {
    this.actions = []

    let filledBays = params.row.bayEvents
      .filter(e => e.type === Enums.BayEvent.Fill)
      .map(e => params.extraData.bays.find(b => b.id === e.bay).name);
      
    if (filledBays.length > 0) {
      this.actions.push(
        `Filled - ${filledBays.join(', ')}`
      )
    }

    let turnedBays = params.row.bayEvents
      .filter(e => e.type === Enums.BayEvent.Turn)
      .map(e => params.extraData.bays.find(b => b.id === e.bay).name);
      
    if (turnedBays.length > 0) {
      this.actions.push(
        `Turned - ${turnedBays.join(', ')}`
      )
    }

    let harvestedBays = params.row.bayEvents
      .filter(e => e.type === Enums.BayEvent.Harvest)
      .map(e => params.extraData.bays.find(b => b.id === e.bay).name);
      
    if (harvestedBays.length > 0) {
      this.actions.push(
        `Harvested - ${harvestedBays.join(', ')}`
      )
    }
  }
}
