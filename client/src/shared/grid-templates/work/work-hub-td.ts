import { ITdTemplate, ITdModel } from "resources/elements/grid/grid-column/grid-column";
import { WorkReadBase, WorkWithHubRead, WorkWithHubAndTechnicianRead } from "mean-au-ts-shared";

export class WorkHubTd implements ITdTemplate<WorkWithHubRead.WorkViewModel & WorkWithHubAndTechnicianRead.WorkViewModel, void> {
  row: WorkWithHubRead.WorkViewModel & WorkWithHubAndTechnicianRead.WorkViewModel
  
  activate(params: ITdModel<WorkWithHubRead.WorkViewModel & WorkWithHubAndTechnicianRead.WorkViewModel, void>) {
    this.row = params.row
  }
}
