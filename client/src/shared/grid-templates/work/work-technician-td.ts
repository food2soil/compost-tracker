import { ITdTemplate, ITdModel } from "resources/elements/grid/grid-column/grid-column";
import { WorkReadBase, WorkWithTechnicianRead } from "mean-au-ts-shared";

export class WorkTechnicianTd implements ITdTemplate<WorkWithTechnicianRead.WorkViewModel, void> {
  row: WorkWithTechnicianRead.WorkViewModel
  
  activate(params: ITdModel<WorkWithTechnicianRead.WorkViewModel, void>) {
    this.row = params.row
  }
}
