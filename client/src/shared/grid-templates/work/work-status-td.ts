import { ITdTemplate, ITdModel } from "resources/elements/grid/grid-column/grid-column";
import { WorkRead, WorkWithHubRead, WorkWithHubAndTechnicianRead } from "mean-au-ts-shared";

export class WorkStatusTd implements ITdTemplate<WorkRead.WorkViewModel & WorkWithHubRead.WorkViewModel & WorkWithHubAndTechnicianRead.WorkViewModel, void> {
  row: WorkRead.WorkViewModel & WorkWithHubRead.WorkViewModel & WorkWithHubAndTechnicianRead.WorkViewModel
  
  activate(params: ITdModel<WorkRead.WorkViewModel & WorkWithHubRead.WorkViewModel & WorkWithHubAndTechnicianRead.WorkViewModel, void>) {
    this.row = params.row
  }
}
