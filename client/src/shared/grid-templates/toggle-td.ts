import { ITdTemplate, ITdModel } from "resources/elements/grid/grid-column/grid-column";
import { IGridRowClickContext } from "resources/elements/grid/grid-row-action/grid-row-click";

export class ToggleTd implements ITdTemplate<any & IGridRowClickContext, void> {
  row: IGridRowClickContext;
  
  activate(params: ITdModel<any & IGridRowClickContext, void>) {
    this.row = params.row
  }

  static toggleRow(row: IGridRowClickContext, rows: IGridRowClickContext[]) {
    row.__gridRow_isActive = !row.__gridRow_isActive
  }
}
