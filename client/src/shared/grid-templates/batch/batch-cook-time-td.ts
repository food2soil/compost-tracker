import { ITdTemplate, ITdModel } from "resources/elements/grid/grid-column/grid-column";
import { BatchRead, BatchShared, Enums, Time } from "mean-au-ts-shared";

export class BatchCookTimeTd implements ITdTemplate<BatchRead.BatchViewModel, void> {
  row: BatchRead.BatchViewModel
  cookTimeWeeks?: string
  
  activate(params: ITdModel<BatchRead.BatchViewModel, void>) {
    this.row = params.row

    let lastEvent = BatchShared.getLastEvent(this.row)
    let endDate = lastEvent && lastEvent.type === Enums.BayEvent.Harvest
      ? lastEvent.date
      : lastEvent
        ? new Date()
        : null
    this.cookTimeWeeks = endDate 
      ? ((endDate.valueOf() - this.row.created.valueOf()) / Time.MilliSecondsPerWeek).toFixed(1)
      : null
  }
}
