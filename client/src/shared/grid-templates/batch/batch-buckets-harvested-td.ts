import { ITdTemplate, ITdModel } from "resources/elements/grid/grid-column/grid-column";
import { BatchRead, BatchShared, Enums, Time } from "mean-au-ts-shared";

export class BatchBucketsHarvestedTd implements ITdTemplate<BatchRead.BatchViewModel, void> {
  row: BatchRead.BatchViewModel
  
  activate(params: ITdModel<BatchRead.BatchViewModel, void>) {
    this.row = params.row
  }
}
