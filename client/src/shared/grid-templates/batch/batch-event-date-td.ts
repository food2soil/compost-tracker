import { ITdTemplate, ITdModel } from "resources/elements/grid/grid-column/grid-column";
import { BatchRead, Enums, BatchShared } from "mean-au-ts-shared";

export interface IBatchEventDateData {
  type: Enums.BayEvent
}

export class BatchEventDateTd implements ITdTemplate<BatchRead.BatchViewModel, IBatchEventDateData> {
  row: BatchRead.BatchViewModel
  date: Date

  activate(params: ITdModel<BatchRead.BatchViewModel, IBatchEventDateData>) {
      this.row = params.row
      
      let event = BatchShared.getLastEvent(this.row, params.extraData.type)
      this.date = event ? event.date : null
  }
}
