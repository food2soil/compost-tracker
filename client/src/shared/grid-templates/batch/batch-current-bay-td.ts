import { ITdTemplate, ITdModel } from "resources/elements/grid/grid-column/grid-column";
import { Enums, BayRead, BatchRead, BatchShared } from "mean-au-ts-shared";
import { transient } from "aurelia-framework";

export interface IBatchCurrentBayData {
  bays: BayRead.BayViewModel[]
}

@transient()
export class BatchCurrentBayTd implements ITdTemplate<BatchRead.BatchViewModel, IBatchCurrentBayData> {
  row: BatchRead.BatchViewModel
  currentBayName: string

  activate(params: ITdModel<BatchRead.BatchViewModel, IBatchCurrentBayData>) {
    this.row = params.row

    let lastEvent = BatchShared.getLastEvent(this.row)
    this.currentBayName = lastEvent
      ? params.extraData.bays.find(bay => bay.id === lastEvent.endBay).name
      : null
  }
}
