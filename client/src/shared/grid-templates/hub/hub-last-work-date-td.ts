import { ITdTemplate, ITdModel } from "resources/elements/grid/grid-column/grid-column";
import { HubRead, Time } from "mean-au-ts-shared";

export class HubLastWorkDateTd implements ITdTemplate<HubRead.HubViewModel, void> {
  row: HubRead.HubViewModel
  dateClass: 'text-danger' | 'text-warning' | 'text-success'
  
  activate(params: ITdModel<HubRead.HubViewModel, void>) {
    this.row = params.row

    if (!this.row.stats.lastWork) return
    if (this.row.stats.lastWork && !this.row.stats.lastWork.isApproved) {
      this.dateClass = 'text-warning'
    } else if (this.row.stats.lastWork) {
      this.dateClass = Date.now() - this.row.stats.lastWork.date.valueOf() > 3 * Time.MilliSecondsPerWeek
        ? 'text-danger'
        : Date.now() - this.row.stats.lastWork.date.valueOf() > 2 * Time.MilliSecondsPerWeek
          ? 'text-warning' 
          : 'text-success'
      }
  }
}
