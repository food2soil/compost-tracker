import { ITdTemplate, ITdModel } from "resources/elements/grid/grid-column/grid-column";
import { InvitationRead, HubReadBase } from "mean-au-ts-shared";

export interface IHubTechnicianStatusData {
  pendingInvites: InvitationRead.InvitationViewModel[]
}

export class HubTechnicianStatusTd implements ITdTemplate<HubReadBase.BaseHubViewModel, IHubTechnicianStatusData> {
  row: HubReadBase.BaseHubViewModel
  pendingTechnicianNames: string[]
  
  activate(params: ITdModel<HubReadBase.BaseHubViewModel, IHubTechnicianStatusData>) {
    this.row = params.row

    this.pendingTechnicianNames = params.extraData.pendingInvites
      .filter(x => x.hubs.some(h => h === this.row.id))
      .map(x => `${x.firstName} ${x.lastName}`)
  }
}
