import { ITdTemplate, ITdModel } from "resources/elements/grid/grid-column/grid-column";
import { HubRead } from "mean-au-ts-shared";

export class CreateWorkTd implements ITdTemplate<HubRead.HubViewModel, void> {
  row: HubRead.HubViewModel
  
  activate(params: ITdModel<HubRead.HubViewModel, void>) {
    this.row = params.row
  }
}
