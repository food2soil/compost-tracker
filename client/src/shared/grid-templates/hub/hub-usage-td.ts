import { ITdTemplate, ITdModel } from "resources/elements/grid/grid-column/grid-column";
import { HubRead, Time } from "mean-au-ts-shared";

export class HubUsageTd implements ITdTemplate<HubRead.HubViewModel, void> {
  row: HubRead.HubViewModel
  
  activate(params: ITdModel<HubRead.HubViewModel, void>) {
    this.row = params.row
  }
}
