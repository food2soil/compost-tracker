import { ITdTemplate, ITdModel } from "resources/elements/grid/grid-column/grid-column";
import { Enums, BaseInvitationRead } from "mean-au-ts-shared";

export class InvitationRolesTd implements ITdTemplate<BaseInvitationRead.InvitationViewModel, void> {
  row: BaseInvitationRead.InvitationViewModel;
  roles: string[]
  
  activate(params: ITdModel<BaseInvitationRead.InvitationViewModel, void>) {
    this.row = params.row
    this.roles = this.row.roles.map(x => Enums.UserRoles[x])
  }
}
