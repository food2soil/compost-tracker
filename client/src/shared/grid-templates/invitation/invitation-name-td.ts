import { ITdTemplate, ITdModel } from "resources/elements/grid/grid-column/grid-column";
import { BaseInvitationRead } from "mean-au-ts-shared";

export class InvitationNameTd implements ITdTemplate<BaseInvitationRead.InvitationViewModel, void> {
  row: BaseInvitationRead.InvitationViewModel;
  
  activate(params: ITdModel<BaseInvitationRead.InvitationViewModel, void>) {
    this.row = params.row
  }
}
