import { ITdTemplate, ITdModel } from "resources/elements/grid/grid-column/grid-column";
import { InvitationWithHubsRead, BaseInvitationRead } from "mean-au-ts-shared";

export class InvitationHubsTd implements ITdTemplate<BaseInvitationRead.IHaveHubs, void> {
  row: BaseInvitationRead.IHaveHubs
  hubNames: string[]
  
  activate(params: ITdModel<BaseInvitationRead.IHaveHubs, void>) {
    this.row = params.row
    this.hubNames = this.row.hubs.map(x => x.name)
  }
}
