import { ITdTemplate, ITdModel } from "resources/elements/grid/grid-column/grid-column";
import { InvitationRead, BaseInvitationRead } from "mean-au-ts-shared";
import { autoinject } from "aurelia-framework";

export interface IInvitationActionsData {
  resendInvitation: (inviteId: string) => Promise<void>
  deleteInvitation: (inviteId: string) => Promise<void>
}

@autoinject
export class InvitationActionsTd implements ITdTemplate<BaseInvitationRead.InvitationViewModel, IInvitationActionsData> {
  row: BaseInvitationRead.InvitationViewModel;
  isDeleting: boolean
  isResending: boolean
  resendInvitation: (inviteId: string) => Promise<void>
  deleteInvitation: (inviteId: string) => Promise<void>

  resendInvite(inviteId: string) {
    this.isResending = true;

    return this.resendInvitation(inviteId)
      .then(() => this.isResending = false)
      .catch(() => this.isResending = false)
  }

  deleteInvite(inviteId: string) {
    this.isDeleting = true;

    return this.deleteInvitation(inviteId)
      .then(() => this.isDeleting = false)
      .catch(() => this.isDeleting = false)
  }
  
  activate(params: ITdModel<BaseInvitationRead.InvitationViewModel, IInvitationActionsData>) {
    this.row = params.row
    this.resendInvitation = params.extraData.resendInvitation || (() => Promise.resolve())
    this.deleteInvitation = params.extraData.deleteInvitation || (() => Promise.resolve())
  }
}
