import { ValidateResult } from "aurelia-validation";

export abstract class IValidator {
  abstract validateObject(obj: object, toastError: boolean): Promise<ValidateResult[]>;
}

export class ValidationError extends Error {
  constructor(
    public errors: ValidateResult[]
  ) {
    super(errors[0].message);
  }
}
