import { IEnv } from "config/env.config";
import { autoinject, computedFrom, Aurelia, PLATFORM } from "aurelia-framework";
import { GeneralDto, Enums, UserRead, UserRequests } from "mean-au-ts-shared";
import { Router } from "aurelia-router";
import { IAuth } from "shared/services/auth/auth.service";
import { ICache } from "shared/services/cache/cache.service";
import { AuthApi } from "../../apis/auth.api";
import { IHttp } from "shared/services/http/http.service";
import { MeApi } from "../../apis/me.api";

@autoinject
export class JwtAuth implements IAuth {

  signInRedirect: string;
  
  private authAlteredFlag: boolean;

  @computedFrom('authAlteredFlag')
  get currentUser(): UserRead.UserViewModel {
    return this.cache.get(ICache.Mode.Global, this.env.localStorage.userKey);
  }

  @computedFrom('authAlteredFlag')
  get isAdmin(): boolean {
    if (!this.currentUser) return false

    return this.currentUser.roles
      .some(x => x === Enums.UserRoles.Admin || x === Enums.UserRoles.Owner)
  }

  constructor(
    private cache: ICache,
    private env: IEnv,
    private aurelia: Aurelia,
    private router: Router,
    private meApi: MeApi,
    private authApi: AuthApi,
    private http: IHttp
  ) { 
    this.http.authHook = this.authorizeRequest.bind(this)
  }

  authorizeRequest(request: Request): Request {
    let token = this.cache.get(ICache.Mode.Global, this.env.localStorage.authKey);
    
    if (token == null) return request;
    request.headers.set('Authorization', `JWT ${token}`)

    return request;
  }

  signOut(goToSignIn: boolean = false): Promise<void> {
    if (!this.currentUser) return Promise.resolve();

    this.cache.clear();
    this.authAlteredFlag = !this.authAlteredFlag
    
    return this.authApi.signout()
      .then(() => this.aurelia.setRoot(PLATFORM.moduleName('shells/anonymous/index')))
      .then(() => {
        if (goToSignIn) {
          let route = this.signInRedirect
            ? `signin?redirect=${this.signInRedirect}`
            : 'signin'
    
          this.router.navigate(route, { replace: true, trigger: true })
        } else {
          this.router.navigate('/', { replace: true, trigger: true });
        }
      });
  }

  signIn(credentials: UserRequests.SignIn, route?: string): Promise<void> {
    if (this.currentUser) return;

    return this.authApi.signin(credentials)
      .then(user => {
        this.cache.set(ICache.Mode.Global, this.env.localStorage.userKey, user);
        this.authAlteredFlag = !this.authAlteredFlag
        this.router.navigate(route || '/', { replace: true, trigger: false })

        return this.aurelia.setRoot(PLATFORM.moduleName('shells/admin/index'))
      })
      .then(() => undefined)
  }

  processResponse(response: Response): Response {
    response.clone().json().then((body: GeneralDto.SuccessResponseBody) => {
      if (this.currentUser && body.handledAs === Enums.UserRoles.Anonymous) {
        this.signOut();
        return response;
      }

      // Save the token if we got a new one
      if (body.token) {
        this.cache.set(ICache.Mode.Global, this.env.localStorage.authKey, body.token);
      }
    })

    return response;
  }

  refreshUserInfo() {
    return this.meApi.me()
      .then(user => {
        this.cache.set(ICache.Mode.Global, this.env.localStorage.userKey, user);
        this.authAlteredFlag = !this.authAlteredFlag
      })
      .catch(err => {
        return this.signOut()
      })
  }
}
