import { UserRead, UserRequests, Enums } from "mean-au-ts-shared";
import { Redirect } from "aurelia-router";

export abstract class IAuth { 
  signInRedirect: string

  abstract get currentUser(): UserRead.UserViewModel
  abstract get isAdmin(): boolean 

  abstract authorizeRequest(req: Request): Request
  abstract processResponse(response: Response): Response
  abstract signIn(credentials: UserRequests.SignIn, route?: string): Promise<void>
  abstract signOut(goToSignIn?: boolean): Promise<void>
  abstract refreshUserInfo(): Promise<void>
}
