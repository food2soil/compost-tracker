import { autoinject } from "aurelia-framework";
import { IAuth } from "../../auth/auth.service";
import { Router } from "aurelia-router";
import { IHttp } from "../http.service";
import * as toastr from 'toastr'

@autoinject
export class FetchResponseErrorEventHandler {
  constructor(
    private auth: IAuth,
    private http: IHttp
  ) { 
    this.http.onResponseError((response: Response) => {
      this._handleResponse(response)
    })
  }

  private _handle401(response: Response) {
    this.auth.signOut(true)
  }

  private _handleResponse(response: Response) {
    if (response.status === 401) {
      this._handle401(response)
    }

    response.clone().json().then(body =>{
      toastr.error(body.message)
    })
  }
}
