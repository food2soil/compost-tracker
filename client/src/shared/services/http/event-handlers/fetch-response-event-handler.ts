import { autoinject } from "aurelia-framework";
import { IAuth } from "shared/services/auth/auth.service";
import { IHttp } from "../http.service";

@autoinject
export class FetchResponseEventHandler {
  constructor(
    private auth: IAuth,
    private http: IHttp
  ) { 
    this.http.onResponse((response) => {
      this._handleResponse(response)
    })
  }

  private _handleResponse(response) {
    return this.auth.processResponse(response);
  }
}
