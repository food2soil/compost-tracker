import { GeneralDto } from 'mean-au-ts-shared'

export abstract class IHttp {
  abstract authHook: (request: Request) => Request

  abstract get(url: string, params?: object, squashErrorToast?: boolean): Promise<GeneralDto.SuccessResponseBody>;
  abstract post(url: string, body?: object, squashErrorToast?: boolean): Promise<GeneralDto.SuccessResponseBody>;
  abstract delete(url: string, body?: object, squashErrorToast?: boolean): Promise<GeneralDto.SuccessResponseBody>;
  abstract put(url: string, body?: object, squashErrorToast?: boolean): Promise<GeneralDto.SuccessResponseBody>;

  abstract onResponse(callback: (response: Response) => void)
  abstract onResponseError(callback: (response: Response) => void)
}
