import 'whatwg-fetch';
import { autoinject } from 'aurelia-framework';
import { HttpClient, json } from 'aurelia-fetch-client';
import { IHttp } from './http.service';
import { buildQueryString } from 'aurelia-path';
import { GeneralDto } from 'mean-au-ts-shared';
import * as toastr from 'toastr';
import { EventAggregator } from 'aurelia-event-aggregator';

enum Methods {
  GET = 'get',
  POST = 'post',
  PUT = 'put',
  DELETE = 'delete'
}

@autoinject
export class Fetch implements IHttp {
  private static readonly ResponseEventId: string = 'event-http-response'
  private static readonly ResponseErrorEventId: string = 'event-http-response-error'

  authHook: (request: Request) => Request

  constructor(
    private httpClient: HttpClient,
    private eventAggregator: EventAggregator
  ) {
    this.configure();
  }

  private configure() {
    this.httpClient.configure(config => {
      config
        .useStandardConfiguration()
        .withDefaults({
          credentials: 'same-origin',
          headers: {
            'X-Requested-With': 'Fetch'
          }
        })
        .withInterceptor({
          request: (request: Request) => {
            if (this.authHook) {
              request = this.authHook(request)
            }

            return request
          },
          response: (response: Response) => {
            this.eventAggregator.publish(Fetch.ResponseEventId, response)
            return response
          },
          responseError: (response: Response) => {
            this.eventAggregator.publish(Fetch.ResponseErrorEventId, response)

            return response.json()
              .then(body => {
                throw new Error(body.message)
              })
          }
        });

        return config;
    });
  }

  private sendRequest(url: string, method: string, body: object, squashErrorToast: boolean = false): Promise<GeneralDto.SuccessResponseBody> {
    var init = {
      method: method,
      body: body ? json(body) : undefined
    }

    return this.httpClient.fetch(url, init)
      .then((response: Response) => {
        return response.json();
      })
      .catch((error: Error) => { throw error });
  }

  get(url: string, params?: object, squashErrorToast: boolean = false): Promise<GeneralDto.SuccessResponseBody> {
    let stringifiedParams = buildQueryString(params);
    return this.sendRequest(`${url}?${stringifiedParams}`, Methods.GET, null, squashErrorToast);
  }

  post(url: string, body?: object, squashErrorToast: boolean = false): Promise<GeneralDto.SuccessResponseBody> {
    return this.sendRequest(url, Methods.POST, body, squashErrorToast);
  }

  delete(url: string, body?: object, squashErrorToast: boolean = false): Promise<GeneralDto.SuccessResponseBody> {
    return this.sendRequest(url, Methods.DELETE, body, squashErrorToast);
  }

  put(url: string, body?: object, squashErrorToast: boolean = false): Promise<GeneralDto.SuccessResponseBody> {
    return this.sendRequest(url, Methods.PUT, body, squashErrorToast);
  }

  onResponse(callback: (response: Response) => void) {
    this.eventAggregator.subscribe(Fetch.ResponseEventId, callback)
  }

  onResponseError(callback: (response: Response) => void) {
    this.eventAggregator.subscribe(Fetch.ResponseErrorEventId, callback)
  }
}
