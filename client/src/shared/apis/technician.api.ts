import { IHttp } from "../services/http/http.service";
import { UserRead, Utilities } from "mean-au-ts-shared";
import { autoinject } from "aurelia-framework";

@autoinject
export class TechnicianApi {
  get baseUrl() {
    return 'api/technician/';
  }

  constructor(
    private http: IHttp
  ) { }

  list(): Promise<UserRead.UserViewModel[]> {
    return this.http.get(`${this.baseUrl}list`)
      .then(responseBody => {
        return Utilities.castToArray(
          responseBody.data, UserRead.UserViewModel)
      })
  }

  read(technicianId: string): Promise<UserRead.UserViewModel> {
    return this.http.get(`${this.baseUrl}read/${technicianId}`)
      .then(responseBody => {
        return Utilities.castTo(
          responseBody.data, UserRead.UserViewModel)
      })
  }
}
