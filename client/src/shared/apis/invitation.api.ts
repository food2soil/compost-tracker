import { autoinject } from "aurelia-framework";
import { UserRequests, InvitationRequests, InvitationRead, Utilities, InvitationWithInvitedByRead, InvitationWithInvitedByAndHubsRead, InvitationWithHubsRead } from "mean-au-ts-shared";
import { IHttp } from "shared/services/http/http.service";
import { IAuth } from "shared/services/auth/auth.service";

@autoinject
export class InvitationApi {
  get baseUrl() {
    return 'api/invite/';
  }

  constructor(
    private http: IHttp,
    private auth: IAuth
  ) { }

  list(): Promise<InvitationRead.InvitationViewModel[]> {
    return this.http.get(`${this.baseUrl}list`)
      .then(response => {
        return Utilities.castToArray(
          response.data, InvitationRead.InvitationViewModel)
      });
  }

  listWithHubs(): Promise<InvitationWithHubsRead.InvitationViewModel[]> {
    return this.http.get(`${this.baseUrl}list/with-hubs`)
      .then(response => {
        return Utilities.castToArray(
          response.data, InvitationWithHubsRead.InvitationViewModel)
      });
  }

  read(inviteToken: string): Promise<InvitationRead.InvitationViewModel> {
    return this.http.get(`${this.baseUrl}read/${inviteToken}`)
      .then(response => {
        return Utilities.castTo(
          response.data, InvitationRead.InvitationViewModel)
      });
  }

  readWithInvitedBy(inviteToken: string): Promise<InvitationWithInvitedByRead.InvitationViewModel> {
    return this.http.get(`${this.baseUrl}read/${inviteToken}/with-invited-by`)
      .then(response => {
        return Utilities.castTo(
          response.data, InvitationWithInvitedByRead.InvitationViewModel)
      });
  }

  readWithInvitedByAndHubs(inviteToken: string): Promise<InvitationWithInvitedByAndHubsRead.InvitationViewModel> {
    return this.http.get(`${this.baseUrl}read/${inviteToken}/with-invited-by-hubs`)
      .then(response => {
        return Utilities.castTo(
          response.data, InvitationWithInvitedByAndHubsRead.InvitationViewModel)
      });
  }

  checkEmail(email: string): Promise<boolean> {
    let query = new InvitationRequests.CheckEmail()
    query.email = email

    return this.http.get(`${this.baseUrl}check-email`, query)
      .then(response => response.data)
  }

  inviteTechnician(body: InvitationRequests.InviteTechnician): Promise<void> {
    return this.http.post(`${this.baseUrl}send`, body)
      .then(response => undefined);
  }

  claimInvite(body: InvitationRequests.ClaimInvite): Promise<void> {
    return this.http.post(`${this.baseUrl}claim`, body)
      .then(response => undefined);
  }

  resendInvite(inviteId: string): Promise<void> {
    return this.http.post(`${this.baseUrl}pending/${inviteId}`)
      .then(response => undefined);
  }

  deleteInvite(inviteId: string): Promise<void> {
    return this.http.delete(`${this.baseUrl}pending/${inviteId}`)
      .then(response => undefined);
  }
}
