import { IHttp } from "../services/http/http.service";
import { Utilities, WorkRequests, WorkRead, WorkWithTechnicianRead, WorkWithHubRead, WorkWithHubAndTechnicianRead } from "mean-au-ts-shared";
import { autoinject } from "aurelia-framework";

@autoinject
export class WorkApi {
  get baseUrl() {
    return 'api/work/';
  }

  constructor(
    private http: IHttp
  ) { }

  list(query?: WorkRequests.List): Promise<WorkRead.WorkViewModel[]> {
    return this.http.get(`${this.baseUrl}list`, query)
      .then(responseBody => {
        return Utilities.castToArray(
          responseBody.data, WorkRead.WorkViewModel);
      });
  }

  listWithTech(query?: WorkRequests.List): Promise<WorkWithTechnicianRead.WorkViewModel[]> {
    return this.http.get(`${this.baseUrl}list/with-tech`, query)
      .then(responseBody => {
        return Utilities.castToArray(
          responseBody.data, WorkWithTechnicianRead.WorkViewModel);
      });
  }

  listWithHub(query?: WorkRequests.List): Promise<WorkWithHubRead.WorkViewModel[]> {
    return this.http.get(`${this.baseUrl}list/with-hub`, query)
      .then(responseBody => {
        return Utilities.castToArray(
          responseBody.data, WorkWithHubRead.WorkViewModel);
      });
  }

  listWithHubTech(query?: WorkRequests.List): Promise<WorkWithHubAndTechnicianRead.WorkViewModel[]> {
    return this.http.get(`${this.baseUrl}list/with-hub-tech`, query)
      .then(responseBody => {
        return Utilities.castToArray(
          responseBody.data, WorkWithHubAndTechnicianRead.WorkViewModel);
      });
  }

  read(workId: string): Promise<WorkRead.WorkViewModel> {
    return this.http.get(`${this.baseUrl}read/${workId}`)
      .then(responseBody => {
        return Utilities.castTo(
          responseBody.data, WorkRead.WorkViewModel)
        });
  }

  readWithTech(workId: string): Promise<WorkWithTechnicianRead.WorkViewModel> {
    return this.http.get(`${this.baseUrl}read/${workId}/with-tech`)
      .then(responseBody => {
        return Utilities.castTo(
          responseBody.data, WorkWithTechnicianRead.WorkViewModel)
        });
  }

  readWithHub(workId: string): Promise<WorkWithHubRead.WorkViewModel> {
    return this.http.get(`${this.baseUrl}read/${workId}/with-hub`)
      .then(responseBody => {
        return Utilities.castTo(
          responseBody.data, WorkWithHubRead.WorkViewModel)
        });
  }

  readWithHubTech(workId: string): Promise<WorkWithHubAndTechnicianRead.WorkViewModel> {
    return this.http.get(`${this.baseUrl}read/${workId}/with-hub-tech`)
      .then(responseBody => {
        return Utilities.castTo(
          responseBody.data, WorkWithHubAndTechnicianRead.WorkViewModel)
        });
  }

  create(body: WorkRequests.Create): Promise<void> {
    return this.http.post(`${this.baseUrl}`, body)
      .then(responseBody => undefined);
  }

  approve(workId: string): Promise<void> {
    return this.http.post(`${this.baseUrl}${workId}/approve`)
      .then(responseBody => undefined);
  }

  undo(workId: string): Promise<void> {
    return this.http.post(`${this.baseUrl}${workId}/undo`)
      .then(responseBody => undefined);
  }

  delete(workId: string): Promise<void> {
    return this.http.delete(`${this.baseUrl}${workId}`)
      .then(responseBody => undefined);
  }

  update(workId: string, body: WorkRequests.Edit): Promise<void> {
    return this.http.put(`${this.baseUrl}${workId}`, body)
      .then(responseBody => undefined);
  }
}
