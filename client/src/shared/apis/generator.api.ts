import { IHttp } from "../services/http/http.service";
import { Utilities, GeneratorRequests, ContactRequests, GeneratorRead } from "mean-au-ts-shared";
import { autoinject } from "aurelia-framework";

@autoinject
export class GeneratorApi {
  get baseUrl() {
    return 'api/generator/';
  }

  constructor(
    private http: IHttp
  ) { }

  list(): Promise<GeneratorRead.GeneratorViewModel[]> {
    return this.http.get(`${this.baseUrl}list`)
      .then(responseBody => {
        return Utilities.castToArray<GeneratorRead.GeneratorViewModel>(
          responseBody.data, GeneratorRead.GeneratorViewModel);
      });
  }

  read(id: string): Promise<GeneratorRead.GeneratorViewModel> {
    return this.http.get(`${this.baseUrl}read/${id}`)
      .then(responseBody => {
        return Utilities.castTo<GeneratorRead.GeneratorViewModel>(
          responseBody.data, GeneratorRead.GeneratorViewModel);
      });
  }

  create(body: GeneratorRequests.Create): Promise<GeneratorRead.GeneratorViewModel> {
    return this.http.post(`${this.baseUrl}`, body)
      .then(responseBody => Utilities.castTo(responseBody.data, GeneratorRead.GeneratorViewModel));
  }

  update(body: GeneratorRequests.Update): Promise<void> {
    return this.http.put(`${this.baseUrl}${body.id}`, body)
      .then(responseBody => undefined);
  }

  addContact(generatorId: string, body: ContactRequests.Create): Promise<void> {
    return this.http.post(`${this.baseUrl}${generatorId}/contacts`, body)
      .then(responseBody => undefined);
  }

  editContact(generatorId: string, body: ContactRequests.Update): Promise<void> {
    return this.http.put(`${this.baseUrl}${generatorId}/contacts/${body.id}`, body)
      .then(responseBody => undefined);
  }

  removeContact(generatorId: string, contactId: string): Promise<void> {
    return this.http.delete(`${this.baseUrl}${generatorId}/contacts/${contactId}`)
      .then(responseBody => undefined);
  }
}
