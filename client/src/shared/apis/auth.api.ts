import { autoinject } from "aurelia-framework";
import { Utilities, UserRequests, UserRead } from "mean-au-ts-shared";
import { IHttp } from "shared/services/http/http.service";

@autoinject
export class AuthApi {
  get baseUrl() {
    return 'api/auth/';
  }

  constructor(
    private http: IHttp
  ) { }

  signin(credentials: UserRequests.SignIn, postSigninRedirect?: string): Promise<UserRead.UserViewModel> {
    return this.http.post(`${this.baseUrl}signin`, credentials)
      .then(responseBody => Utilities.castTo(responseBody.data, UserRead.UserViewModel));
  }

  signout(): Promise<void> {
    return this.http.post(`${this.baseUrl}signout`)
      .then(() => undefined)
  }

  sendForgotPassword(body: UserRequests.SendForgotPassword): Promise<void> {
    return this.http.post(`${this.baseUrl}send-forgot-password`, body)
      .then(responseBody => undefined);
  }

  testForgotPassword(params: UserRequests.TestForgotPassword): Promise<void> {
    return this.http.get(`${this.baseUrl}forgot-password`, params, true)
      .then(responseBody => undefined);
  }

  resetForgottenPassword(body: UserRequests.ForgotPassword): Promise<void> {
    return this.http.post(`${this.baseUrl}forgot-password`, body)
      .then(responseBody => undefined);
  }

  test401(): Promise<void> {
    return this.http.get(`${this.baseUrl}test-401`)
      .then(responseBody => undefined);
  }

  test422(): Promise<void> {
    return this.http.get(`${this.baseUrl}test-422`)
      .then(responseBody => undefined);
  }
}
