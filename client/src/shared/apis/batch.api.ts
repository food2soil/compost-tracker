import { IHttp } from "../services/http/http.service";
import { Utilities, BatchRead, WorkRead, WorkWithTechnicianRead, BatchRequests } from "mean-au-ts-shared";
import { autoinject } from "aurelia-framework";

@autoinject
export class BatchApi {
  get baseUrl() {
    return 'api/batch/';
  }

  constructor(
    private http: IHttp
  ) { }

  list(query: BatchRequests.List): Promise<BatchRead.BatchViewModel[]> {
    return this.http.get(`${this.baseUrl}list`, query)
      .then(responseBody => {
        return Utilities.castToArray(
          responseBody.data, BatchRead.BatchViewModel)
      })
  }

  read(batchId: string): Promise<BatchRead.BatchViewModel> {
    return this.http.get(`${this.baseUrl}read/${batchId}`)
      .then(responseBody => {
        return Utilities.castTo<BatchRead.BatchViewModel>(
          responseBody.data, BatchRead.BatchViewModel)
      })
  }

  readWorkList(batchId: string): Promise<WorkRead.WorkViewModel[]> {
    return this.http.get(`${this.baseUrl}read/${batchId}/work`)
      .then(responseBody => {
        return Utilities.castToArray(
          responseBody.data, WorkRead.WorkViewModel)
      });
  }

  readWorkWithTechList(batchId: string): Promise<WorkWithTechnicianRead.WorkViewModel[]> {
    return this.http.get(`${this.baseUrl}read/${batchId}/work-with-tech`)
      .then(responseBody => {
        return Utilities.castToArray(
          responseBody.data, WorkWithTechnicianRead.WorkViewModel)
      });
  }
}
