import { autoinject } from "aurelia-framework";
import { Utilities, UserRead, UserRequests } from "mean-au-ts-shared";
import { IHttp } from "shared/services/http/http.service";

@autoinject
export class MeApi {
  get baseUrl() {
    return 'api/me/';
  }

  constructor(
    private http: IHttp
  ) { }

  me(): Promise<UserRead.UserViewModel> {
    return this.http.get(this.baseUrl)
      .then(response => {
        return Utilities.castTo<UserRead.UserViewModel>(
          response.data, UserRead.UserViewModel)
      })
  }

  changePassword(body: UserRequests.ChangePassword): Promise<void> {
    return this.http.post(`${this.baseUrl}/change-password`, body)
      .then(() => undefined)
  }

  updateProfile(body: UserRequests.EditProfile): Promise<void> {
    return this.http.put(this.baseUrl, body)
      .then(() => undefined)
  }
}
