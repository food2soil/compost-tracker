import { IHttp } from "../services/http/http.service";
import { Utilities, HubRequests, HubRead, HubWithTechniciansRead, HubWithBaysRead, HubWithGeneratorsRead, HubWithTechniciansAndBaysRead, HubWithBaysAndGeneratorsRead, HubWithTechniciansAndGeneratorsRead, HubWithTechniciansBaysAndGeneratorsRead, BayRead, BatchRead, ContactRequests } from "mean-au-ts-shared";
import { autoinject } from "aurelia-framework";

@autoinject
export class HubApi {
  get baseUrl() {
    return 'api/hub/';
  }

  constructor(
    private http: IHttp
  ) { }

  list(query?: HubRequests.List): Promise<HubRead.HubViewModel[]> {
    return this.http.get(`${this.baseUrl}list`, query)
      .then(responseBody => {
        return Utilities.castToArray(
          responseBody.data, HubRead.HubViewModel);
      });
  }

  create(body: HubRequests.Create): Promise<HubRead.HubViewModel> {
    return this.http.post(`${this.baseUrl}`, body)
      .then(responseBody => {
        return Utilities.castTo(
          responseBody.data, HubRead.HubViewModel);
      });
  }

  read(hubId: string): Promise<HubRead.HubViewModel> {
    return this.http.get(`${this.baseUrl}read/${hubId}`)
      .then(responseBody => {
        return Utilities.castTo(
          responseBody.data, HubRead.HubViewModel)
      });
  }

  readWithTech(hubId: string): Promise<HubWithTechniciansRead.HubViewModel> {
    return this.http.get(`${this.baseUrl}read/${hubId}/with-tech`)
      .then(responseBody => {
        return Utilities.castTo(
          responseBody.data, HubWithTechniciansRead.HubViewModel)
      });
  }

  readWithBay(hubId: string): Promise<HubWithBaysRead.HubViewModel> {
    return this.http.get(`${this.baseUrl}read/${hubId}/with-bay`)
      .then(responseBody => {
        return Utilities.castTo(
          responseBody.data, HubWithBaysRead.HubViewModel)
      });
  }

  readWithGen(hubId: string): Promise<HubWithGeneratorsRead.HubViewModel> {
    return this.http.get(`${this.baseUrl}read/${hubId}/with-gen`)
      .then(responseBody => {
        return Utilities.castTo(
          responseBody.data, HubWithGeneratorsRead.HubViewModel)
      });
  }

  readWithTechBay(hubId: string): Promise<HubWithTechniciansAndBaysRead.HubViewModel> {
    return this.http.get(`${this.baseUrl}read/${hubId}/with-tech-bay`)
      .then(responseBody => {
        return Utilities.castTo(
          responseBody.data, HubWithTechniciansAndBaysRead.HubViewModel)
      });
  }

  readWithTechGen(hubId: string): Promise<HubWithTechniciansAndGeneratorsRead.HubViewModel> {
    return this.http.get(`${this.baseUrl}read/${hubId}/with-tech-gen`)
      .then(responseBody => {
        return Utilities.castTo(
          responseBody.data, HubWithTechniciansAndGeneratorsRead.HubViewModel)
      });
  }

  readWithBayGen(hubId: string): Promise<HubWithBaysAndGeneratorsRead.HubViewModel> {
    return this.http.get(`${this.baseUrl}read/${hubId}/with-bay-gen`)
      .then(responseBody => {
        return Utilities.castTo(
          responseBody.data, HubWithBaysAndGeneratorsRead.HubViewModel)
      });
  }

  readWithTechBayGen(hubId: string): Promise<HubWithTechniciansBaysAndGeneratorsRead.HubViewModel> {
    return this.http.get(`${this.baseUrl}read/${hubId}/with-tech-bay-gen`)
      .then(responseBody => {
        return Utilities.castTo(
          responseBody.data, HubWithTechniciansBaysAndGeneratorsRead.HubViewModel)
      });
  }

  readBayList(hubId: string): Promise<BayRead.BayViewModel[]> {
    return this.http.get(`${this.baseUrl}read/${hubId}/bays`)
      .then(responseBody => {
        return Utilities.castToArray(
          responseBody.data, BayRead.BayViewModel)
      });
  }

  readActiveBatchesList(hubId: string): Promise<BatchRead.BatchViewModel[]> {
    return this.http.get(`${this.baseUrl}read/${hubId}/active-batches`)
      .then(responseBody => {
        return Utilities.castToArray(
          responseBody.data, BatchRead.BatchViewModel)
      });
  }

  update(body: HubRequests.Update): Promise<void> {
    return this.http.put(`${this.baseUrl}${body.id}`, body)
      .then(responseBody => undefined);
  }

  updateTechnician(body: HubRequests.UpdateTechnicians): Promise<void> {
    return this.http.put(`${this.baseUrl}${body.id}/technicians`, body)
      .then(responseBody => undefined);
  }

  updateGenerators(body: HubRequests.UpdateGenerators): Promise<void> {
    return this.http.put(`${this.baseUrl}${body.id}/generators`, body)
      .then(responseBody => undefined);
  }

  addContact(hubId: string, body: ContactRequests.Create): Promise<void> {
    return this.http.post(`${this.baseUrl}${hubId}/contacts`, body)
      .then(responseBody => undefined);
  }

  editContact(hubId: string, body: ContactRequests.Update): Promise<void> {
    return this.http.put(`${this.baseUrl}${hubId}/contacts/${body.id}`, body)
      .then(responseBody => undefined);
  }

  removeContact(hubId: string, contactId: string): Promise<void> {
    return this.http.delete(`${this.baseUrl}${hubId}/contacts/${contactId}`)
      .then(responseBody => undefined);
  }
}
