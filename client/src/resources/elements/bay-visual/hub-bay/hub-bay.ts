import { bindable, autoinject, computedFrom } from "aurelia-framework";
import { BayRead, WorkRead, Time, BatchRead, Enums, WorkReadBase } from "mean-au-ts-shared";

@autoinject
export class HubBay {
  @bindable bay: BayRead.BayViewModel
  @bindable lastWork: WorkRead.WorkViewModel

  measurementForBay: WorkReadBase.BayMeasurementViewModel
  lastBatchEvent: BatchRead.BatchEventViewModel
  isLastEventFill: boolean 
  isActionable: boolean

  bind() {
    this.bay.activeBatches.forEach(x => {
      x.events.forEach(y => {
        if (this.lastBatchEvent && this.lastBatchEvent.date > y.date) return 
          
        this.lastBatchEvent = y
      })
    })

    this.isActionable =
      !this.bay.isHolding &&
      this.bay.activeBatches.length > 0 &&
      Date.now() - this.lastBatchEvent.date.valueOf() > 2 * Time.MilliSecondsPerWeek

    this.isLastEventFill = !!this.lastBatchEvent && this.lastBatchEvent.type === Enums.BayEvent.Fill
    this.measurementForBay = this.lastWork 
      ? this.lastWork.bayMeasurements.find(x => x.bay === this.bay.id) 
      : null
  }
}