import { bindable, computedFrom, BindingEngine, Disposable, autoinject } from "aurelia-framework";
import { BayShared, BayRead, WorkReadBase, WorkRead, Enums, BatchRead, Time } from "mean-au-ts-shared";

@autoinject
export class WorkBay {
  @bindable bay: BayRead.BayViewModel
  @bindable bayIdToName: {[key: string]: string}
  @bindable measurement: WorkReadBase.BayMeasurementViewModel
  @bindable events: WorkReadBase.BayEventViewModel[]
  @bindable isDisabled: boolean

  actions: {
    isFilled?: boolean
    isHarvested?: boolean
    turnedInto?: string
    bucketsHarvested?: number
    isEmptied?: boolean
  }

  @computedFrom('bay')
  get canTurn(): boolean {
    return BayShared.canTurn(this.bay)
  }

  @computedFrom('bay', 'actions.turnedInto')
  get canFill(): boolean {
    return BayShared.canFill(this.bay) || !!this.actions.turnedInto
  }

  actionsChanged() {
    this.events.length = 0

    if (this.actions.isFilled) {
      let fillEvent = new WorkReadBase.BayEventViewModel()
      fillEvent.bay = this.bay.id
      fillEvent.type = Enums.BayEvent.Fill
      
      this.events.push(fillEvent)
    }

    if (this.actions.turnedInto) {
      let turnEvent = new WorkReadBase.BayEventViewModel()
      turnEvent.bay = this.bay.id
      turnEvent.type = Enums.BayEvent.Turn
      turnEvent.destination = this.actions.turnedInto

      this.events.push(turnEvent)
    }

    if (this.actions.bucketsHarvested > 0) {
      let harvestEvent = new WorkReadBase.BayEventViewModel()
      harvestEvent.bay = this.bay.id
      harvestEvent.type = Enums.BayEvent.Harvest
      harvestEvent.bucketsHarvested = this.actions.bucketsHarvested
      harvestEvent.isFullHarvest = this.actions.isEmptied

      this.events.push(harvestEvent)
    } else {
      this.actions.isEmptied = false
    }
  }

  bind() {
    this.actions = this.actions || {}

    this.events.forEach(x => {
      if (x.type === Enums.BayEvent.Fill) {
        this.actions.isFilled = true
      }

      if (x.type === Enums.BayEvent.Turn) {
        this.actions.turnedInto = x.destination
      }

      if (x.type === Enums.BayEvent.Harvest) {
        this.actions.isEmptied = x.isFullHarvest
        this.actions.bucketsHarvested = x.bucketsHarvested
      }
    })
  }
}
