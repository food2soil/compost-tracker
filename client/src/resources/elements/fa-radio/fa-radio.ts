import { observable } from "aurelia-binding";
import { bindable } from "aurelia-framework";
import { Utilities } from "mean-au-ts-shared";
import { Contexts } from "shared/utilities/theme-helper";

export class FaCheck {
  @bindable for: string
  @bindable model: any
  @bindable checkIcon: string
  @bindable type: Contexts

  @observable value: boolean

  id: string = Utilities.newGuid()

  clear() {
    this.value = false;
  }

  modelChanged() {
    this.value = this.model[this.for];
  }

  valueChanged() {
    if (!this.model) return;

    this.model[this.for] = this.value;
  }

  toggleValue() {
    this.value = !this.value;
  }

  bind() {
    this.type = this.type || 'primary'
    this.checkIcon = this.checkIcon || 'fa-check'
  }
}
