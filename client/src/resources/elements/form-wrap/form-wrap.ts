import { customElement, bindable, observable, TaskQueue } from "aurelia-framework";
import { FormGroup, IFormGroup } from "../form-group/form-group";
import { children } from "aurelia-templating";

@customElement('form-wrap')
export class FormWrap {
  @bindable novalidate: boolean;
  @bindable model: any;
  @bindable noSubmit: boolean;

  private isAttached: boolean;
  deferredRefresh: () => void;

  @observable allFormGroups: IFormGroup[];
  @children('form-group') formGroups: IFormGroup[];
  @children('form-group-check') formGroupChecks: IFormGroup[];
  @children('form-group-date') formGroupDates: IFormGroup[];
  @children('form-group-select') formGroupSelects: IFormGroup[];
  @children('form-group-textarea') formGroupTextareas: IFormGroup[];
  @children('fa-check') faChecks: IFormGroup[];

  private _setFgProps() {
    this.allFormGroups.forEach(fg => {
      fg.model = fg.model || this.model;
      fg.value = fg.model[fg.for];
    });
  }

  private refreshAllFormGroups() {
    this.allFormGroups = (this.formGroups || [])
      .concat(this.formGroupChecks || [])
      .concat(this.formGroupDates || [])
      .concat(this.formGroupSelects || [])
      .concat(this.formGroupTextareas || [])
      .concat(this.faChecks || [])

    this._setFgProps()
  }

  formGroupsChanged() {
    this.refreshAllFormGroups()
  }

  formGroupChecksChanged() {
    this.refreshAllFormGroups()
  }

  formGroupDatesChanged() {
    this.refreshAllFormGroups()
  }

  formGroupSelectsChanged() {
    this.refreshAllFormGroups()
  }

  formGroupTextareasChanged() {
    this.refreshAllFormGroups()
  }

  faChecksChanged() {
    this.refreshAllFormGroups()
  }

  clear() {
    this.allFormGroups.forEach(fg => fg.clear());
  }

  bind() {
    if (this.novalidate == null) {
      this.novalidate = true;
    }
  }
}
