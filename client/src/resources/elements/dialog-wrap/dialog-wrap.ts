export interface IDialogPage {
  title: string
  goNext(): Promise<void>
  initialize(): Promise<void>
}

export interface IDialogModel<TOutput = void> {
  okDelegate?: (output?: TOutput) => Promise<void>
}

export class DialogWrap { }
