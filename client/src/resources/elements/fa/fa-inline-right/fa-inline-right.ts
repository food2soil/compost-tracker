import { bindable } from "aurelia-framework";

export class FaInlineRight {
  @bindable icon: string
}
