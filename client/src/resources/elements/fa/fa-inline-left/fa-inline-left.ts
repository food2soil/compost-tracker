import { bindable } from "aurelia-framework";

export class FaInlineLeft {
  @bindable icon: string
}
