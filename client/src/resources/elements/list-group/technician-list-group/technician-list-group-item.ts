import { UserRead } from "mean-au-ts-shared";

export class TechnicianListGroupItem {
  technician: UserRead.UserViewModel

  activate(params: UserRead.UserViewModel) {
    this.technician = params
  }
}
