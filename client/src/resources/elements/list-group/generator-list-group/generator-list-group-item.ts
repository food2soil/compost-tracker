import { GeneratorRead } from "mean-au-ts-shared";

export class GeneratorListGroupItem {
  generator: GeneratorRead.GeneratorViewModel

  activate(params: GeneratorRead.GeneratorViewModel) {
    this.generator = params
  }
}
