import { ContactRead, ContactRequests } from "mean-au-ts-shared";
import { bindable, autoinject } from "aurelia-framework";
import { DialogService } from "aurelia-dialog";
import { ConfirmModal } from "shared/modals/confirm-modal";
import * as toastr from 'toastr'
import { AddContactModal } from "shared/modals/contacts/add-contact-modal";
import { EditContactModal } from "shared/modals/contacts/edit-contact-modal";
import { IGridRowClick, IGridRowClickContext } from "../grid/grid-row-action/grid-row-click";

export interface IListGroupContext {
  __listGroup_isBusy?: boolean
  __listGroup_templateVm?: any
}

export class ListGroup<TItem> {
  @bindable title: string
  @bindable titleIcon: string
  
  @bindable items: (TItem & IListGroupContext)[]
  @bindable itemVmCtor: new () => any
  
  @bindable itemClick: IGridRowClick<TItem>

  handleItemClick(item: TItem & IGridRowClickContext) {
    if (!this.itemClick || !this.itemClick.action) return true;

    this.itemClick.action(item, this.items)
  }

  newVmInstance() {
    return new this.itemVmCtor();
  }
}
