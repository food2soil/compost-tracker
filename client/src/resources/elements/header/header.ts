import { bindable, customElement, BindingEngine, autoinject } from "aurelia-framework";
import { typeToIcon, Entity } from "shared/utilities/theme-helper";

export interface IBreadcrumb {
  href?: string
  title: string
}

@customElement('header')
@autoinject
export class Header {
  @bindable icon: string
  @bindable title: string
  @bindable subtitle: string
  @bindable markerType: Entity
  @bindable hasHelp: boolean
  @bindable breadcrumbs: IBreadcrumb[]
  @bindable isHome: boolean

  isHelpToggled: boolean
  helpContent: string
  helpWrap: Element

  constructor(private bindingEngine: BindingEngine) { }

  toggleHelp() {
    this.isHelpToggled = !this.isHelpToggled
  }

  bind() {
    this.icon = this.icon || typeToIcon(this.markerType);
  }
}
