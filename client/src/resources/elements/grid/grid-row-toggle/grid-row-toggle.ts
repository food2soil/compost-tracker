import { bindable } from "aurelia-framework";

export class GridRowToggle { 
  @bindable isSelected: boolean
}
