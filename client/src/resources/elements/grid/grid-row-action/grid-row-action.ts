export interface IGridRowAction<TRow> {
  title: string
  icon?: string
  action: (row: TRow) => void
}
