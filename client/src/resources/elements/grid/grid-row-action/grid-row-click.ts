export interface IGridRowClickContext {
  __gridRow_isActive?: boolean
}

export interface IGridRowClick<TRow> {
  getHref?: (row: TRow) => string
  action?: (row: TRow & IGridRowClickContext, rows: TRow[] & IGridRowClickContext) => void
  activeClasses?: string
}
