import { bindable } from "aurelia-framework";

export class TdFiller {
  @bindable isCenteredV: boolean
  @bindable isCenteredH: boolean
  @bindable classes: string
  @bindable href: string
  @bindable action: (row: any) => void

  tdFillerClasses: string

  buildTdFillerClasses(): string {
    let ret: string[] = [this.classes]

    if (this.isCenteredV) ret.push('td-filler-content-centered-v');
    if (this.isCenteredH) ret.push('td-filler-content-centered-h');

    return ret.join(' ');
  }

  bind() {
    this.isCenteredV = this.isCenteredV == null ? true : this.isCenteredV;
    this.isCenteredH = this.isCenteredH == null ? true : this.isCenteredH;

    this.tdFillerClasses = this.buildTdFillerClasses();
  }
}
