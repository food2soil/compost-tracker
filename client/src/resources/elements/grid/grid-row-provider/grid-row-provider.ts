export interface IGridRowProvider<TRow> {
  filteredRows: TRow[]
  numPages: number
  numPerPage: number
  totalRecords: number
  onReloaded: () => void
  onFilteredChanged: () => void

  reload(rows?: TRow[]): void
  getRows(pageNumber: number): TRow[]
  search(search: string, searchGetters: ((row: TRow) => any)[]): TRow[]
  filter(filter: (row: TRow) => boolean): TRow[]
}
