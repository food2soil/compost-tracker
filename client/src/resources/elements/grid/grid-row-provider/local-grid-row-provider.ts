import { IGridRowProvider } from "./grid-row-provider";
import { computedFrom, observable } from "aurelia-binding";

export class LocalGridRowProvider<TRow> implements IGridRowProvider<TRow> {
  private cachedSearch: string
  private cachedSearchGetters: ((row: TRow) => any)[]
  private cachedFilter: (row: TRow) => boolean

  @observable filteredRows: TRow[]
  numPerPage: number = 5
  totalRecords: number
  onReloaded: () => void
  onFilteredChanged: () => void

  @computedFrom('numPerPage', 'filteredRows')
  get numPages(): number {
    return this.filteredRows.length % this.numPerPage === 0
      ? Math.floor(this.filteredRows.length / this.numPerPage)
      : Math.floor(this.filteredRows.length / this.numPerPage) + 1
  }

  constructor(private allRows?: TRow[]) {
    this.reload(allRows);
  }

  filteredRowsChanged() {
    if (this.onFilteredChanged) this.onFilteredChanged()
  }

  reload(rows?: TRow[]): void {
    this.allRows = rows ? rows.slice() : [];
    this.totalRecords = this.allRows.length
    this.filteredRows = this.allRows.slice();
    
    if (this.onReloaded) this.onReloaded();
  }

  getRows(pageNumber: number): TRow[] {
    let startIndex = pageNumber <= 1 ? 0 : (pageNumber - 1) * this.numPerPage;
    if (startIndex > this.filteredRows.length - 1) return [];

    let endIndex = startIndex + this.numPerPage
    return this.filteredRows.slice(startIndex, endIndex);
  }

  search(search: string, searchGetters: ((row: TRow) => any)[]): TRow[] {
    let lowerSearch = search.toLowerCase()

    let searched = this.allRows.slice()
      .filter(x => searchGetters.some(f => {
        return f(x).toString().toLowerCase().indexOf(lowerSearch) !== -1
      }))

    if (this.cachedFilter) {
      searched = searched.filter(this.cachedFilter)
    }

    this.filteredRows = searched
    this.cachedSearch = search
    this.cachedSearchGetters = searchGetters
    return this.filteredRows;
  }

  filter(filter: (row: TRow) => boolean): TRow[] {
    let filtered = this.cachedSearch && this.cachedSearchGetters
      ? this.search(this.cachedSearch, this.cachedSearchGetters).filter(filter)
      : this.allRows.slice().filter(filter)

    this.cachedFilter = filter
    this.filteredRows = filtered
    return this.filteredRows;
  }
}
