import { customElement, bindable, computedFrom, TemplatingEngine, autoinject, TemplateDependency, inject, observable, Container } from "aurelia-framework";
import { GridColumn } from "./grid-column/grid-column";
import { IGridRowProvider } from "./grid-row-provider/grid-row-provider";
import { IGridRowAction } from "./grid-row-action/grid-row-action";
import { IGridRowClick, IGridRowClickContext } from "./grid-row-action/grid-row-click";
import { WorkActionsTd } from "shared/grid-templates/work/work-actions-td";

export interface IGridConfig<TRow> {
  columns: GridColumn<TRow, any>[];
  rowProvider: IGridRowProvider<TRow>;
  rowClick?: IGridRowClick<TRow>;
}

@customElement('grid')
@autoinject()
export class Grid<TRow> {
  @bindable config: IGridConfig<TRow>
  @bindable noRowsMessage: string;
  @bindable showHeaders: boolean = true;
  @bindable isBusy: boolean

  columns: GridColumn<TRow>[];
  rowProvider: IGridRowProvider<TRow>;
  rowClick: IGridRowClick<TRow>;
  visibleRows: TRow[]
  currentPage: number = 1
  searchGetters: ((row: TRow) => any)[]
  gridBindingContext: Grid<TRow>

  @observable
  searchString: string

  @computedFrom('currentPage', 'searchString')
  get visiblePages(): number[] {
    if (!this.rowProvider) return [];
    if (this.rowProvider.numPages <= 3) return Array(this.rowProvider.numPages).fill(1).map((e,i)=>i+1);
    if (this.currentPage === 1) return [1,2,3];
    if (this.rowProvider.numPages - this.currentPage <= 3) return Array(3).fill(1).map((e,i)=>this.rowProvider.numPages - i - 1).reverse();

    return [this.currentPage - 1, this.currentPage, this.currentPage + 1];
  }

  constructor(
    private container: Container
  ) { }

  setPageTo(pageNumber: number) {
    this.currentPage = pageNumber;
    this.reload()
  }

  reload() {
    if (!this.rowProvider) return;

    this.visibleRows = this.rowProvider.getRows(this.currentPage)
  }

  searchStringChanged() {
    if (!this.rowProvider) return;

    this.rowProvider.search(this.searchString, this.searchGetters)
  }

  handleRowClick(row: TRow & IGridRowClickContext, rows: TRow[] & IGridRowClickContext) {
    if (!this.rowClick || !this.rowClick.action) return true;

    this.rowClick.action(row, rows)
  }

  bind() {
    this.rowProvider = this.config.rowProvider
    this.rowClick = this.config.rowClick
    this.columns = this.config.columns

    this.gridBindingContext = this;
    this.noRowsMessage = this.noRowsMessage || 'There are no items in this list'
    this.searchGetters = this.columns.filter(c => c.isSearchable).map(x => x.searchGetter || ((row: TRow) => row[x.key]));

    if (this.rowProvider) {
      this.rowProvider.onReloaded = () => this.reload();
      this.rowProvider.onFilteredChanged = () => this.reload();
    }

    this.reload();
  }
}
