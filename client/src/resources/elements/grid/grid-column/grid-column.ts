import { InlineViewStrategy, Container } from "aurelia-framework";

export interface ITdModel<TRow, TExtra> {
  extraData?: TExtra
  row: TRow
}

export interface ITdTemplate<TRow, TExtra> {
  row: TRow

  activate(params: ITdModel<TRow, TExtra>): void | boolean | Promise<any>
}

export class GridColumn<TRow, TExtra = void> {
  title: string
  key: keyof TRow
  isSearchable?: boolean
  templateBindingContext?: any
  template?: string
  tdClass?: string
  thClass?: string
  view: InlineViewStrategy
  centerHorizontally?: boolean
  searchGetter?: (row: TRow) => any
  ViewModel: new (...params: any[]) => ITdTemplate<TRow, TExtra>
  extraData: TExtra | object
  valueConverter: any

  constructor(init: Partial<GridColumn<TRow, TExtra>>) {
    this.title = init.title;
    this.key = init.key;
    this.isSearchable = init.isSearchable || !!init.searchGetter;
    this.centerHorizontally = init.centerHorizontally
    this.valueConverter = init.valueConverter || 'default'
    this.template = init.template && !this.centerHorizontally
      ? `<template>${init.template}</template>` 
      : init.template && this.centerHorizontally
        ? `<template>
            <div class="td-filler-content-centered-h">
              ${init.template}
            </div>
          </template>` 
        : !init.template && this.centerHorizontally 
          ? '<template><div class="td-filler-padding td-filler-content-centered-h">${row[self.key]}</div></template>'
          : `<template><div class="td-filler-padding">\${row[self.key] | ${this.valueConverter}}</div></template>`;
    this.tdClass = init.tdClass
    this.thClass = init.thClass
    this.view = new InlineViewStrategy(this.template)
    this.templateBindingContext = init.templateBindingContext || this;
    this.searchGetter = init.searchGetter
    this.ViewModel = init.ViewModel
    this.extraData = init.extraData || {}
  }

  createTemplate() {
    return new Container().get(this.ViewModel)
  }
}
