import { bindable, observable } from "aurelia-framework";
import { Utilities } from "mean-au-ts-shared";
import { IFormGroup } from "../form-group";

export class FormGroupCheck implements IFormGroup {

  @bindable for: string
  @bindable model: any

  @observable value: boolean

  id: string = Utilities.newGuid()

  clear() {
    this.value = false;
  }

  modelChanged() {
    this.value = this.model[this.for];
  }

  valueChanged() {
    if (!this.model) return;

    this.model[this.for] = this.value;
  }
}
