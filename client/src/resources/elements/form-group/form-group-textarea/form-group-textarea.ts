import { customElement, bindable, observable } from "aurelia-framework";
import { Utilities } from "mean-au-ts-shared";
import { IFormGroup } from "../form-group";

export class FormGroupTextarea implements IFormGroup {

  @bindable for: string
  @bindable placeholder: string
  @bindable autofocus: boolean
  @bindable model: any
  @bindable noLabel: boolean
  @bindable fillHeight: boolean
  @bindable rows: number
  @bindable isDisabled: boolean

  @observable value: string
  id: string = Utilities.newGuid()

  clear() {
    this.value = undefined;
  }

  modelChanged() {
    this.value = this.model[this.for];
  }
  
  valueChanged() {
    if (!this.model) return;

    this.model[this.for] = this.value;
  }
}
