import { customElement, bindable, observable } from "aurelia-framework";
import { Utilities } from "mean-au-ts-shared";
import { IFormGroup } from "../form-group";

export interface ISelectOption {
  value: any
  name: string
}

export class FormGroupSelect implements IFormGroup {

  @bindable for: string
  @bindable placeholder: string
  @bindable model: any
  @bindable selectOptions: ISelectOption[]
  @bindable orientation: 'horizontal' | 'vertical'
  @bindable isDisabled: boolean

  @observable value: any

  id: string = Utilities.newGuid()
  isAttached: boolean

  clear() {
    this.value = undefined;
  }

  modelChanged() {
    this.value = this.model[this.for];
  }
  
  valueChanged() {
    if (!this.model || !this.isAttached) return;

    this.model[this.for] = this.value;
  }
  
  attached() {
    this.isAttached = true
  }
}
