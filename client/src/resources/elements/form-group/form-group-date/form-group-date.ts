import { bindable, observable } from "aurelia-framework";
import { Utilities } from "mean-au-ts-shared";
import { IFormGroup } from "../form-group";
import * as moment from 'moment';

export class FormGroupDate implements IFormGroup {
  options: object

  @bindable for: string
  @bindable placeholder: string
  @bindable model: any
  @bindable orientation: 'horizontal' | 'vertical'
  @bindable isDisabled: boolean
  @bindable minDate: Date
  @bindable viewMode: 'decades' | 'years' | 'months' | 'days'
  @bindable format: string
  @bindable noLabel: boolean
  @bindable onChanged: (value: string) => void
  @bindable showTodayButton = true

  @observable value: Date
  id: string = Utilities.newGuid()

  clear() {
    this.value = undefined;
  }

  modelChanged() {
    this.value = this.model[this.for]
  }
  
  valueChanged() {
    if (!this.model) return;
    if (this.minDate && new Date(this.value).valueOf() < this.minDate.valueOf()) {
      this.value = new Date(this.minDate)
      return
    }

    let valueCopy = new Date(this.value)
    if (this.viewMode === 'months') {
      let month = this.value.getMonth()
      let year = this.value.getFullYear()
      
      valueCopy = new Date(year, month, 1)
    }

    if (this.model[this.for].toString() === valueCopy.toString()) return
    
    this.model[this.for] = valueCopy
    this.onChanged(this.value.toISOString())
  }

  isDisabledChanged() {
    let newOptions: any = Object.assign({}, this.options)
    newOptions.ignoreReadonly = !this.isDisabled
    this.options = newOptions
  }

  bind(b) {
    this.viewMode = this.viewMode || 'days'
    this.format = this.format || 'MMM DD, YYYY'
    this.onChanged = this.onChanged || ((value: string) => undefined)

    this.options = {
      ignoreReadonly: !this.isDisabled,
      minDate: this.minDate,
      viewMode: this.viewMode,
      format: this.format,
      showTodayButton: this.showTodayButton
    }
  }
}
