import { customElement, bindable, observable } from "aurelia-framework";
import { Utilities } from "mean-au-ts-shared";

export interface IFormGroup {
  for: string
  model: any
  value: any

  clear(): void
}

export class FormGroup implements IFormGroup {

  private isAttached: boolean;
  private defferedRefresh: () => void;

  @bindable for: string
  @bindable placeholder: string
  @bindable type: string
  @bindable autofocus: boolean
  @bindable model: any
  @bindable orientation: 'horizontal' | 'vertical'
  @bindable isDisabled: boolean
  @bindable onChanged: (value: string) => void

  @observable value: string
  id: string = Utilities.newGuid()

  clear() {
    this.value = undefined;
  }

  modelChanged() {
    this.value = this.model[this.for];
  }
  
  valueChanged() {
    if (!this.model) return;

    this.model[this.for] = this.value;
    this.onChanged(this.value)
  }

  bind() {
    this.type = this.type || 'text';
    this.orientation = this.orientation || 'vertical';
    this.placeholder = this.placeholder || '';
    this.onChanged = this.onChanged || ((value: string) => undefined)

    if (this.model) this.modelChanged()
  }
}
