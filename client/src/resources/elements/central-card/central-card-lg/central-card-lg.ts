import { bindable } from "aurelia-framework";

export class CentralCardLg { 
  @bindable header: string
  @bindable headerIcon: string
  @bindable hasNoBody: boolean
}
