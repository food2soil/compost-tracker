import { bindable, autoinject, TaskQueue } from "aurelia-framework";
import { Utilities } from "mean-au-ts-shared";

@autoinject
export class CentralCardFill { 
  @bindable header: string
  @bindable headerIcon: string
  @bindable hasNoBody: boolean
  @bindable fillHeight: boolean
  @bindable help: string
  @bindable hasSlotHeader: boolean

  isHelpToggled: boolean
  helpContent: string
  helpWrap: Element

  constructor(private taskQueue: TaskQueue) { }

  toggleHelp() {
    this.isHelpToggled = !this.isHelpToggled
  }

  attached() {
    this.helpContent = $(this.helpWrap)[0].innerHTML
      .replace('<!--slot-->', '')
      .replace('<!--anchor-->', '')
      .trim();
  }
}
