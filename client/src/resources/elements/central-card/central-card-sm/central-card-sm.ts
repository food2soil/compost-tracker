import { bindable } from "aurelia-framework";

export class CentralCardSm {
  @bindable header: string
  @bindable headerIcon: string
  @bindable hasNoBody: boolean
 }
