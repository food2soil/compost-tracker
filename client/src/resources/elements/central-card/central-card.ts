import { bindable } from "aurelia-framework";

export class CentralCard {
  @bindable header: string
  @bindable headerIcon: string
  @bindable hasNoBody: boolean
 }
