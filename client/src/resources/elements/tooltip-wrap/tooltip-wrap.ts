import { Utilities } from "mean-au-ts-shared";
import { TooltipOption } from "bootstrap";
import { TaskQueue, autoinject, bindable, Disposable } from "aurelia-framework";
import * as $ from 'jquery';

@autoinject()
export class TooltipWrap {

  @bindable placement: 'left' | 'right' | 'bottom' | 'top'
  @bindable isEnabled: boolean
  
  tooltipContent: string
  id: string = Utilities.newGuid()

  constructor(
    private taskQueue: TaskQueue
  ) { }

  isEnabledChanged() {
    if (this.isEnabled) {
      $(`#${this.id}`).tooltip('enable');
    } else {
      $(`#${this.id}`).tooltip('disable');
    }
  }

  bind() {
    this.placement = this.placement || 'top'
  }

  attached() {
    this.tooltipContent = $(`#tooltip-content-${this.id}`)[0].innerHTML
      .replace('<!--slot-->', '')
      .replace('<!--anchor-->', '')
      .trim();

    // Create the toolip.
    $(`#${this.id}`).tooltip({
      placement: this.placement,
      title: this.tooltipContent,
      html: true
    })

    if (this.isEnabled) return;

    // Disable it if we're not enabled
    $(`#${this.id}`).tooltip('disable')
  }
}
