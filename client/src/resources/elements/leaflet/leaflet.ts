import {Utilities} from 'mean-au-ts-shared'
import * as L from 'leaflet';
import { customElement, bindable, TaskQueue, autoinject, observable } from 'aurelia-framework';

export interface ILeafletContext {
  __leaflet_marker?: L.Marker & { _icon?: any }
}

export interface ILeafletMarker {
  latlng: L.LatLngExpression
  options?: L.MarkerOptions
}

export class RestaurantMarker implements ILeafletMarker {
  options: L.MarkerOptions = {
    icon: (<any>L).AwesomeMarkers.icon({
      prefix: 'fa',
      icon: 'cutlery',
      markerColor: 'blue'
    })
  }

  constructor(public latlng: L.LatLngExpression) {
  }
}

export class ResidentialMarker implements ILeafletMarker {
  options: L.MarkerOptions = {
    icon: (<any>L).AwesomeMarkers.icon({
      prefix: 'fa',
      icon: 'home',
      markerColor: 'green'
    })
  }

  constructor(public latlng: L.LatLngExpression) {
  }
}

export class HubMarker implements ILeafletMarker {
  options: L.MarkerOptions = {
    icon: (<any>L).AwesomeMarkers.icon({
      prefix: 'fa',
      icon: 'leaf',
      markerColor: 'darkpurple'
    })
  }

  constructor(public latlng: L.LatLngExpression) {
  }
}

export class StarMarker implements ILeafletMarker {
  options: L.MarkerOptions = {
    icon: (<any>L).AwesomeMarkers.icon({
      prefix: 'fa',
      icon: 'star',
      markerColor: 'orange'
    })
  }

  constructor(public latlng: L.LatLngExpression) {
  }
}

@customElement('leaflet')
@autoinject
export class Leaflet {
  @bindable onMarkerClick: () => void
  
  @bindable
  markers: (ILeafletMarker & ILeafletContext)[]
  
  @bindable center:[number, number]
  @bindable onAttached: () => void

  id: string = Utilities.newGuid()
  map: L.Map;

  constructor(
    private taskQueue: TaskQueue
  ) { }

  invalidateSize() {
    this.map.invalidateSize();
  }

  bind() {
    this.center = this.center || [32.747041, -117.140229]
  }

  attached() {
    this.map = L.map(this.id).setView(this.center, 11);
    L.tileLayer('https://maps.wikimedia.org/osm-intl/{z}/{x}/{y}.png', {
      attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors',
      maxZoom: 18,
      id: 'osm.test'
    }).addTo(this.map);

    if (!this.markers) return;

    this.markers.forEach(x => {
      x.__leaflet_marker = L.marker(x.latlng, x.options).addTo(this.map).on('click', this.onMarkerClick);
    })

    this.taskQueue.queueMicroTask(() => {
      if (this.onAttached) this.onAttached();
    })
  }
}
