import { bindable } from "aurelia-framework";

export class BusyOverlay {
  @bindable isVisible: boolean
}
