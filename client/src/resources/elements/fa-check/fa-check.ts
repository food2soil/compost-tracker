import { observable, BindingEngine, Disposable } from "aurelia-binding";
import { bindable, autoinject } from "aurelia-framework";
import { Utilities } from "mean-au-ts-shared";
import { Contexts } from "shared/utilities/theme-helper";
import { IFormGroup } from "../form-group/form-group";

@autoinject
export class FaCheck implements IFormGroup {
  value: any;

  @bindable for: string
  @bindable model: any
  @bindable checkIcon: string
  @bindable type: Contexts
  @bindable whenChecked: any
  @bindable isDisabled: boolean
  @bindable orientation: 'horizontal' | 'vertical'
  @bindable onChanged: (value: string) => void

  @observable isChecked: boolean

  id: string = Utilities.newGuid()

  private modelSubscription: Disposable;

  constructor(
    private bindingEngine: BindingEngine
  ) { }

  clear() {
    this.isChecked = false;
  }

  isCheckedChanged() {
    if (!this.model) return;

    if (this.isChecked || !this.isChecked && this.model[this.for] === this.whenChecked) {
      this.value = this.isChecked ? this.whenChecked : null;
      this.model[this.for] = this.value;
      this.onChanged(this.value)
    }
  }

  isDisabledChanged() {
    if (!this.isDisabled) return;

    this.isChecked = false
  }

  modelChanged() {
    if (this.modelSubscription) this.modelSubscription.dispose();

    if (!this.model) return;

    // Set initial checked value
    this.isChecked = this.model[this.for] === this.whenChecked;

    // Update isChecked on property change
    this.modelSubscription = this.bindingEngine.propertyObserver(this.model, this.for)
      .subscribe((n, o) => {
        if (n === o) return;
        if (this.isDisabled) return;

        this.isChecked = this.model[this.for] === this.whenChecked;
      });
  }

  toggleIsChecked(event: Event) {
    if (this.isDisabled) return;

    this.isChecked = !this.isChecked;
    event.stopPropagation()
  }

  bind() {
    this.whenChecked = this.whenChecked || true
    this.type = this.type || 'primary'
    this.checkIcon = this.checkIcon || 'fa-check'
    this.orientation = this.orientation || 'vertical'
    this.onChanged = this.onChanged || ((value: string) => undefined)

    if (this.model) this.modelChanged()
  }
  
  detached() {
    if (this.modelSubscription) this.modelSubscription.dispose();
  }
}
