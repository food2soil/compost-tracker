import { ContactRead, ContactRequests } from "mean-au-ts-shared";
import { bindable, autoinject } from "aurelia-framework";
import { DialogService } from "aurelia-dialog";
import { ConfirmModal, IConfirmModalModel } from "shared/modals/confirm-modal";
import * as toastr from 'toastr'
import { AddContactModal, IAddContactModalModel } from "shared/modals/contacts/add-contact-modal";
import { EditContactModal, IEditContactModalModel } from "shared/modals/contacts/edit-contact-modal";

@autoinject
export class ContactList {
  @bindable contacts: ContactRead.ContactViewModel[]
  @bindable owningDocId: string
  @bindable addContactEndPoint: (owningDocId: string, contact: ContactRequests.Create) => Promise<void>
  @bindable editContactEndPoint: (owningDocId: string, contact: ContactRequests.Update) => Promise<void>
  @bindable removeContactEndPoint: (owningDocId: string, contactId: string) => Promise<void>
  @bindable onChanged: () => Promise<void>

  isRemoving: boolean[]
  isRemovingAllFlag: boolean
  isEditing: boolean
  isAdding: boolean

  constructor(
    private dialogService: DialogService
  ) { }

  addContact() {
    this.isAdding = true

    let model: IAddContactModalModel = {
      addContactEndPoint: this.addContactEndPoint,
      owningDocId: this.owningDocId,
      onSaved: this.onChanged
    }

    this.dialogService.open({
      viewModel: AddContactModal,
      model: model,
      lock: false 
    }).whenClosed(result => {
      if (result.wasCancelled) return
      
      this.isRemoving = Array(this.contacts.length).fill(false);
      toastr.success('Successfully added contact')
      this.isAdding = false
    }).catch(() => this.isAdding = false)
  }

  editContact(contact: ContactRead.ContactViewModel) {
    this.isEditing = true

    let model: IEditContactModalModel = {
      editContactEndPoint: this.editContactEndPoint,
      owningDocId: this.owningDocId,
      toEdit: contact,
      onSaved: this.onChanged
    }

    this.dialogService.open({
      viewModel: EditContactModal,
      model: model,
      lock: false 
    }).whenClosed(result => {
      if (result.wasCancelled) throw 'cancelled'

      toastr.success('Successfully updated contact')
      this.isEditing = false
    }).catch(() => this.isEditing = false)
  }

  removeContact(contact: ContactRead.ContactViewModel, index: number) {
    this.isRemoving[index] = true;
    this.isRemovingAllFlag = true

    let model: IConfirmModalModel = {
      prompt: 'Are you sure you wish to remove this contact?',
      title: 'Remove Contact',
      okLabel: 'Remove',
      okDelegate: () => {
        return this.removeContactEndPoint(this.owningDocId, contact.id)
          .then(() => this.onChanged())
      }
    }

    this.dialogService.open({
      viewModel: ConfirmModal, 
      model: model, 
      lock: false 
    }).whenClosed(result => {
      if (result.wasCancelled) throw new Error('canceled')
    }).then(() => {
      toastr.success('Successfully removed contact')
      this.isRemoving = Array(this.contacts.length).fill(false);
      this.isRemovingAllFlag = false
    }).catch(() => {
      this.isRemoving[index] = false
      this.isRemovingAllFlag = false
    })
  }

  bind() {
    this.onChanged = this.onChanged || (() => Promise.resolve())
    this.isRemoving = Array(this.contacts.length).fill(false);
  }
}
