import { bindable } from "aurelia-framework";

export class ButtonWrap {
  @bindable type: string
  @bindable size: string
  @bindable classes: string
  @bindable isDisabled: boolean
  @bindable action: () => void
  @bindable isWorking: boolean
  
  bind() {
    this.type = this.type || 'primary';
    this.size = this.size || 'md';
  }

  invokeAction() {
    if (this.isWorking || this.isDisabled || !this.action) return;

    this.action();
  }
}
