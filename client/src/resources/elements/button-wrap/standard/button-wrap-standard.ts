import { bindable } from "aurelia-framework";

export class StandardButton {
  @bindable action: () => void
  @bindable isDisabled: boolean
  @bindable isWorking: boolean
  @bindable href: string
  @bindable size: 'lg' | 'md' | 'sm'
  @bindable text: string
  @bindable icon: string
  @bindable type: 'primary' | 'danger' | 'warning' | 'success' | 'secondary'
}
