import { bindable } from "aurelia-framework";

export class DeleteButton {
  @bindable action: () => void
  @bindable isDisabled: boolean
  @bindable isWorking: boolean
  @bindable href: string
  @bindable text: string
  @bindable size: 'lg' | 'md' | 'sm'
}
