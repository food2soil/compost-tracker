import { bindable } from "aurelia-framework";

export class EditButton {
  @bindable action: () => void
  @bindable isDisabled: boolean
  @bindable isWorking: boolean
  @bindable href: string
  @bindable size: 'lg' | 'md' | 'sm'
}
