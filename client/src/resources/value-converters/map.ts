export class MapValueConverter {
  toView(value: object[], format: string) {
    return value.map(x => x[format])
  }
}
