import * as moment from 'moment'

export class DateValueConverter {
  toView(value, format) {
    if (!value) return '--'

    if (!format) format = 'MMM DD, YYYY'
    return moment(value).format(format);
  }
  fromView(str, format){
    return moment(str, format);
  }
}
