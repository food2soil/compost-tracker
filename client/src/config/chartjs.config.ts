import * as Chart from 'chart.js'

export function configureChartJs() {
  const verticalLinePlugin = {
    afterDatasetsDraw: function (chart, easing) {
      if (chart.config.options.lineAtIndex) {
        chart.config.options.lineAtIndex.forEach(function(pointIndex) {
          renderVerticalLine(chart, pointIndex);
        });
      }

      function getLinePosition(chart, pointIndex) {
        const meta = chart.getDatasetMeta(0); // first dataset is used to discover X coordinate of a point
        const data = meta.data;
        return data[pointIndex]._model.x;
      }

      function renderVerticalLine(chartInstance, pointIndex) {
        const lineLeftOffset = getLinePosition(chartInstance, pointIndex);
        const scale = chartInstance.scales['y-axis-0'];
        const context = chartInstance.chart.ctx;

        // render vertical line
        context.beginPath();
        context.strokeStyle = '#db7734';
        context.moveTo(lineLeftOffset, scale.top);
        context.lineTo(lineLeftOffset, scale.bottom);
        context.stroke();

        // write label
        context.fillStyle = '#db7734';
        context.textAlign = 'center';
        context.fillText('TURN', lineLeftOffset, (scale.bottom - scale.top) / 2 + scale.top);
      }
    }
  };

  Chart.plugins.register(verticalLinePlugin);
}
