/// <reference types="aurelia-loader-webpack/src/webpack-hot-interface"/>
// we want font-awesome to load as soon as possible to show the fa-spinner
import 'font-awesome/css/font-awesome.css';
import 'bootstrap/dist/css/bootstrap.css';
import 'toastr/build/toastr.css';
import 'leaflet/dist/leaflet.css';
import '../scripts/awesome-markers/leaflet.awesome-markers.css';
import '../scripts/awesome-markers/leaflet.awesome-markers';
import 'aurelia-bootstrap-datetimepicker/node_modules/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css';
import 'aurelia-bootstrap-datetimepicker/dist/amd/bootstrap-datetimepicker-bs4.css';
import 'nprogress/nprogress.css';

import {Aurelia, Container} from 'aurelia-framework'
import environment from 'environment';
import {PLATFORM} from 'aurelia-pal';
import * as Bluebird from 'bluebird';
import { configureRootContainer } from 'config/container.config';
import { globalResources } from 'config/resource.config';
import 'bootstrap';
import { IAuth } from 'shared/services/auth/auth.service';
import { configureChartJs } from 'config/chartjs.config';
import { AuthApi } from 'shared/apis/auth.api';
import { FetchResponseEventHandler } from 'shared/services/http/event-handlers/fetch-response-event-handler';
import { FetchResponseErrorEventHandler } from 'shared/services/http/event-handlers/fetch-response-error-event-handler';

// remove out if you don't want a Promise polyfill (remove also from webpack.config.js)
Bluebird.config({ warnings: { wForgottenReturn: false } });

export function configure(aurelia: Aurelia) {
  aurelia.use
    .standardConfiguration()
    .globalResources(globalResources)
    .plugin(PLATFORM.moduleName('aurelia-validation'))
    .plugin(PLATFORM.moduleName('aurelia-dialog'))
    .plugin(PLATFORM.moduleName('aurelia-chart'))
    .plugin(PLATFORM.moduleName('aurelia-bootstrap-datetimepicker'), config => {
      config.extra.bootstrapVersion = 4
      config.extra.iconBase = 'font-awesome'
      config.extra.withDateIcon = true
      config.extra.buttonClass = 'btn btn-primary'
      config.options.format = 'MMM DD, YYYY'
      config.options.ignoreReadonly = true
    })

  // Uncomment the line below to enable animation.
  // aurelia.use.plugin(PLATFORM.moduleName('aurelia-animator-css'));
  // if the css animator is enabled, add swap-order="after" to all router-view elements

  // Anyone wanting to use HTMLImports to load views, will need to install the following plugin.
  // aurelia.use.plugin(PLATFORM.moduleName('aurelia-html-import-template-loader'));

  if (environment.debug) {
    aurelia.use.developmentLogging();
  }

  if (environment.testing) {
    aurelia.use.plugin(PLATFORM.moduleName('aurelia-testing'));
  }

  configureRootContainer(aurelia.container);
  configureChartJs();

  aurelia.start().then(() => {
    let auth: IAuth = Container.instance.get(IAuth);

    // Create instances of the fetch event handlers to wire them up
    Container.instance.get(FetchResponseEventHandler)
    Container.instance.get(FetchResponseErrorEventHandler)

    return auth.refreshUserInfo()
      .then(() => {
        if (auth.currentUser) {
          return aurelia.setRoot(PLATFORM.moduleName('shells/admin/index'));
        } else {
          return aurelia.setRoot(PLATFORM.moduleName('shells/anonymous/index'));
        }
      })
  });
}
