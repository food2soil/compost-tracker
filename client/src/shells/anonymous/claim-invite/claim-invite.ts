import { autoinject } from "aurelia-framework";
import { Router, RoutableComponentActivate, RouteConfig, NavigationInstruction, IObservable } from "aurelia-router";
import { Validation, InvitationRequests, InvitationWithInvitedByRead, Enums } from "mean-au-ts-shared";
import { FormWrap } from "resources/elements/form-wrap/form-wrap";
import { ComponentDetached } from "aurelia-templating";
import { ValidationRules } from "aurelia-validation";
import { IValidator } from "shared/services/validator/validator.service";
import * as toastr from 'toastr';
import { InvitationApi } from "shared/apis/invitation.api";

@autoinject
export class ClaimInvite implements RoutableComponentActivate, ComponentDetached {

  isWorking: boolean
  read: InvitationWithInvitedByRead.InvitationViewModel
  inviteBody: InvitationRequests.ClaimInvite = new InvitationRequests.ClaimInvite();
  form: FormWrap
  UserRoles = Enums.UserRoles

  constructor(
    private invitationApi: InvitationApi,
    private validator: IValidator,
    private router: Router
  ) { 
    Validation.ensureDecoratorsOn(InvitationRequests.ClaimInvite, ValidationRules);
  }

  claimAccount() {
    this.validator.validateObject(this.inviteBody, true)
      .then(() => {
        this.isWorking = true
        return this.invitationApi.claimInvite(this.inviteBody)
      })
      .then(data => {
        toastr.success(`Successfully created your account.  You may now sign in`);
        this.router.navigate('signin')
      })
      .catch(() => this.isWorking = false)
  }

  activate(params: any, routeConfig: RouteConfig, navigationInstruction: NavigationInstruction): void | Promise<void> | PromiseLike<void> | IObservable {
    this.inviteBody.inviteToken = params.id;

    return this.invitationApi.readWithInvitedBy(params.id)
      .then(res => {
        this.read = res;
      })
  }

  detached() {
    this.form.clear();
  }
}
