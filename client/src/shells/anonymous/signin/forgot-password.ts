import { autoinject } from "aurelia-framework";
import { UserRequests, Validation } from "mean-au-ts-shared";
import { Router } from "aurelia-router";
import { ValidationRules } from "aurelia-validation";
import * as toastr from 'toastr'
import { FormWrap } from "resources/elements/form-wrap/form-wrap";
import { IValidator } from "shared/services/validator/validator.service";
import { AuthApi } from "shared/apis/auth.api";

@autoinject
export class SendForgotPassword {
  private requestBody: UserRequests.SendForgotPassword = new UserRequests.SendForgotPassword();

  isWorking: boolean
  form: FormWrap

  constructor(
    private validator: IValidator,
    private authApi: AuthApi,
    private router: Router
  ) {
    Validation.ensureDecoratorsOn(UserRequests.SendForgotPassword, ValidationRules);
   }
  
  sendForgotPasswordEmail() {
    this.validator.validateObject(this.requestBody, true)
      .then(() => {
        this.isWorking = true

        return this.authApi.sendForgotPassword(this.requestBody)
      })
      .then(() => {
        toastr.success(`Password reset sent to: ${this.requestBody.email}`);
        this.form.clear();
        this.router.navigateToRoute('signin')
      })
      .catch(() => this.isWorking = false)
  }
}
