import { autoinject, ComponentDetached } from "aurelia-framework";
import { Router } from "aurelia-router";
import { UserRequests, Validation } from "mean-au-ts-shared";
import {ValidationRules} from 'aurelia-validation';
import * as toastr from 'toastr';
import { FormWrap } from "resources/elements/form-wrap/form-wrap";
import { AuthApi } from "shared/apis/auth.api";
import { IValidator } from "shared/services/validator/validator.service";
import { IAuth } from "shared/services/auth/auth.service";

@autoinject
export class SignInMain implements ComponentDetached {

  private requestBody: UserRequests.SignIn = new UserRequests.SignIn();
  private redirect: string

  isWorking: boolean
  form: FormWrap

  constructor(
    private auth: IAuth,
    private validator: IValidator
  ) { 
    Validation.ensureDecoratorsOn(UserRequests.SignIn, ValidationRules);
  }

  signIn() {
    this.validator.validateObject(this.requestBody, true)
      .then(() => {
        this.isWorking = true

        return this.auth.signIn(this.requestBody, this.redirect ? `/${this.redirect}` : undefined)
      })
      .then(() => {
        toastr.success(`Welcome, ${this.auth.currentUser.firstName} ${this.auth.currentUser.lastName}`);
      })
      .catch(() => this.isWorking = false)
  }

  activate(params: any) {
    this.redirect = params.redirect;
  }

  detached() {
    this.form.clear();
  }
}
