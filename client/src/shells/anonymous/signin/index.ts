import { autoinject, PLATFORM } from "aurelia-framework";
import { Router, RouterConfiguration } from "aurelia-router";

@autoinject
export class SignInIndex {
  router: Router;

  configureRouter(config: RouterConfiguration, router: Router) {
    config.map([
      {
        route: '',
        name: 'signin',
        moduleId: PLATFORM.moduleName('./signin'),
        nav: true
      }, {
        route: 'forgot',
        name: 'forgot-password',
        moduleId: PLATFORM.moduleName('./forgot-password'),
        nav: true,
        title: 'Forgot Password'
      },
    ]);

    this.router = router;
  }
}
