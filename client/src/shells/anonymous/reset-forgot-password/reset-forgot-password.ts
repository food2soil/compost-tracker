import { RoutableComponentActivate, Router } from "aurelia-router";
import { autoinject } from "aurelia-framework";
import { Validation, UserRequests } from "mean-au-ts-shared";
import { ValidationRules } from "aurelia-validation";
import * as toastr from 'toastr';
import { FormWrap } from "resources/elements/form-wrap/form-wrap";
import { AuthApi } from "shared/apis/auth.api";
import { IValidator } from "shared/services/validator/validator.service";
 
@autoinject
export class ResetForgotPassword implements RoutableComponentActivate {

  isWorking: boolean
  isErrored: boolean
  changePasswordDetails: UserRequests.ForgotPassword = new UserRequests.ForgotPassword();
  changePasswordForm: FormWrap

  constructor(
    private authApi: AuthApi,
    private validator: IValidator,
    private router: Router
  ) {
    Validation.ensureDecoratorsOn(UserRequests.ForgotPassword, ValidationRules)
  }

  resetPassword() {
    this.validator.validateObject(this.changePasswordDetails, true)
      .then(results => {
        this.isWorking = true
        return this.authApi.resetForgottenPassword(this.changePasswordDetails)
      })
      .then(() => {
        toastr.success('Password reset successfully.  Please sign in')
        this.changePasswordForm.clear()
        this.router.navigateToRoute('signin')
      })
      .catch(() => this.isWorking = false)
  }

  activate(params: { id: string }) {
    this.changePasswordDetails.forgotPasswordToken = params.id

    return this.authApi.testForgotPassword({ forgotPasswordToken: params.id })
      .catch(() => {
        this.isErrored = true
      })
  }
}
