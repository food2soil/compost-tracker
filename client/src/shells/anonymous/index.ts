import { Aurelia, autoinject } from 'aurelia-framework';
import { Router, RouterConfiguration, NavigationInstruction } from 'aurelia-router';
import { PLATFORM } from 'aurelia-pal';
import { IAuth } from 'shared/services/auth/auth.service';
import * as $ from 'jquery'

@autoinject
export class App {
  router: Router;

  constructor(
    private auth: IAuth,
    private aurelia: Aurelia
  ) { }

  configureRouter(config: RouterConfiguration, router: Router) {
    config.title = 'Food2Soil';
    config.options.pushState = true;

    config.addPreRenderStep({
      run: (instruction: NavigationInstruction, next) => {
        // Force collapse the navbar
        $('.navbar-collapse').collapse('hide');
        return next()
      }
    })

    config.map([{
      route: '',
      name: 'home',
      moduleId: PLATFORM.moduleName('./home/home'),
      nav: true
    }, {
      route: 'signin',
      name: 'signin',
      moduleId: PLATFORM.moduleName('./signin/index'),
      nav: true,
      title: 'Sign In'
    }, {
      route: 'reset-forgot/:id',
      name: 'reset-forgot',
      moduleId: PLATFORM.moduleName('./reset-forgot-password/reset-forgot-password'),
      title: 'Reset Password'
    }, {
      route: 'claim-invite/:id',
      name: 'claim-invite',
      moduleId: PLATFORM.moduleName('./claim-invite/claim-invite'),
      title: 'Claim Invite'
    }]);

    config.mapUnknownRoutes(PLATFORM.moduleName('./home/home'));

    this.router = router;
  }

  activate() {
    if (!this.auth.currentUser) return;

    this.router.navigate('/', { replace: true, trigger: false });
    return this.aurelia.setRoot(PLATFORM.moduleName('shells/admin/index'));
  }
}
