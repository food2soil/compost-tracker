import { autoinject } from "aurelia-framework";
import { Leaflet, ILeafletMarker, ResidentialMarker, HubMarker, RestaurantMarker } from "resources/elements/leaflet/leaflet";

@autoinject
export class HomeDashboard {
  
  isDrawerOpen: boolean;
  map: Leaflet
  markers: ILeafletMarker[] = []

  showDrawer() {
    this.isDrawerOpen = true;
  }

  randomSeed() {
    for (let i = 0; i < 100; i++) {
      let markerType = Math.floor(Math.random() * 8)
      let loc: [number, number] = [this.map.center[0] + (((Math.random() * 12) - 6) * 0.01), this.map.center[1] + (((Math.random() * 12) - 6) * 0.01)]
      switch (markerType) {
        case 0:
        case 1:
        case 2:
        case 3:
          this.markers.push(new ResidentialMarker(loc))
          break;
        case 4:
        case 5:
        case 6:
          this.markers.push(new RestaurantMarker(loc))
          break;
        case 7:
          this.markers.push(new HubMarker(loc))
          break;
      }
    }
  }

  attached() {
    this.randomSeed()
  }
}
