import { ILeafletMarker, HubMarker, Leaflet, ResidentialMarker, RestaurantMarker, ILeafletContext } from "resources/elements/leaflet/leaflet";
import { HubApi } from "shared/apis/hub.api";
import { HubRead, Enums, HubRequests, Utilities, GeneratorRead, GeneratorShared, AddressShared, HubShared, HubWithGeneratorsRead } from "mean-au-ts-shared";
import { autoinject, TaskQueue, Task } from "aurelia-framework";
import { GeneratorApi } from "shared/apis/generator.api";
import { IValidator } from "shared/services/validator/validator.service";
import * as toastr from 'toastr';
import { Router } from "aurelia-router";

interface ILinkGeneratorsContext {
  __linkGenerators_marker?: ILeafletMarker & ILeafletContext
  __linkGenerators_isLinked?: boolean
  __linkGenerators_capacityContext?: string
  __linkGenerators_capacity?: number
  __linkGenerators_isLinkedOther?: boolean
  __linkGenerators_distance?: number
}

interface ILinkGeneratorsHubContext {
  __linkGenerators_filledCapacity?: number
}

@autoinject()
export class HubLinkGenerators {
  centerMarker: ILeafletMarker
  markers: (ILeafletMarker & ILeafletContext)[] = []
  map: Leaflet

  hub: HubWithGeneratorsRead.HubViewModel & ILinkGeneratorsHubContext
  generators: (GeneratorRead.GeneratorViewModel & ILinkGeneratorsContext)[]
  allOtherHubs: HubRead.HubViewModel[]

  fillCapacityContextClass: string;
  isSelectOpen: boolean = true
  isSaving: boolean

  constructor(
    private hubApi: HubApi,
    private generatorApi: GeneratorApi,
    private taskQueue: TaskQueue,
    private validator: IValidator,
    private router: Router
  ) { }
  
  _getFillCapacityContextClass(): string {
    if (this.hub.__linkGenerators_filledCapacity >= this.hub.capacityGallonsPerWeek) return 'text-danger';
    if (this.hub.__linkGenerators_filledCapacity / this.hub.capacityGallonsPerWeek > 0.8) return 'text-warning';

    return 'text-success';
  }
  
  _setGeneratorCapacityContext(x: GeneratorRead.GeneratorViewModel & ILinkGeneratorsContext) {
      if (x.__linkGenerators_isLinked)
        return x.__linkGenerators_capacityContext = 'text-success';

      if (Math.abs(this.hub.capacityGallonsPerWeek - this.hub.__linkGenerators_filledCapacity - x.__linkGenerators_capacity) < 20)
        return x.__linkGenerators_capacityContext = 'text-warning';

      if (this.hub.capacityGallonsPerWeek - this.hub.__linkGenerators_filledCapacity - x.__linkGenerators_capacity < 0)
        return x.__linkGenerators_capacityContext = 'text-danger';
  
      return x.__linkGenerators_capacityContext = 'text-success';
  }

  _setGeneratorContextOneTime() {
    this.generators.forEach(x => {
      x.__linkGenerators_capacity = GeneratorShared.getWeeklyCollection(x);
      x.__linkGenerators_isLinkedOther = this.allOtherHubs.some(hub => HubShared.isLinkedToGeneratorNoPop(hub, x.id))
      x.__linkGenerators_distance = Utilities.roundTo(AddressShared.getDistanceFromLatLonInMiles(
        this.hub.address.loc[0], this.hub.address.loc[1],
        x.address.loc[0], x.address.loc[1]), 1)
    })
  }

  _setGeneratorContext() {
    this.generators.forEach(x => {
      x.__linkGenerators_isLinked = HubShared.isLinkedToGenerator(this.hub, x.id)
      this._setGeneratorCapacityContext(x)
    })
  }

  toggleDrawer() {
    this.isSelectOpen = !this.isSelectOpen;
    
    this.taskQueue.queueMicroTask(() => {
      setTimeout(() => {
        this.map.invalidateSize();
      }, 260);
    })
  }

  toggleLink(generator: GeneratorRead.GeneratorViewModel & ILinkGeneratorsContext) {
    let linkedIndex = this.hub.generators.map(x => x.id).indexOf(generator.id)
    
    if (linkedIndex === -1) {
      this.hub.generators.push(generator);

      if (!generator.__linkGenerators_marker || !generator.__linkGenerators_marker.__leaflet_marker) return;
      $(generator.__linkGenerators_marker.__leaflet_marker._icon).removeClass('unlinked-marker')
    } else {
      this.hub.generators.splice(linkedIndex, 1);

      if (!generator.__linkGenerators_marker || !generator.__linkGenerators_marker.__leaflet_marker) return;
      $(generator.__linkGenerators_marker.__leaflet_marker._icon).addClass('unlinked-marker')
    }

    // update context classes
    this.hub.__linkGenerators_filledCapacity = HubShared.getFilledCapacity(this.hub)
    this.fillCapacityContextClass = this._getFillCapacityContextClass();
    this._setGeneratorContext()
  }

  previewGenerator(generator: GeneratorRead.GeneratorViewModel & ILinkGeneratorsContext) {
    if (!generator.__linkGenerators_marker || !generator.__linkGenerators_marker.__leaflet_marker) return;

    $(generator.__linkGenerators_marker.__leaflet_marker._icon).addClass('previewed-marker')
  }

  unpreviewGenerator(generator: GeneratorRead.GeneratorViewModel & ILinkGeneratorsContext) {
    if (!generator.__linkGenerators_marker || !generator.__linkGenerators_marker.__leaflet_marker) return;

    $(generator.__linkGenerators_marker.__leaflet_marker._icon).removeClass('previewed-marker')
  }

  save() {
    this.isSaving = true;

    let requestBody: HubRequests.UpdateGenerators = {
      id: this.hub.id,
      generatorIds: this.hub.generators.map(x => x.id)
    }
    return this.validator.validateObject(requestBody, true)
      .then(() => this.hubApi.updateGenerators(requestBody))
      .then(() => {
        toastr.success('Successfully updated linked generators')
        this.router.navigate(`read/${this.hub.id}`)
      })
      .catch(() => this.isSaving = false)
  }

  activate(params: any) {
    this.markers.splice(0, this.markers.length)

    let generatorsPromise = this.generatorApi.list()
      .then(x => {
        this.generators = x

        this.generators.forEach(x => {
          let marker;
          if (x.generatorType === Enums.GeneratorType.Residential) {
            marker = new ResidentialMarker({lat: x.address.loc[1], lng: x.address.loc[0]})
          } else if (x.generatorType === Enums.GeneratorType.Restaurant) {
            marker = new RestaurantMarker({lat: x.address.loc[1], lng: x.address.loc[0]})
          }

          if (marker) this.markers.push(marker)

          x.__linkGenerators_marker = marker;
        })
      })

    let hubPromise = this.hubApi.readWithGen(params.id)
      .then(x => {
        this.hub = x
        

        return this.hubApi.list()
      })
      .then(x => {
        this.allOtherHubs = x.filter(x => x.id !== this.hub.id)
      })

    return Promise.all([generatorsPromise, hubPromise])
      .then(() => {
        this._setGeneratorContextOneTime()
        this._setGeneratorContext()

        this.generators.sort((a, b) => a.__linkGenerators_distance - b.__linkGenerators_distance)

        this.hub.__linkGenerators_filledCapacity = HubShared.getFilledCapacity(this.hub)
        this.fillCapacityContextClass = this._getFillCapacityContextClass();
      })
  }

  bind() {
    this.centerMarker = new HubMarker({lat: this.hub.address.loc[1], lng: this.hub.address.loc[0]})
    this.markers.push(this.centerMarker);
  }

  initMarkers() {
    this.generators.filter(x => !x.__linkGenerators_isLinked)
      .forEach(x => {
        $(x.__linkGenerators_marker.__leaflet_marker._icon).addClass('unlinked-marker')
      })
  }
}
