import { LocalGridRowProvider } from "resources/elements/grid/grid-row-provider/local-grid-row-provider";
import { GridColumn } from "resources/elements/grid/grid-column/grid-column";
import { HubApi } from "shared/apis/hub.api";
import { HubRead } from "mean-au-ts-shared";
import { autoinject } from "aurelia-framework";
import { IGridConfig } from "resources/elements/grid/grid";
import { IBreadcrumb } from "resources/elements/header/header";
import { IAuth } from "shared/services/auth/auth.service";

@autoinject
export class HubList {
  private hubs: HubRead.HubViewModel[]

  gridConfig: IGridConfig<HubRead.HubViewModel>
  breadcrumbs: IBreadcrumb[]

  constructor(
    public auth: IAuth,
    private hubApi: HubApi
  ) { }

  activate() {
    this.breadcrumbs = []

    return this.hubApi.list().then((hubs) => {
      this.gridConfig = {
        rowClick: { getHref: (row) => `hubs/read/${row.id}` },
        rowProvider: new LocalGridRowProvider<HubRead.HubViewModel>(hubs),
        columns: [
          new GridColumn<HubRead.HubViewModel>({
            title: 'Name',
            key: 'name',
            isSearchable: true
          })
        ]
      }
    })
  }
}
