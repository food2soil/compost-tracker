import { HubApi } from "shared/apis/hub.api";
import { Router } from "aurelia-router";
import { Validation, Enums, WorkRequests, HubWithBaysRead } from "mean-au-ts-shared";
import { autoinject } from "aurelia-framework";
import { WorkApi } from "shared/apis/work.api";
import { ValidationRules } from "aurelia-validation";
import { IValidator } from "shared/services/validator/validator.service";
import * as toastr from 'toastr'
import { IAuth } from "shared/services/auth/auth.service";
import { ISelectOption } from "resources/elements/form-group/form-group-select/form-group-select";
import { TechnicianApi } from "shared/apis/technician.api";
import { WorkBays, IWorkBaysModel } from "../../shared/bay-visuals/work/work-bays";
import { DialogService } from "aurelia-dialog";
import { ConfirmModal, IConfirmModalModel } from "shared/modals/confirm-modal";
import { IBreadcrumb } from "resources/elements/header/header";

@autoinject()
export class WorkCreate {
  read: HubWithBaysRead.HubViewModel
  createBody: WorkRequests.Create = new WorkRequests.Create()
  workBaysModel: IWorkBaysModel
  isCreating: boolean
  lastWorkDate: Date
  minNewWorkDate: Date
  technicianSelectOptions: ISelectOption[]
  breadcrumbs: IBreadcrumb[]

  constructor(
    private hubApi: HubApi,
    private technicianApi: TechnicianApi,
    private workApi: WorkApi,
    private validator: IValidator,
    private router: Router,
    private auth: IAuth,
    public workBaysVm: WorkBays,
    private dialogService: DialogService
  ) { 
    Validation.ensureDecoratorsOn(WorkRequests.Create, ValidationRules);
  }

  create() {
    this.isCreating = true;

    this.createBody.hubId = this.read.id
    this.createBody.bayMeasurements = this.workBaysVm.bayMeasurements
    this.createBody.bayEvents = this.workBaysVm.bayEvents

    let baysNeedingMeasurements = this.read.bays.filter(x => {
      let measurementForBay = this.createBody.bayMeasurements.find(y => y.bay === x.id)
      return x.activeBatches.length > 0 && !measurementForBay
    })

    return this.validator.validateObject(this.createBody, true)
      .then(() => {
        if (baysNeedingMeasurements.length === 0) return this.workApi.create(this.createBody)

        return this.dialogService.open({
          viewModel: ConfirmModal,
          model: <IConfirmModalModel>{
            prompt: `<div class="text-center">You did not record a measurement for the following bays that contain batches:<br/><br/><strong>${baysNeedingMeasurements.map(x => x.name).join(', ')}</strong><br/><br/>Do you want to continue?</div>`,
            title: 'Missing Measurements',
            okLabel: 'Continue',
            okDelegate: () => this.workApi.create(this.createBody)
          }
        }).whenClosed(result => {
          if (result.wasCancelled) throw 'cancelled'
        })
      })
      .then(response => {
        toastr.success('Successfully logged work')
        this.router.navigate(`read/${this.read.id}`)
      })
      .catch(() => this.isCreating = false)
  }
  
  activate(params: any) {
    this.createBody.date = new Date()

    let technicianPromise = this.technicianApi.list().then(x => {
      this.technicianSelectOptions = x.map(t => {
        return <ISelectOption>{
          name: t.displayName,
          value: t.id
        }
      })

      this.createBody.technicianId = this.auth.currentUser.id
    })

    let hubPromise = this.hubApi.readWithBay(params.id)
      .then(x => {
        this.read = x;

        this.workBaysModel = {
          bays: this.read.bays
        }

        this.breadcrumbs = [{
          href: 'hubs',
          title: 'Hubs'
        }, {
          href: `hubs/read/${this.read.id}`,
          title: this.read.name
        }]
      })

    let workPromise = this.workApi.list({ hub: params.id, sortBy: 'date', sortDir: 'desc', limit: 1 })
      .then(work => {
        this.lastWorkDate = work.length === 0 
        ? null
        : new Date(work[0].date)

        this.minNewWorkDate = !this.lastWorkDate
          ? null
          : new Date(new Date(this.lastWorkDate)
            .setDate(work[0].date.getDate() + 1))
      })

    return Promise.all([technicianPromise, hubPromise, workPromise])
  }
}
