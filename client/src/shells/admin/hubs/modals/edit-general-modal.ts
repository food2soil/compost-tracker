import { Validation, HubRequests } from "mean-au-ts-shared";
import { autoinject, transient } from "aurelia-framework";
import { DialogController } from 'aurelia-dialog';
import { FormWrap } from "resources/elements/form-wrap/form-wrap";
import { ValidationRules } from "aurelia-validation";
import * as _ from 'lodash'
import { HubApi } from "shared/apis/hub.api";
import { IValidator } from "shared/services/validator/validator.service";

export interface IEditHubGeneralInfoModalModel {
  updateModel: HubRequests.Update,
  onSaved?: () => Promise<void>
}

@autoinject
@transient()
export class EditHubGeneralInfoModal {
  update: HubRequests.Update = new HubRequests.Update();
  original: HubRequests.Update = new HubRequests.Update();
  form: FormWrap
  onSaved: () => Promise<void>
  isSaving: boolean

  constructor(
    public dialogController: DialogController,
    private validator: IValidator,
    private hubApi: HubApi
  ) {
    Validation.ensureDecoratorsOn(HubRequests.Update, ValidationRules);
  }

  save() {
    this.isSaving = true;

    this.validator.validateObject(this.update, true)
      .then(() => this.hubApi.update(this.update))
      .then(() => this.onSaved())
      .then(() => this.dialogController.ok())
      .catch(() => this.isSaving = false)
  }

  activate(params: IEditHubGeneralInfoModalModel) {
    this.original = _.clone(params.updateModel);
    this.update = _.clone(params.updateModel);
    this.onSaved = params.onSaved || (() => Promise.resolve())
  }
}
