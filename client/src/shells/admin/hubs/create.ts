import { ComponentDetached, autoinject } from "aurelia-framework";
import { HubRequests, Validation } from "mean-au-ts-shared";
import { FormWrap } from "resources/elements/form-wrap/form-wrap";
import { HubApi } from "shared/apis/hub.api";
import { IValidator } from "shared/services/validator/validator.service";
import { ValidationRules } from "aurelia-validation";
import * as toastr from 'toastr'
import { Router } from "aurelia-router";
import { IBreadcrumb } from "resources/elements/header/header";

@autoinject
export class HubCreate implements ComponentDetached {
  createBody: HubRequests.Create = new HubRequests.Create();
  infoForm: FormWrap
  contactForm: FormWrap
  addressForm: FormWrap
  isCollectionPointForm: FormWrap
  isCompostingForm: FormWrap
  notesForm: FormWrap
  isSaving: boolean
  breadcrumbs: IBreadcrumb[]

  constructor(
    private hubApi: HubApi,
    private validator: IValidator,
    private router: Router
  ) { 
    Validation.ensureDecoratorsOn(HubRequests.Create, ValidationRules);
  }

  create() {
    this.isSaving = true;

    this.validator.validateObject(this.createBody, true)
      .then(() => this.hubApi.create(this.createBody))
      .then(newHub => {
          toastr.success(`Succesfully created hub`);

          this.infoForm.clear();
          this.contactForm.clear();
          this.addressForm.clear();
          this.isCollectionPointForm.clear();
          this.isCompostingForm.clear();
          this.notesForm.clear()

          this.isSaving = false;
          this.router.navigate(`read/${newHub.id}`)
      }).catch(() => this.isSaving = false)
  }

  activate() {
    this.breadcrumbs = [{
      href: 'hubs',
      title: 'Hubs'
    }]
  }

  detached(): void {
    this.infoForm.clear();
    this.contactForm.clear();
    this.addressForm.clear();
    this.isCollectionPointForm.clear();
    this.isCompostingForm.clear();
    this.notesForm.clear();
  }
}
