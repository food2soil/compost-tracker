import { Aurelia, autoinject } from 'aurelia-framework';
import { Router, RouterConfiguration } from 'aurelia-router';
import { PLATFORM } from 'aurelia-pal';
import { IAuth } from 'shared/services/auth/auth.service';

@autoinject
export class Hubs {
  router: Router;

  constructor(
    private auth: IAuth,
    private aurelia: Aurelia
  ) { }

  configureRouter(config: RouterConfiguration, router: Router) {
    config.options.pushState = true;

    config.map([{
      route: '',
      name: 'list',
      moduleId: PLATFORM.moduleName('./list'),
      nav: true,
      title: 'List'
    }, {
      route: 'create',
      name: 'create',
      moduleId: PLATFORM.moduleName('./create'),
      nav: true,
      title: 'Create'
    }, {
      href: 'read/:id',
      route: 'read/:id',
      moduleId: PLATFORM.moduleName('./read/read'),
      nav: true,
      title: 'Info'
    }, {
      href: 'link-generators/:id',
      route: 'link-generators/:id',
      moduleId: PLATFORM.moduleName('./link-generators'),
      nav: true,
      title: 'Link Generators'
    }, {
      href: 'create-work/:id',
      route: 'create-work/:id',
      moduleId: PLATFORM.moduleName('./work/create-work'),
      nav: true,
      title: 'New Work'
    }]);

    this.router = router;
  }
}
