import { autoinject, observable } from 'aurelia-framework';
import { HubRequests, Utilities, Validation, GeneratorRead, UserRead, AddressShared, HubWithTechniciansAndGeneratorsRead, WorkRead, BayRead } from 'mean-au-ts-shared';
import { DialogService } from 'aurelia-dialog';
import * as toastr from 'toastr'
import { HubApi } from 'shared/apis/hub.api';
import { SelectTechnicianModal, ISelectTechnicianModalActivate } from 'shared/modals/technicians/select-technician-modal';
import { IValidator } from 'shared/services/validator/validator.service';
import { ValidationRules } from 'aurelia-validation';
import { TechnicianListGroupItem } from 'resources/elements/list-group/technician-list-group/technician-list-group-item';
import { GeneratorListGroupItem } from 'resources/elements/list-group/generator-list-group/generator-list-group-item';
import { IGridRowClick } from 'resources/elements/grid/grid-row-action/grid-row-click';
import { EditHubGeneralInfoModal, IEditHubGeneralInfoModalModel } from '../modals/edit-general-modal';
import { ActiveBatchesList, IActiveBatchesListParams } from './list-active-batches';
import { FinishedBatchesList } from './list-finished-batches';
import { RecentWorkList, IRecentWorkListParams } from './list-recent-work';
import { WorkApi } from 'shared/apis/work.api';
import { HubBays, IHubBaysModel } from '../../shared/bay-visuals/hub/hub-bays';
import { IBreadcrumb } from 'resources/elements/header/header';
import { IAuth } from 'shared/services/auth/auth.service';

declare type HubReadPage = 'general' | 'batches' | 'work' | ''

@autoinject
export class HubReadVm {
  TechnicianListItem = TechnicianListGroupItem
  GeneratorListItem = GeneratorListGroupItem

  private bays: BayRead.BayViewModel[]

  currentPage: HubReadPage
  hubBaysModel: IHubBaysModel
  
  generatorsBody: HubRequests.UpdateGenerators = new HubRequests.UpdateGenerators();
  techniciansBody: HubRequests.UpdateTechnicians = new HubRequests.UpdateTechnicians();

  generatorItemClick: IGridRowClick<GeneratorRead.GeneratorViewModel> = {
    getHref: (item) => `generators/read/${item.id}`
  }

  technicianItemClick: IGridRowClick<UserRead.UserViewModel> = {
    getHref: (item) => this.auth.currentUser.id === item.id ? `technicians/read/${item.id}` : undefined
  }

  adminRead: HubWithTechniciansAndGeneratorsRead.HubViewModel
  lastWork: WorkRead.WorkViewModel
  lastApprovedWork: WorkRead.WorkViewModel
  addressString: string

  isSelectingTechnicians: boolean
  isSelectingGenerators: boolean
  isEditing: boolean

  activeBatchesListParams: IActiveBatchesListParams
  recentWorkListParams: IRecentWorkListParams

  breadcrumbs: IBreadcrumb[]

  constructor(
    public auth: IAuth,
    private hubApi: HubApi,
    private workApi: WorkApi,
    private validator: IValidator,
    private dialogService: DialogService,
    public activeBatches: ActiveBatchesList,
    public finishedBatches: FinishedBatchesList,
    public recentWork: RecentWorkList,
    public hubBaysVm: HubBays
  ) { 
    Validation.ensureDecoratorsOn(HubRequests.UpdateGenerators, ValidationRules);
    Validation.ensureDecoratorsOn(HubRequests.UpdateTechnicians, ValidationRules);
  }

  _getUpdateModel() {
    let model = Utilities.castTo<HubRequests.Update>(this.adminRead, HubRequests.Update);
    
    model.addressPrimaryLine = this.adminRead.address.primaryLine
    model.addressSecondaryLine = this.adminRead.address.secondaryLine
    model.addressCity = this.adminRead.address.city
    model.addressState = this.adminRead.address.state
    model.addressZip = this.adminRead.address.zip

    return model;
  }

  setPage(page: HubReadPage) {
    this.currentPage = page == null || page === '' ? 'general' : page
  }

  selectTechnicians() {
    this.isSelectingTechnicians = true;

    return this.dialogService.open({
      viewModel: SelectTechnicianModal,
      model: <ISelectTechnicianModalActivate>{
        initialSelections: this.adminRead.technicians,
        postSelect: (selectedTechnicians: UserRead.UserViewModel[]) => {
          this.techniciansBody.primaryTechnicianIds = selectedTechnicians.map(x => x.id);

          return this.validator.validateObject(this.techniciansBody, true)
            .then(() => this.hubApi.updateTechnician(this.techniciansBody))
            .then(() => this.onHubChanged())
        }
      }
    }).whenClosed(result => { 
      if (result.wasCancelled) throw 'dialog canceled';
    })
    .then(() => {
      toastr.success('Successfully updated hub technicians')
      this.isSelectingTechnicians = false
    }).catch(() => this.isSelectingTechnicians = false)
  }

  editGeneralInfo() {
    this.isEditing = true

    let model: IEditHubGeneralInfoModalModel = {
      updateModel: this._getUpdateModel(),
      onSaved: () => this.onHubChanged()
    }

    this.dialogService.open({
      viewModel: EditHubGeneralInfoModal,
      model: model,
      lock: false 
    }).whenClosed(result => {
      if (result.wasCancelled) throw 'edit canceled'

      toastr.success('Successfully updated Hub')
      this.isEditing = false
    }).catch(() => this.isEditing = false)
  }

  onHubChanged() {
    return this.hubApi.readWithTechGen(this.adminRead.id)
      .then(x => {
        this.adminRead = x;
        this.addressString = AddressShared.addressToString(this.adminRead.address);  
      })
  }

  activate(params: { id: string }) {
    this.currentPage = 'general'
    this.generatorsBody.id = params.id
    this.techniciansBody.id = params.id
    
    let baysPromise = this.hubApi.readBayList(params.id)
      .then(x => {
        this.bays = x
        
        this.activeBatchesListParams = {
          bays: this.bays,
          hubId: params.id
        }
        
        this.recentWorkListParams = {
          bays: this.bays,
          hubId: params.id
        }
      })

    let hubPromise = this.hubApi.readWithTechGen(params.id)
      .then(x => {
        this.adminRead = x;
        this.addressString = AddressShared.addressToString(this.adminRead.address);  
        this.breadcrumbs = [{
          href: 'hubs',
          title: 'Hubs'
        }]
      })

    let lastWorkPromise = this.workApi.list({
      hub: params.id, 
      sortBy: 'date', 
      sortDir: 'desc',
      limit: 1
    }).then(work => {
      this.lastWork = work[0]
      
      if (!this.lastWork || this.lastWork.isApproved) {
        return Promise.resolve([this.lastWork])
      } else {
        return this.workApi.list({
          hub: params.id, 
          sortBy: 'date', 
          sortDir: 'desc',
          limit: 1,
          isApproved: true
        })
      }
    })
    .then(work => {
      this.lastApprovedWork = work[0]
    })

    return Promise.all([hubPromise, lastWorkPromise, baysPromise])
      .then(() => {
        this.hubBaysModel = {
          bays: this.bays,
          lastWork: this.lastApprovedWork
        }
      })
  }
}
