import { HubRead, BatchRead, Enums, BatchRequests } from "mean-au-ts-shared";
import { GridColumn } from "resources/elements/grid/grid-column/grid-column";
import { LocalGridRowProvider } from "resources/elements/grid/grid-row-provider/local-grid-row-provider";
import { BatchCookTimeTd } from "shared/grid-templates/batch/batch-cook-time-td";
import { IBatchEventDateData, BatchEventDateTd } from "shared/grid-templates/batch/batch-event-date-td";
import { IGridConfig } from "resources/elements/grid/grid";
import { autoinject } from "aurelia-framework";
import { BatchApi } from "shared/apis/batch.api";
import { BatchBucketsHarvestedTd } from "../../../../shared/grid-templates/batch/batch-buckets-harvested-td";

@autoinject
export class FinishedBatchesList {
  noRowsMessage: string = 'There are no finished batches at this hub'
  gridConfig: IGridConfig<BatchRead.BatchViewModel>

  constructor(
    private batchApi: BatchApi
  ) { }

  activate(params: HubRead.HubViewModel) {
    let query: BatchRequests.List = {
      hub: params.id,
      isActive: false
    }

    return this.batchApi.list(query)
      .then(x => {
        this.gridConfig = {
          rowClick: { getHref: (row) => `batches/read/${row.id}` },
          rowProvider: new LocalGridRowProvider<BatchRead.BatchViewModel>(x),
          columns: [
            new GridColumn<BatchRead.BatchViewModel>({
              title: 'Name',
              key: 'name',
              isSearchable: true,
              tdClass: 'td-fit-content'
            }),
            new GridColumn<BatchRead.BatchViewModel>({
              title: 'Fill Start',
              key: 'created',
              valueConverter: 'date',
              tdClass: 'td-fit-content td-no-wrap'
            }),
            new GridColumn<BatchRead.BatchViewModel, IBatchEventDateData>({
              title: 'Harvest',
              ViewModel: BatchEventDateTd,
              extraData: { type: Enums.BayEvent.Harvest },
              tdClass: 'td-fit-content td-no-wrap'
            }),
            new GridColumn<BatchRead.BatchViewModel>({
              title: 'Cook Time (weeks)',
              ViewModel: BatchCookTimeTd,
              tdClass: 'td-fit-content'
            }),
            new GridColumn<BatchRead.BatchViewModel>({
              title: 'Buckets of Compost',
              ViewModel: BatchBucketsHarvestedTd,
              tdClass: 'td-fit-content'
            })
          ]
        }
      })
  }
}
