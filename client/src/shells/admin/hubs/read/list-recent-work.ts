import { BayRead, WorkWithTechnicianRead, WorkReadBase } from "mean-au-ts-shared";
import { GridColumn } from "resources/elements/grid/grid-column/grid-column";
import { LocalGridRowProvider } from "resources/elements/grid/grid-row-provider/local-grid-row-provider";
import { autoinject } from "aurelia-framework";
import { WorkApi } from "shared/apis/work.api";
import { WorkActionsTd, IWorkActionsData } from "shared/grid-templates/work/work-actions-td";
import { WorkStatusTd } from "shared/grid-templates/work/work-status-td";
import { WorkTechnicianTd } from "shared/grid-templates/work/work-technician-td";
import { IGridConfig } from "resources/elements/grid/grid";

export interface IRecentWorkListParams {
  hubId: string
  bays: BayRead.BayViewModel[]
}

@autoinject
export class RecentWorkList {
  private bays: BayRead.BayViewModel[]

  noRowsMessage: string = 'There is no recent work for this hub'
  gridConfig: IGridConfig<WorkReadBase.BaseWorkViewModel | WorkReadBase.IHaveTechnician>

  constructor(
    private workApi: WorkApi,
  ) { }

  activate(params: IRecentWorkListParams) {
    this.bays = params.bays

    return this.workApi.listWithTech({
      hub: params.hubId, 
      sortBy: 'date', 
      sortDir: 'desc' 
    }).then(work => {
      this.gridConfig = {
        rowClick: { getHref: (row: WorkWithTechnicianRead.WorkViewModel) => `work/read/${row.id}` },
        rowProvider: new LocalGridRowProvider<WorkWithTechnicianRead.WorkViewModel>(work),
        columns: [
          new GridColumn<WorkReadBase.BaseWorkViewModel>({
            title: 'Date',
            key: 'date',
            valueConverter: 'date',
            tdClass: 'td-fit-content td-no-wrap'
          }),
          new GridColumn<WorkReadBase.IHaveTechnician>({
            title: 'Technician',
            ViewModel: WorkTechnicianTd,
            centerHorizontally: false
          }),
          new GridColumn<WorkReadBase.BaseWorkViewModel, IWorkActionsData>({
            title: 'Actions',
            ViewModel: WorkActionsTd,
            extraData: { bays: this.bays },
            centerHorizontally: false
          }),
          new GridColumn<WorkReadBase.BaseWorkViewModel>({
            title: 'Hours',
            tdClass: 'td-fit-content',
            centerHorizontally: true,
            key: 'timeSpent',
          }),
          new GridColumn<WorkReadBase.BaseWorkViewModel>({
            title: 'Status',
            ViewModel: WorkStatusTd,
            tdClass: 'td-fit-content'
          })
        ]
      }
    })
  }
}
