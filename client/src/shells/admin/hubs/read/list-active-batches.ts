import { BatchRead, BayRead, Enums, BatchRequests } from "mean-au-ts-shared";
import { GridColumn } from "resources/elements/grid/grid-column/grid-column";
import { LocalGridRowProvider } from "resources/elements/grid/grid-row-provider/local-grid-row-provider";
import { autoinject } from "aurelia-framework";
import { BatchCookTimeTd } from "shared/grid-templates/batch/batch-cook-time-td";
import { BatchCurrentBayTd, IBatchCurrentBayData } from "shared/grid-templates/batch/batch-current-bay-td";
import { BatchEventDateTd, IBatchEventDateData } from "shared/grid-templates/batch/batch-event-date-td";
import { IGridConfig } from "resources/elements/grid/grid";
import { BatchApi } from "shared/apis/batch.api";
import { BatchBucketsHarvestedTd } from "shared/grid-templates/batch/batch-buckets-harvested-td";

export interface IActiveBatchesListParams {
  hubId: string
  bays: BayRead.BayViewModel[]
}

@autoinject
export class ActiveBatchesList {
  private bays: BayRead.BayViewModel[]

  noRowsMessage: string = 'There are no active batches at this hub'
  gridConfig: IGridConfig<BatchRead.BatchViewModel>

  constructor(
    private batchApi: BatchApi
  ) { }

  activate(params: IActiveBatchesListParams) {
    this.bays = params.bays

    let query: BatchRequests.List = {
      hub: params.hubId,
      isActive: true
    }

    return this.batchApi.list(query)
      .then(batches => {
        this.gridConfig = {
          rowClick: { getHref: (row) => `batches/read/${row.id}` },
          rowProvider: new LocalGridRowProvider<BatchRead.BatchViewModel>(batches),
          columns: [
            new GridColumn<BatchRead.BatchViewModel>({
              title: 'Name',
              key: 'name',
              isSearchable: true,
              tdClass: 'td-fit-content'
            }),
            new GridColumn<BatchRead.BatchViewModel>({
              title: 'Fill Start',
              key: 'created',
              valueConverter: 'date',
              tdClass: 'td-fit-content td-no-wrap'
            }),
            new GridColumn<BatchRead.BatchViewModel, IBatchEventDateData>({
              title: 'Last Turn',
              ViewModel: BatchEventDateTd,
              extraData: { type: Enums.BayEvent.Turn },
              tdClass: 'td-fit-content td-no-wrap'
            }),
            new GridColumn<BatchRead.BatchViewModel, IBatchCurrentBayData>({
              title: 'Current Bay',
              ViewModel: BatchCurrentBayTd,
              extraData: { bays: this.bays },
              tdClass: 'td-fit-content'
            }),
            new GridColumn<BatchRead.BatchViewModel>({
              title: 'Cook Time (weeks)',
              ViewModel: BatchCookTimeTd,
              tdClass: 'td-fit-content'
            }),
            new GridColumn<BatchRead.BatchViewModel>({
              title: 'Buckets of Compost',
              ViewModel: BatchBucketsHarvestedTd,
              tdClass: 'td-fit-content'
            })
          ]
        }
      })
  }
}
