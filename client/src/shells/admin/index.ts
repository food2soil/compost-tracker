import { Aurelia, autoinject, TaskQueue } from 'aurelia-framework';
import { Router, RouterConfiguration, PipelineStep, Next, NavigationInstruction, Redirect } from 'aurelia-router';
import { PLATFORM } from 'aurelia-pal';
import { IAuth } from 'shared/services/auth/auth.service';
import { AuthApi } from 'shared/apis/auth.api';
import { BusyService } from 'shared/services/busy/busy.service';
import { DialogCloseError } from 'aurelia-dialog/dist/commonjs/dialog-close-error';
import { DialogService } from 'aurelia-dialog';
import { WorkApi } from 'shared/apis/work.api';
import { WorkRequests, HubRequests, Time } from 'mean-au-ts-shared';
import { HubApi } from 'shared/apis/hub.api';
import * as nprogress from 'nprogress';
import * as $ from 'jquery'

@autoinject
export class App {
  router: Router;
  hasAlert: boolean
  isLoading: boolean

  constructor(
    private auth: IAuth,
    private authApi: AuthApi,
    private aurelia: Aurelia,
    public busyService: BusyService,
    private dialogService: DialogService,
    private workApi: WorkApi,
    private hubApi: HubApi,
    private taskQueue: TaskQueue
  ) { }

  signOut() {
    this.authApi.signout()
  }

  configureRouter(config: RouterConfiguration, router: Router) {
    config.title = 'Food2Soil';
    config.options.pushState = true;

    config.addPreActivateStep({
      run: (instruction: NavigationInstruction, next) => {
        // Start Loading
        this.isLoading = true
        nprogress.start()
        return next()
      }
    })

    config.addPreActivateStep({
      run: (instruction: NavigationInstruction, next) => {
        // Save the url of the route we're navigating to before activate for redirect in case of 401
        this.auth.signInRedirect = instruction.fragment.substring(1);
        return next()
      }
    })

    config.addPreRenderStep({
      run: (instruction: NavigationInstruction, next) => {
        // close all open dialogs when navigating
        this.dialogService.closeAll()
        return next()
      }
    })

    config.addPreRenderStep({
      run: (instruction: NavigationInstruction, next) => {
        // Force collapse the navbar
        $('.navbar-collapse').collapse('hide');
        return next()
      }
    })

    config.addPostRenderStep({
      run: (navigationInstruction: NavigationInstruction, next: Next) => {
        // Scroll to top of the page
        window.scroll(0, 0);
        return next();
      }
    });

    config.addPostRenderStep({
      run: (navigationInstruction: NavigationInstruction, next: Next) => {
        // Finish Loading
        nprogress.done()
        setTimeout(() => this.isLoading = false, 500);
        return next();
      }
    });

    config.map([
      {
        route: [''],
        name: 'home',
        moduleId: PLATFORM.moduleName('./home/home'),
        nav: true,
        title: 'Home'
      }, {
        route: 'account',
        name: 'account',
        moduleId: PLATFORM.moduleName('./account/index'),
        nav: true,
        title: 'Account'
      }, {
        route: 'hubs',
        name: 'hubs',
        moduleId: PLATFORM.moduleName('./hubs/index'),
        nav: true,
        title: 'Hub'
      }, {
        route: 'technicians',
        name: 'technicians',
        moduleId: PLATFORM.moduleName('./technicians/index'),
        nav: true,
        title: 'Technician'
      }, {
        route: 'generators',
        name: 'generators',
        moduleId: PLATFORM.moduleName('./generators/index'),
        nav: true,
        title: 'Generator'
      }, {
        route: 'batches',
        name: 'batches',
        moduleId: PLATFORM.moduleName('./batches/index'),
        nav: true,
        title: 'Batch'
      }, {
        route: 'work',
        name: 'work',
        moduleId: PLATFORM.moduleName('./work/index'),
        nav: true,
        title: 'Work'
      }
    ]);

    config.mapUnknownRoutes(PLATFORM.moduleName('./home/home'));

    this.router = router;
  }

  activate() {
    if (!this.auth.currentUser) {
      return this.aurelia.setRoot(PLATFORM.moduleName('shells/anonymous/index'));
    }

    let workQuery: WorkRequests.List = {
      isApproved: false,
      sortBy: 'date', 
      sortDir: 'desc' 
    }

    return this.workApi.listWithHubTech(workQuery)
      .then(work => {
        this.hasAlert = work.length > 0

        if (this.hasAlert) return Promise.resolve([])
        return this.hubApi.list({ needsCarbon: true })
      })
      .then(stats => {
        if (this.hasAlert) return
        this.hasAlert = stats.length > 0
      })
  }
}
