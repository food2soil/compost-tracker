import { RoutableComponentActivate } from "aurelia-router";
import { autoinject } from "aurelia-framework";
import { Validation, UserRequests } from "mean-au-ts-shared";
import { ValidationRules } from "aurelia-validation";
import * as toastr from 'toastr';
import { FormWrap } from "resources/elements/form-wrap/form-wrap";
import { MeApi } from "shared/apis/me.api";
import { IValidator } from "shared/services/validator/validator.service";
import { AuthApi } from "shared/apis/auth.api";
 
@autoinject
export class AccountProfile implements RoutableComponentActivate {

  isChanging: boolean
  isUpdating: boolean

  generalInformationEdit: UserRequests.EditProfile = new UserRequests.EditProfile();
  changePasswordDetails: UserRequests.ChangePassword = new UserRequests.ChangePassword();

  generalInformationForm: FormWrap
  changePasswordForm: FormWrap

  constructor(
    private meApi: MeApi,
    private validator: IValidator,
  ) {
    Validation.ensureDecoratorsOn(UserRequests.EditProfile, ValidationRules);
    Validation.ensureDecoratorsOn(UserRequests.ChangePassword, ValidationRules);
  }

  changePassword() {
    this.isChanging = true

    this.validator.validateObject(this.changePasswordDetails, true)
      .then(() => this.meApi.changePassword(this.changePasswordDetails))
      .then(() => {
        toastr.success('Password changed successfully');
        this.changePasswordForm.clear();
        this.isChanging = false
     })
      .catch(() => this.isChanging = false)
  }

  updateGeneralInfo() {
    this.isUpdating = true

    this.validator.validateObject(this.generalInformationEdit, true)
      .then(() => {
        return this.meApi.updateProfile(this.generalInformationEdit)})
      .then(() => {
        toastr.success('Profile updated successfully')
        this.isUpdating = false
      })
      .catch(() => this.isUpdating = false)
  }

  activate() {
    return this.meApi.me().then(response => {
      this.generalInformationEdit = response;
    })
  }
}
