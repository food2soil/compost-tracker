import { Router, RouterConfiguration } from 'aurelia-router';
import { PLATFORM } from 'aurelia-pal';

export class AccountIndex {
  router: Router;

  configureRouter(config: RouterConfiguration, router: Router) {
    
    config.map([{
      route: '',
      name: 'profile',
      moduleId: PLATFORM.moduleName('./profile'),
      nav: true,
      title: 'Settings'
    }]);

    this.router = router;
  }
}
