import { WorkWithHubRead } from "mean-au-ts-shared";
import { GridColumn } from "resources/elements/grid/grid-column/grid-column";
import { LocalGridRowProvider } from "resources/elements/grid/grid-row-provider/local-grid-row-provider";
import { autoinject } from "aurelia-framework";
import { WorkApi } from "shared/apis/work.api";
import { WorkHubTd } from "shared/grid-templates/work/work-hub-td";
import { WorkStatusTd } from "shared/grid-templates/work/work-status-td";
import { IGridConfig } from "resources/elements/grid/grid";

@autoinject
export class LoggedWorkList {
  private technicianId: string

  noRowsMessage: string = 'This technician has no logged work for this timeframe'
  gridConfig: IGridConfig<WorkWithHubRead.WorkViewModel>


  monthFilter: { date: Date }

  constructor(
    private workApi: WorkApi
  ) { }

  updateList() {
    this.gridConfig.rowProvider.filter((row) => {
      return row.date.valueOf() >= this.monthFilter.date.valueOf() && row.date.valueOf() < new Date(this.monthFilter.date).setMonth(this.monthFilter.date.getMonth() + 1)
    })
  }

  activate(params: { technicianId: string }) {
    this.monthFilter = { date: new Date() }
    this.technicianId = params.technicianId

    return this.workApi.listWithHub({
      technician: this.technicianId, 
      sortBy: 'date',
      sortDir: 'desc'
    }).then(work => {
      this.gridConfig = {
        rowClick: { getHref: (row) => `work/read/${row.id}` },
        rowProvider: new LocalGridRowProvider(work),
        columns: [
          new GridColumn<WorkWithHubRead.WorkViewModel>({
            title: 'Date',
            key: 'date',
            valueConverter: 'date',
            tdClass: 'td-no-wrap td-fit-content',
            thClass: 'text-center'
          }),
          new GridColumn({
            title: 'Hub',
            ViewModel: WorkHubTd,
            searchGetter: (row) => row.hub.name,
            thClass: 'text-center'
          }),
          new GridColumn<WorkWithHubRead.WorkViewModel>({
            title: 'Hours Spent',
            key: 'timeSpent',
            centerHorizontally: true,
            thClass: 'text-center th-no-wrap'
          }),
          new GridColumn({
            title: 'Status',
            ViewModel: WorkStatusTd,
            tdClass: 'td-no-wrap',
            thClass: 'text-center'
          }),
        ]
      }
    })
  }
}
