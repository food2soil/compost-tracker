import { TechnicianApi } from "shared/apis/technician.api";
import { autoinject, BindingEngine, Disposable } from "aurelia-framework";
import { getMonthsStats, UserRead } from "mean-au-ts-shared";
import { LoggedWorkList } from "./list-logged-work";
import { IBreadcrumb } from "resources/elements/header/header";
import { IAuth } from "shared/services/auth/auth.service";

@autoinject
export class TechnicianReadPage {
  private statsSubscription: Disposable

  read: UserRead.UserViewModel
  hoursCurrentMonth: number
  breadcrumbs: IBreadcrumb[]

  constructor(
    private technicianApi: TechnicianApi,
    private bindingEngine: BindingEngine,
    private loggedWorkList: LoggedWorkList,
    private auth: IAuth
  ) { }

  _refreshMonthlyHours() {
    let stats = getMonthsStats(this.loggedWorkList.monthFilter.date, this.read.stats.monthly)
    this.hoursCurrentMonth = stats ? stats.stats.hours : 0
  }

  activate(params: { id: string }) {
    if (this.auth.isAdmin) {
      this.breadcrumbs = [{
        href: 'technicians',
        title: 'Technicians'
      }]
    }
    
    return this.technicianApi.read(params.id)
      .then(x => this.read = x)
  }

  attached() {
    this.statsSubscription = this.bindingEngine
      .propertyObserver(this.loggedWorkList.monthFilter, 'date')
      .subscribe(() => this._refreshMonthlyHours())

    this._refreshMonthlyHours();
  }

  detached() {
    if (this.statsSubscription) this.statsSubscription.dispose()
  }
}
