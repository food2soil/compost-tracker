import { autoinject } from "aurelia-framework";
import { InvitationWithHubsRead, BaseInvitationRead } from "mean-au-ts-shared";
import { GridColumn } from "resources/elements/grid/grid-column/grid-column";
import { LocalGridRowProvider } from "resources/elements/grid/grid-row-provider/local-grid-row-provider";
import { InvitationApi } from "shared/apis/invitation.api";
import { InvitationNameTd } from "shared/grid-templates/invitation/invitation-name-td";
import { InvitationRolesTd } from "shared/grid-templates/invitation/invitation-roles-td";
import { InvitationHubsTd } from "shared/grid-templates/invitation/invitation-hubs-td";
import { InvitationActionsTd, IInvitationActionsData } from "shared/grid-templates/invitation/invitation-actions-td";
import { IGridConfig } from "resources/elements/grid/grid";
import * as toastr from 'toastr'

@autoinject
export class PendingInvites {
  pendingInvites: InvitationWithHubsRead.InvitationViewModel[]
  gridConfig: IGridConfig<BaseInvitationRead.InvitationViewModel | BaseInvitationRead.IHaveHubs>

  constructor(
    private invitationApi: InvitationApi
  ) { }

  reload() {
    return this.invitationApi.listWithHubs()
      .then(invites => {
        this.pendingInvites = invites
        this.gridConfig.rowProvider.reload(invites)
      })
  }

  resendInvite(inviteId: string) {
    return this.invitationApi.resendInvite(inviteId)
      .then(() => {
        toastr.success('Successfully resent Invitation')
      })
  }

  deleteInvite(inviteId: string) {
    return this.invitationApi.deleteInvite(inviteId)
      .then(() => this.reload())
      .then(() => {
        toastr.success('Successfully deleted Invitation')
      })
  }

  activate() {
    this.gridConfig = {
      rowProvider: new LocalGridRowProvider<InvitationWithHubsRead.InvitationViewModel>(),
      columns: [
        new GridColumn<BaseInvitationRead.InvitationViewModel, IInvitationActionsData>({
          title: '',
          ViewModel: InvitationActionsTd,
          extraData: { 
            deleteInvitation: (inviteId) => this.deleteInvite(inviteId),
            resendInvitation: (inviteId) => this.resendInvite(inviteId)
          }
        }), 
        new GridColumn<BaseInvitationRead.InvitationViewModel>({
          title: 'Name',
          ViewModel: InvitationNameTd,
          searchGetter: (row) =>  row.firstName + ' ' + row.lastName
        }), 
        new GridColumn<InvitationWithHubsRead.InvitationViewModel>({
          title: 'Email',
          key: 'email',
          isSearchable: true
        }), 
        new GridColumn<BaseInvitationRead.IHaveHubs>({
          title: 'Hub(s)',
          ViewModel: InvitationHubsTd
        }),
        new GridColumn<BaseInvitationRead.InvitationViewModel>({
          title: 'User Roles',
          ViewModel: InvitationRolesTd
        })
      ]
    }
    
    return this.reload()
  }
}
