import { LocalGridRowProvider } from "resources/elements/grid/grid-row-provider/local-grid-row-provider";
import { GridColumn } from "resources/elements/grid/grid-column/grid-column";
import { UserRead } from "mean-au-ts-shared";
import { autoinject } from "aurelia-framework";
import { TechnicianApi } from "shared/apis/technician.api";
import { PendingInvites } from "./list-pending-invites";
import { DialogService } from "aurelia-dialog";
import { SendInviteModal } from "./modal-send-invite/modal-send-invite";
import { Router } from "aurelia-router";
import { IGridConfig } from "resources/elements/grid/grid";
import { IDialogModel } from "resources/elements/dialog-wrap/dialog-wrap";
import { TechnicianStatsAllTimeHoursTd } from "shared/grid-templates/technician-stats/technician-stats-alltime-hours-td";
import { TechnicianStatsMonthHoursTd, IMonthStatsData } from "shared/grid-templates/technician-stats/technician-stats-month-hours-td";

@autoinject
export class TechnicianList {
  isInviting: boolean
  defaultInvite: boolean
  gridConfig: IGridConfig<UserRead.UserViewModel>

  constructor(
    private technicianApi: TechnicianApi,
    public listPendingInvites: PendingInvites,
    private dialogService: DialogService,
    private router: Router
  ) { }

  sendInvite() {
    this.isInviting = true

    let model: IDialogModel = {
      okDelegate: () => this.listPendingInvites.reload()
    }

    this.dialogService.open({
      viewModel: SendInviteModal,
      model: model,
      lock: false
    }).whenClosed(() => this.isInviting = false)
  }

  activate(params: any) {
    this.defaultInvite = !!params.invite

    // Overwrite the href to remove the invite param
    this.router.navigate('technicians', { replace: true, trigger: false })

    return this.technicianApi.list()
      .then(stats => {
        this.gridConfig = {
          rowClick: { getHref: (row) => `technicians/read/${row.id}` },
          rowProvider: new LocalGridRowProvider(stats),
          columns: [
            new GridColumn<UserRead.UserViewModel>({
              title: 'Name',
              key: 'displayName',
              centerHorizontally: true,
              tdClass: 'td-fit-content td-no-wrap',
              thClass: 'th-no-wrap'
            }),
            new GridColumn<UserRead.UserViewModel>({
              title: 'Hours (All Time)',
              ViewModel: TechnicianStatsAllTimeHoursTd,
              tdClass: 'td-fit-content',
              thClass: 'th-no-wrap'
            }),
            new GridColumn<UserRead.UserViewModel, IMonthStatsData>({
              title: 'Hours (This Month)',
              ViewModel: TechnicianStatsMonthHoursTd,
              tdClass: 'td-fit-content',
              thClass: 'th-no-wrap'
            }),
            new GridColumn<UserRead.UserViewModel, IMonthStatsData>({
              title: 'Hours (Last Month)',
              ViewModel: TechnicianStatsMonthHoursTd,
              extraData: { lastMonth: true },
              tdClass: 'td-fit-content',
              thClass: 'th-no-wrap'
            }),
          ]
        }
      })
  }

  attached() {
    if (this.defaultInvite) {
      this.sendInvite()
    }
  }
}
