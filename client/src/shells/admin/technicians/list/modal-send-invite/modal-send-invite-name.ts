
import { autoinject } from "aurelia-framework";
import { Validation, InvitationRequests } from "mean-au-ts-shared";
import { IValidator } from "shared/services/validator/validator.service";
import { ValidationRules } from "aurelia-validation";
import { InvitationApi } from "shared/apis/invitation.api";
import { IDialogPage } from "resources/elements/dialog-wrap/dialog-wrap";

@autoinject
export class SendInviteModalName implements IDialogPage {
  title: string = 'Name, Email, and Role'
  isWorking: boolean
  inviteBody: InvitationRequests.InviteTechnician

  constructor(
    private invitationApi: InvitationApi,
    private validator: IValidator
  ) { 
    Validation.ensureDecoratorsOn(InvitationRequests.InviteTechnician, ValidationRules);
  }

  goNext() {
    this.isWorking = true;

    return this.validator.validateObject(this.inviteBody, false)
      .then(() => this.invitationApi.checkEmail(this.inviteBody.email))
      .then(isEmailAvailable => {
        if (isEmailAvailable) return

        this.isWorking = false
        throw new Error('This email is already in use')
      })
  }

  initialize(): Promise<void> {
    return Promise.resolve()
  }

  activate(inviteBody: InvitationRequests.InviteTechnician) {
    this.inviteBody = inviteBody
  }
}
