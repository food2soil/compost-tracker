import { LocalGridRowProvider } from "resources/elements/grid/grid-row-provider/local-grid-row-provider";
import { GridColumn } from "resources/elements/grid/grid-column/grid-column";
import { HubApi } from "shared/apis/hub.api";
import { HubRead, InvitationRequests, InvitationRead, HubReadBase } from "mean-au-ts-shared";
import { autoinject } from "aurelia-framework";
import { IDialogPage } from "resources/elements/dialog-wrap/dialog-wrap";
import { IGridRowClickContext } from "resources/elements/grid/grid-row-action/grid-row-click";
import { InvitationApi } from "shared/apis/invitation.api";
import { ToggleTd } from "shared/grid-templates/toggle-td";
import { HubTechnicianStatusTd, IHubTechnicianStatusData } from "shared/grid-templates/hub/hub-technician-status-td";
import { IGridConfig } from "resources/elements/grid/grid";

@autoinject
export class SendInviteModalHub implements IDialogPage {
  private hubs: (HubRead.HubViewModel & IGridRowClickContext)[]
  private inviteBody: InvitationRequests.InviteTechnician
  private pendingInvites: InvitationRead.InvitationViewModel[]

  title: string = 'Default Hubs'
  gridConfig: IGridConfig<HubReadBase.BaseHubViewModel | IGridRowClickContext>

  constructor(
    private hubApi: HubApi,
    private invitationApi: InvitationApi
  ) { }

  goNext(): Promise<void> {
    this.inviteBody.hubs = this.hubs.filter(x => x.__gridRow_isActive).map(x => x.id)
    return Promise.resolve()
  }

  initialize(): Promise<void> {
    return this.invitationApi.list()
      .then(invites => {
        this.pendingInvites = invites

        return this.hubApi.list()
      })
      .then((hubs) => {
        this.hubs = hubs

        this.gridConfig = {
          rowClick: { action: ToggleTd.toggleRow },
          rowProvider: new LocalGridRowProvider<HubRead.HubViewModel>(this.hubs),
          columns: [
            new GridColumn<IGridRowClickContext>({
              ViewModel: ToggleTd
            }),
            new GridColumn<HubReadBase.BaseHubViewModel>({
              title: 'Name',
              key: 'name',
              isSearchable: true
            }),
            new GridColumn<HubReadBase.BaseHubViewModel, IHubTechnicianStatusData>({
              title: 'Status',
              ViewModel: HubTechnicianStatusTd,
              extraData: { pendingInvites: this.pendingInvites },
              centerHorizontally: false
            })
          ]
        }
      })
  }

  activate(inviteBody: InvitationRequests.InviteTechnician) {
    this.inviteBody = inviteBody
  }
}
