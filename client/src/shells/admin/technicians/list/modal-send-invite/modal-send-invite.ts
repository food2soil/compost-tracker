import { autoinject } from "aurelia-framework";
import { Validation, InvitationRequests } from "mean-au-ts-shared";
import { IValidator } from "shared/services/validator/validator.service";
import { ValidationRules } from "aurelia-validation";
import * as toastr from 'toastr'
import { InvitationApi } from "shared/apis/invitation.api";
import { DialogController } from "aurelia-dialog";
import { SendInviteModalName } from "./modal-send-invite-name";
import { SendInviteModalHub } from "./modal-send-invite-hubs";
import { IDialogPage, IDialogModel } from "resources/elements/dialog-wrap/dialog-wrap";

@autoinject
export class SendInviteModal {

  model: IDialogModel
  isWorking: boolean
  inviteBody: InvitationRequests.InviteTechnician = new InvitationRequests.InviteTechnician();
  pages: IDialogPage[]
  pageIndex: number

  constructor(
    private invitationApi: InvitationApi,
    private validator: IValidator,
    private dialogController: DialogController,
    private namePage: SendInviteModalName,
    private hubPage: SendInviteModalHub
  ) { 
    Validation.ensureDecoratorsOn(InvitationRequests.InviteTechnician, ValidationRules);
  }

  cancel() {
    this.dialogController.cancel()
  }

  prevPage() {
    this.pageIndex--
  }

  nextPage() {
    this.isWorking = true

    this.pages[this.pageIndex].goNext()
      .then(() => {
        return this.pageIndex === this.pages.length - 1
          ? this.invite()
          : this.pages[this.pageIndex + 1].initialize()
      })
      .then(() => {
        this.pageIndex++
        this.isWorking = false
      })
      .catch(err => {
        toastr.error(err.message)
        this.isWorking = false
      })
  }

  invite() {
    this.isWorking = true;

    return this.validator.validateObject(this.inviteBody, true)
      .then(() => this.invitationApi.inviteTechnician(this.inviteBody))
      .then(() => this.model.okDelegate())
      .then(data => {
        toastr.success(`Succesfully sent invite email to ${this.inviteBody.email}`);
        this.dialogController.ok()
      })
      .catch(() => this.isWorking = false);
  }

  activate(model: IDialogModel) {
    this.model = model
    this.model.okDelegate = this.model.okDelegate || (() =>  Promise.resolve())

    this.pageIndex = 0
    this.pages = [
      this.namePage,
      this.hubPage
    ]
  }
}
