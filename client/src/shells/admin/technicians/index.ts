import { Aurelia, autoinject } from 'aurelia-framework';
import { Router, RouterConfiguration } from 'aurelia-router';
import { PLATFORM } from 'aurelia-pal';
import { IAuth } from 'shared/services/auth/auth.service';

@autoinject
export class Technicians {
  router: Router;

  constructor(
    private auth: IAuth,
    private aurelia: Aurelia
  ) { }

  configureRouter(config: RouterConfiguration, router: Router) {
    config.title = 'Aurelia';
    config.options.pushState = true;

    config.map([{
      route: '',
      name: 'list',
      moduleId: PLATFORM.moduleName('./list/list'),
      nav: true,
      title: 'List'
    }, {
      href: 'read/:id',
      route: 'read/:id',
      moduleId: PLATFORM.moduleName('./read'),
      nav: true,
      title: 'Info'
    }]);

    this.router = router;
  }
}
