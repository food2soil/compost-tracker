import { autoinject } from "aurelia-framework";
import { IAuth } from "shared/services/auth/auth.service";
import { HubApi } from "shared/apis/hub.api";
import { GeneratorApi } from "shared/apis/generator.api";
import { TechnicianApi } from "shared/apis/technician.api";
import { HubRead, UserRead } from "mean-au-ts-shared";
import { PendingWorkList } from "./list-pending-work";
import { ActiveHubsList } from "./list-active-hubs";
import { MyHubsList } from "./list-my-hubs";
import { NeedsCarbonList } from "./list-needs-carbon";

@autoinject
export class HomeDashboard {
  numHubs: number
  numTechnicians: number
  numGenerators: number
  user: UserRead.UserViewModel

  constructor(
    private auth: IAuth,
    private hubApi: HubApi,
    private generatorApi: GeneratorApi,
    private technicianApi: TechnicianApi,
    public pendingWorkList: PendingWorkList,
    public activeHubsList: ActiveHubsList,
    public myHubsList: MyHubsList,
    public needsCarbonList: NeedsCarbonList
  ) { }

  activate() {
    this.user = this.auth.currentUser

    if (!this.auth.isAdmin) return

    return Promise.all([
      this.hubApi.list(),
      this.technicianApi.list(),
      this.generatorApi.list()
    ]).then(results => {
      this.numHubs = results[0].length
      this.numTechnicians = results[1].length
      this.numGenerators = results[2].length
    })
  }
}
