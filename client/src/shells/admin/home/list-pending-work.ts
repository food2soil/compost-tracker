import { WorkReadBase, WorkWithHubAndTechnicianRead, WorkRequests, BayRead } from "mean-au-ts-shared";
import { GridColumn } from "resources/elements/grid/grid-column/grid-column";
import { LocalGridRowProvider } from "resources/elements/grid/grid-row-provider/local-grid-row-provider";
import { autoinject } from "aurelia-framework";
import { WorkApi } from "shared/apis/work.api";
import { WorkStatusTd } from "shared/grid-templates/work/work-status-td";
import { WorkTechnicianTd } from "shared/grid-templates/work/work-technician-td";
import { WorkHubTd } from "shared/grid-templates/work/work-hub-td";
import { IGridConfig } from "resources/elements/grid/grid";

@autoinject
export class PendingWorkList {
  work: WorkWithHubAndTechnicianRead.WorkViewModel[]
  gridConfig: IGridConfig<WorkReadBase.BaseWorkViewModel | WorkReadBase.IHaveHub | WorkReadBase.IHaveTechnician>

  constructor(
    private workApi: WorkApi
  ) { }

  activate() {
    let query: WorkRequests.List = {
      isApproved: false,
      sortBy: 'date', 
      sortDir: 'desc' 
    }

    return this.workApi.listWithHubTech(query)
      .then(work => {
        this.work = work

        this.gridConfig = {
          rowClick: { getHref: (row: WorkReadBase.BaseWorkViewModel) => `work/read/${row.id}` },
          rowProvider: new LocalGridRowProvider<WorkWithHubAndTechnicianRead.WorkViewModel>(work),
          columns: [
            new GridColumn<WorkWithHubAndTechnicianRead.WorkViewModel>({
              title: 'Date',
              key: 'date',
              valueConverter: 'date',
              tdClass: 'td-fit-content td-no-wrap'
            }),
            new GridColumn<WorkReadBase.IHaveHub>({
              title: 'Hub',
              ViewModel: WorkHubTd,
              centerHorizontally: false
            }),
            new GridColumn<WorkReadBase.IHaveTechnician>({
              title: 'Technician',
              ViewModel: WorkTechnicianTd,
              centerHorizontally: false
            }),
            new GridColumn<WorkWithHubAndTechnicianRead.WorkViewModel>({
              title: 'Hours',
              tdClass: 'td-fit-content',
              centerHorizontally: true,
              key: 'timeSpent',
            }),
            new GridColumn<WorkReadBase.BaseWorkViewModel>({
              title: 'Status',
              ViewModel: WorkStatusTd,
              tdClass: 'td-fit-content'
            })
          ]
        }
      })
  }
}
