import { HubRequests, HubRead } from "mean-au-ts-shared";
import { GridColumn } from "resources/elements/grid/grid-column/grid-column";
import { LocalGridRowProvider } from "resources/elements/grid/grid-row-provider/local-grid-row-provider";
import { autoinject } from "aurelia-framework";
import { IGridConfig } from "resources/elements/grid/grid";
import { HubApi } from "shared/apis/hub.api";
import { IAuth } from "shared/services/auth/auth.service";
import { CreateWorkTd } from "shared/grid-templates/hub/hub-create-work-td";
import { HubLastWorkDateTd } from "shared/grid-templates/hub/hub-last-work-date-td";
import { HubNumActiveBatchesTd } from "shared/grid-templates/hub/hub-num-active-batches-td";

@autoinject
export class MyHubsList {
  gridConfig: IGridConfig<HubRead.HubViewModel>

  constructor(
    private hubApi: HubApi,
    private auth: IAuth
  ) { }

  activate() {
    let query: HubRequests.List = {
      technician: this.auth.currentUser.id
    }

    return this.hubApi.list(query)
      .then(x => {
        x.sort((a, b) => {
          if (!a.stats.lastWork) return -1
          if (!b.stats.lastWork) return 1

          return a.stats.lastWork.date.valueOf() - b.stats.lastWork.date.valueOf()
        })

        this.gridConfig = {
          rowClick: { getHref: (row ) => `hubs/read/${row.id}` },
          rowProvider: new LocalGridRowProvider(x),
          columns: [
            new GridColumn<HubRead.HubViewModel>({
              ViewModel: CreateWorkTd,
              tdClass: 'td-fit-content'
            }),
            new GridColumn<HubRead.HubViewModel>({
              title: 'Name',
              key: 'name',
              thClass: 'th-no-wrap text-center',
              tdClass: 'td-no-wrap text-center',
              isSearchable: true
            }),
            new GridColumn<HubRead.HubViewModel>({
              title: '# Active Batches',
              ViewModel: HubNumActiveBatchesTd,
              thClass: 'th-no-wrap text-center',
              tdClass: 'td-no-wrap text-center td-fit-content'
            }),
            new GridColumn({
              title: 'Last Work',
              ViewModel: HubLastWorkDateTd,
              thClass: 'th-no-wrap text-center',
              tdClass: 'td-no-wrap text-center'
            })
          ]
        }
      })
  }
}
