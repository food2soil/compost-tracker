import { autoinject } from 'aurelia-framework';
import { BatchApi } from 'shared/apis/batch.api';
import { BatchRead, WorkWithTechnicianRead, Enums, HubWithBaysRead } from 'mean-au-ts-shared';
import { HubApi } from 'shared/apis/hub.api';
import { ContributingWork } from './list-contributing-work';
import { IBreadcrumb } from 'resources/elements/header/header';

interface IChartData {
  labels: string[]
  datasets: {
    label: string
    backgroundColor: string
    data: number[]
  }[]
}

@autoinject
export class BatchReadVm {

  read: BatchRead.BatchViewModel
  hub: HubWithBaysRead.HubViewModel
  contributingWork: WorkWithTechnicianRead.WorkViewModel[]
  breadcrumbs: IBreadcrumb[]

  bayIdToName: {[key: string]: string} = { }

  chartOptions = {
      legend: {
      display: true
    },
    lineAtIndex: [],
    scales: {
      yAxes: [{
        display: true,
        ticks: {
          min: 80,
          max: 180
        }
      }]
    }
  }

  temperatureData: IChartData = { 
    labels: [],
    datasets: []
  }

  colors: string[] = [
    "rgba(52,152,219,0.5)",
    "rgba(119,52,219,0.5)",
    // "rgba(219,119,52,0.5)",
    "rgba(152,219,52,0.5)"
  ]

  constructor(
    private batchApi: BatchApi,
    private hubApi: HubApi,
    public contributingWorkVm: ContributingWork
  ) { }

  private initLabels() {
    let startDate = this.read.events.find(x => x.type === Enums.BayEvent.Fill).date
    let harvestEvent = this.read.events.find(x => x.type === Enums.BayEvent.Harvest)
    let endDate = harvestEvent ? harvestEvent.date : new Date()

    this.temperatureData.labels = []

    let ctr = 0
    while(true) {
      let labelDate = new Date(new Date(startDate.valueOf()).setDate(startDate.getDate() + ctr))
      this.temperatureData.labels.push(labelDate.toLocaleDateString())
      ctr++

      if (labelDate.valueOf() > endDate.valueOf() || ctr > 120) break;
    }
  }

  private initDatasets() {
    this.temperatureData.datasets = []

    this.read.events
      .sort((a, b) => a.date.valueOf() - b.date.valueOf())
      .forEach((event, index) => {
        // Keep the harvest in the collection for date purposes, but dont create a dataset for it
        // This is because no measurements happen after the harvest
        if (event.type === Enums.BayEvent.Harvest || !event.endBay) return
        
        // Create a new dataset for each non-harvest event
        // This effectively lets us use a different color for each bay that a batch has lived in
        this.temperatureData.datasets[index] = {
          label: `Bay ${this.bayIdToName[event.endBay]}`,
          backgroundColor: this.colors[index],
          data: []
        }

        let measurementsForThisBay = {}
        this.read.measurements
          .filter(x => {
            // Measurements in this bay occurred after the current event's date and
            // before the next event's date (if it exists)
            return !!this.read.events[index + 1]
              ? x.date.valueOf() > event.date.valueOf() && x.date.valueOf() <= this.read.events[index + 1].date.valueOf()
              : x.date.valueOf() > event.date.valueOf() && x.date.valueOf() <= Date.now()
            })
          .forEach(x => {
            // Create a map for easier extraction later
            measurementsForThisBay[x.date.toLocaleDateString()] = x.temperature 
          })
            
        this.temperatureData.labels.forEach((label, xIndex) => {
          // build out the data array with measurement values
          // this will add undefined values to dates with no measurements for each bay
          this.temperatureData.datasets[index].data[xIndex] = measurementsForThisBay[label]
        })
      })

      this.temperatureData.labels.forEach((label, xIndex) => {
        // Add turn indicators
        if (this.read.events.some(x => x.type === Enums.BayEvent.Turn && x.date.toLocaleDateString() === label)) {
          this.chartOptions.lineAtIndex.push(xIndex)
        }
      })
    }

  activate(params: any) {
    return this.batchApi.readWorkWithTechList(params.id)
      .then(work => {
        this.contributingWork = work

        let hubPromise = this.hubApi.readWithBay(this.contributingWork[0].hub)
        let batchPromise = this.batchApi.read(params.id)
        return Promise.all([hubPromise, batchPromise])
      })
      .then(results => {
        this.read = results[1]

        this.hub = results[0]
        results[0].bays.forEach(x => {
          this.bayIdToName[x.id] = x.name
        })

        this.breadcrumbs = [{
          href: `hubs/read/${this.hub.id}`,
          title: this.hub.name
        }]
      })
      .then(() => {
        this.initLabels()
        this.initDatasets()
      })
  }
}
