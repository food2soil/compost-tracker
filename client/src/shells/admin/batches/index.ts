import { autoinject } from 'aurelia-framework';
import { Router, RouterConfiguration } from 'aurelia-router';
import { PLATFORM } from 'aurelia-pal';

@autoinject
export class Batches {
  router: Router;

  configureRouter(config: RouterConfiguration, router: Router) {
    config.options.pushState = true;

    config.map([{
      href: 'read/:id',
      route: 'read/:id',
      moduleId: PLATFORM.moduleName('./read'),
      nav: true,
      title: 'Info'
    }]);

    this.router = router;
  }
}
