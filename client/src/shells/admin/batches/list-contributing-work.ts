import { WorkWithTechnicianRead } from "mean-au-ts-shared";
import { GridColumn } from "resources/elements/grid/grid-column/grid-column";
import { LocalGridRowProvider } from "resources/elements/grid/grid-row-provider/local-grid-row-provider";
import { HubApi } from "shared/apis/hub.api";
import { autoinject } from "aurelia-framework";
import { WorkTechnicianTd } from "shared/grid-templates/work/work-technician-td";
import { IGridConfig } from "resources/elements/grid/grid";

@autoinject
export class ContributingWork {
  private work: WorkWithTechnicianRead.WorkViewModel[]

  gridConfig: IGridConfig<WorkWithTechnicianRead.WorkViewModel>

  constructor(
    private hubApi: HubApi
  ) { }

  activate(params: { work: WorkWithTechnicianRead.WorkViewModel[] }) {
    this.work = params.work.slice().sort((a, b) => a.date.valueOf() - b.date.valueOf())
    this.gridConfig = {
      rowClick: { getHref: (row) => `work/read/${row.id}` },
      rowProvider: new LocalGridRowProvider(this.work),
      columns: [
        new GridColumn<WorkWithTechnicianRead.WorkViewModel>({
          title: 'Date',
          key: 'date',
          valueConverter: 'date'
        }),
        new GridColumn({
          title: 'Technician',
          ViewModel: WorkTechnicianTd,
          centerHorizontally: false
        }),
        new GridColumn<WorkWithTechnicianRead.WorkViewModel>({
          title: 'Hours Worked',
          key: 'timeSpent',
          valueConverter: 'toFixed: 1'
        })
      ]
    }
    
  }
}
