import { RoutableComponentActivate } from 'aurelia-router'
import { autoinject } from 'aurelia-framework';
import { GeneratorApi } from 'shared/apis/generator.api';
import { GeneratorRead, GeneratorRequests, Utilities, AddressShared } from 'mean-au-ts-shared';
import { DialogService } from 'aurelia-dialog';
import { EditGeneratorCollectionInfoModal } from './modals/edit-collection-modal';
import * as toastr from 'toastr'
import { EditGeneratorGeneralInfoModal, IEditGeneratorGeneralInfoModalModel } from './modals/edit-general-modal';
import { BusyService } from 'shared/services/busy/busy.service';
import { IBreadcrumb } from 'resources/elements/header/header';
import { IAuth } from 'shared/services/auth/auth.service';

@autoinject
export class GeneratorReadVm implements RoutableComponentActivate {

  isEditing: boolean
  isEditingCollection: boolean
  adminRead: GeneratorRead.GeneratorViewModel
  addressString: string
  generatorId: string
  breadcrumbs: IBreadcrumb[]

  constructor(
    public auth: IAuth,
    private generatorApi: GeneratorApi,
    private dialogService: DialogService,
    private busyService: BusyService
  ) { }

  _getUpdateModel() {
    let model = Utilities.castTo<GeneratorRequests.Update>(this.adminRead, GeneratorRequests.Update);
    
    model.addressPrimaryLine = this.adminRead.address.primaryLine
    model.addressSecondaryLine = this.adminRead.address.secondaryLine
    model.addressCity = this.adminRead.address.city
    model.addressState = this.adminRead.address.state
    model.addressZip = this.adminRead.address.zip
    model.containerType = this.adminRead.containerHistory[this.adminRead.containerHistory.length - 1].volumePerContainer
    model.containersPerWeek = this.adminRead.containerHistory[this.adminRead.containerHistory.length - 1].containersPerWeek

    return model;
  }

  editCollectionInfo() {
    this.isEditingCollection = true

    this.dialogService.open({
      viewModel: EditGeneratorCollectionInfoModal,
      model: { model: this._getUpdateModel() },
      lock: false 
    }).whenClosed(result => {
      if (result.wasCancelled) throw 'cancelled'

      return this.activate({ id: this.generatorId })
    })
    .then(() => {
      toastr.success('Successfully updated generator')
      this.isEditingCollection = false
    })
    .catch(() => this.isEditingCollection = false)
  }

  editGeneralInfo() {
    this.isEditing = true

    let model: IEditGeneratorGeneralInfoModalModel = {
      updateModel: this._getUpdateModel(),
      onSaved: () => this.onGeneratorChanged()
    }

    this.dialogService.open({
      viewModel: EditGeneratorGeneralInfoModal,
      model: model,
      lock: false 
    }).whenClosed(result => {
      if (result.wasCancelled) throw 'cancelled'

      toastr.success('Successfully updated generator')
      this.isEditing = false
    })
    .catch(() => this.isEditing = false)
  }

  onGeneratorChanged() {
    return this.generatorApi.read(this.adminRead.id).then(x => {
      this.adminRead = x;
      this.addressString = AddressShared.addressToString(this.adminRead.address);  
    })
  }

  activate(params: any) {
    this.generatorId = params.id;
    this.breadcrumbs= [{
      href: 'generators',
      title: 'Generators'
    }]

    return this.generatorApi.read(params.id).then(x => {
      this.adminRead = x;
      this.addressString = AddressShared.addressToString(this.adminRead.address);  
    })
  }
}
