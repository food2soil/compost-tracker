import { IGridRowProvider } from "resources/elements/grid/grid-row-provider/grid-row-provider";
import { LocalGridRowProvider } from "resources/elements/grid/grid-row-provider/local-grid-row-provider";
import { GridColumn } from "resources/elements/grid/grid-column/grid-column";
import { autoinject } from "aurelia-framework";
import { GeneratorRead } from 'mean-au-ts-shared'
import { GeneratorApi } from "shared/apis/generator.api";
import { IGridRowClick } from "resources/elements/grid/grid-row-action/grid-row-click";
import { IGridConfig } from "resources/elements/grid/grid";
import { IAuth } from "shared/services/auth/auth.service";

@autoinject
export class GeneratorList {
  gridConfig: IGridConfig<GeneratorRead.GeneratorViewModel>

  constructor(
    public auth: IAuth,
    private generatorApi: GeneratorApi
  ) { }

  activate() {
    return this.generatorApi.list().then(x => {
      this.gridConfig = {
        rowClick: { getHref: (row) => `generators/read/${row.id}` },
        rowProvider: new LocalGridRowProvider(x),
        columns: [
          new GridColumn<GeneratorRead.GeneratorViewModel>({
            title: 'Name',
            key: 'name',
            isSearchable: true
          }), 
          new GridColumn<GeneratorRead.GeneratorViewModel>({
            title: 'Type',
            key: 'generatorType',
            tdClass: 'td-fit-content'
          })
        ]
      }
    })
  }
}
