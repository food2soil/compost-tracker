import { autoinject } from 'aurelia-framework';
import { Router, RouterConfiguration } from 'aurelia-router';
import { PLATFORM } from 'aurelia-pal';
import { IAuth } from 'shared/services/auth/auth.service';

@autoinject
export class Generators {
  router: Router;

  constructor(
    private auth: IAuth
  ) { }

  configureRouter(config: RouterConfiguration, router: Router) {
    config.options.pushState = true;

    config.map([{
      route: '',
      name: 'list',
      moduleId: PLATFORM.moduleName('./list'),
      nav: true,
      title: 'List'
    }, {
      route: 'create',
      name: 'create',
      moduleId: PLATFORM.moduleName('./create'),
      nav: true,
      title: 'Create'
    }, {
      href: 'read/:id',
      route: 'read/:id',
      moduleId: PLATFORM.moduleName('./read'),
      nav: true,
      title: 'Info'
    }]);

    this.router = router;
  }
}
