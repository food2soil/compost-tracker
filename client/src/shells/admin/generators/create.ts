import { ComponentDetached, autoinject } from "aurelia-framework";
import { Validation, GeneratorRequests, Enums } from "mean-au-ts-shared";
import { ValidationRules } from "aurelia-validation";
import * as toastr from 'toastr'
import { FormWrap } from "resources/elements/form-wrap/form-wrap";
import { IValidator } from "shared/services/validator/validator.service";
import { GeneratorApi } from "shared/apis/generator.api";
import { enumToSelectOptions } from "shared/utilities/misc";
import { ISelectOption } from "resources/elements/form-group/form-group-select/form-group-select";
import { Router } from "aurelia-router";
import { IBreadcrumb } from "resources/elements/header/header";

@autoinject
export class GeneratorCreate implements ComponentDetached {
  isSaving: boolean
  createBody: GeneratorRequests.Create = new GeneratorRequests.Create();
  breadcrumbs: IBreadcrumb[]

  generatorTypeSelect: ISelectOption[] = enumToSelectOptions(Enums.GeneratorType);
  collectionTypeSelect: ISelectOption[] = enumToSelectOptions(Enums.CollectionType);
  containerTypeSelect: ISelectOption[] = [{
    name: `${Enums.ContainerType.Big} Gallons`,
    value: Enums.ContainerType.Big
  }, {
    name: `${Enums.ContainerType.Small} Gallons`,
    value: Enums.ContainerType.Small
  }]

  infoForm: FormWrap
  containerForm: FormWrap
  addressForm: FormWrap
  contactForm: FormWrap

  constructor(
    private generatorApi: GeneratorApi,
    private validator: IValidator,
    private router: Router
  ) { 
    Validation.ensureDecoratorsOn(GeneratorRequests.Create, ValidationRules);
  }

  create() {
    this.isSaving = true;

    this.validator.validateObject(this.createBody, true)
      .then(() => this.generatorApi.create(this.createBody))
      .then(newGenerator => {
        toastr.success(`Succesfully created generator`);

        this.infoForm.clear();
        this.containerForm.clear();
        this.addressForm.clear();
        this.contactForm.clear();
        
        this.isSaving = false;
        this.router.navigate(`read/${newGenerator.id}`)
        }).catch(error => this.isSaving = false)
  }

  activate() {
    this.breadcrumbs = [{
      href: 'generators',
      title: 'Generators'
    }]
  }

  detached(): void {
    this.infoForm.clear();
    this.containerForm.clear();
    this.addressForm.clear();
    this.contactForm.clear();
  }
}
