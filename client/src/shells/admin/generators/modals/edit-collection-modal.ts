import { Validation, GeneratorRequests, Enums } from "mean-au-ts-shared";
import { autoinject, transient } from "aurelia-framework";
import { DialogController } from 'aurelia-dialog';
import { FormWrap } from "resources/elements/form-wrap/form-wrap";
import { ValidationRules } from "aurelia-validation";
import { GeneratorApi } from "shared/apis/generator.api";
import { IValidator } from "shared/services/validator/validator.service";
import { ISelectOption } from "resources/elements/form-group/form-group-select/form-group-select";
import { enumToSelectOptions } from "shared/utilities/misc";

@autoinject
@transient()
export class EditGeneratorCollectionInfoModal {
  update: GeneratorRequests.Update = new GeneratorRequests.Update();
  form: FormWrap

  generatorTypeSelect: ISelectOption[] = enumToSelectOptions(Enums.GeneratorType);
  collectionTypeSelect: ISelectOption[] = enumToSelectOptions(Enums.CollectionType);
  containerTypeSelect: ISelectOption[] = [{
    name: `${Enums.ContainerType.Big} Gallons`,
    value: Enums.ContainerType.Big.toString()
  }, {
    name: `${Enums.ContainerType.Small} Gallons`,
    value: Enums.ContainerType.Small.toString()
  }]

  isSaving: boolean
  origContainerType: number
  origContainersPerWeek: number

  constructor(
    public dialogController: DialogController,
    private validator: IValidator,
    private generatorApi: GeneratorApi
  ) {
    Validation.ensureDecoratorsOn(GeneratorRequests.Update, ValidationRules);
  }

  save() {
    this.isSaving = true;

    this.validator.validateObject(this.update, true)
      .then(() => {
        // HACK: Convert it back to a number
        this.update.containerType = Number(this.update.containerType);
        this.update.isNewContainer = 
          this.update.containerType != this.origContainerType ||
          this.update.containersPerWeek != this.origContainersPerWeek

        return this.generatorApi.update(this.update)
      })
      .then(() => this.dialogController.ok())
      .catch(() => this.isSaving = false)
  }

  activate(params: any) {
    this.update = params.model;
    this.origContainerType = this.update.containerType;
    this.origContainersPerWeek = this.update.containersPerWeek;

    // HACK: The select doesnt seem to bind initially with numeric values
    (<any>this.update).containerType = this.update.containerType.toString()
  }
}
