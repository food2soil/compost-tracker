import { Validation, GeneratorRequests } from "mean-au-ts-shared";
import { autoinject, transient } from "aurelia-framework";
import { DialogController } from 'aurelia-dialog';
import { FormWrap } from "resources/elements/form-wrap/form-wrap";
import { ValidationRules } from "aurelia-validation";
import { GeneratorApi } from "shared/apis/generator.api";
import { IValidator } from "shared/services/validator/validator.service";

export interface IEditGeneratorGeneralInfoModalModel {
  updateModel: GeneratorRequests.Update,
  onSaved?: () => Promise<void>
}

@autoinject
@transient()
export class EditGeneratorGeneralInfoModal {
  update: GeneratorRequests.Update = new GeneratorRequests.Update();
  form: FormWrap
  onSaved: () => Promise<void>
  isSaving: boolean

  constructor(
    public dialogController: DialogController,
    private validator: IValidator,
    private generatorApi: GeneratorApi
  ) {
    Validation.ensureDecoratorsOn(GeneratorRequests.Update, ValidationRules);
  }

  save() {
    this.isSaving = true;

    this.validator.validateObject(this.update, true)
      .then(() => this.generatorApi.update(this.update))
      .then(() => this.onSaved())
      .then(() => this.dialogController.ok())
      .catch(() => this.isSaving = false)
  }

  activate(params: IEditGeneratorGeneralInfoModalModel) {
    this.update = params.updateModel;
    this.onSaved = params.onSaved || (() => Promise.resolve())
  }
}
