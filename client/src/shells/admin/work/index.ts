import { autoinject } from 'aurelia-framework';
import { Router, RouterConfiguration } from 'aurelia-router';
import { PLATFORM } from 'aurelia-pal';

@autoinject
export class Work {
  router: Router;

  configureRouter(config: RouterConfiguration, router: Router) {
    config.options.pushState = true;

    config.map([{
      route: '',
      name: 'list',
      moduleId: PLATFORM.moduleName('./list'),
      nav: true,
      title: 'List'
    }, {
      href: 'read/:id',
      route: 'read/:id',
      name: 'read',
      moduleId: PLATFORM.moduleName('./read'),
      nav: false,
      title: 'Info'
    }, {
      href: 'edit/:id',
      route: 'edit/:id',
      name: 'edit',
      moduleId: PLATFORM.moduleName('./edit'),
      nav: false,
      title: 'Edit'
    }]);

    this.router = router;
  }
}
