import { autoinject, computedFrom } from 'aurelia-framework';
import { DialogService } from 'aurelia-dialog';
import * as toastr from 'toastr'
import { WorkWithHubAndTechnicianRead, BayRead, WorkRead } from 'mean-au-ts-shared';
import { WorkApi } from 'shared/apis/work.api';
import { ISelectOption } from 'resources/elements/form-group/form-group-select/form-group-select';
import { TechnicianApi } from 'shared/apis/technician.api';
import { Router } from 'aurelia-router';
import { HubApi } from 'shared/apis/hub.api';
import { WorkBays, IWorkBaysModel } from '../shared/bay-visuals/work/work-bays';
import { IBreadcrumb } from 'resources/elements/header/header';
import { IAuth } from 'shared/services/auth/auth.service';
import { ConfirmModal, IConfirmModalModel } from 'shared/modals/confirm-modal';

@autoinject
export class WorkReadVm {

  technicianSelectOptions: ISelectOption[]
  read: WorkWithHubAndTechnicianRead.WorkViewModel
  lastWork: WorkRead.WorkViewModel
  bayReads: BayRead.BayViewModel[]
  breadcrumbs: IBreadcrumb[]
  workBaysModel: IWorkBaysModel
  
  isApproving: boolean
  isUndoing: boolean
  isDeleting: boolean
  
  @computedFrom('read', 'lastWork')
  get canUndo() {
    if (!this.lastWork || this.read.id === this.lastWork.id) return this.read.isApproved

    return false
  }

  @computedFrom('read')
  get canEditOrDelete() {
    return !this.read.isApproved
      && (this.auth.isAdmin || this.read.technician.id === this.auth.currentUser.id)
  }

  constructor(
    public auth: IAuth,
    private workApi: WorkApi,
    private dialogService: DialogService,
    private technicianApi: TechnicianApi,
    private router: Router,
    private hubApi: HubApi,
    public workBaysVm: WorkBays
  ) { }

  approve() {
    this.isApproving = true

    this.workApi.approve(this.read.id)
      .then(() => this.workApi.readWithHubTech(this.read.id))
      .then(x => {
        this.read = x
        this.isApproving = false
        toastr.success(`Succesfully approved work`);
      })
      .catch(() => this.isApproving = false)
  }

  undo() {
    this.isUndoing = true

    this.workApi.undo(this.read.id)
      .then(() => this.workApi.readWithHubTech(this.read.id))
      .then(x => {
        this.read = x
        this.isUndoing = false
        toastr.success(`Succesfully finished undo`);
      })
      .catch(() => this.isUndoing = false)
  }

  delete() {
    this.isDeleting = true

    return this.dialogService.open({
      viewModel: ConfirmModal,
      model: <IConfirmModalModel>{
        prompt: 
          `<div class="text-center">
            This action cannot be undone.<br/><br/>
            Are you sure you wish to delete this Work Record?
          </div>`,
        title: 'Delete Work Record',
        okLabel: 'Delete',
        okDelegate: () => this.workApi.delete(this.read.id),
        closeOnError: true
      }
    }).whenClosed(result => {
      if (result.wasCancelled) throw 'cancelled'
    })
    .then((() => {
      toastr.success('Successfully deleted work record')
      this.router.navigate(`/hubs/read/${this.read.hub.id}`)
    }))
    .catch(() => this.isDeleting = false)
  }

  activate(params: any) {
    let work = this.workApi.readWithHubTech(params.id)
      .then(x => {
        this.read = x;

        this.breadcrumbs = [{
          href: 'work',
          title: 'Work Records'
        }, {
          href: `technicians/read/${this.read.technician.id}`,
          title: this.read.technician.displayName
        }, {
          href: `hubs/read/${this.read.hub.id}`,
          title: this.read.hub.name
        }]

        return this.hubApi.readBayList(this.read.hub.id)
      })
      .then(bays => {
        this.bayReads = bays
        
        this.workBaysModel = {
          bays: this.bayReads,
          work: this.read,
          isDisabled: true
        }

        return this.workApi.list({ 
          hub: this.read.hub.id, 
          sortBy: 'date', 
          sortDir: 'desc', 
          limit: 1 })
      })
      .then(lastWork => this.lastWork = lastWork[0])

    let technicians = this.technicianApi.list()
      .then(x => {
        this.technicianSelectOptions = x.map(t => {
          return <ISelectOption>{
            name: t.displayName,
            value: t.id
          }
        })
      })

    return Promise.all([work, technicians])
  }
}
