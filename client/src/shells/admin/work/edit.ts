import { Router } from "aurelia-router";
import { Validation, WorkRequests, HubWithBaysRead, WorkRead, WorkWithHubAndTechnicianRead } from "mean-au-ts-shared";
import { autoinject } from "aurelia-framework";
import { WorkApi } from "shared/apis/work.api";
import { ValidationRules } from "aurelia-validation";
import { IValidator } from "shared/services/validator/validator.service";
import * as toastr from 'toastr'
import { ISelectOption } from "resources/elements/form-group/form-group-select/form-group-select";
import { TechnicianApi } from "shared/apis/technician.api";
import { IWorkBaysModel, WorkBays } from "../shared/bay-visuals/work/work-bays";
import { HubApi } from "shared/apis/hub.api";

@autoinject()
export class WorkCreate {
  read: WorkWithHubAndTechnicianRead.WorkViewModel
  edit: WorkRequests.Edit = new WorkRequests.Edit()
  
  technicianSelectOptions: ISelectOption[]
  workBaysModel: IWorkBaysModel
  minNewWorkDate: Date

  isSaving: boolean

  constructor(
    private hubApi: HubApi,
    private technicianApi: TechnicianApi,
    private workApi: WorkApi,
    private validator: IValidator,
    private router: Router,
    public workBaysVm: WorkBays
  ) { 
    Validation.ensureDecoratorsOn(WorkRequests.Create, ValidationRules);
  }

  save() {
    this.isSaving = true;

    this.edit.bayMeasurements = this.workBaysVm.bayMeasurements
    this.edit.bayEvents = this.workBaysVm.bayEvents

    return this.validator.validateObject(this.edit, true)
      .then(() => this.workApi.update(this.read.id, this.edit))
      .then(() => {
        toastr.success('Successfully updated work record')
        this.router.navigate(`read/${this.read.id}`)
      })
      .catch(() => this.isSaving = false)
  }
  
  activate(params: any) {
    let technicianPromise = this.technicianApi.list().then(x => {
      this.technicianSelectOptions = x.map(t => {
        return <ISelectOption>{
          name: t.displayName,
          value: t.id
        }
      })
    })

    let workPromise = this.workApi.readWithHubTech(params.id)
      .then(work => {
        this.read = work

        this.edit.bayEvents = work.bayEvents
        this.edit.bayMeasurements = work.bayMeasurements
        this.edit.date = work.date
        this.edit.hoursSpent = work.timeSpent
        this.edit.notes = work.notes
        this.edit.technicianId = work.technician.id

        return this.hubApi.readBayList(this.read.hub.id)
        
      })
      .then(bays => {
        this.workBaysModel = {
          bays: bays,
          work: this.read,
          isDisabled: false
        }
      })

    let latestWorkPromise = this.workApi.list({ hub: params.id, sortBy: 'date', sortDir: 'desc', limit: 1 })
      .then(work => {
        this.minNewWorkDate = work.length === 0 
          ? undefined
          : new Date(new Date(work[0].date)
            .setDate(work[0].date.getDate() + 1))
      })

    return Promise.all([technicianPromise, workPromise, latestWorkPromise])
  }
}
