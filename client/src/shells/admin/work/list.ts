import { WorkWithHubRead, WorkWithHubAndTechnicianRead } from "mean-au-ts-shared";
import { GridColumn } from "resources/elements/grid/grid-column/grid-column";
import { LocalGridRowProvider } from "resources/elements/grid/grid-row-provider/local-grid-row-provider";
import { autoinject, Disposable } from "aurelia-framework";
import { WorkApi } from "shared/apis/work.api";
import { WorkHubTd } from "shared/grid-templates/work/work-hub-td";
import { WorkStatusTd } from "shared/grid-templates/work/work-status-td";
import { IGridConfig } from "resources/elements/grid/grid";

@autoinject
export class AllWorkList {
  private technicianId: string

  noRowsMessage: string = 'No work has been recorded for this timeframe'
  gridConfig: IGridConfig<WorkWithHubAndTechnicianRead.WorkViewModel>
  monthFilter: { date: Date }
  hoursCurrentMonth: number

  constructor(
    private workApi: WorkApi
  ) { }

  updateList() {
    this.gridConfig.rowProvider.filter((row) => {
      return row.date.valueOf() >= this.monthFilter.date.valueOf() && row.date.valueOf() < new Date(this.monthFilter.date).setMonth(this.monthFilter.date.getMonth() + 1)
    })

    this.hoursCurrentMonth = this.gridConfig.rowProvider.filteredRows
      .map(x => x.timeSpent)
      .reduce((a, b) => a + b, 0);
  }

  activate(params: { technicianId: string }) {
    this.monthFilter = { date: new Date() }
    this.technicianId = params.technicianId

    return this.workApi.listWithHubTech({
      technician: this.technicianId, 
      sortBy: 'date',
      sortDir: 'desc'
    }).then(work => {
      this.gridConfig = {
        rowClick: { getHref: (row) => `work/read/${row.id}` },
        rowProvider: new LocalGridRowProvider(work),
        columns: [
          new GridColumn<WorkWithHubAndTechnicianRead.WorkViewModel>({
            title: 'Date',
            key: 'date',
            valueConverter: 'date',
            tdClass: 'td-no-wrap td-fit-content',
            thClass: 'text-center'
          }),
          new GridColumn<WorkWithHubAndTechnicianRead.WorkViewModel>({
            title: 'Hub',
            ViewModel: WorkHubTd,
            searchGetter: (row) => row.hub.name,
            thClass: 'text-center'
          }),
          new GridColumn<WorkWithHubAndTechnicianRead.WorkViewModel>({
            title: 'Hours Spent',
            key: 'timeSpent',
            centerHorizontally: true,
            thClass: 'text-center th-no-wrap'
          }),
          new GridColumn<WorkWithHubAndTechnicianRead.WorkViewModel>({
            title: 'Status',
            ViewModel: WorkStatusTd,
            tdClass: 'td-no-wrap',
            thClass: 'text-center'
          }),
        ]
      }
    })
  }
}
