import { BayRead, WorkReadBase } from "mean-au-ts-shared";
import { transient } from "aurelia-framework";

export interface IWorkBaysModel {
  work?: WorkReadBase.IHaveBayEvents & WorkReadBase.IHaveBayMeasurements
  bays: BayRead.BayViewModel[]
  isDisabled?: boolean
}

@transient()
export class WorkBays {
  isDisabled: boolean

  work: WorkReadBase.IHaveBayEvents & WorkReadBase.IHaveBayMeasurements
  bays: BayRead.BayViewModel[]
  regularBays: BayRead.BayViewModel[]
  holdingBays: BayRead.BayViewModel[]

  bayIdToName: {[key: string]: string} = { }
  bayEventsMap: {[key: string]: WorkReadBase.BayEventViewModel[]} = { }
  bayMeasurementsMap: {[key: string]: WorkReadBase.BayMeasurementViewModel} = { }

  get bayEvents(): WorkReadBase.BayEventViewModel[] {
    let ret = []
    Object.keys(this.bayEventsMap).forEach(k => {
      ret = ret.concat(this.bayEventsMap[k])
    })
    return ret
  }

  get bayMeasurements(): WorkReadBase.BayMeasurementViewModel[] {
    return Object.keys(this.bayMeasurementsMap)
      .map(k => this.bayMeasurementsMap[k])
      .filter(x => !!x.temperature)
  }

  activate(params: IWorkBaysModel) {
    this.work = params.work
    this.bays = params.bays
    this.isDisabled = params.isDisabled

    this.regularBays = this.bays.filter(x => !x.isHolding)
    this.holdingBays = this.bays.filter(x => x.isHolding)

    this.bays.forEach(x => {
      this.bayIdToName[x.id] = x.name
      this.bayEventsMap[x.id] = []
      
      let measurement = new WorkReadBase.BayMeasurementViewModel()
      measurement.bay = x.id
      this.bayMeasurementsMap[x.id] = measurement
    })

    if (!this.work) return

    this.work.bayEvents.forEach(x => {
      this.bayEventsMap[x.bay].push(x)
    })

    this.work.bayMeasurements.forEach(x => {
      this.bayMeasurementsMap[x.bay] = x
    })
  }
}
