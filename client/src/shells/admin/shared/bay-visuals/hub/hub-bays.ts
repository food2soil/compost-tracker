import { BayRead, WorkRead } from "mean-au-ts-shared";
import { transient } from "aurelia-framework";

export interface IHubBaysModel {
  lastWork: WorkRead.WorkViewModel
  bays: BayRead.BayViewModel[]
}

@transient()
export class HubBays {
  lastWork: WorkRead.WorkViewModel
  bays: BayRead.BayViewModel[]
  regularBays: BayRead.BayViewModel[]
  holdingBays: BayRead.BayViewModel[]

  activate(params: IHubBaysModel) {
    this.lastWork = params.lastWork
    this.bays = params.bays

    this.regularBays = this.bays.filter(x => !x.isHolding)
    this.holdingBays = this.bays.filter(x => x.isHolding)
  }
}
