# Food2Soil Compost Tracker v2

## Prerequisites
Make sure you have installed all of the following prerequisites on your development machine:
* Git
* Node.js
* MongoDB
* Yarn
* Aurelia CLI
```
npm install yarn aurelia-cli -g
```

## Install dependencies
Dependencies for each of the three parts of the project need to be installed separately.  From the respository directory execute the following commands
```
cd ./client && yarn
```
```
cd ./server && yarn
```
```
cd ./shared && yarn
```

## Running your application
There are three steps to running the application
1. Build the shared dependencies
```
cd ./shared && npm run build
```
or to run tsc in watch mode
```
cd ./shared && npm run watch
```
2. Run the server.  This command starts an instance of [ts-node](https://github.com/TypeStrong/ts-node/) which runs the backend
```
cd ./server && npm run start:dev
```
or to use nodemon and restart the server when a file changes
```
cd ./server && npm run watch:dev
```
3. Run the client.  This command starts a webpack server which bundles up all your ts and hosts it.
```
cd ./client && au run
```
or to run webpack in watch mode and take advantage of hot module reload
```
cd ./client && au run --watch
```

## Using the App
At this point, your front end webpack server should be running at
```
http://localhost:8080
```
and your backend will be running at
```
http://localhost:3030
```

Webpack conveniently proxies API requests to the node server at localhost:3030 automagically
