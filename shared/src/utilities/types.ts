export namespace Types {
  export declare type SortDirection = 'asc' | 'desc';
}
