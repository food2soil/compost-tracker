export module Utilities {
  export function newGuid() {
    var d = new Date().getTime();
    if (typeof performance !== 'undefined' && typeof performance.now === 'function'){
        d += performance.now(); //use high-precision timer if available
    }

    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
      var r = (d + Math.random() * 16) % 16 | 0;
      d = Math.floor(d / 16);
      return (c === 'x' ? r : (r & 0x3 | 0x8)).toString(16);
    });
  }

  function maskObject<T = object>(target: T, source: any): T {
    for(var key in target) {
      if (!target.hasOwnProperty(key)) continue;

      if (typeof(target[key]) === 'function') {
        // Skip any functions that may be on the prototype
        continue;
      } else if(source[key] == null) {
        target[key] = null
      } else if (target[key] && target[key].constructor === Date) {
        // Handle dates and other built-in object types before other objects
        target[key] = <any>new Date(source[key])
      } else if (typeof(target[key]) == 'object' && !Array.isArray(target[key])) {
        // Recursively populate non-array objects.
        target[key] = maskObject(target[key], source[key]);
      } else if (Array.isArray(target[key]) && typeof(target[key][0]) == 'object') {
        // Recursively populate arrays of objects
        let ctor = (<any>target[key]).pop().constructor;
        target[key] = source[key].map(s => castTo(s, ctor));;
      } else {
        // Just assign the value if it's a primitive like string, number, or boolean
        target[key] = source[key];
      }
    }
    return target;
  }

  export function castToArray<T = object>(obj: any, CastTo: new () => T): T[] {
    return obj.map(x => maskObject(new CastTo(), x))
  }

  export function castTo<T = object>(obj: any, CastTo: new () => T): T {
    return maskObject(new CastTo(), obj);
  }

  export function roundTo(value: number, decimalPoints: number) {
    return +(value).toFixed(decimalPoints)
  }
}
