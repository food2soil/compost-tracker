import { getEmissionsReducedFromGallonsComposted } from "../shared/stats-shared.dto";

export interface IHaveContainerHistory {
  containerHistory: {
    volumePerContainer: number
    containersPerWeek: number
  }[]
}

export namespace GeneratorShared {
  export function getWeeklyCollection(generator: IHaveContainerHistory) {
    return generator.containerHistory[generator.containerHistory.length - 1].volumePerContainer
      * generator.containerHistory[generator.containerHistory.length - 1].containersPerWeek
  }

  export function getWeeklyMileageEmissionsReduced(generator: IHaveContainerHistory) {
    let gallonsPerWeek = getWeeklyCollection(generator)
    
    return getEmissionsReducedFromGallonsComposted(gallonsPerWeek)
  }
}
