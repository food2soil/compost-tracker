import { ContactRead } from "../shared/contact-read.dto";
import { AddressRead } from "../shared/address-read.dto";
import { Enums } from "../..";

export namespace GeneratorRead {
  export class ContainerHistoryViewModel {
    id: string = undefined
    volumePerContainer: number = undefined
    containersPerWeek: number = undefined
    dateStartedUsing: Date = new Date()
  }

  export class GeneratorViewModel {
    id: string = undefined
    created: Date = new Date()
    name: string = undefined
    containerHistory: ContainerHistoryViewModel[] = [new ContainerHistoryViewModel()]
    contacts: ContactRead.ContactViewModel[] = [new ContactRead.ContactViewModel()]
    address: AddressRead.AddressViewModel = new AddressRead.AddressViewModel()
    isActive: boolean = undefined
    generatorType: Enums.GeneratorType = undefined
    collectionType: Enums.CollectionType = undefined
  }
}
