import { required, maxLength, greaterThanValue, requiredWithout, phone, email } from "../../decorators/validation.decorators";
import { Enums } from "../..";

export namespace GeneratorRequests {
  export class Create {
    @required()
    @maxLength(200)
    name: string = undefined

    @required()
    generatorType: Enums.GeneratorType = undefined

    @required()
    collectionType: Enums.CollectionType = undefined

    // Container History
    @required()
    containerType: Enums.ContainerType = undefined

    @required()
    @greaterThanValue(0, 'Num containers per week must be greater than zero')
    containersPerWeek: number = undefined

    // Primary Contact
    @required()
    @maxLength(200)
    primaryContactName: string = undefined

    @maxLength(200)
    primaryContactTitle?: string = undefined

    @requiredWithout<Create>('primaryContactEmail', 'Either a phone number or email address is required')
    @phone()
    primaryContactPhone?: string = undefined

    @requiredWithout<Create>('primaryContactPhone', 'Either a phone number or email address is required')
    @email()
    @maxLength(200)
    primaryContactEmail?: string = undefined

    // Address
    @required()
    @maxLength(200)
    addressPrimaryLine: string = undefined

    @maxLength(200)
    addressSecondaryLine?: string = undefined

    @required()
    @maxLength(200)
    addressCity: string = undefined

    @required()
    @maxLength(2)
    addressState: string = undefined

    @required()
    @maxLength(5)
    addressZip: string = undefined
  }

  export class Update {
    @required()
    id: string = undefined

    @required()
    @maxLength(200)
    name: string = undefined

    @required()
    generatorType: Enums.GeneratorType = undefined

    @required()
    collectionType: Enums.CollectionType = undefined

    // Container History
    isNewContainer: boolean = false

    @required()
    containerType: Enums.ContainerType = undefined

    @required()
    @greaterThanValue(0, 'Num containers per week must be greater than zero')
    containersPerWeek: number = undefined

    // Address
    @required()
    @maxLength(200)
    addressPrimaryLine: string = undefined

    @maxLength(200)
    addressSecondaryLine?: string = undefined

    @required()
    @maxLength(200)
    addressCity: string = undefined

    @required()
    @maxLength(2)
    addressState: string = undefined

    @required()
    @maxLength(5)
    addressZip: string = undefined
  }
}
