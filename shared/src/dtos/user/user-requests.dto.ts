import { maxLength, phone, email, required, sameAs, password, lessThan } from "../../decorators/validation.decorators";

export namespace UserRequests {
  export class SignIn {
    @required('Please provide your email')
    @email()
    @maxLength(200)
    email: string = undefined

    @required('Please provide a password')
    @maxLength(200)
    password: string = undefined
  }

  export class SignUp {
    @required('Please provide your first name')
    @maxLength(200)
    firstName: string = undefined

    @required('Please provide your last name')
    @maxLength(200)
    lastName: string = undefined

    @required('Please provide your email')
    @email()
    @maxLength(200)
    email: string = undefined

    @required('Please provide a password')
    @password()
    @maxLength(200)
    password: string = undefined

    @phone()
    phone: string = undefined
  }

  export class ForgotPassword {
    @required()
    forgotPasswordToken: string = undefined

    @required('Please enter a new password')
    @password()
    @maxLength(200)
    newPassword: string = undefined

    @required('Please verify your new password')
    @password()
    @sameAs<ForgotPassword>('newPassword', 'Your new passwords do not match')
    verifyPassword: string = undefined
  }

  export class SendForgotPassword {
    @required()
    @email()
    email: string = undefined
  }

  export class TestForgotPassword {
    @required()
    forgotPasswordToken: string = undefined
  }

  export class ChangePassword {
    @required('Please enter your current password')
    @maxLength(200)
    currentPassword: string = undefined

    @required('Please enter a new password')
    @password()
    @maxLength(200)
    newPassword: string = undefined

    @required('Please verify your new password')
    @password()
    @sameAs<ChangePassword>('newPassword', 'Your new passwords do not match')
    @maxLength(200)
    verifyPassword: string = undefined
  }

  export class EditProfile {
    @required()
    @maxLength(200)
    firstName: string = undefined

    @required()
    @maxLength(200)
    lastName: string = undefined

    @required()
    @email()
    @maxLength(200)
    email: string = undefined

    @phone()
    @maxLength(200)
    phone: string = undefined
  }
}
