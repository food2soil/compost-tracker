import { Enums } from "../../enums/enums";
import { UserRead } from "./user-read.dto";

export namespace UserShared {
  export function getHighestRole(user: UserRead.UserViewModel): Enums.UserRoles {
    return user.roles.sort()[user.roles.length - 1];
  };
}
