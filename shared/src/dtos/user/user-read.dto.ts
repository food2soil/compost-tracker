import { Enums } from "../../enums/enums";

export namespace UserRead {
  export class MonthlyStatsViewModel {
    hours: number = undefined
    bucketsHarvested: number = undefined
  }

  export class MonthlyViewModel {
    month: string = undefined
    year: string = undefined
    stats: MonthlyStatsViewModel = new MonthlyStatsViewModel()
  }

  export class StatsViewModel {
    id: string = undefined
    hoursAllTime: number = undefined
    bucketsHarvestedAllTime: number = undefined
    monthly: MonthlyViewModel[] = [new MonthlyViewModel()]
  }

  export class UserViewModel {
    id: string = undefined
    firstName: string = undefined
    lastName: string = undefined
    displayName: string = undefined
    email: string = undefined
    phone: string = undefined
    roles: Enums.UserRoles[] = undefined
    stats: StatsViewModel = new StatsViewModel()
  }
}
