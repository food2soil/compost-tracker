import { maxLength, password, required, phone, email, lessThan, sameAs } from "../../decorators/validation.decorators";
import { Enums } from "../..";

export namespace InvitationRequests {
  export class InviteTechnician {
    @required()
    firstName: string = undefined

    @required()
    lastName: string = undefined

    @email()
    @required()
    email: string = undefined

    isAdmin: boolean = undefined
    hubs: string[] = undefined
  }

  export class ClaimInvite {
    @required()
    inviteToken: string = undefined

    @phone()
    phone: string = undefined

    @required('Please provide a password')
    @password()
    @maxLength(200)
    password: string = undefined

    @sameAs<ClaimInvite>('password', 'Your passwords do not match')
    confirmPassword: string = undefined
  }

  export class CheckEmail {
    @required()
    email: string = undefined
  }
}
