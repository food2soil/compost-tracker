import { Enums } from "../..";
import { UserRead } from "../user/user-read.dto";
import { HubRead } from "../hub/hub-read.dto";

export namespace BaseInvitationRead {
  export interface IHaveHubs {
    hubs: HubRead.HubViewModel[]
  }

  export interface IHaveInvitedBy {
    invitedBy: UserRead.UserViewModel
  }

  export abstract class InvitationViewModel {
    abstract invitedBy: string | UserRead.UserViewModel
    abstract hubs: string[] | HubRead.HubViewModel[]

    id: string = undefined
    firstName: string = undefined
    lastName: string = undefined
    roles: Enums.UserRoles[] = undefined
    email: string = undefined
  }
}

export namespace InvitationRead {
  export class InvitationViewModel extends BaseInvitationRead.InvitationViewModel{
    invitedBy: string = undefined
    hubs: string[] = undefined
  }
}

export namespace InvitationWithInvitedByRead {
    export class InvitationViewModel extends BaseInvitationRead.InvitationViewModel{
      invitedBy: UserRead.UserViewModel = new UserRead.UserViewModel()
      hubs: string[] = undefined
  }
}

export namespace InvitationWithHubsRead {
  export class InvitationViewModel extends BaseInvitationRead.InvitationViewModel{
    invitedBy: string = undefined
    hubs: HubRead.HubViewModel[] = [new HubRead.HubViewModel()]
}
}

export namespace InvitationWithInvitedByAndHubsRead {
  export class InvitationViewModel extends BaseInvitationRead.InvitationViewModel{
    invitedBy: UserRead.UserViewModel = new UserRead.UserViewModel()
    hubs: HubRead.HubViewModel[] = [new HubRead.HubViewModel()]
}
}
