import { ContactRead } from "../shared/contact-read.dto";
import { AddressRead } from "../shared/address-read.dto";
import { BatchRead } from "../batch/batch-read.dto";
import { UserRead } from "../user/user-read.dto";
import { BayRead } from "../bay/bay-read.dto";
import { GeneratorRead } from "../generator/generator-read.dto";
import { WorkRead, WorkReadBase } from "../work/work-read.dto";

export namespace HubReadBase {
  export interface IHaveBays {
    bays: BayRead.BayViewModel[]
  }

  export interface IHaveTechnicians {
    technicians: UserRead.UserViewModel[]
  }

  export interface IHaveGenerators {
    generators: GeneratorRead.GeneratorViewModel[]
  }

  export class StatsViewModel {
    lastWork: WorkRead.WorkViewModel = new WorkRead.WorkViewModel()
    numActiveBatches: number = undefined
    finishedCompostMade: number = undefined
    numResidential: number = undefined
    numRestaurants: number = undefined
    needsCarbon: boolean = undefined
    usagePercentage: number = undefined
  }

  export abstract class BaseHubViewModel {
    abstract bays: string[] | BayRead.BayViewModel[]
    abstract technicians: string[] | UserRead.UserViewModel[]
    abstract generators: string [] | GeneratorRead.GeneratorViewModel[]

    id: string = undefined
    created: Date = new Date()
    name: string = undefined
    contacts: ContactRead.ContactViewModel[] = [new ContactRead.ContactViewModel()]
    address: AddressRead.AddressViewModel = new AddressRead.AddressViewModel()
    isActive: boolean = undefined
    isCollectionPoint: boolean = undefined
    isComposting: boolean = undefined
    notes: string = undefined
    capacityGallonsPerWeek: number = undefined
    finishedBatches: BatchRead.BatchViewModel[] = [new BatchRead.BatchViewModel()]
    stats: StatsViewModel = new StatsViewModel()
  }
}

export namespace HubRead {
  export class HubViewModel extends HubReadBase.BaseHubViewModel {
    bays: string[] = undefined
    technicians: string[] = undefined
    generators: string[] = undefined
  }
}

export namespace HubWithTechniciansRead {
  export class HubViewModel extends HubReadBase.BaseHubViewModel {
    bays: string[] = undefined
    technicians: UserRead.UserViewModel[] = [new UserRead.UserViewModel]
    generators: string[] = undefined
  }
}

export namespace HubWithBaysRead {
  export class HubViewModel extends HubReadBase.BaseHubViewModel {
    bays: BayRead.BayViewModel[] = [new BayRead.BayViewModel()]
    technicians: string[] = undefined
    generators: string[] = undefined
  }
}

export namespace HubWithGeneratorsRead {
  export class HubViewModel extends HubReadBase.BaseHubViewModel {
    bays: string[] = undefined
    technicians: string[] = undefined
    generators: GeneratorRead.GeneratorViewModel[] = [new GeneratorRead.GeneratorViewModel()]
  }
}

export namespace HubWithTechniciansAndBaysRead {
  export class HubViewModel extends HubReadBase.BaseHubViewModel {
    bays: BayRead.BayViewModel[] = [new BayRead.BayViewModel()]
    technicians: UserRead.UserViewModel[] = [new UserRead.UserViewModel]
    generators: string[] = undefined
  }
}

export namespace HubWithTechniciansAndGeneratorsRead {
  export class HubViewModel extends HubReadBase.BaseHubViewModel {
    bays: string[] = undefined
    technicians: UserRead.UserViewModel[] = [new UserRead.UserViewModel]
    generators: GeneratorRead.GeneratorViewModel[] = [new GeneratorRead.GeneratorViewModel()]
  }
}
export namespace HubWithBaysAndGeneratorsRead {
  export class HubViewModel extends HubReadBase.BaseHubViewModel {
    bays: BayRead.BayViewModel[] = [new BayRead.BayViewModel()]
    technicians: string[] = undefined
    generators: GeneratorRead.GeneratorViewModel[] = [new GeneratorRead.GeneratorViewModel()]
  }
}

export namespace HubWithTechniciansBaysAndGeneratorsRead {
  export class HubViewModel extends HubReadBase.BaseHubViewModel {
    bays: BayRead.BayViewModel[] = [new BayRead.BayViewModel()]
    technicians: UserRead.UserViewModel[] = [new UserRead.UserViewModel]
    generators: GeneratorRead.GeneratorViewModel[] = [new GeneratorRead.GeneratorViewModel()]
  }
}
