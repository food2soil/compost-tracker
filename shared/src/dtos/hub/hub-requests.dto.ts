import { required, maxLength, email, requiredWithout, phone, greaterThanValue } from "../../decorators/validation.decorators";

export namespace HubRequests {
  export class Create {
    @required()
    @maxLength(200)
    name: string = undefined

    @required()
    @maxLength(200)
    primaryContactName: string = undefined

    @maxLength(200)
    primaryContactTitle?: string = undefined

    @requiredWithout<Create>('primaryContactEmail', 'Either a phone number or email address is required')
    @phone()
    primaryContactPhone?: string = undefined

    @requiredWithout<Create>('primaryContactPhone', 'Either a phone number or email address is required')
    @email()
    @maxLength(200)
    primaryContactEmail?: string = undefined

    @required()
    @maxLength(200)
    addressPrimaryLine: string = undefined

    @maxLength(200)
    addressSecondaryLine?: string = undefined

    @required()
    @maxLength(200)
    addressCity: string = undefined

    @required()
    @maxLength(2)
    addressState: string = undefined

    @required()
    @maxLength(5)
    addressZip: string = undefined

    isCollectionPoint: boolean = false
    isComposting: boolean = false

    @maxLength(1000)
    notes: string = undefined

    @required()
    @greaterThanValue(0, 'Weekly capacity must be greater than 0')
    capacityGallonsPerWeek: number = undefined
  }

  export class Update {
    @required()
    id: string = undefined

    @required()
    @maxLength(200)
    name: string = undefined

    @required()
    @maxLength(200)
    addressPrimaryLine: string = undefined

    @maxLength(200)
    addressSecondaryLine?: string = undefined

    @required()
    @maxLength(200)
    addressCity: string = undefined

    @required()
    @maxLength(2)
    addressState: string = undefined

    @required()
    @maxLength(5)
    addressZip: string = undefined

    @required()
    isActive: boolean = undefined

    @maxLength(1000)
    notes: string = undefined
  }

  export class UpdateTechnicians {
    @required()
    id: string = undefined

    primaryTechnicianIds: string[] = undefined
  }

  export class UpdateGenerators {
    @required()
    id: string = undefined

    generatorIds: string[] = undefined
  }

  export class List {
    hasActiveBatches?: boolean | string
    technician?: string
    needsCarbon?: boolean | string
  }
}
