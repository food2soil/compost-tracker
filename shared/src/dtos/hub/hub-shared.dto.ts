import { HubWithGeneratorsRead, HubWithBaysAndGeneratorsRead, HubWithTechniciansAndGeneratorsRead, HubWithTechniciansBaysAndGeneratorsRead, HubRead } from "./hub-read.dto";
import { GeneratorShared } from "../generator/generator-shared.dto";

export namespace HubShared {
  export function getFilledCapacity(hub: HubWithGeneratorsRead.HubViewModel | HubWithBaysAndGeneratorsRead.HubViewModel | HubWithTechniciansAndGeneratorsRead.HubViewModel | HubWithTechniciansBaysAndGeneratorsRead.HubViewModel) {
    return !hub.generators || hub.generators.length === 0
      ? 0
      : hub.generators.length === 1
        ? GeneratorShared.getWeeklyCollection(hub.generators[0])
        : hub.generators.map(x => GeneratorShared.getWeeklyCollection(x))
          .reduce((a, b) => a + b);
  }

  export function isLinkedToGenerator(hub: HubWithGeneratorsRead.HubViewModel | HubWithBaysAndGeneratorsRead.HubViewModel | HubWithTechniciansAndGeneratorsRead.HubViewModel | HubWithTechniciansBaysAndGeneratorsRead.HubViewModel, generatorId: string) {
    return hub.generators && hub.generators.map(x => x.id).indexOf(generatorId) !== -1;
  }

  export function isLinkedToGeneratorNoPop(hub: HubRead.HubViewModel, generatorId: string) {
    return hub.generators && hub.generators.map(x => x).indexOf(generatorId) !== -1;
  }
}
