import { BatchRead } from "../batch/batch-read.dto";

export namespace BayRead {
  export class BayViewModel {
    id: string = undefined
    name: string = undefined
    isStart: boolean = undefined
    destinations: string[] = []
    isHolding: boolean = undefined
    displayOrder: number = undefined
    activeBatches: BatchRead.BatchViewModel[] = [new BatchRead.BatchViewModel()]
  }
}
