import { BayRead } from "./bay-read.dto";
import { Time } from "../../utilities/time";

export namespace BayShared {
  export function canFill(bay: BayRead.BayViewModel): boolean {
    return bay.isStart
      && (!bay.activeBatches || bay.activeBatches.length === 0)
  }

  export function canTurn(bay: BayRead.BayViewModel): boolean {
    return bay.activeBatches && bay.activeBatches.length > 0
  }

  export function isHarvest(bay: BayRead.BayViewModel): boolean {
    return !bay.destinations || bay.destinations.length === 0
  }

  export function isCooking(bay: BayRead.BayViewModel): boolean {
    // Bay is cooking if the bay can be turned and the last event occurred less than 2 weeks ago
    return canTurn(bay)
      && bay.activeBatches
      && bay.activeBatches.length > 0
      && bay.activeBatches[0].events
      && bay.activeBatches[0].events.length > 0
      && new Date().valueOf() - new Date(bay.activeBatches[0].events[0].date).valueOf() < 2 * Time.MilliSecondsPerWeek;
  }
}
