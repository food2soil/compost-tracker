export namespace AddressRead {
  export class AddressViewModel {
    primaryLine: string = undefined
    secondaryLine?: string = undefined
    city: string = undefined
    state: string = undefined
    zip: string = undefined
    loc?: [number, number] = [0, 0]
  }
}
