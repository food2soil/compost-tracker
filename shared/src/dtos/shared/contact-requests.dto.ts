import { maxLength, requiredWithout, required } from "../../decorators/validation.decorators";

export namespace ContactRequests {
  export class Create {
    @required()
    @maxLength(200)
    name: string = undefined

    @maxLength(200)
    title?: string = undefined

    @requiredWithout<Create>('email', 'Either a phone number or email address is required')
    @maxLength(200)
    phone?: string = undefined

    @requiredWithout<Create>('phone', 'Either a phone number or email address is required')
    @maxLength(200)
    email?: string = undefined

    isPrimary: boolean = false
  }

  export class Update {
    id: string = undefined

    @required()
    @maxLength(200)
    name: string = undefined

    @maxLength(200)
    title?: string = undefined

    @requiredWithout<Create>('email', 'Either a phone number or email address is required')
    @maxLength(200)
    phone?: string = undefined

    @requiredWithout<Create>('phone', 'Either a phone number or email address is required')
    @maxLength(200)
    email?: string = undefined

    isPrimary: boolean = false
  }
}
