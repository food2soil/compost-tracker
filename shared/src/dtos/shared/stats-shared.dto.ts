export interface IMonthlyStats<T> {
  month: string
  year: string
  stats: T
}

export function getMonthsStats<T>(date: Date, stats: IMonthlyStats<T>[]) {
  // this is base-0, but saved in db as base-1
  let month = date.getMonth() + 1
  let year = date.getFullYear()

  return stats.find(x => x.month === month.toFixed(0) && x.year === year.toFixed(0))
}

export function getThisMonthsStats<T>(stats: IMonthlyStats<T>[]) {
  return getMonthsStats(new Date(), stats)
}

export function getLastMonthsStats<T>(stats: IMonthlyStats<T>[]) {
  // this is base-0, but saved in db as base-1
  let lastMonth = new Date().getMonth()
  let lastMonthYear = new Date().getFullYear()

  if (lastMonth === 0) {
    lastMonth = 12
    lastMonthYear--
  }

  return stats.find(x => x.month === lastMonth.toFixed(0) && x.year === lastMonthYear.toFixed(0))
}

export function getEmissionsReducedFromGallonsComposted(gallonsComposted: number) {
  let poundsPerGallon = 5
  let poundsPerTon = 2000
  let tonsDiverted = gallonsComposted * poundsPerGallon / poundsPerTon
  
  let mtCo2PerTon = 0.69
  let mtCo2Diverted = tonsDiverted * mtCo2PerTon

  let milesPerMtCo2 = 2451
  let milesDiverted = mtCo2Diverted * milesPerMtCo2

  return milesDiverted
}
