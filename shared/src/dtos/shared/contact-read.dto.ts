export namespace ContactRead {
  export class ContactViewModel {
    id: string = undefined
    name: string = undefined
    title?: string = undefined
    phone?: string = undefined
    email?: string = undefined
    isPrimary: boolean = undefined
  }
}
