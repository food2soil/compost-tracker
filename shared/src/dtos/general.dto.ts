import { Enums, Types } from "..";
import { required } from "../decorators/validation.decorators";

export namespace GeneralDto {
  export class SuccessResponseBody<T = any> {
    handledAs: Enums.UserRoles = undefined
    data: T = undefined
    token?: string = undefined
  }

  export class ErrorResponseBody {
    message: string = undefined
  }

  export class ReadQuery {
    id: string = undefined
  }
}

export namespace BaseRequests {
  export abstract class BaseList<T> {
    sortBy?: (keyof T) = undefined
    sortDir?: Types.SortDirection = undefined

    /**
     * Query property values will always be read as strings by express and therefore
     * need to be converted to Number before being used in mongoose's limit function
     *
     * ie - Number(req.query.limit)
     */
    limit?: number | string = undefined
  }
}
