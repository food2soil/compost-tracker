import { required, greaterThanValue, maxLength } from "../../decorators/validation.decorators";
import { WorkRead, WorkReadBase } from "./work-read.dto";
import { BaseRequests } from "../general.dto";

export namespace WorkRequests {
  export class Create {
    @required()
    hubId: string = undefined

    @required()
    date: Date = new Date()

    @required()
    technicianId: string = undefined

    @required()
    @greaterThanValue(0, 'Time spent must be a number greater than 0')
    hoursSpent: number = undefined

    bayEvents: WorkReadBase.BayEventViewModel[] = [new WorkReadBase.BayEventViewModel()]
    bayMeasurements: WorkReadBase.BayMeasurementViewModel[] = [new WorkReadBase.BayMeasurementViewModel()]

    @maxLength(1000)
    notes: string = undefined

    isCarbonNeeded: boolean = undefined
  }

  export class Edit {
    @required()
    date: Date = new Date()

    @required()
    technicianId: string = undefined

    @required()
    @greaterThanValue(0, 'Time spent must be a number greater than 0')
    hoursSpent: number = undefined

    bayEvents: WorkReadBase.BayEventViewModel[] = [new WorkReadBase.BayEventViewModel()]
    bayMeasurements: WorkReadBase.BayMeasurementViewModel[] = [new WorkReadBase.BayMeasurementViewModel()]

    @maxLength(1000)
    notes: string = undefined

    isCarbonNeeded: boolean = undefined
  }

  export class List extends BaseRequests.BaseList<WorkRead.WorkViewModel> {
    hub?: string = undefined
    technician?: string = undefined
    from?: Date = new Date()
    to?: Date = new Date()
    isApproved?: boolean = undefined
  }
}
