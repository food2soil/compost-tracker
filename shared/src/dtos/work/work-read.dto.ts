import { Enums } from "../..";
import { UserRead } from "../user/user-read.dto";
import { HubRead } from "../hub/hub-read.dto";

export namespace WorkReadBase {
  export interface IHaveBayEvents {
    bayEvents: BayEventViewModel[]
  }

  export interface IHaveTechnician {
    technician: UserRead.UserViewModel
  }

  export interface IHaveHub {
    hub: HubRead.HubViewModel
  }

  export interface IHaveBayMeasurements {
    bayMeasurements: BayMeasurementViewModel[]
  }

  export class BayMeasurementViewModel {
    temperature: number = undefined
    bay: string = undefined
  }

  export class BayEventViewModel {
    type: Enums.BayEvent = undefined
    bay: string = undefined
    destination: string = undefined
    bucketsHarvested: number = undefined
    isFullHarvest: boolean = undefined
  }

  export abstract class BaseWorkViewModel implements IHaveBayEvents, IHaveBayMeasurements {
    abstract technician: string | UserRead.UserViewModel
    abstract hub: string | HubRead.HubViewModel

    id: string = undefined
    date: Date = new Date()
    bayEvents: BayEventViewModel[] = [new BayEventViewModel()]
    bayMeasurements: BayMeasurementViewModel[] = [new BayMeasurementViewModel()]
    timeSpent: number = undefined
    notes: string = undefined
    isApproved: boolean = undefined
    isCarbonNeeded: boolean = undefined
  }
}

export namespace WorkRead {
  export class WorkViewModel extends WorkReadBase.BaseWorkViewModel {
    technician: string = undefined
    hub: string = undefined
  }
}

export namespace WorkWithTechnicianRead {
  export class WorkViewModel extends WorkReadBase.BaseWorkViewModel {
    technician: UserRead.UserViewModel = new UserRead.UserViewModel()
    hub: string = undefined
  }
}

export namespace WorkWithHubRead {
  export class WorkViewModel extends WorkReadBase.BaseWorkViewModel {
    technician: string = undefined
    hub: HubRead.HubViewModel = new HubRead.HubViewModel()
  }
}

export namespace WorkWithHubAndTechnicianRead {
  export class WorkViewModel extends WorkReadBase.BaseWorkViewModel {
    technician: UserRead.UserViewModel = new UserRead.UserViewModel()
    hub: HubRead.HubViewModel = new HubRead.HubViewModel()
  }
}
