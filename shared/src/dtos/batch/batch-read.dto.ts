import { Enums } from "../..";

export namespace BatchRead {
  export class StatsViewModel {
    hub: string = undefined
    finishedCompostMade: number = undefined
    isActive: boolean = undefined
  }

  export class BatchMeasurementViewModel {
    date: Date = new Date()
    temperature: number = undefined
    createdBy: string = undefined
  }

  export class BatchEventViewModel {
    type: Enums.BayEvent = undefined
    date: Date = new Date()
    startBay: string = undefined
    endBay: string = undefined
    createdBy: string = undefined
  }

  export class BatchViewModel {
    id: string = undefined
    created: Date = new Date()
    name: string = undefined
    measurements: BatchMeasurementViewModel[] = [new BatchMeasurementViewModel()]
    events: BatchEventViewModel[] = [new BatchEventViewModel()]
    notes: string = undefined
    stats: StatsViewModel = new StatsViewModel()
  }
}
