import { BatchRead } from "./batch-read.dto";
import { Enums } from "../../enums/enums";

export namespace BatchShared {
  export function getLastEvent(batch: BatchRead.BatchViewModel, type?: Enums.BayEvent): BatchRead.BatchEventViewModel {
    let sorted = batch.events.sort(((a, b) => b.date.valueOf() - a.date.valueOf()))

    return type ? sorted.find(x => x.type === type) : sorted[0]
  }
}
