export namespace Enums {
  export enum UserRoles {
    Owner = 3,
    Admin = 2,
    Technician = 1,
    Anonymous = 0
  }

  export enum GeneratorType {
    Restaurant = 'Restaurant',
    Residential = 'Residential'
  }

  export enum CollectionType {
    Dropoff = 'Dropoff',
    Pickup = 'Pickup'
  }

  export enum ContainerType {
    Big = 32,
    Small = 5
  }

  export enum BayEvent {
    Fill = 'fill',
    Turn = 'turn',
    Harvest = 'harvest'
  }
}
